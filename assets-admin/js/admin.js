/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
    $('.add-image').each(function(){
        var t = $(this).attr('data-upload');
        $('#'+t).change(function(e){
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#'+t).parents('.form-group').find('.image-preview').html('<img id="'+t+'-image-preview" width="200"/>');
                $('#'+t+'-image-preview').attr('src', e.target.result);
            }
            if($(this).get(0).files.length < 1){//cancel
                $('#'+t).parents('.form-group').find('.image-preview').html('');
                $('#'+t).parents('.form-group').find('.remove-image').addClass('hidden');
            }else{
                reader.readAsDataURL($(this).get(0).files[0]);
                $('#'+t).parents('.form-group').find('.remove-image').removeClass('hidden');
            }
        });
    })
    
    $('.remove-image').click(function(){
        $(this).parents('.form-group').find('input[type=file]').val('');
        $(this).parents('.form-group').find('input').val('');
        $(this).parents('.form-group').find('.image-render').html('');
        $(this).parents('.form-group').find('.image-preview').html('');
        $(this).addClass('hidden');
    })
    
    $('.radio-switch').each(function(){
        var t = $(this).attr('data-input');
        $(this).find('li').click(function(){
            var v = $(this).attr('data-value');
            $('#'+t).val(v);
            $(this).siblings('li').removeClass('active');
            $(this).addClass('active');
        })
    })
    
    $('.add-image').click(function(){
        var t = $(this).attr('data-upload');
        $('#'+t).click();
    })
    
    $('#btn-save-draft').click(function(){
        var form = $(this).attr('data-form');
        $('#'+form).find('input[name=status]').val(0);
        $('#'+form).submit();
    })
    $('#btn-save-publish').click(function(){
        var form = $(this).attr('data-form');
        $('#'+form).find('input[name=status]').val(1);
        $('#'+form).submit();
    })
    
    
    $('#btn-update').click(function(){
        $(this).parents('form').find('input[name=mode]').val('update');
        var t = $(this).parents('form').find('select[name=action]').val();
        if(t.length < 1){
            $(this).parents('form').find('select[name=action]').focus();
            return;
        }
        $('#frm_table').find('input[name=action]').val(t);
        $('#frm_table').submit();
    })
    $('#btn-search').click(function(){
        $(this).parents('form').find('input[name=mode]').val('search');
        $(this).parents('form').submit();
    })
    
    
    $('#select_all').click(function(){
        $(this).parents('table').find('input[type=checkbox]').prop('checked',$(this).prop("checked"));
    })
    
})