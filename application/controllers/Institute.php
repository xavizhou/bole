<?php
class institute extends MY_User{
    public function __construct()
    {
      parent::__construct();

    }
  function index() {
      $this->show_list('e_newsletter');
  }
  function report_insights(){
      $this->show_list('report_insights');
  }
  function knowledge_center(){
      $this->show_list('knowledge_center');
  }
  function e_newsletter(){
      $this->show_list('e_newsletter');
  }
  function blog(){
      $this->show_list('blog');
  }
  
  function show_list($category){
    $data = $this->data;
    $this->load->model('news_model');
    $page = (int)$this->input->get('page');
    

    if($category) $condition[] = array('where','category',$category);
    
    $condition[] = array('where','delete',0);
    $condition[] = array('order','order','desc');
    
    $lang = $this->data['lang'];
    $posts = $this->news_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->news_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
            endif;
        endfor;
        $data['posts'] = $posts;
    }
    
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    $data['category'] = $category;
    $this->load->view('news-list',$data);
  }
  
  
  function detail() {
    $data = $this->data;
    $this->load->model('news_model');
    $id = (int)$this->input->get('id');
   
    
    $lang = $data['lang'];
    $post = $this->news_model->Get($id);
    $item = $this->news_model->GetItem($post['id'],$lang);
    
    if(is_array($item)):
        $post['title'] = $item['title'];
        $post['subtitle'] = $item['subtitle'];
        $post['excerpt'] = $item['excerpt'];
        $post['content'] = $item['content'];
    endif;
    
    
    if ($post) {
      $data['post'] = $post;
      $data['category'] = $post['category'];
    }
    
    $this->load->view('news-detail',$data);
  }
  
  
  function ajax_get(){
    $this->load->model('news_model');
    
    $category = $this->input->get('category');
    if(!$category):
        $category = 'e_newsletter';
    endif;
      
    $page = (int)$this->input->get('page');

    if($category):
        $condition[] = array('where','category',$category);
    endif;
    
    $condition[] = array('where','delete',0);
    $condition[] = array('order','order','desc');
    
    $lang = $this->data['lang'];
    $posts = $this->news_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->news_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
            endif;
            $posts[$i]['thumb'] = ($posts[$i]['thumb'])?$posts[$i]['thumb']:'';
            $posts[$i]['content'] = mb_substr(strip_tags($posts[$i]['content']),0, 200,'UTF-8');
        endfor;
        
    }else{
        $posts = array();
    }
    echo json_encode($posts);
  }
}
