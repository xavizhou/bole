<?php

class City extends MY_User{

  public function __construct()
  {
    parent::__construct();
  }

//  function index() {
//    $data = $this->data;
//    $files= array();
//    read('assets/city.json',$content);
//    if($content):
//        $content = json_decode($content,TRUE);
//    endif;
//    $data['city_list'] = $content;
//    $this->load->view('admin/city',$data);
//  }

  function get_city_nearby() {
      $data = $this->data;
      $lang = $data['lang'];
      $content = '';
//            lat: 1.4707591,
//            lng: 104.0884807 
      $lat = (float)$this->input->get('lat');
      $lng = (float)$this->input->get('lng');
      $city_code = $this->input->get('city_code');
      
    $files= array();
    read('assets/city.json',$content);
    if($content):
        $content = json_decode($content,TRUE);
    endif;
    
      if($city_code):
      else:

            $dist = 99999;
            $code = '';
            foreach($content as $k=>$v):
                if(!$v['location']):
                    continue;
                endif;
                list($_lat,$_lng) = explode(",", $v['location']);

                $_dist = pow($lat - $_lat,2) + pow($lng - $_lng,2);

                //echo "pow($lat - $_lat,2) + pow($lng - $_lng,2) = $_dist <br />";

                if($_dist < $dist):
                    $code = $k;
                    $dist = $_dist;
                endif;

            endforeach;
            $city_code = $code;
      endif;
      
      echo json_encode(array(
          'city_code'=>$city_code,
          'city_name'=>$content[$city_code][$lang]
      ));
  }
}
