<?php
class workwithus extends MY_User{
    public function __construct()
    {
      parent::__construct();
    }
    public function index(){   
        $data = $this->data;
        $this->load->model('page_model');
        $post = $this->page_model->GetBySlug('workwithus',$data['lang']);
        
        $data['post']=$post;
        $this->load->view('workwithus',$data);
    }

}
