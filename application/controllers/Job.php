<?php

class job extends MY_User{
    private $jobtype_list;
    private $location_list;
    private $industry_list;
    private $type_list;
    public function __construct()
    {
      parent::__construct();

        $this->jobtype_list = $this->config->item('jobtype_list');
        $this->location_list = $this->config->item('location_list');
        $this->industry_list = $this->config->item('industry_list');

        $this->data['jobtype_list'] = $this->jobtype_list;
        $this->data['location_list'] = $this->location_list;
        $this->data['industry_list'] = $this->industry_list;
    }
    
    function index() {
        $this->submit_cv();
    }
    
    function submit_cv(){
        $data = $this->data;
        
        $this->load->model('page_model');
        $post = $this->page_model->GetBySlug('submit_cv',$data['lang']);
        $data['post'] = $post;
        
        
        
        $this->load->model('job_model');
        $location = (int)($this->input->get('location'));
        $type = (int)($this->input->get('type'));
        $recommend = (int)($this->input->get('recommend'));


        if($location) $condition[] = array('where','location',$location);
        if($type) $condition[] = array('where','type',$type);
        if($recommend) $condition[] = array('where','recommend',$recommend);

        $condition[] = array('where','delete',0);
        $condition[] = array('order','order','desc');
            
    
        $posts = $this->job_model->Search(0,999,$condition);
        if ($posts) {
          $data['job_list'] = $posts;
        }
        $data['location'] = $location;
        $data['type'] = $type;
        $data['recommend'] = $recommend;
    
        $this->load->view('submit',$data);
    }
}