<?php
class Whoarewe extends MY_User{
    public function __construct()
    {
      parent::__construct();
    }
    public function index(){   
        $data = $this->data;
        $data['post'] = $this->get('origin_of_bole');
        $this->load->view('whoarewe',$data);
    }
    public function origin_of_bole(){
        $data = $this->data;
        $data['post'] = $this->get('origin_of_bole');
        $this->load->view('whoarewe',$data);
    }
    public function bole_history(){
        $data = $this->data;
        
        
        $this->load->model('history_model');

        $condition[] = array('where','delete',0);
        $condition[] = array('order','date','asc');

        $lang = $this->data['lang'];
        $posts = $this->history_model->Search(0,999,$condition);

        if ($posts) {
            for($i=0;$i<count($posts);$i++):
                $item = $this->history_model->GetItem($posts[$i]['id'],$lang);
                if($item):
                    $posts[$i]['title'] = $item['title'];
                    $posts[$i]['subtitle'] = $item['subtitle'];
                    $posts[$i]['excerpt'] = $item['excerpt'];
                    $posts[$i]['content'] = $item['content'];
                endif;
            endfor;
            $data['posts'] = $posts;
        }else{
            $data['posts']  = array();
        }

                            
        $this->load->view('whoarewe-history',$data);
    }
    public function about_bole_associate(){
        $data = $this->data;
        $data['post'] = $this->get('about_bole_associate');
        $this->load->view('whoarewe',$data);
    }
    public function bole_associates_service(){
        $data = $this->data;
        $data['post'] = $this->get('bole_associates_service');
        $this->load->view('whoarewe',$data);
    }
    public function standard_procedure(){
        $data = $this->data;
        $data['post'] = $this->get('standard_procedure');
        $this->load->view('whoarewe',$data);
    }
    
    
    private function get($slug){
        $data = $this->data;
        if(empty($slug)){
            return false;
        }
        
        $this->load->model('page_model');
        $post = $this->page_model->GetBySlug($slug,$data['lang']);
        
        return $post;
    }

}
