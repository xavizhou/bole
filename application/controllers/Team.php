<?php
class team extends MY_User{
    public function __construct()
    {
      parent::__construct();

    }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list(){
      
    $data = $this->data;
    $this->load->model('team_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $category = (int)$this->input->get('category');
    

    if($category) $condition[] = array('where','category',$category);
    
    if($q) $condition[] = array('where','((title LIKE "%'.$q.'%") OR (content LIKE "%'.$q.'%") OR (keywords LIKE "%'.$q.'%"))',NULL);
    
    $condition[] = array('where','delete',0);
    //$condition[] = array('where','lang',$data['lang']);
    $condition[] = array('order','order','desc');
    
    $lang = $this->data['lang'];
    $posts = $this->team_model->Search(0,999,$condition);
    
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->team_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
                $posts[$i]['language_skill'] = $item['language_skill'];
            endif;
        endfor;
        $data['posts'] = $posts;
    }
    
    //$data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    
    $this->load->helper('form');
    
    $data['q'] = $q;
    $data['category'] = $category;
    $this->load->view('team',$data);
  }
  
  
  function detail() {
    $data = $this->data;
    $this->load->model('team_model');
    $id = (int)$this->input->get('id');
   
    
    $lang = $data['lang'];
    $post = $this->team_model->Get($id);
    $item = $this->team_model->GetItem($post['id'],$lang);
    
    if(is_array($item)):
        $post['title'] = $item['title'];
        $post['subtitle'] = $item['subtitle'];
        $post['excerpt'] = $item['excerpt'];
        $post['content'] = $item['content'];
        $post['language_skill'] = $item['language_skill'];
    endif;
    
    
    if ($post) {
      $data['post'] = $post;
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
    $this->load->view('team-detail',$data);
  }
  
  
  function ajax_star(){
    $data = $this->data;
    $this->load->model('team_model');
    $loc = trim($this->input->get('loc'));
    $condition[] = array('like','loc',$loc);
    $condition[] = array('where','delete',0);
    $condition[] = array('where','status',2);
    $condition[] = array('order','order','desc');
    
    $lang = $this->data['lang'];
    $posts = $this->team_model->Search(0,4,$condition);
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->team_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
                $posts[$i]['language_skill'] = $item['language_skill'];
            endif;
        endfor;
        
    }else{
        $posts = array();
    }
    echo json_encode($posts);
  }

}
