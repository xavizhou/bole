<?php

class Submit extends MY_User{

  public function __construct()
  {
    parent::__construct();
  }

  function consultant(){
      if($_SERVER['REQUEST_METHOD'] == 'POST'):
          $id = (int)$this->input->post('id');
        $config['upload_path'] = 'cv/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '2048';
        $this->load->library('upload');
        
        $this->upload->initialize($config);
        if ($this->upload->do_upload('cvFile'))
        {
            $data = array('upload_data' => $this->upload->data());
            $arr = array('status'=>1);
            $this->load->model('consultant_model');
            $consultant = $this->consultant_model->Get($id);
//            print_r($data);
//            print_r($consultant);
//            die();
        }else{
            $arr = array('status'=>0,'error' => $this->upload->display_errors());
            //$arr = array('status'=>0);
        }
      else:
          $arr = array('status'=>0);
      endif;
      json_encode($arr);
  }
  function cv() {
      if($_SERVER['REQUEST_METHOD'] == 'POST'):
        $config['upload_path'] = 'cv/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '2048';
        $this->load->library('upload');
        
        $this->upload->initialize($config);
        if ($this->upload->do_upload('cvFile'))
        {
            $data = array('upload_data' => $this->upload->data());
            $arr = array('status'=>1);
            $this->load->model('cv_model');
            $saveData = array();
            $saveData['name'] = $this->input->post('name');
            $saveData['email'] = $this->input->post('email');
            $saveData['phone'] = $this->input->post('phone');
            $saveData['cv'] = $data['upload_data']['file_name'];
            $this->cv_model->Save($saveData);
        }else{
            $arr = array('status'=>0,'error' => $this->upload->display_errors());
            //$arr = array('status'=>0);
        }
      else:
          $arr = array('status'=>0);
      endif;
      json_encode($arr);
  }

}
