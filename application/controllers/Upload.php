<?php
class Upload extends MY_User{
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      return;
        $uploaddir = 'uploads/';
        $t = pathinfo(basename($_FILES['userfile']['name']));
        $uploadfile = $uploaddir .time().rand(100,999).'.'. $t['extension'];
        $editor = $_POST['editor'];//a global variable of the editor
        if(!$editor) $editor = 'mce_26_editor';
        
        
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            echo "File is valid, and was successfully uploaded.";
            $err = '';
        } else {
            $err = "文件上传失败，文件不能大于8M，文件名只能为英文和数字";
        }
    
        $image_type = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/bmp');

        echo '<script>';
        if($err){
            echo 'alert("'.$err.'");';
        }else{
            if(in_array($_FILES['userfile']['type'], $image_type)) {
                    echo 'window.parent.'.$editor.'.insertContent("<img src=\''.  base_url().$uploaddir.'\/'.$_FILES['userfile']['name'].'\' />");';
            } else {
                    echo 'window.parent.'.$editor.'.insertContent("<a href=\''.  base_url().$uploaddir.'\/'.$_FILES['userfile']['name'].'\'>'.$_FILES['userfile']['name'].'</a>");';
            }
        }
        echo 'window.parent.'.$editor.'.plugins.upload.finish();';
        echo '</script>';
  }
  
  
  function summernote() {

        $uploaddir = 'uploads/';
        $t = pathinfo(basename($_FILES['userfile']['name']));
        $uploadfile = $uploaddir .time().rand(100,999).'.'. $t['extension'];
        
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            echo $uploadfile;//change this URL
        } else {
            echo 'false';
        }
        
  }
  
  
}
