<?php

class star extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/admin/login/show_login');
    }
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_main();
  }
  function show_main() {
    $this->load->model('star_model');
    $this->load->library('constellation');    
    $constellations = $this->constellation->get_constellation_list();

    $constellation = $this->input->get('constellation', true);
    if(empty($constellation)) $constellation = '';
    $stars = $this->star_model->get_stars( $constellation, 0 );
    
    // Load posts based on the user's permission. Admins can see
    // everything, and regular users can only see posts from
    // their own team.

    $data['is_admin'] = $is_admin;
    $data['num_stars'] = $stars ? count($stars) : 0;
    $data['stars'] = $stars;
    $data['constellation'] = $constellations[$constellation];
    $data['constellations'] = $constellations;
    
    $this->load->library('form_builder',array('id'=>'star_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/star/create_new_star"'));
    $this->load->library('constellation');
    
    
    $fields['constellation'] = array('type' => 'select','default'=>$constellation, 'label' => '星座','options' => $this->constellation->get_constellation_list(), 'required' => TRUE);

    $fields['name'] = array('size' => 25, 'label' => 'Star Name', 'required' => TRUE);
    $fields['nick'] = array('size' => 25, 'label' => 'Nick Name', 'required' => FALSE);
    $fields['dec'] = array('size' => 25, 'label' => 'Dec.', 'required' => TRUE);
    $fields['ra'] = array('size' => 25, 'label' => 'RA.', 'required' => TRUE);
    $fields['v'] = array('size' => 25, 'label' => 'V', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $values['constellation'] = $constellation;
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['star_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/star',$data);
  }

  function star_modelessage() {
    $message = $this->input->post('message');

    if ( $message ) {
      $this->load->model('star_model');
      $saved = $this->star_model->save_post($message);
    }

    if ( isset($saved) && $saved ) {
       echo "<tr><td>". $saved['body'] ."</td><td>". $saved['createdDate'] ."</td></tr>";
    } else {

    }
  }

  function create_new_star() {
    $star = $this->input->post(null,true);
    if( count($star) ) {
      $this->load->model('star_model');
      $saved = $this->star_model->create_new_star($star);
    }

    if ( isset($saved) && $saved ) {
       redirect('/admin/star/show_main?message=success&constellation='.$star['constellation']);
    }
  }

  function update_tagline() {
    $new_tagline = $this->input->post('message');
    $user_id = $this->session->userdata('id');

    if( isset($new_tagline) && $new_tagline != "" ) {
      $this->load->model('user_m');
      $saved = $this->user_m->update_tagline($user_id, $new_tagline);
    }

    if ( isset($saved) && $saved ) {
      $this->session->set_userdata(array('tagline'=>$new_tagline));
      echo "success";
    }
  }

}
