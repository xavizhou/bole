<?php

class regcode extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/admin/login/show_login');
    }
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->regcode_list();
  }

  function regcode_list(){
    $this->load->model('regcode_model');
    $page = (int)$this->input->get('page');
    $page = ($page)?$page:0;
    $regcodes = $this->regcode_model->get_regcode();
    if ($regcodes) {
      $data['regcodes'] = $regcodes;
    }
    $this->load->view('admin/regcode-list',$data);
  }
  
  
  function create_new() {
    $num =(int) $this->input->get('num',true);
    if( $num) {
      $this->load->model('regcode_model');
      $saved = $this->regcode_model->generate_regcode($num);
    }
    redirect('admin/regcode');
  }

  
  function unactive() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('regcode_model');
      $saved = $this->regcode_model->unactive($id);
    }
    redirect('admin/book?message=success');
  }
  function active() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('regcode_model');
      $saved = $this->regcode_model->active($id);
    }
    redirect('admin/book?message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('regcode_model');
      $saved = $this->regcode_model->remove($id);
    }
    redirect('admin/book?message=success');
  }
}
