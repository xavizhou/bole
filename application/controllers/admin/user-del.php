<?php

class user extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/admin/login/show_login');
    }
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->load->model('user_model');
    $page = (int) $this->input->get('page');
    $conditions = $this->input->get(null,true);
    $users = $this->user_model->get_user( $conditions, 50, $page );
    $total = $this->user_model->get_user_number( $conditions);
    
    $data['users'] = $users;
    $data['total'] = $total;
    $data['per'] = 50;
    $this->load->view('admin/user-list',$data);
  }
  function add() {
    $this->load->model('user_model');
    $this->load->library('form_builder',array('id'=>'user_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/user/create_new"'));

    $fields['username'] = array('size' => 32, 'label' => '用户名', 'required' => TRUE);
    $fields['display_name'] = array('size' => 32, 'label' => '显示名称', 'required' => TRUE);
    $fields['real_name'] = array('size' => 32, 'label' => '真实姓名', 'required' => TRUE);
    $fields['email'] = array('size' => 32, 'label' => '邮箱', 'required' => TRUE);
    $fields['gender'] = array('type' => 'enum','label' => '性别','options' => array(0=>'女',1=>'男'), 'required' => TRUE);
    $fields['birthdate'] = array('size' => 32, 'label' => '出生日期', 'required' => TRUE,'class'=>'datepicker');
    $fields['level'] = array('size' => 32, 'label' => '等级', 'required' => TRUE);
    $fields['status'] = array('size' => 32, 'label' => '状态', 'required' => TRUE);
    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = 1;
    $values['gender'] = 1;
    $values['status'] = 1;
    $this->form_builder->set_field_values($values);
    $this->form_builder->show_required = TRUE;
    $data['user_new_form'] = $this->form_builder->render();
    $this->load->view('admin/user-edit',$data);
  }
  
  function edit() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    $this->load->model('user_model');
    $this->load->library('form_builder',array('id'=>'user_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/user/modify"'));

    $fields['id'] = array('type' => 'hidden');
    $fields['username'] = array('size' => 32, 'label' => '用户名','readonly'=>TRUE, 'required' => TRUE);
    $fields['display_name'] = array('size' => 32, 'label' => '显示名称', 'required' => TRUE);
    $fields['real_name'] = array('size' => 32, 'label' => '真实姓名', 'required' => TRUE);
    $fields['email'] = array('size' => 32, 'label' => '邮箱', 'readonly'=>TRUE,'required' => TRUE);
    $fields['gender'] = array('type' => 'enum','label' => '性别','options' => array(0=>'女',1=>'男'), 'required' => TRUE);
    $fields['birthdate'] = array('size' => 32, 'label' => '出生日期', 'required' => TRUE,'class'=>'datepicker');
    $fields['level'] = array('size' => 32, 'label' => '等级', 'required' => TRUE);
    $fields['status'] = array('size' => 32, 'label' => '状态', 'required' => TRUE);
    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    if(!empty($user)){
        $this->form_builder->set_field_values($user);
    }
    $this->form_builder->show_required = TRUE;
    $data['user_new_form'] = $this->form_builder->render();
    
    
    
    $this->load->model('post_model');
    $posts = $this->post_model->get_posts_for_user($user['id']);
    $data['posts'] = $posts;
    
    $this->load->view('admin/user-edit',$data);
  }
  
  function create_new() {
    $user = $this->input->post(null,true);
    if( count($user) ) {
      $this->load->model('user_model');
      $saved = $this->user_model->create_new($user);
    }

    if ( isset($saved) && $saved ) {
       redirect('/admin/user/add?message=success');
    }
  }
  function modify() {
    $user = $this->input->post(null,true);
    if( count($user) ) {
      $this->load->model('user_model');
      $saved = $this->user_model->modify($user);
    }
    if ( isset($saved) && $saved ) {
       redirect('/admin/user/edit?message=success&id='.$user['id']);
    }
  }
  
  function unactive() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('user_model');
      $saved = $this->user_model->unactive($id);
    }
    redirect('admin/user?message=success');
  }
  function active() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('user_model');
      $saved = $this->user_model->active($id);
    }
    redirect('admin/user?message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('user_model');
      $saved = $this->user_model->remove($id);
    }
    redirect('admin/user?message=success');
  }

  function update_tagline() {
    $new_tagline = $this->input->post('message');
    $user_id = $this->session->userdata('id');

    if( isset($new_tagline) && $new_tagline != "" ) {
      $this->load->model('user_m');
      $saved = $this->user_m->update_tagline($user_id, $new_tagline);
    }

    if ( isset($saved) && $saved ) {
      $this->session->set_userdata(array('tagline'=>$new_tagline));
      echo "success";
    }
  }

}
