<?php

class backup extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
  }

  function index() {
    $data = $this->data;
    $files= array();
    if ($handle = opendir('uploads/database/')) {
      while (false !== ($entry = readdir($handle))) {
          if ($entry != "." && $entry != "..") {
              $files[] = $entry;
              //echo "$entry size " . filesize('uploads/database/'.$entry) . "\n";
          }
      }
      closedir($handle);
  }
    $data['files'] = $files;
    $this->load->view('admin/backup-list',$data);
  }

  function create() {
    $data = $this->data;
    $this->load->dbutil();
    $backup =& $this->dbutil->backup();

    $this->load->helper('file');
    $file = date('YmdHis').'.gz';
    write_file('uploads/database/'.$file, $backup);
    $data['file'] = $file;
    $this->session->set_flashdata( 'message', array( 'title' => '数据库备份成功', 'content' => '数据库备份成功', 'type' => 'success' ));  
    redirect('admin/backup');
  }
}
