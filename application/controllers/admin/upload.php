<?php
class Upload extends MY_Controller{
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
        $uploaddir = 'uploads/';
        $t = pathinfo(basename($_FILES['userfile']['name']));
        $uploadfile = $uploaddir .time().rand(100,999).'.'. $t['extension'];
        $editor = $_POST['editor'];//a global variable of the editor
        if(!$editor) $editor = 'mce_26_editor';
        
        
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            echo "File is valid, and was successfully uploaded.";
            $err = '';
        } else {
            $err = "文件上传失败，文件不能大于8M，文件名只能为英文和数字";
        }
    
        $image_type = array('image/jpeg', 'image/pjpeg', 'image/gif', 'image/png', 'image/x-png', 'image/bmp');

        echo '<script>';
        if($err){
            echo 'alert("'.$err.'");';
        }else{
            if(in_array($_FILES['userfile']['type'], $image_type)) {
                    echo 'window.parent.'.$editor.'.insertContent("<img src=\''.  base_url().$uploaddir.'\/'.$_FILES['userfile']['name'].'\' />");';
            } else {
                    echo 'window.parent.'.$editor.'.insertContent("<a href=\''.  base_url().$uploaddir.'\/'.$_FILES['userfile']['name'].'\'>'.$_FILES['userfile']['name'].'</a>");';
            }
        }
        echo 'window.parent.'.$editor.'.plugins.upload.finish();';
        echo '</script>';
  }
  
  
  
    
    function gallery(){
        $t = array();
        $t[0] = microtime_float();
        
        $title = $this->input->post('title', true);
        $divId = $this->input->post('divId', true);
        $imagedata = $this->input->post('imagedata', false);
        $thumbdata = $this->input->post('thumbdata', false);
        $exif = $this->input->post('exif', true);
        $rel = $divId;
        
        if(strlen($imagedata) < strlen('data:image/jpeg;base64')):
            echo '{"status":"error","rel":"'.$rel.'"}';
            die();
        endif;
        $path2 = 'uploads/';
        $path3 = 'uploads/thumb/';
        $imagedata =  base64_decode(substr($imagedata, strlen('data:image/jpeg;base64')));
        $thumbdata =  base64_decode(substr($thumbdata, strlen('data:image/jpeg;base64')));
        
        $filename = time().rand(10000,99999).'.jpg';
        
        write($path2.$filename, $imagedata);
        write($path3.$filename, $thumbdata);
        $oss_file['file'] = $path2.$filename;
        $oss_file['thumb'] = $path3.$filename;
        
        
        
        $this->load->model('gallery_model');
        $saveData = array(
            'title'=>$title,
            'file'=>$filename,
            'thumb'=>$filename,
        );
        $id = $this->gallery_model->Save($saveData);
        
        if($oss_file['file']){
            $arr = array(
                'status'=>'success',
                'file'=>$oss_file['file'],
                'thumb'=>$oss_file['thumb'],
                'rel'=>$rel,
                'mode'=>'image',
                'id'=>$id
            );
            echo json_encode($arr);
            exit();
        }else{
            echo '{"status":"failure","filename":"","rel":"'.$rel.'","message":"文件上传失败",}';
            exit;
        }
    }
    
}
