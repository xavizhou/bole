<?php

class customer extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * This is the controller method that drives the application.
   * After a customer logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
        //$this->load->view('admin/customer-export',$data);
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function edit() {
    $data = $this->data;
    $this->load->model('customer_model');
    $this->load->model('room_model');
    $id = (int)$this->input->get('id');
    $user = $this->customer_model->Get($id);
    $data['post'] = $user;
        
    $this->load->helper('form');
    $this->load->library('form_validation');
    
    $condition = array();
    $condition[] = array('where','fid',0);
    $rooms = $this->room_model->Search(0,0,$condition);
    if ($rooms) {
        $tmp = array();
        foreach($rooms as $v){
            $tmp[$v['id']] = $v['name'];
        }
        $data['rooms'] = $tmp;
    }else{
        $data['rooms'] = array();
    }
    $this->load->view('admin/customer-edit',$data);
  }
  
  function show_main() {
    $this->load->model('public_model');
    $post_id = $this->input->get('post_id');
    // Load all of the logged-in user's posts

    if( count($post_id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $post_id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }

  function save() {
        $postData = $this->input->post(null, false);
        
        $this->load->model('customer_model');
        
        
        $saveData = array(
            'username' => $postData['username']
        );
        if($postData['password'] != '******'){
            $saveData['password'] = md5($postData['password']);
        }
        if($postData['room']):
            $saveData['room'] = implode(",", $postData['room']);
        else:
            $saveData['room'] = '';
        endif;
        
        $id = $this->customer_model->Save($saveData,$postData['id']);
    
    
        if($id){
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        }else{
            $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
        }
        redirect('/admin/customer/edit?id='.$id);
  }
  function search(){
    $data = $this->data;
    $this->load->model('customer_model');
    $this->load->model('room_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $month = (int)$this->input->get('month');
    
    $condition = array();
    if($q) $condition = array(array('like','username',$q));
    
    $rooms = $this->room_model->Search();
    if ($rooms) {
        $tmp = array();
        foreach($rooms as $v){
            $tmp[$v['id']] = $v['name'];
        }
        $data['rooms'] = $tmp;
    }else{
        $data['rooms'] = array();
    }
    
    $posts = $this->customer_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    $total = $this->customer_model->GetCountSearch($condition);
    
    //echo $total;die();
    if ($posts) {
      $data['posts'] = $posts;
    }
    $data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    $data['q'] = $q;
    $data['month'] = $month;
    
    $this->load->view('admin/customer-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        
        $this->load->model('customer_model');
        $ids = $postData['ids'];
        switch($postData['action']){
            case 'set_publ':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->customer_model->UpdateBatch('status',$data);
                break;
            case 'set_draf':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->customer_model->UpdateBatch('status',$data);
                break;
                break;
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->customer_model->UpdateBatch('display',$data);
                break;
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->customer_model->UpdateBatch('display',$data);
                break;
            case 'set_dele':
                $this->customer_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->customer_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  function add() {
    $this->load->model('public_model');
    
    $this->load->library('form_builder',array('id'=>'post_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/post/create_new_post"'));

    $fields['title'] = array('size' => 25, 'label' => '标题', 'required' => TRUE);
    $fields['content'] = array('type' => 'textarea', 'label' => '正文', 'required' => FALSE);
    $fields['user_id'] = array('size' => 25, 'label' => '用户id', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['post_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/post-edit',$data);
  }  
  
  
  function view() {
    $this->load->model('public_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    if( count($id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $posts = $this->public_model->get_posts_for_user($user_id);
    
    $data['posts'] = $posts;
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/post/?message=success');
    }else{
        redirect('/admin/post/?message=failure');
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->remove($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  
  function ajax_check_username(){
    $q = $this->input->get('q',true);
    if ($q) {
        $this->load->model('customer_model');
        $condition = array(array('where','username',$q));
        $posts = $this->customer_model->Search(0,0,$condition);
        if($posts){
            $arr = array (
                'status'=>'failure',
                'message'=>_('user existing')
            );
        }else{
            $arr = array (
                'status'=>'success',
                'message'=>_('empty')
            );
        }
    }else{
        $arr = array (
            'status'=>'success',
            'message'=>_('empty')
        );
    }
    echo json_encode($arr);
  }
    
  function ajax_check_email(){
    $q = $this->input->get('q',true);
    if ($q) {
        $this->load->model('customer_model');
        $condition = array(array('where','email',$q));
        $posts = $this->customer_model->Search(0,0,$condition);
        if($posts){
            $arr = array (
                'status'=>'failure',
                'message'=>_('email existing')
            );
        }else{
            $arr = array (
                'status'=>'success',
                'message'=>_('empty')
            );
        }
    }else{
        $arr = array (
            'status'=>'success',
            'message'=>_('empty')
        );
    }
    echo json_encode($arr);
  }
  function export(){
    $s = $this->input->post('startDate',true);
    $e = $this->input->post('endDate',true);
    $this->load->model('customer_model');
    $posts = $this->customer_model->getByRange($s,$e);
    
    if(empty($posts)) die('数据不存在');
    $data = "注册名,邮箱,姓名,生日,性别,手机,城市,地址,注册日期\n";
    for($i=0;$i<count($posts);$i++){
        $email = $posts[$i]['email'];
        $name = $posts[$i]['name'];
        $username = $posts[$i]['username'];
        $birthdate = $posts[$i]['birthdate'];
        $gender = ($posts[$i]['gender'] == 1)?'男':'女';
        $city = $posts[$i]['city'];
        $createdate = $posts[$i]['createdate'];
        $mobile = $posts[$i]['mobile'];
        $address = $posts[$i]['address'];
        $data.= "$username,$email,$name,$birthdate,$gender,$mobile,$city,$address,$createdate\n";
    }
    
    $filename = date('Ymd').'.csv'; //设置文件名 
    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header("Content-Disposition:attachment;filename=".$filename); 
    header('Cache-Control:must-revalidate,post-check=0,pre-check=0'); 
    header('Expires:0'); 
    header('Pragma:public'); 
    echo "\xEF\xBB\xBF";
    echo $data; 
    
    
  }

}
