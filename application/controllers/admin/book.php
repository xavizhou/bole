<?php

class book extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/admin/login/show_login');
    }
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->book_list();
  }

  function book_list(){
    $this->load->model('book_model');
    $page = (int)$this->input->get('page');
    $page = ($page)?$page:0;
    $books = $this->book_model->get_books();
    if ($books) {
      $data['books'] = $books;
    }
    $this->load->view('admin/book-list',$data);
  }
  
  function add() {
    $this->load->model('book_model');
    $this->load->library('form_builder',array('id'=>'book_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/book/create_new"'));

    $fields['name'] = array('size' => 32, 'label' => '书名', 'required' => TRUE);
    $fields['intro'] = array('type' => 'textarea', 'label' => '简介', 'required' => TRUE);
    $fields['author'] = array('size' => 32, 'label' => '作者', 'required' => TRUE);
    $fields['url'] = array('size' => 32, 'label' => '网址', 'required' => TRUE);
    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = 1;
    $this->form_builder->set_field_values($values);
    $this->form_builder->show_required = TRUE;
    $data['book_new_form'] = $this->form_builder->render();
    $this->load->view('admin/book-edit',$data);
  }
  
  function edit() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $query = $this->db->get_where(TBL_BOOK, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $book = $query->row_array();
      }
    }
    $this->load->model('book_model');
    $this->load->library('form_builder',array('id'=>'book_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/book/modify"'));

    $fields['id'] = array('type' => 'hidden');
    $fields['name'] = array('size' => 32, 'label' => '书名', 'required' => TRUE);
    $fields['intro'] = array('type' => 'textarea', 'label' => '简介', 'required' => TRUE);
    $fields['author'] = array('size' => 32, 'label' => '作者', 'required' => TRUE);
    $fields['url'] = array('size' => 32, 'label' => '网址', 'required' => TRUE);
    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    if(!empty($book)){
        $this->form_builder->set_field_values($book);
    }
    $this->form_builder->show_required = TRUE;
    $data['book_new_form'] = $this->form_builder->render();
    
    $books = $this->book_model->get_books(10,0);
    $data['books'] = $books;
    
    $this->load->view('admin/book-edit',$data);
  }
  
  
  function create_new() {
    $bookData = $this->input->post(null,true);
    if( count($bookData) ) {
      $this->load->model('book_model');
      $saved = $this->book_model->create_new($bookData);
    }
    if ( isset($saved) && $saved ) {
        redirect('admin/book?id='.$saved);
    }
  }

  function modify() {
    $book = $this->input->post(null,true);
    if( count($book) ) {
      $this->load->model('book_model');
      $saved = $this->book_model->modify($book);
    }
    if ( isset($saved) && $saved ) {
       redirect('/admin/book/edit?message=success&id='.$book['id']);
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('book_model');
      $saved = $this->book_model->unactive($id);
    }
    redirect('admin/book?message=success');
  }
  function active() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('book_model');
      $saved = $this->book_model->active($id);
    }
    redirect('admin/book?message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('book_model');
      $saved = $this->book_model->remove($id);
    }
    redirect('admin/book?message=success');
  }
}
