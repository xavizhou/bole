<?php

class user extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
    return ;
    $data = $this->data;
    $this->load->model('user_model');
    $page = $this->input->get('page');
    $data['type'] = $type;
    
    $condition = array(array('order','id','asc'));
            
    $posts = $this->user_model->Search($page*20,20,$condition);
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    
    $this->load->view('admin/user-list',$data);
  }
  
  function edit() {
    $data = $this->data;
    $this->load->model('user_model');
    $id = (int)$this->input->get('id');
    $type = (int)$this->input->get('type');
    
    $post = $this->user_model->Get($id);
    
    if ($post) {
      $data['post'] = $post;
    }
      
    $this->load->helper('form');
    $this->load->library('form_validation');
    
    $this->load->view('admin/user-edit',$data);
  }
  
  function show_main() {
    $this->load->model('public_model');
    $post_id = $this->input->get('post_id');
    // Load all of the logged-in user's posts

    if( count($post_id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $post_id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }

  function save() {
        $postData = $this->input->post(null, true);
        
        $this->load->model('user_model');
        
        //if(empty($postData['username']) || empty($postData['email'])){
        if(empty($postData['username'])){
            redirect('/admin/user/edit?id='.$postData['id']);
        }
        if(empty($postData['id']) && empty($postData['password'])){
            redirect('/admin/user/edit');
        }
        
        $saveData = array();
        $saveData['username'] = $postData['username'];
        $saveData['status'] = (int)$postData['status'];
//        $saveData['email'] = $postData['email'];
        if(($postData['password']) && ($postData['password'] != PASSWORD_DEFAULT)){
            $saveData['password'] = md5($postData['password']);
        }
        
        $id = $this->user_model->Save($saveData,$postData['id']);
        $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'message' ));  
        
        redirect('/admin/user/edit?id='.$id);
        
  }
  function search(){
    $data = $this->data;
    $this->load->model('user_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    
    $condition = array();
    if($q) $condition = array(array('like','username',$q),array('or_like','email',$q));
    
    $posts = $this->user_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    $total = $this->user_model->GetCountSearch($condition);
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    $data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    $data['q'] = $q;
    
    $this->load->view('admin/user-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        
        $this->load->model('user_model');
        $ids = $postData['ids'];
        switch($postData['action']){
            case 'set_publ':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->user_model->UpdateBatch('status',$data);
                break;
            case 'set_draf':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->user_model->UpdateBatch('status',$data);
                break;
                break;
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->user_model->UpdateBatch('display',$data);
                break;
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->user_model->UpdateBatch('display',$data);
                break;
            case 'set_dele':
                $this->user_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->user_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  function add() {
    $this->load->model('public_model');
    
    $this->load->library('form_builder',array('id'=>'post_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/post/create_new_post"'));

    $fields['title'] = array('size' => 25, 'label' => '标题', 'required' => TRUE);
    $fields['content'] = array('type' => 'textarea', 'label' => '正文', 'required' => FALSE);
    $fields['user_id'] = array('size' => 25, 'label' => '用户id', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['post_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/post-edit',$data);
  }  
  
  
  function view() {
    $this->load->model('public_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    if( count($id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $posts = $this->public_model->get_posts_for_user($user_id);
    
    $data['posts'] = $posts;
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/post/?message=success');
    }else{
        redirect('/admin/post/?message=failure');
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->remove($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  
  function ajax_check_username(){
    $q = $this->input->get('q',true);
    if ($q) {
        $this->load->model('user_model');
        $condition = array(array('where','username',$q));
        $posts = $this->user_model->Search(0,0,$condition);
        if($posts){
            $arr = array (
                'status'=>'failure',
                'message'=>_('user existing')
            );
        }else{
            $arr = array (
                'status'=>'success',
                'message'=>_('empty')
            );
        }
    }else{
        $arr = array (
            'status'=>'success',
            'message'=>_('empty')
        );
    }
    echo json_encode($arr);
  }
    
  function ajax_check_email(){
    $q = $this->input->get('q',true);
    if ($q) {
        $this->load->model('user_model');
        $condition = array(array('where','email',$q));
        $posts = $this->user_model->Search(0,0,$condition);
        if($posts){
            $arr = array (
                'status'=>'failure',
                'message'=>_('email existing')
            );
        }else{
            $arr = array (
                'status'=>'success',
                'message'=>_('empty')
            );
        }
    }else{
        $arr = array (
            'status'=>'success',
            'message'=>_('empty')
        );
    }
    echo json_encode($arr);
  }

}
