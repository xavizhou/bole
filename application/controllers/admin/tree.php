<?php

class tree extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/admin/login/show_login');
    }
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  function show_attribute(){
    $this->load->model('tree_model');
    $page = (int)$this->input->get('page');
    $status = (int)$this->input->get('status');
    if(!$status) $status = CONT_SEND;
    $page = ($page)?$page:0;
    $posts = $this->tree_model->get_contribute($status,20,$page*20);
    $total = $this->tree_model->get_contribute_count(array());

    $data['total'] = $total;
    $data['per'] = 20;
    $data['user_id'] = $user_id;
    // If posts were fetched from the database, assign them to $data
    // so they can be passed into the view.
    if ($posts) {
      $data['posts'] = $posts;
    }
    
    switch($status){
        case CONT_REFU:
            $data['title'] = '退稿文章';
            break;
        case CONT_SEND:
            $data['title'] = '投稿文章';
            break;
    }
    $data['status'] = $status;
    $this->load->view('admin/contribute-list',$data);
  }
  function show_list() {
    $this->load->model('tree_model');
    $page = (int)$this->input->get('page');
    $year = (int)$this->input->get('year');
    $page = ($page)?$page:0;
    
    $c = array(
        'year'=>$year
    );
    $posts = $this->tree_model->get_posts($c,20,$page*20);
    $total = $this->tree_model->get_post_count($c);
    $data['total'] = $total;
    $data['per'] = 20;
    $data['user_id'] = $user_id;

    // If posts were fetched from the database, assign them to $data
    // so they can be passed into the view.
    if ($posts) {
      $data['posts'] = $posts;
    }
    $data['year'] = $year;
    $data['title'] = '光阴树文章';
    $this->load->view('admin/tree-list',$data);
  }
  function update_position(){
    $postData = $this->input->post(null,true);
    $data['year'] = $postData['year'];
    $data['position'] = $postData['position'];
    
    $this->load->model('tree_model');
    $this->tree_model->update_position($data['position']);
    
    
    redirect('/admin/tree/?year='.$data['year']);
    
  }
  function show_main() {
    $this->load->model('tree_model');
    $post_id = $this->input->get('post_id');
    // Load all of the logged-in user's posts

    if( count($post_id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $post_id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $data['user'] = $user;
    $this->load->view('admin/tree-detail',$data);
  }


  
  function view() {
    $this->load->model('tree_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    $this->load->model('tree_model');
    $tree_posts = $this->tree_model->get_posts_by_id($id);
    
    if( count($id) ) {
      $query = $this->db->get_where(TBL_PCON, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $data['tree_posts'] = $tree_posts;
    
    
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
        $this->load->model('post_model');
        $this->load->model('tag_model');
        //get tags
        $r = $this->post_model->get_post_tags($post['post_id'],$user_id);
        if($r){
            for($i=0;$i<count($r);$i++){
                $tags[] = $r[$i]['tag_id'];
            }
        }
        if($tags){
            $_tmp = $this->tag_model->get_tags_for_post($tags);
            $_r = array();
            for($i=0;$i<count($_tmp);$i++){
                $_r[$_tmp[$i]['id']] = $_tmp[$i]['value'];
            }
        }
        $data['tags'] = $_tmp;
        //get tags
        
    
    $data['user'] = $user;
    $data['id'] = $id;
    $this->load->view('admin/tree-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('tree_model');
      $saved = $this->tree_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/tree/?message=success');
    }else{
        redirect('/admin/tree/?message=failure');
    }
  }

  function accept_contribute(){
    $data = $this->input->post(null,true);
    $this->load->model('tree_model');
    if(empty($data['year']) || empty($data['tag'])){
        redirect('/admin/tree/view?id='.$data['id'].'&message=failure');
    }
    $post = $this->tree_model->get_post_by_contribute_id($data['id']);
    $saved = $this->tree_model->accpet_contribute($data['id']);
    $postData['contribute_id'] = $data['id'];
    $postData['post_id'] = $data['post_id'];
    $postData['year'] = $data['year'];
    $postData['tags'] = implode(",", $data['tag']);
    $saved = $this->tree_model->add_post($postData);
    
     
    $this->config->load('email', TRUE);
    $tag_list = $this->config->item('tag_list', 'email');
    if($post['status'] == CONT_SEND){//第一次投稿 发通知
        $contents = '';
        $content = read(FCPATH.'assets/email/accept.html',$contents);
        $cc = $this->session->userdata('email');
        $data['to'] = $post['email'];
        
        $title = $post['title'];
        $year = implode("/", $postData['year']);
        if(is_array($data['tag'])){
            foreach($data['tag'] as $v){
                $tags[] = $tag_list[$v];
            }
        }
        $tags = implode("/", $tags);
        
        $site_url = site_url();
        $date = date('Y-m-d');;

        $data['subject'] = sprintf(_('您好，%sEVERLIVE光阴树选用了您的投稿'),$post['display_name']);
        $contents = str_replace('{username}', $username, $contents);
        $contents = str_replace('{site_url}', $site_url, $contents);
        $contents = str_replace('{date}', $date, $contents);
        $contents = str_replace('{year}', $year, $contents);
        $contents = str_replace('{tags}', $tags, $contents);
        $data['message'] = $contents;

        $this->load->model('email_model');
        $r = $this->email_model->sendmail($data,$error);
        
    }
    
    redirect('/admin/tree/view?id='.$data['id'].'&message=success');
  }
  
  function cancel() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('tree_model');
      $saved = $this->tree_model->cancel_contribute($id);
    }
    redirect('admin/tree/');
  } 
  
  function refuse() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('tree_model');
      $saved = $this->tree_model->refuse_contribute($id);
      $this->tree_model->remove_post($id);
    }
    redirect('admin/tree/show_attribute');
  } 
  
  function accept() {
    $id = $this->input->get('id');
    $postData = $this->input->post(null,true);
    if( count($id) ) {
      $this->load->model('tree_model');
      $saved = $this->tree_model->accpet_contribute($id);
    }
    redirect('admin/tree?message=success');
  }
  function active() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('tree_model');
      $saved = $this->tree_model->refuse_contribute($id);
    }
    redirect('admin/tree?message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('tree_model');
      $saved = $this->tree_model->remove($id);
    }
    redirect('admin/tree?user_id='.$user_id.'&message=success');
  }

}
