<?php
class setting extends MY_Controller{
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
        $name_list = array(
            'home_mid_1' => _('首页中部左'),
            'home_mid_2' => _('首页中部中'),
            'home_mid_3' => _('首页中部右')
        );

        $data['name_list'] = $name_list;
        $this->load->model('setting_model');
        $this->load->model('product_model');
        $this->load->model('news_model');
        
        $all = $this->setting_model->getAll();
        
        $condition[] = array('where','status',1);
        $condition[] = array('where','display',1);
        $condition[] = array('where','delete',0);
        $condition[] = array('order','order','desc');
        $r = $this->product_model->Search(0,0,$condition);
        
        $posts = array();
        
        for($i=0;$i<count($r);$i++):
            $posts['product_'.$r[$i]['id']] = $r[$i]['title'];
        endfor;
        
        $r = $this->news_model->Search(0,0,$condition);
        for($i=0;$i<count($r);$i++):
            $posts['story_'.$r[$i]['id']] = $r[$i]['title'];
        endfor;
        
        $r = $all;
        $setting_list = array();
        $setting = array();
        $curr = array();
        for($i=0;$i<count($r);$i++):
            if($name_list[$r[$i]['name']]):
                $curr[$r[$i]['name']] = $r[$i]['value'];
            endif;
        endfor;
        
        foreach($name_list as $name=>$v):
            $setting_list[$name] = $posts;
            $setting[$name] = $curr[$name];
        endforeach;
        
        
        //job feature
        $name_list = array(
            'home_job_1' => _('首页job 1'),
            'home_job_2' => _('首页job 2'),
            'home_job_3' => _('首页job 3'),
            'home_job_4' => _('首页job 4'),
            'home_job_5' => _('首页job 5'),
            'home_job_6' => _('首页job 6'),
            'home_job_7' => _('首页job 7'),
            'home_job_8' => _('首页job 8'),
            'home_job_9' => _('首页job 9'),
            'home_job_10' => _('首页job 10')
        );

        $data['job_list'] = $name_list;
        $this->load->model('job_model');
        $condition = array();
        $condition[] = array('where','status',1);
        //$condition[] = array('where','display',1);
        $condition[] = array('where','delete',0);
        $condition[] = array('order','order','desc');
        $r = $this->job_model->Search(0,0,$condition);
        $posts = array();
        
        for($i=0;$i<count($r);$i++):
            $posts[$r[$i]['id']] = $r[$i]['title'];
        endfor;
        
        $r = $all;
//        $setting_list = array();
//        $setting = array();
//        $curr = array();
        for($i=0;$i<count($r);$i++):
            if($name_list[$r[$i]['name']]):
                $curr[$r[$i]['name']] = $r[$i]['value'];
            endif;
        endfor;
        
        foreach($name_list as $name=>$v):
            $setting_list[$name] = $posts;
            $setting[$name] = $curr[$name];
        endforeach;
        
        
        
        //images
        $name_list = array(
            'home_slider_1' => _('首页slider 1(1360*540)'),
            'home_slider_2' => _('首页slider 2(1360*540)'),
            'home_slider_3' => _('首页slider 3(1360*540)'),
            'home_slider_4' => _('首页slider 4(1360*540)'),
            'home_bottom_1' => _('首页左下方(height:196px)'),
            'home_bottom_2' => _('首页右下方(height:196px)')
        );

        $data['image_list'] = $name_list;
        
        for($i=0;$i<count($r);$i++):
            if($name_list[$r[$i]['name']]):
                $curr[$r[$i]['name']] = $r[$i]['value'];
            endif;
        endfor;
        
        foreach($name_list as $name=>$v):
            $setting[$name] = $curr[$name];
        endforeach;
        
        $data['setting'] = $setting;
        $data['setting_list'] = $setting_list;
        
        $this->load->view('admin/setting',$data);
  }
  
  function save() {
      $postData = $this->input->post(null,true);
      
        $this->load->model('setting_model');
        foreach($postData as $k=>$v):
            $data = array(
                'name'=>$k,
                'value'=>$v
            );
              $condition = array();
              $condition[] = array('name'=>$k);
              $r = $this->setting_model->RemoveByCondition(array('name'=>$k));
              $id = $r[0]['id'];
              $this->setting_model->Save($data,$id);
        endforeach;
      
        
        
        $name_list = array(
            'home_slider_1' => _('首页slider 1'),
            'home_slider_2' => _('首页slider 2'),
            'home_slider_3' => _('首页slider 3'),
            'home_slider_4' => _('首页slider 4'),
            'home_bottom_1' => _('首页左下方'),
            'home_bottom_2' => _('首页右下方')
        );
        
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '2048';
        $config['max_width']  = '2048';
        $config['max_height']  = '1516';
        $config['encrypt_name']  = true;
        
        $this->load->library('upload');
        $this->load->library('image_lib'); 
        
        foreach($name_list as $k=>$v):
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file_'.$k))
            {
                $data = array('upload_data' => $this->upload->data());
                
                $condition = array();
                $condition[] = array('name'=>$k);
                $r = $this->setting_model->RemoveByCondition(array('name'=>$k));
                $data = array(
                    'name'=>$k,
                    'value'=>$data['upload_data']['file_name']
                );
                $this->setting_model->Save($data);
            }else{
                //$error = array('error' => $this->upload->display_errors()); 
            }
        endforeach;
        
        
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'message' ));  
            redirect('/admin/setting');
  }
}
