<?php
class gallery extends MY_Controller{
    var $news_category_list;
    public function __construct()
    {
      parent::__construct();
      $this->data['news_category_list'] = $this->config->item('news_category');

    }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function ajax_remove(){
    $this->load->model('gallery_model');
    $id = (int)$this->input->get('id');
    if($id):
        $this->gallery_model->Remove($id);
        $arr = array (
            'status'=>'success',
            'id'=>$id
        );
        echo json_encode($arr);
        die();
    else:
        $arr = array (
            'status'=>'failure',
            'id'=>0
        );
        echo json_encode($arr);
        die();
    endif;
  }

  function ajax_save() {
    $data = $this->data;
    $this->load->model('gallery_model');
    $id = (int)$this->input->get('id');
    $title = $this->input->post('title',true);
    $desc = $this->input->post('content',true);
    $Savedata['title'] = $title;
    $Savedata['content'] = $desc;
    if($id):
        $this->gallery_model->Save($Savedata,$id);
        $arr = array (
            'status'=>'success',
            'id'=>$id
        );
    
        echo json_encode($arr);
        die();
    else:
        $arr = array (
            'status'=>'failure',
            'id'=>0
        );
    
        echo json_encode($arr);
        die();
    endif;
    
//    if($id){
//        $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
//    }else{
//        $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
//    }
        
  }
  function search(){
    $data = $this->data;
    $this->load->model('gallery_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $category = (int)$this->input->get('category');
    

    if($category) $condition[] = array('where','category',$category);
    
    if($q) $condition[] = array('where','((title LIKE "%'.$q.'%") OR (content LIKE "%'.$q.'%") OR (keywords LIKE "%'.$q.'%"))',NULL);
    
    $condition[] = array('where','delete',0);
//    $condition[] = array('where','lang',$data['lang']);
    $condition[] = array('order','id','desc');
    
    //$posts = $this->gallery_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    //$total = $this->gallery_model->GetCountSearch($condition);
    $posts = $this->gallery_model->Search(0,999,$condition);
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    //$data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    
    $this->load->helper('form');
    $data['news_category_list'] = $this->news_category_list;
    
    $data['q'] = $q;
    $data['category'] = $category;
    $this->load->view('admin/gallery',$data);
  }
  
  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->remove($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
    function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  function import(){
      
      $this->load->model('gallery_model');
        function cmp($a, $b)
        {
            if ($a['ntime'] == $b['ntime']) {
                return 0;
            }
            return (strtotime($a['ntime']) > strtotime($b['ntime'])) ? 1 : -1;
        }

        include('tplx/news.php');
      
        $lang = 'zh_CN';
        $list = $news[$lang];
        //ksort($list);
        usort($list, "cmp");
        $i=1;
        foreach($list as $_n){
            $saveData = array();
            $saveData['title'] = $_n['title'];
            $saveData['content'] = $_n['content'];
            $saveData['createdate'] = date('Y-m-d',strtotime($_n['ntime']));
            $saveData['status'] = 1;
            $saveData['delete'] = 0;
            $saveData['lang'] = $lang;
            
            $saveData['order'] = ++$i;
            $id = $this->gallery_model->Save($saveData);
        }
        
        
        $lang = 'en_US';
        $list = $news[$lang];
        //ksort($list);
        usort($list, "cmp");
        $i=1;
        foreach($list as $_n){
            $saveData = array();
            $saveData['title'] = $_n['title'];
            $saveData['content'] = $_n['content'];
            $saveData['createdate'] = date('Y-m-d',strtotime($_n['ntime']));
            $saveData['status'] = 1;
            $saveData['delete'] = 0;
            $saveData['lang'] = $lang;
            
            $saveData['order'] = ++$i;
            $id = $this->gallery_model->Save($saveData);
        }
                    
  }

}
