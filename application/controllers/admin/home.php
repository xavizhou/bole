<?php
class home extends MY_Controller{
  public function __construct()
  {
    parent::__construct();
  }

    function slider(){
        $data = $this->data;
        $nav = array('home','slider');
        
        $data['nav'] = $nav;
        $this->load->view('admin/'.  implode('_', $nav),$data);
    }
}
