<?php
class History extends MY_Controller{
    private $city_list;
    public function __construct()
    {
      parent::__construct();
    }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function edit() {
    $data = $this->data;
    $this->load->model('history_model');
    $id = (int)$this->input->get('id');
    $type = (int)$this->input->get('type');
    $post = array(
        'title'=>'',
        'category'=>'',
        'content'=>'',
        'image'=>''
    );
    
    $lang = $data['lang'];
    $post = $this->history_model->Get($id);
    $item = $this->history_model->GetItem($post['id'],$lang);
    
    if(is_array($item)):
        $post['title'] = $item['title'];
        $post['subtitle'] = $item['subtitle'];
        $post['excerpt'] = $item['excerpt'];
        $post['content'] = $item['content'];
        $post['language_skill'] = $item['language_skill'];
    endif;
    
    
    $data['type'] = ($type)?$type:1;
    if ($post) {
      $data['post'] = $post;
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
      
    $this->load->helper('form');
    $this->load->library('form_validation');
    
    
    $this->load->view('admin/history-edit',$data);
  }
  

  function save() {
        $postData = $this->input->post(null, true);
        $postData['content'] = $this->input->post('content', false);
        
        if(empty($postData['title'])){
            redirect('/admin/history/edit?id='.$postData['id']);
        }
        
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '4096';
        
        $this->load->library('upload');
        $this->load->library('image_lib'); 
        
        $this->upload->initialize($config);
        if ($this->upload->do_upload('thumb_upload'))
        {
            $data = array('upload_data' => $this->upload->data());
            $postData['thumb'] = $data['upload_data']['file_name'];
        }else{
            $error = array('error' => $this->upload->display_errors()); 
        }
            
        for($i=0;$i<7;$i++){
            $this->upload->initialize($config);
            if ($this->upload->do_upload('image'.$i.'_upload'))
            {
                $data = array('upload_data' => $this->upload->data());
                $postData['image'.$i] = $data['upload_data']['file_name'];
                
            }else{
                $error = array('error' => $this->upload->display_errors()); 
            }
        }
        
        $this->load->model('history_model');
        
        if(empty($postData['title'])){
            redirect('/admin/history/edit?id='.$postData['id']);
        }
        if(!$postData['order']){
            $order = $this->history_model->GetMaxOrder();
            $postData['order'] = $order+1;
        }
        $saveData = array();
        
        $data = $this->data;
        
        $saveData['lang'] = $data['lang'];
        $saveData['status'] = (int)$postData['status'];
        $saveData['display'] = (int)$postData['display'];
        
        $saveData['title'] = $postData['title'];
        $saveData['date'] = $postData['date'];
        $saveData['content'] = $postData['content'];
        
        $saveData['gender'] = $postData['gender'];
        $saveData['category'] = $postData['category'];
        $saveData['recommend'] = $postData['recommend'];
        
        
        $saveData['thumb'] = $postData['thumb'];
        //$saveData['loc'] = implode(',', $postData['loc']);
        
        $j=0;
        for($i=0;$i< 7 ;$i++){
            if(empty($postData['image'.$i])) continue;
            $saveData['image'.$j] = $postData['image'.$i];
            $j++;
        }
        for($i=$j;$i<7 ;$i++){
            $saveData['image'.$i] = '';
        }
        $saveData['status'] = ($postData['star'])?2:1;
//        print_r($postData);
//        print_r($saveData);die();
                
        $saveData['url'] = $postData['url'];
        
        $saveData['order'] = $postData['order'];
                
        $id = $this->history_model->Save($saveData,$postData['id']);
        
        $saveData = array();
        $saveData['title'] = $postData['title'];
        $saveData['subtitle'] = $postData['subtitle'];
        $saveData['excerpt'] = $postData['excerpt'];
        $saveData['content'] = $postData['content'];
        
        $lang = $this->data['lang'];
        $this->history_model->SaveItem($saveData,$lang,$id);
        
        
        if($id){
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        }else{
            $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
        }
        redirect('/admin/history/edit?id='.$id);
        
  }
  function search(){
    $data = $this->data;
    $this->load->model('history_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $category = (int)$this->input->get('category');
    

    if($category) $condition[] = array('where','category',$category);
    
    if($q) $condition[] = array('where','((title LIKE "%'.$q.'%") OR (content LIKE "%'.$q.'%") OR (keywords LIKE "%'.$q.'%"))',NULL);
    
    $condition[] = array('where','delete',0);
    //$condition[] = array('where','lang',$data['lang']);
    $condition[] = array('order','date','desc');
    
    $lang = $this->data['lang'];
    $posts = $this->history_model->Search(0,999,$condition);
    
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->history_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
                $posts[$i]['language_skill'] = $item['language_skill'];
            endif;
        endfor;
        $data['posts'] = $posts;
    }
    
    //$data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    
    $this->load->helper('form');
    
    $data['q'] = $q;
    $data['category'] = $category;
    $this->load->view('admin/history-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        $action = $postData['action'];
        $this->load->model('history_model');
        $ids = $postData['ids'];
        switch($postData['action']){
            case 'set_publ':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->history_model->UpdateBatch('status',$data);
                $this->history_model->resetStatus($changed);
                if($changed){
                    $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的新闻无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
                }
                
                break;
            case 'set_draf':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->history_model->UpdateBatch('status',$data);
                break;
                break;
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->history_model->UpdateBatch('display',$data);
                break;
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->history_model->UpdateBatch('display',$data);
                break;
            case 'set_dele':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->history_model->UpdateBatch('delete',$data);
                //$this->history_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->history_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  
  function remove() {
    $id = $this->input->get('id');
    $ids = explode(",", $id);
    if( $id ) {
      $this->load->model('history_model');
        for($i=0;$i<count($ids);$i++){
            $data[$ids[$i]]=1;
        }
        $this->history_model->UpdateBatch('delete',$data);
    }
    
    $this->session->set_flashdata( 'message', array( 'title' => '删除成功', 'content' => '删除成功', 'type' => 'success' ));  
    redirect('admin/history');
  }
  

}
