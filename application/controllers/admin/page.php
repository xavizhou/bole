<?php
class Page extends MY_Controller{
  public function __construct()
  {
    parent::__construct();
  }
    function index(){
        $this->edit();
    }

    function edit(){
        $data = $this->data;
        $slug = $this->input->get('slug',true);
        
        if(empty($slug)){
            redirect('/admin/');
        }
        
        $this->load->model('page_model');
        
        $post = $this->page_model->GetBySlug($slug,$data['lang']);
        
        $data['slug'] = $slug;
        $data['post'] = $post;
        
        $this->load->view('admin/page',$data);
    }
    
    function save(){
        $postData = $this->input->post(null, true);
        $slug = $postData['slug'];
        $id = $postData['id'];
        $postData['content'] = $this->input->post('content', false);
        
        
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '8192';
        $config['max_width']  = '2048';
        $config['max_height']  = '2048';
        $config['encrypt_name']  = TRUE;
        
        $this->load->library('upload');
        $this->load->library('image_lib'); 
        
        $this->upload->initialize($config);
        if ($this->upload->do_upload('thumb_upload'))
        {
            $data = array('upload_data' => $this->upload->data());
            $postData['thumb'] = $data['upload_data']['file_name'];
        }else{
            $error = array('error' => $this->upload->display_errors()); 
            //print_r($error);
        }
            
        for($i=0;$i<7;$i++){
            $this->upload->initialize($config);
            if ($this->upload->do_upload('image'.$i.'_upload'))
            {
                $data = array('upload_data' => $this->upload->data());
                $postData['image'.$i] = $data['upload_data']['file_name'];
                //创建缩略图
                $image_config["image_library"] = "gd2";
                $image_config["source_image"] = $config['upload_path'].$data['upload_data']['file_name'];
                $image_config['new_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
                $image_config['create_thumb'] = FALSE;
                $image_config['maintain_ratio'] = TRUE;
                $image_config['quality'] = "100%";
                $image_config['width'] = 182;
                $image_config['height'] = 124;
                $image_config['encrypt_name']  = TRUE;
                //print_r($image_config);
                list($width, $height, $type, $attr) = getimagesize($image_config["source_image"]);
                $dim = ($width / $height) - ($image_config['width'] / $image_config['height']);
                $image_config['master_dim'] = ($dim > 0)? "height" : "width";
            
                $this->image_lib->clear();
                $this->image_lib->initialize($image_config); 

                if ($this->image_lib->resize())
                {
                    $image_config['image_library'] = 'gd2';
                    $image_config['source_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
                    $image_config['new_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
                    $image_config['quality'] = "100%";
                    $image_config['maintain_ratio'] = TRUE;
                    $image_config['width'] = 182;
                    $image_config['height'] = 124;
                    list($width, $height, $type, $attr) = getimagesize($image_config["source_image"]);
                    $image_config['x_axis'] = ($width-182)/2;
                    $image_config['y_axis'] = ($height-124)/2;

                    $this->image_lib->clear();
                    $this->image_lib->initialize($image_config); 
                    $this->image_lib->crop();
                }else{
                    
                    echo $this->image_lib->display_errors();;
                }
            }else{
                $error = array('error' => $this->upload->display_errors()); 
                //print_r($error);
            }
        }
        
        $this->load->model('page_model');
        
        
        $saveData = array();
        $data = $this->data;
        $saveData['lang'] = $data['lang'];
        $saveData['status'] = (int)$postData['status'];
        $saveData['display'] = (int)$postData['display'];
        
        $saveData['title'] = $postData['title'];
        $saveData['subtitle'] = $postData['subtitle'];
        $saveData['excerpt'] = $postData['excerpt'];
        $saveData['content'] = $postData['content'];
        $saveData['content_ext0'] = $postData['content_ext0'];
        $saveData['content_ext1'] = $postData['content_ext1'];
        
        $saveData['thumb'] = $postData['thumb'];
        
        $saveData['slug'] = $postData['slug'];
        
        $j=0;
        for($i=0;$i< 7 ;$i++){
            if(empty($postData['image'.$i])) continue;
            $saveData['image'.$j] = $postData['image'.$i];
            $j++;
        }
        for($i=$j;$i<7 ;$i++){
            $saveData['image'.$i] = '';
        }
        
        $_save_ = '_save_'.$slug;
        if(method_exists($this, $_save_)) { 
            $this->$_save_($saveData);
        }
        $id = $this->page_model->Save($saveData,$postData['id']);
        
        if($id){
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        }else{
            $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
        }
        //$this->session->set_flashdata( 'message', array( 'title' => '未上传缩略图和照片的无法发布', 'content' => '未上传缩略图和照片的新闻无法发布', 'type' => 'message' ));  
        redirect('/admin/page/edit?slug='.$slug);
    }
    
    
//    function _save_home_slider(&$saveData){
//    }
}
