<?php

class nav extends MY_Controller{

  private $filename;
  public function __construct()
  {
    $this->filename = 'uploads/config/nav.json';
    parent::__construct();
  }

  function index() {
    $data = $this->data;
    $files= array();

    $this->load->helper('file');
    $json = read_file($this->filename);
    $data['nav'] = json_decode($json,true);
    $this->load->view('admin/nav',$data);
  }

  function save() {
    $data = $this->data;
    $nav = $this->input->post('nav',false);
    $json = json_encode($nav);

    $this->load->helper('file');
    write_file($this->filename, $backup);

    $this->session->set_flashdata( 'message', array( 'title' => '导航保存成功', 'content' => '导航保存成功', 'type' => 'success' ));  
    redirect('admin/nav');
  }
}
