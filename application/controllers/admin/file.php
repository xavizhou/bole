<?php

class file extends MY_Controller{
    var $news_category_list;
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function edit() {
    $data = $this->data;
    $id = (int)$this->input->get('id');
    $fid = (int)$this->input->get('fid');
    
    $this->load->model('file_model');
    $post = $this->file_model->Get($id);
    if ($post) {
      $data['post'] = $post;
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
    $this->load->model('room_model');
    $condition = array();
    $condition[] = array('order','fid','0');
    $rooms = $this->room_model->Search(0,0,$condition);
    
    $tmp=array();
    $tmp2=array();
    $last_fid = -1;
    if ($rooms) {
        foreach($rooms as $v){
            if($v['fid']==0){
                $tmp2[$v['id']] = $v['name'];
                $tmp[$v['name']][$v['id']] = $v['name'];
            }else{
                $tmp[$tmp2[$v['fid']]][$v['id']] = $v['name'];
            }
        }
        $data['rooms'] = $tmp;
    }else{
        $data['rooms'] = array();
    }
    $data['fid'] = $fid;
    $this->load->view('admin/file-edit',$data);
  }
  
  function edit_room() {
    $data = $this->data;
    $id = (int)$this->input->get('id');
    $fid = (int)$this->input->get('fid');
    $this->load->model('room_model');
    $post = $this->room_model->Get($id);
    if ($post) {
      $data['post'] = $post;
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
    $condition = array();
    //$condition[] = array('where','fid','0');
    $rooms = $this->room_model->Search(0,0,$condition);
    if ($rooms) {
        $tmp = array();
        foreach($rooms as $v){
            $tmp[$v['id']] = $v['name'];
        }
        $data['rooms'] = $tmp;
    }else{
        $data['rooms'] = array();
    }
    $data['fid'] = $fid;
    $this->load->view('admin/room-edit',$data);
  }
  
  function save_room() {
        $postData = $this->input->post(null, true);
        
        $this->load->model('room_model');
        
        
        $saveData['name'] = $postData['title'];
        $saveData['fid'] = $postData['fid'];
        $id = $this->room_model->Save($saveData,$postData['id']);
        if($id){
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        }else{
            $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
        }
        redirect('/admin/file');
        
  }
  
  
  function save() {
        $postData = $this->input->post(null, true);
        
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '0';
        $config['encrypt_name'] = TRUE;


        $this->load->library('upload');
        $this->load->library('image_lib'); 
        
        $this->upload->initialize($config);
        if ($this->upload->do_upload('thumb_upload'))
        {
            $data = array('upload_data' => $this->upload->data());
            $saveData['filename'] = $data['upload_data']['orig_name'];
            $saveData['path'] = $data['upload_data']['file_name'];
            $saveData['size'] = $data['upload_data']['file_size'];
            $saveData['room_id'] = $postData['room_id'];
        }else{
            $error = array('error' => $this->upload->display_errors()); 
            if(!$postData['id']):
                redirect('/admin/file/edit');
            endif;
            $saveData['room_id'] = $postData['room_id'];
            
        }
        
        $this->load->model('file_model');
        
        
                
        $id = $this->file_model->Save($saveData,$postData['id']);
        if($id){
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        }else{
            $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
        }
        redirect('/admin/file/edit?id='.$id);
        
  }
  function search(){
    $data = $this->data;
    $this->load->model('file_model');
    $this->load->model('room_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $room_id = (int)$this->input->get('room_id');
    
    $condition = array();
    if($room_id):
        $condition = array();
        $condition[] = array('where','fid',$room_id);
        $folders = $this->room_model->Search(0,0,$condition);
        $ids = array();
        $ids[] = $room_id;
        if($folders):
            for($i=0;$i<count($folders);$i++):
                $ids[]=$folders[$i]['id'];
            endfor;
        endif;
        $condition = array();
        $condition[] = array('where_in','room_id',$ids);
    endif;
    
    
    $condition[] = array('order','date','desc');
    
    $posts = $this->file_model->Search(0,0,$condition);
    $total = $this->file_model->GetCountSearch($condition);
    
    
    $rooms = $this->room_model->Search();
    if ($rooms) {
        $tmp = array();
        foreach($rooms as $v){
            $tmp[$v['id']] = $v;
        }
        $data['rooms_all'] = $tmp;
    }else{
        $data['rooms_all'] = array();
    }
    
    
    
    $condition = array();
    $condition[] = array('where','fid','0');
    $rooms = $this->room_model->Search(0,0,$condition);
    if ($rooms) {
        $tmp = array();
        foreach($rooms as $v){
            $tmp[$v['id']] = $v['name'];
        }
        $data['rooms'] = $tmp;
    }else{
        $data['rooms'] = array();
    }
    
    
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    $data['total'] = $total;
    $data['page'] = $page;
    $data['room_id'] = $room_id;
    $data['per'] = PAGE_NUM;
    
    
    $this->load->helper('form');
    $this->load->view('admin/file-list',$data);
  }
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/post/?message=success');
    }else{
        redirect('/admin/post/?message=failure');
    }
  }

  function room_list(){
    $data = $this->data;
    $this->load->model('room_model');
    $rooms = $this->room_model->Search();
    $tmp=array();
    $tmp2=array();
    if ($rooms) {
        foreach($rooms as $v){
            if($v['fid']==0){
                $tmp2[$v['id']] = $v['name'];
            }else{
                $tmp[$v['fid']][$v['id']] = $v['name'];
            }
        }
        $data['rooms'] = $tmp2;
        $data['folder'] = $tmp;
    }else{
        $data['rooms'] = array();
        $data['folder'] = array();
    }
    $this->load->view('admin/room-list',$data);
  }
  
  function remove_room(){
        $id = $this->input->get('id',NULL);
        $this->load->model('room_model');
        $this->load->model('file_model');
        
        $condition = array();
        $condition[] = array('where','fid',$id);
        $folder = $this->room_model->Search(0,0,$condition);
        if($folder):
            $this->room_model->RemoveByCondition(array('fid'=>$id));
            for($i=0;$i<count($folder);$i++):
                $_id = $folder[$i]['id'];
                $this->file_model->RemoveByCondition(array('room_id'=>$_id));
            endfor;
        endif;
        $this->room_model->Remove($id);
        $this->file_model->RemoveByCondition(array('room_id'=>$id));
        redirect(site_url().'/admin/file/');
  }
  function remove(){
        $id = $this->input->get('id',NULL);
        $this->load->model('room_model');
        $this->load->model('file_model');
        $this->file_model->Remove($id);
        redirect(site_url().'/admin/file/');
  }
  
  
  function batch(){
        $postData = $this->input->post(null, true);
        
        $this->load->model('file_model');
        $ids = $postData['ids'];
        
        switch($postData['action']){
            case 'set_publ':
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->file_model->UpdateBatch('status',$data);
//                $this->file_model->resetStatus($changed);
//                if($changed){
//                    $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的产品无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
//                }
                //$this->session->set_flashdata( 'message', array( 'title' => '未上传照片的记录无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
                break;
            case 'set_draf':
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->file_model->UpdateBatch('status',$data);
                break;
                break;
//            case 'set_show':
//                for($i=0;$i<count($ids);$i++){
//                    $data[$ids[$i]]=1;
//                }
//                $this->file_model->UpdateBatch('display',$data);
//                break;
//            case 'set_hide':
//                for($i=0;$i<count($ids);$i++){
//                    $data[$ids[$i]]=0;
//                }
//                $this->file_model->UpdateBatch('display',$data);
//                break;
            case 'set_dele':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                //$this->file_model->UpdateBatch('delete',$data);
                $this->file_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->file_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  
  
}
