<?php
class news extends MY_Controller{
    var $news_category_list;
    public function __construct()
    {
      parent::__construct();
      $this->data['news_category_list'] = $this->config->item('news_category');

    }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function edit() {
    $data = $this->data;
    $this->load->model('news_model');
    $id = (int)$this->input->get('id');
    $category = $this->input->get('category');
    $data['category'] = $category;
    $post = array(
        'title'=>'',
        'category'=>'',
        'content'=>'',
        'image'=>''
    );
    $post = $this->news_model->Get($id);
    
    //$data['type'] = ($type)?$type:1;
    if ($post) {
      $data['post'] = $post;
      $data['category'] = $post['category'];
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
      
    $this->load->helper('form');
    $this->load->library('form_validation');
    
    
    $this->load->view('admin/news-edit',$data);
  }
  
  function show_main() {
    $this->load->model('news_model');
    $post_id = $this->input->get('post_id');
    // Load all of the logged-in user's posts

    if( count($post_id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $post_id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }

  function save() {
        $postData = $this->input->post(null, true);
//        print_r($_FILES);
//        print_r($postData);die();
        $postData['content'] = $this->input->post('content', false);
        
        if(empty($postData['title'])){
            redirect('/admin/news/edit?id='.$postData['id']);
        }
        
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '4096';
//        $config['max_width']  = '2048';
//        $config['max_height']  = '1516';
        


        $this->load->library('upload');
        $this->load->library('image_lib'); 
        
        $this->upload->initialize($config);
        if ($this->upload->do_upload('thumb_upload'))
        {
            $data = array('upload_data' => $this->upload->data());
            $postData['thumb'] = $data['upload_data']['file_name'];
        }else{
            $error = array('error' => $this->upload->display_errors()); 
        }
            
        for($i=0;$i<7;$i++){
            $this->upload->initialize($config);
            if ($this->upload->do_upload('image'.$i.'_upload'))
            {
                $data = array('upload_data' => $this->upload->data());
                $postData['image'.$i] = $data['upload_data']['file_name'];
                
//                
//                //创建缩略图
//                $image_config["image_library"] = "gd2";
//                $image_config["source_image"] = $config['upload_path'].$data['upload_data']['file_name'];
//                $image_config['new_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
//                $image_config['create_thumb'] = FALSE;
//                $image_config['maintain_ratio'] = TRUE;
//                $image_config['quality'] = "100%";
//                $image_config['width'] = 182;
//                $image_config['height'] = 124;
//                //print_r($image_config);
//                list($width, $height, $type, $attr) = getimagesize($image_config["source_image"]);
//                $dim = ($width / $height) - ($image_config['width'] / $image_config['height']);
//                $image_config['master_dim'] = ($dim > 0)? "height" : "width";
//            
//                $this->image_lib->clear();
//                $this->image_lib->initialize($image_config); 
//
//                if ($this->image_lib->resize())
//                {
//                    $image_config['image_library'] = 'gd2';
//                    $image_config['source_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
//                    $image_config['new_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
//                    $image_config['quality'] = "100%";
//                    $image_config['maintain_ratio'] = TRUE;
//                    $image_config['width'] = 182;
//                    $image_config['height'] = 124;
//                    list($width, $height, $type, $attr) = getimagesize($image_config["source_image"]);
//                    $image_config['x_axis'] = ($width-182)/2;
//                    $image_config['y_axis'] = ($height-124)/2;
//
//                    $this->image_lib->clear();
//                    $this->image_lib->initialize($image_config); 
//                    $this->image_lib->crop();
//                }else{
//                    
//                    echo $this->image_lib->display_errors();
//                }
                
                
            }else{
                $error = array('error' => $this->upload->display_errors()); 
            }
        }
        
        
        
        
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '8192';
        $this->upload->initialize($config);
        if ($this->upload->do_upload('document_upload'))
        {
            $data = array('upload_data' => $this->upload->data());
            $postData['document'] = $data['upload_data']['file_name'];
        }else{
            $error = array('error' => $this->upload->display_errors()); 
        }
        
        
        $this->load->model('news_model');
        
        if(empty($postData['title'])){
            redirect('/admin/news/edit?id='.$post['id']);
        }
        if(!$postData['order']){
            $order = $this->news_model->GetMaxOrder();
            $postData['order'] = $order+1;
        }
        $saveData = array();
        
        $data = $this->data;
        
        $saveData['lang'] = $data['lang'];
        $saveData['status'] = (int)$postData['status'];
        $saveData['display'] = (int)$postData['display'];
        
        $saveData['title'] = $postData['title'];
        $saveData['content'] = $postData['content'];
        
        $saveData['gender'] = $postData['gender'];
        $saveData['category'] = $postData['category'];
        $saveData['recommend'] = $postData['recommend'];
        $saveData['author'] = $postData['author'];
        $saveData['keywords'] = $postData['keywords'];
        
        $saveData['createdate'] = ($postData['createdate'])?$postData['createdate']:GetCurrentTime();
        //$saveData['publishdate'] = ($postData['publishdate'])?$postData['publishdate']:GetCurrentTime();
        
        $saveData['size'] = $postData['size'];
        $saveData['color'] = $postData['color'];
        $saveData['thumb'] = $postData['thumb'];
        $saveData['document'] = $postData['document'];
        
        $j=0;
        for($i=0;$i< 7 ;$i++){
            if(empty($postData['image'.$i])) continue;
            $saveData['image'.$j] = $postData['image'.$i];
            $j++;
        }
        for($i=$j;$i<7 ;$i++){
            $saveData['image'.$i] = '';
        }
        //$saveData['status'] = ($saveData['thumb'] && $saveData['image0'])?$postData['status']:0;
//        print_r($postData);
//        print_r($saveData);die();
                
        $saveData['url'] = $postData['url'];
        
        $saveData['order'] = $postData['order'];
                
        $id = $this->news_model->Save($saveData,$postData['id']);
        
//        if(empty($saveData['thumb']) && empty($saveData['image0']) && $postData['status']){
//            $this->session->set_flashdata( 'message', array( 'title' => '未上传缩略图和照片的新闻无法发布', 'content' => '未上传缩略图和照片的新闻无法发布', 'type' => 'message' ));  
//            redirect('/admin/news/edit?id='.$id);
//        }else{
//            redirect('/admin/news/');
//        }
        if($id){
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        }else{
            $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
        }
        redirect('/admin/news/edit?category='.$postData['category'].'&id='.$id);
        
  }
  function search(){
    $data = $this->data;
    $this->load->model('news_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $category = $this->input->get('category');
    

    if($category) $condition[] = array('where','category',$category);
    
    $condition[] = array('where','delete',0);
    $condition[] = array('where','lang',$data['lang']);
    $condition[] = array('order','order','desc');
    
    //$posts = $this->news_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    //$total = $this->news_model->GetCountSearch($condition);
    $posts = $this->news_model->Search(0,999,$condition);
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    //$data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    
    $this->load->helper('form');
    $data['news_category_list'] = $this->news_category_list;
    
    $data['q'] = $q;
    $data['category'] = $category;
    $this->load->view('admin/news-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        $action = $postData['action'];
        $this->load->model('news_model');
        $ids = $postData['ids'];
        switch($postData['action']){
            case 'set_publ':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->news_model->UpdateBatch('status',$data);
                $this->news_model->resetStatus($changed);
                if($changed){
                    $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的新闻无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
                }
                
                break;
            case 'set_draf':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->news_model->UpdateBatch('status',$data);
                break;
                break;
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->news_model->UpdateBatch('display',$data);
                break;
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->news_model->UpdateBatch('display',$data);
                break;
            case 'set_dele':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->news_model->UpdateBatch('delete',$data);
                //$this->news_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->news_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  function add() {
    $this->load->model('news_model');
    
    $this->load->library('form_builder',array('id'=>'post_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/post/create_new_post"'));

    $fields['title'] = array('size' => 25, 'label' => '标题', 'required' => TRUE);
    $fields['content'] = array('type' => 'textarea', 'label' => '正文', 'required' => FALSE);
    $fields['user_id'] = array('size' => 25, 'label' => '用户id', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['post_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/post-edit',$data);
  }  
  
  
  function view() {
    $this->load->model('news_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    if( count($id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $posts = $this->news_model->get_posts_for_user($user_id);
    
    $data['posts'] = $posts;
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('news_model');
      $saved = $this->news_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/post/?message=success');
    }else{
        redirect('/admin/post/?message=failure');
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('news_model');
      $saved = $this->news_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('news_model');
      $saved = $this->news_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  
  function remove() {
    $id = $this->input->get('id');
    $ids = explode(",", $id);
    if( $id ) {
      $this->load->model('news_model');
        for($i=0;$i<count($ids);$i++){
            $data[$ids[$i]]=1;
        }
        $this->news_model->UpdateBatch('delete',$data);
    }
    
    $this->session->set_flashdata( 'message', array( 'title' => '删除成功', 'content' => '删除成功', 'type' => 'success' ));  
    redirect('admin/news');
  }
  
  
    function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  function import(){
      
      $this->load->model('news_model');
        function cmp($a, $b)
        {
            if ($a['ntime'] == $b['ntime']) {
                return 0;
            }
            return (strtotime($a['ntime']) > strtotime($b['ntime'])) ? 1 : -1;
        }

        include('tplx/news.php');
      
        $lang = 'zh_CN';
        $list = $news[$lang];
        //ksort($list);
        usort($list, "cmp");
        $i=1;
        foreach($list as $_n){
            $saveData = array();
            $saveData['title'] = $_n['title'];
            $saveData['content'] = $_n['content'];
            $saveData['createdate'] = date('Y-m-d',strtotime($_n['ntime']));
            $saveData['status'] = 1;
            $saveData['delete'] = 0;
            $saveData['lang'] = $lang;
            
            $saveData['order'] = ++$i;
            $id = $this->news_model->Save($saveData);
        }
        
        
        $lang = 'en_US';
        $list = $news[$lang];
        //ksort($list);
        usort($list, "cmp");
        $i=1;
        foreach($list as $_n){
            $saveData = array();
            $saveData['title'] = $_n['title'];
            $saveData['content'] = $_n['content'];
            $saveData['createdate'] = date('Y-m-d',strtotime($_n['ntime']));
            $saveData['status'] = 1;
            $saveData['delete'] = 0;
            $saveData['lang'] = $lang;
            
            $saveData['order'] = ++$i;
            $id = $this->news_model->Save($saveData);
        }
                    
  }

}
