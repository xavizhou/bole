<?php

class City extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
  }

  function index() {
    $data = $this->data;
    $files= array();
    read('assets/city.json',$content);
    if($content):
        $content = json_decode($content,TRUE);
    endif;
    $data['city_list'] = $content;
    $this->load->view('admin/city',$data);
  }

  function save() {
    $data = $this->data;
    $citys = $this->input->post('city',FALSE);
    $codes = $citys['code'];
    $all = array();
    //print_r($citys);die();
    for($i=0;$i<count($codes);$i++):
        if(!$codes[$i]):
            continue;
        endif;
        $o = array();
        foreach($data['lang_list'] as $k=>$v):
            $o[$k] = $citys[$k][$i];
        endforeach;
        $k = $codes[$i];
        
        if($citys['location'][$i]){
            $o['location'] = $citys['location'][$i];
        }else{
            $o['location'] = $this->get_location($citys['en_US'][$i]);
        }
        
        
        $all[$k]=$o;
    endfor;
    $this->load->helper('file');
    //print_r($all);die();
    write_file('assets/city.json', json_encode($all));

        
    $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
    redirect('admin/city');
  }
  
  private function get_location($city){
      if(!$city):
          return '';
      endif;
      $str = file_get_contents('http://ditu.google.cn/maps/api/geocode/json?address='.$city);
      $jsonArr = json_decode($str,TRUE);
      
      //print_r($jsonArr);
      if(isset($jsonArr['results'][0]['geometry']['location'])):
          return implode(",", $jsonArr['results'][0]['geometry']['location']);
      endif;
      
  }
  
  
}
