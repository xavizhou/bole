<?php

class job extends MY_Controller{
    private $jobtype_list;
    private $location_list;
    private $industry_list;
    private $type_list;
  public function __construct()
  {
    parent::__construct();
    
//    $this->config->load('my_config', TRUE);
//    $this->jobtype_list = $this->config->item('jobtype_list', 'my_config');
//    $this->location_list = $this->config->item('location_list', 'my_config');
//    $this->industry_list = $this->config->item('industry_list', 'my_config');
    
    $this->jobtype_list = $this->config->item('jobtype_list');
    $this->location_list = $this->config->item('location_list');
    $this->industry_list = $this->config->item('industry_list');
    
    $this->data['jobtype_list'] = $this->jobtype_list;
    $this->data['location_list'] = $this->location_list;
    $this->data['industry_list'] = $this->industry_list;
    
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function edit() {
    $data = $this->data;
    $this->load->model('job_model');
    $id = (int)$this->input->get('id');
    $type = (int)$this->input->get('type');
    $post = array(
        'title'=>'',
        'category'=>'',
        'content'=>'',
        'image'=>''
    );
    $post = $this->job_model->Get($id);
    
    $data['type'] = ($type)?$type:1;
    if ($post) {
      $data['post'] = $post;
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
      
    $this->load->helper('form');
    $this->load->library('form_validation');
    
    
    $this->load->view('admin/job-edit',$data);
  }
  
  function show_main() {
    $this->load->model('public_model');
    $post_id = $this->input->get('post_id');
    // Load all of the logged-in user's posts

    if( count($post_id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $post_id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }

  function save() {
        $postData = $this->input->post(null, true);
        $postData['content'] = $this->input->post('content', false);
        if(empty($postData['title'])){
            redirect('/admin/job/edit?id='.$postData['id']);
        }
        
        $this->load->model('job_model');
        
        if(empty($postData['title'])){
            redirect('/admin/job/edit?id='.$postData['id']);
        }
        if(!$postData['order']){
            $order = $this->job_model->GetMaxOrder();
            $postData['order'] = $order+1;
        }
        $saveData = array();
        
        $saveData['status'] = (int)$postData['status'];
        
        $saveData['title'] = $postData['title'];
        $saveData['excerpt'] = $postData['excerpt'];
        $saveData['content'] = $postData['content'];
        $saveData['company'] = $postData['company'];
        $saveData['reference'] = $postData['reference'];
        
        $saveData['location'] = $postData['location'];
        //$saveData['location'] = (int)$postData['location'];
        //$saveData['city'] = $postData['city'];
        $saveData['industry'] = ($postData['industry'])?implode(',', $postData['industry']):'';
        $saveData['type'] = (int)$postData['type'];
        
        $saveData['order'] = $postData['order'];    
                
        $id = $this->job_model->Save($saveData,$postData['id']);
        $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        redirect('/admin/job/edit?id='.$id);
  }
  
  function search(){
    $data = $this->data;
    $this->load->model('job_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    
    $this->load->model('job_model');
    
    $this->load->helper('form');
    
    $q = trim($this->input->get('q'));
    $location = (int)($this->input->get('location'));
    $type = (int)($this->input->get('type'));
    $recommend = (int)($this->input->get('recommend'));
    
    
    if($location) $condition[] = array('where','location',$location);
    if($type) $condition[] = array('where','type',$type);
    if($recommend) $condition[] = array('where','recommend',$recommend);
    
    if($q) $condition[] = array('where','((title LIKE "%'.$q.'%") OR (content LIKE "%'.$q.'%"))',NULL);
    $condition[] = array('where','delete',0);
    $condition[] = array('order','order','desc');
            
    
    $posts = $this->job_model->Search(0,999,$condition);
    //$total = $this->job_model->GetCountSearch($condition);
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    $data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    $data['q'] = $q;
    $data['location'] = $location;
    $data['type'] = $type;
    $data['recommend'] = $recommend;
    $this->load->view('admin/job-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        
        $this->load->model('job_model');
        $ids = $postData['ids'];
        
        switch($postData['action']){
            case 'set_publ':
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->job_model->UpdateBatch('status',$data);
//                $this->job_model->resetStatus($changed);
//                if($changed){
//                    $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的产品无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
//                }
                //$this->session->set_flashdata( 'message', array( 'title' => '未上传照片的记录无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
                break;
            case 'set_draf':
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->job_model->UpdateBatch('status',$data);
                break;
                break;
//            case 'set_show':
//                for($i=0;$i<count($ids);$i++){
//                    $data[$ids[$i]]=1;
//                }
//                $this->job_model->UpdateBatch('display',$data);
//                break;
//            case 'set_hide':
//                for($i=0;$i<count($ids);$i++){
//                    $data[$ids[$i]]=0;
//                }
//                $this->job_model->UpdateBatch('display',$data);
//                break;
            case 'set_dele':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->job_model->UpdateBatch('delete',$data);
                //$this->job_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->job_model->UpdateOrder($postData['orders']);
                break;
            case 'set_noti':
                $this->load->model('jobsearch_model');
                $result = array();
                $jobs = array();
                for($i=0;$i<count($ids);$i++){
                    $job = $this->job_model->Get($ids[$i]);
                    $jobs[$ids[$i]] = $job;
                    $condition = array();
                    $condition[] = array('where_or','user_id',$user_id);

                    $list = $this->jobsearch_model->get_relative($job['location'],$job['type'],$job['industry'],$job['discipline']);

                    for($j=0;$j< count($list);$j++):
                        if($list[$j]['keywords']):
                            if(strpos($job['title'],$list[$j]['keywords']) != -1):
                                $result[$ids[$i]][$list[$j]['user_id']] = 1;
                            elseif(strpos($job['content'],$list[$j]['keywords']) != -1):
                                $result[$ids[$i]][$list[$j]['user_id']] = 1;
                            endif;
                        else:
                           $result[$ids[$i]][$list[$j]['user_id']] = 1;
                        endif;
                    endfor;
                }
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  function add() {
    $this->load->model('public_model');
    
    $this->load->library('form_builder',array('id'=>'post_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/post/create_new_post"'));

    $fields['title'] = array('size' => 25, 'label' => '标题', 'required' => TRUE);
    $fields['content'] = array('type' => 'textarea', 'label' => '正文', 'required' => FALSE);
    $fields['user_id'] = array('size' => 25, 'label' => '用户id', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['post_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/post-edit',$data);
  }  
  
  
  function view() {
    $this->load->model('public_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    if( count($id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $posts = $this->public_model->get_posts_for_user($user_id);
    
    $data['posts'] = $posts;
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/post/?message=success');
    }else{
        redirect('/admin/post/?message=failure');
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->remove($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
    function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }

}
