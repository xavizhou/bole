<?php

class music extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/admin/login/show_login');
    }
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->music_list();
  }

  function music_list(){
    $this->load->model('music_model');
    $page = (int)$this->input->get('page');
    $page = ($page)?$page:0;
    $musics = $this->music_model->get_music(50);
    if ($musics) {
      $data['musics'] = $musics;
    }
    $this->load->view('admin/music-list',$data);
  }
  
  function add() {
    $this->load->model('music_model');
    $this->load->library('form_builder',array('id'=>'music_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/music/create_new"'));

    $fields['name'] = array('size' => 32, 'label' => '书名', 'required' => TRUE);
    $fields['intro'] = array('type' => 'textarea', 'label' => '简介', 'required' => TRUE);
    $fields['author'] = array('size' => 32, 'label' => '作者', 'required' => TRUE);
    $fields['url'] = array('size' => 32, 'label' => '网址', 'required' => TRUE);
    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = 1;
    $this->form_builder->set_field_values($values);
    $this->form_builder->show_required = TRUE;
    $data['music_new_form'] = $this->form_builder->render();
    $this->load->view('admin/music-edit',$data);
  }
  
  function edit() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $query = $this->db->get_where(TBL_MUSI, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $music = $query->row_array();
      }
    }
    $this->load->model('music_model');
    $this->load->library('form_builder',array('id'=>'music_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/music/modify"'));

    $fields['id'] = array('type' => 'hidden');
    $fields['name'] = array('size' => 32, 'label' => '书名', 'required' => TRUE);
    $fields['intro'] = array('type' => 'textarea', 'label' => '简介', 'required' => TRUE);
    $fields['author'] = array('size' => 32, 'label' => '作者', 'required' => TRUE);
    $fields['url'] = array('size' => 32, 'label' => '网址', 'required' => TRUE);
    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    if(!empty($music)){
        $this->form_builder->set_field_values($music);
    }
    $this->form_builder->show_required = TRUE;
    $data['music_new_form'] = $this->form_builder->render();
    
    $musics = $this->music_model->get_music(10,0);
    $data['musics'] = $musics;
    
    $this->load->view('admin/music-edit',$data);
  }
  
  
  function create_new() {
    $musicData = $this->input->post(null,true);
    if( count($musicData) ) {
      $this->load->model('music_model');
      $saved = $this->music_model->create_new($musicData);
    }
    if ( isset($saved) && $saved ) {
        redirect('admin/music?id='.$saved);
    }
  }

  function modify() {
    $music = $this->input->post(null,true);
    if( count($music) ) {
      $this->load->model('music_model');
      $saved = $this->music_model->modify($music);
    }
    if ( isset($saved) && $saved ) {
       redirect('/admin/music/edit?message=success&id='.$music['id']);
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('music_model');
      $saved = $this->music_model->unactive($id);
    }
    redirect('admin/music?message=success');
  }
  function active() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('music_model');
      $saved = $this->music_model->active($id);
    }
    redirect('admin/music?message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('music_model');
      $saved = $this->music_model->remove($id);
    }
    redirect('admin/music?message=success');
  }
}
