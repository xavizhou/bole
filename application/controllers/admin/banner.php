<?php

class banner extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
    return ;
    $data = $this->data;
    $this->load->model('banner_model');
    $user_id = $this->input->get('user_id');
    $type = (int)$this->input->get('type');
    $type = ($type)?$type:1;
    $data['type'] = $type;
    
    $condition = array(array('where','type',$type),array('where','delete',0),array('order','order','desc'));
            
    $posts = $this->banner_model->Search(0,0,$condition);
    
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    
    $this->load->view('admin/banner-list',$data);
  }
  
  function edit() {
    $data = $this->data;
    $this->load->model('banner_model');
    $id = (int)$this->input->get('id');
    $type = (int)$this->input->get('type');
    $post = array(
        'title'=>'',
        'category'=>'',
        'content'=>'',
        'image'=>''
    );
    $post = $this->banner_model->Get($id);
    
    $data['type'] = ($type)?$type:1;
    $data['type'] = 1;
    if ($post) {
      $data['post'] = $post;
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
      
    $this->load->helper('form');
    $this->load->library('form_validation');
    $data['form']  = createForm($form);
    
    $this->load->view('admin/banner-edit',$data);
  }
  
  function show_main() {
    $this->load->model('public_model');
    $post_id = $this->input->get('post_id');
    // Load all of the logged-in user's posts

    if( count($post_id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $post_id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }

  function save() {
        $postData = $this->input->post(null, true);
        $postData['content'] = $this->input->post('content', false);
        //print_r($postData);die();
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '2048';
        $config['max_width']  = '2048';
        $config['max_height']  = '1516';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('image_upload'))
        {
        $error = array('error' => $this->upload->display_errors());
        } 
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $postData['image'] = $data['upload_data']['file_name'];
        }
        
        $this->load->model('banner_model');
        
        if(empty($postData['title'])){
            redirect('/admin/banner/edit?id='.$post['id']);
        }
        if(!$postData['order']){
            $order = $this->banner_model->GetMaxOrder(array('type'=>$postData['type']));
            $postData['order'] = $order+1;
        }
        $saveData = array();
        $saveData['title'] = $postData['title'];
        $saveData['display'] = (int)$postData['display'];
        $saveData['type'] = $postData['type'];
        $saveData['title'] = $postData['title'];
        $saveData['content'] = $postData['content'];
        $saveData['image'] = ($data['file_name'])?$data['file_name']:$postData['image'];
        $saveData['url'] = $postData['url'];
        $saveData['video'] = $this->input->post('video', false);
        $saveData['url_target'] = $postData['url_target'];
        $saveData['order'] = $postData['order'];
        $saveData['is_link'] = $postData['is_link'];
                
        $saveData['status'] = ($saveData['image'])?(int)$postData['status']:0;
        
        $id = $this->banner_model->Save($saveData,$postData['id']);
        if(empty($saveData['image']) && $postData['status']){
            $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的横幅无法发布', 'content' => '未上传照片的横幅无法发布', 'type' => 'message' ));  
            redirect('/admin/banner/edit?type='.$saveData['type'].'&id='.$id);
        }else{
            redirect('/admin/banner/?type='.$saveData['type']);
        }
        
        
        
  }
  function search(){
    $data = $this->data;
    $this->load->model('banner_model');
    $type = (int)$this->input->get('type');
    $page = (int)$this->input->get('page');
    $q = trim($this->input->get('q'));
    
    $type = ($type)?$type:1;
    $data['type'] = $type;
    
    
    $condition[] = array('where','type',$type);
    $condition[] = array('where','delete',0);
    if($q) $condition[] = array('like','title',$q);
    $condition[] = array('order','order','desc');
    
    $posts = $this->banner_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    $total = $this->banner_model->GetCountSearch($condition);
            
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    $data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    
    $data['q'] = $q;
    $data['gender'] = $gender;
    $data['category'] = $category;
    $data['recommend'] = $recommend;
    
    $this->load->view('admin/banner-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        
        $this->load->model('banner_model');
        $ids = $postData['ids'];
        switch($postData['action']){
            case 'set_publ':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->banner_model->UpdateBatch('status',$data);
                $this->banner_model->resetStatus($changed);
                if($changed){
                    $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的横幅无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
                }
                break;
            case 'set_draf':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->banner_model->UpdateBatch('status',$data);
                break;
                break;
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->banner_model->UpdateBatch('display',$data);
                break;
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->banner_model->UpdateBatch('display',$data);
                break;
            case 'set_dele':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->banner_model->UpdateBatch('delete',$data);
                //$this->banner_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->banner_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  function add() {
    $this->load->model('public_model');
    
    $this->load->library('form_builder',array('id'=>'post_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/post/create_new_post"'));

    $fields['title'] = array('size' => 25, 'label' => '标题', 'required' => TRUE);
    $fields['content'] = array('type' => 'textarea', 'label' => '正文', 'required' => FALSE);
    $fields['user_id'] = array('size' => 25, 'label' => '用户id', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['post_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/post-edit',$data);
  }  
  
  
  function view() {
    $this->load->model('public_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    if( count($id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $posts = $this->public_model->get_posts_for_user($user_id);
    
    $data['posts'] = $posts;
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/post/?message=success');
    }else{
        redirect('/admin/post/?message=failure');
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->remove($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
    function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }

}
