<?php

class login extends CI_Controller {

    function index() {
        if( $this->session->userdata('isLoggedIn') ) {
            redirect('/admin/main/show_main');
        } else {
            $this->show_login(false);
        }
    }

    function login_user() {
        // Create an instance of the user model
        $this->load->model('admin_model');

        // Grab the email and password from the form POST
        $username = $this->input->post('email');
        $pass  = $this->input->post('password');
        
//        $captcha  = $this->input->post('captcha');
//        $this->load->model('captcha_model');
//        $valid_cap = ($captcha && ($this->captcha_model->check_captcha($captcha) == true))?true:false;
//        
//        if($valid_cap == false){
//            redirect('/admin/main/show_main?error=cap');
//        }
        $valid_cap = true;
        
        //Ensure values exist for email and pass, and validate the user's credentials
        if( $username && $pass && $this->admin_model->validate_user($username,$pass) && $valid_cap) {
            // If the user is valid, redirect to the main view
            redirect('/admin/main/show_main');
        } else {
            // Otherwise show the login screen with an error message.
            $this->session->set_flashdata( 'message', array( 'title' => '用户名或者密码不正确', 'content' => '用户名或者密码不正确', 'type' => 'error' ));  
            redirect('/admin/login/show_login');
        }
    }

    function show_login( $show_error = false ) {
        $data['error'] = $show_error;

//        $this->load->model('captcha_model');
//        $cap = $this->captcha_model->create_captcha();
//        $data['cap'] = $cap;
        $this->load->helper('form');
        $this->load->view('admin/login',$data);
    }

    function logout_user() {
      $this->session->sess_destroy();
      $this->index();
    }

    function showphpinfo() {
        //echo phpinfo();
    }


}
