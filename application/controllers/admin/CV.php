<?php
class cv extends MY_Controller{
    public function __construct()
    {
      parent::__construct();
    }

  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function detail() {
    $data = $this->data;
    $this->load->model('cv_model');
    $id = (int)$this->input->get('id');
    $category = $this->input->get('category');
    $data['category'] = $category;
    $post = array(
        'title'=>'',
        'category'=>'',
        'content'=>'',
        'image'=>''
    );
    $post = $this->cv_model->Get($id);
    
    //$data['type'] = ($type)?$type:1;
    if ($post) {
      $data['post'] = $post;
      $data['category'] = $post['category'];
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
      
    $this->load->helper('form');
    $this->load->library('form_validation');
    
    
    $this->load->view('admin/cv-edit',$data);
  }
  
  function search(){
    $data = $this->data;
    $this->load->model('cv_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $category = $this->input->get('category');
    

    if($category) $condition[] = array('where','category',$category);
    
    //$condition[] = array('where','delete',0);
    //$condition[] = array('where','lang',$data['lang']);
    $condition[] = array('order','id','desc');
    
    //$posts = $this->cv_model->Search($page*PAGE_NUM,PAGE_NUM,$condition);
    //$total = $this->cv_model->GetCountSearch($condition);
    $posts = $this->cv_model->Search(0,999,$condition);
    
    if ($posts) {
      $data['posts'] = $posts;
    }
    //$data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    
    $this->load->helper('form');
    $data['cv_category_list'] = $this->cv_category_list;
    
    $data['q'] = $q;
    $data['category'] = $category;
    $this->load->view('admin/cv-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        $action = $postData['action'];
        $this->load->model('cv_model');
        $ids = $postData['ids'];
        switch($postData['action']){
            case 'set_publ':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->cv_model->UpdateBatch('status',$data);
                $this->cv_model->resetStatus($changed);
                if($changed){
                    $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的新闻无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
                }
                
                break;
            case 'set_draf':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->cv_model->UpdateBatch('status',$data);
                break;
                break;
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->cv_model->UpdateBatch('display',$data);
                break;
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->cv_model->UpdateBatch('display',$data);
                break;
            case 'set_dele':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->cv_model->UpdateBatch('delete',$data);
                //$this->cv_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->cv_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  function add() {
    $this->load->model('cv_model');
    
    $this->load->library('form_builder',array('id'=>'post_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/post/create_new_post"'));

    $fields['title'] = array('size' => 25, 'label' => '标题', 'required' => TRUE);
    $fields['content'] = array('type' => 'textarea', 'label' => '正文', 'required' => FALSE);
    $fields['user_id'] = array('size' => 25, 'label' => '用户id', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['post_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/post-edit',$data);
  }  
  
  
  function view() {
    $this->load->model('cv_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    if( count($id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $posts = $this->cv_model->get_posts_for_user($user_id);
    
    $data['posts'] = $posts;
    $data['user'] = $user;
    $this->load->view('admin/post-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('cv_model');
      $saved = $this->cv_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/post/?message=success');
    }else{
        redirect('/admin/post/?message=failure');
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('cv_model');
      $saved = $this->cv_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('cv_model');
      $saved = $this->cv_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  
  function remove() {
    $id = $this->input->get('id');
    $ids = explode(",", $id);
    if( $id ) {
      $this->load->model('cv_model');
        for($i=0;$i<count($ids);$i++){
            $data[$ids[$i]]=1;
        }
        $this->cv_model->UpdateBatch('delete',$data);
    }
    
    $this->session->set_flashdata( 'message', array( 'title' => '删除成功', 'content' => '删除成功', 'type' => 'success' ));  
    redirect('admin/cv');
  }
  
  
    function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  function import(){
      
      $this->load->model('cv_model');
        function cmp($a, $b)
        {
            if ($a['ntime'] == $b['ntime']) {
                return 0;
            }
            return (strtotime($a['ntime']) > strtotime($b['ntime'])) ? 1 : -1;
        }

        include('tplx/cv.php');
      
        $lang = 'zh_CN';
        $list = $cv[$lang];
        //ksort($list);
        usort($list, "cmp");
        $i=1;
        foreach($list as $_n){
            $saveData = array();
            $saveData['title'] = $_n['title'];
            $saveData['content'] = $_n['content'];
            $saveData['createdate'] = date('Y-m-d',strtotime($_n['ntime']));
            $saveData['status'] = 1;
            $saveData['delete'] = 0;
            $saveData['lang'] = $lang;
            
            $saveData['order'] = ++$i;
            $id = $this->cv_model->Save($saveData);
        }
        
        
        $lang = 'en_US';
        $list = $cv[$lang];
        //ksort($list);
        usort($list, "cmp");
        $i=1;
        foreach($list as $_n){
            $saveData = array();
            $saveData['title'] = $_n['title'];
            $saveData['content'] = $_n['content'];
            $saveData['createdate'] = date('Y-m-d',strtotime($_n['ntime']));
            $saveData['status'] = 1;
            $saveData['delete'] = 0;
            $saveData['lang'] = $lang;
            
            $saveData['order'] = ++$i;
            $id = $this->cv_model->Save($saveData);
        }
                    
  }

}
