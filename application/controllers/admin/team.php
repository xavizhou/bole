<?php

class team extends MY_Controller{
    var $news_category_list;
  public function __construct()
  {
    parent::__construct();
    $this->data['team_loc_category'] = $this->config->item('team_loc_category');
    $this->data['team_fund_category'] = $this->config->item('team_fund_category');
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list() {
    $this->search();
  }
  
  function edit() {
    $data = $this->data;
    $this->load->model('team_model');
    $id = (int)$this->input->get('id');
//    $type = (int)$this->input->get('type');
//    $post = array(
//        'title'=>'',
//        'category'=>'',
//        'content'=>'',
//        'image'=>''
//    );
    $lang = $data['lang'];
    
    $post = $this->team_model->Get($id);
    $item = $this->team_model->GetItem($post['id'],$lang);
    
    if(is_array($item)):
        $post['title'] = $item['title'];
        $post['subtitle'] = $item['subtitle'];
        $post['excerpt'] = $item['excerpt'];
        $post['content'] = $item['content'];
    endif;
    
    if ($post) {
      $data['post'] = $post;
    }
      
    $this->load->helper('form');
    $this->load->library('form_validation');
    
    $this->load->view('admin/team-edit',$data);
  }
  
  function show_main() {
    $this->load->model('public_model');
    $post_id = $this->input->get('post_id');
    // Load all of the logged-in user's posts

    if( count($post_id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $post_id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $data['user'] = $user;
    $this->load->view('admin/team-detail',$data);
  }

  function save_batch(){
        $postData = $this->input->post(null, true);
        $sortOrder = $postData['SortOrder'];
        if($sortOrder == -1){
            redirect('admin/team');
        }
        $sortOrder = explode(",", $sortOrder);
        $this->load->model('team_model');
        $this->team_model->save_batch($sortOrder);
        $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        redirect('/admin/team/');
  }
  
  function save() {
        $postData = $this->input->post(null, true);
        $postData['content'] = $this->input->post('content', false);
        
        if(empty($postData['title'])){
            redirect('/admin/team/edit?id='.$saveData['id']);
        }
        
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png';
        $config['max_size'] = '2048';
        $config['max_width']  = '2048';
        $config['max_height']  = '1516';
        $config['encrypt_name']  = TRUE;
        

        $this->load->library('upload');
        $this->load->library('image_lib'); 
        
        $this->upload->initialize($config);
        if ($this->upload->do_upload('thumb_upload'))
        {
            $data = array('upload_data' => $this->upload->data());
            $postData['thumb'] = $data['upload_data']['file_name'];
            
            
            
//            //create grey background image
//            $src_im  = imagecreatefrompng('uploads/'.$data['upload_data']['file_name']);
//            $f = str_ireplace(".png", ".jpg", $data['upload_data']['file_name']);
//            if($src_im):
//                $transparentColor = imagecolorallocate($src_im, 255, 255, 255);
//                imagecolortransparent ( $src_im ,$transparentColor );
//                    
//                $im  = imagecreatetruecolor(190, 135);
//                $bgc = imagecolorallocate($im, 247, 247, 247);
//                imagefilledrectangle($im, 0, 0, 190, 135, $bgc);
//                imagecopy($im, $src_im, 0, 0, 0, 0, 190, 135); 
//                imagejpeg($im, 'uploads/c_'.$f);
//                imagedestroy($im);
//                
//                $im  = imagecreatetruecolor(190, 135);
//                imagefilledrectangle($im, 0, 0, 190, 135, $bgc);
//                imagecopymergegray($im, $src_im, 0, 0, 0, 0, 190, 135); 
//                imagejpeg($im, 'uploads/g_'.$f);
//                imagedestroy($im);
//            else:
//                copy('uploads/g_'.$f,'uploads/'.$data['upload_data']['file_name']);
//                copy('uploads/c_'.$f,'uploads/'.$data['upload_data']['file_name']);
//            endif;
//            die();
            
            
            
            
        }else{
            $error = array('error' => $this->upload->display_errors()); 
            //print_r($error);
        }
            
            
        for($i=0;$i<7;$i++){
            $this->upload->initialize($config);
            if ($this->upload->do_upload('image'.$i.'_upload'))
            {
                $data = array('upload_data' => $this->upload->data());
                $postData['image'.$i] = $data['upload_data']['file_name'];
                
                
                //创建缩略图
                $image_config["image_library"] = "gd2";
                $image_config["source_image"] = $config['upload_path'].$data['upload_data']['file_name'];
                $image_config['new_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
                $image_config['create_thumb'] = FALSE;
                $image_config['maintain_ratio'] = TRUE;
                $image_config['quality'] = "100%";
                $image_config['width'] = 182;
                $image_config['height'] = 124;
                $image_config['encrypt_name']  = TRUE;
                //print_r($image_config);
                list($width, $height, $type, $attr) = getimagesize($image_config["source_image"]);
                $dim = ($width / $height) - ($image_config['width'] / $image_config['height']);
                $image_config['master_dim'] = ($dim > 0)? "height" : "width";
            
                $this->image_lib->clear();
                $this->image_lib->initialize($image_config); 

                if ($this->image_lib->resize())
                {
                    $image_config['image_library'] = 'gd2';
                    $image_config['source_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
                    $image_config['new_image'] = $config['upload_path'].'thumb/'.$data['upload_data']['file_name'];
                    $image_config['quality'] = "100%";
                    $image_config['maintain_ratio'] = TRUE;
                    $image_config['width'] = 182;
                    $image_config['height'] = 124;
                    list($width, $height, $type, $attr) = getimagesize($image_config["source_image"]);
                    $image_config['x_axis'] = ($width-182)/2;
                    $image_config['y_axis'] = ($height-124)/2;

                    $this->image_lib->clear();
                    $this->image_lib->initialize($image_config); 
                    $this->image_lib->crop();
                }else{
                    
                    echo $this->image_lib->display_errors();;
                }
                
                
            }else{
                $error = array('error' => $this->upload->display_errors()); 
                //print_r($error);
            }
        }
        
        $this->load->model('team_model');
        
        if(empty($postData['title'])){
            redirect('/admin/team/edit?id='.$post['id']);
        }
        if(!$postData['order']){
            $order = $this->team_model->GetMaxOrder();
            $postData['order'] = $order+1;
        }
        $saveData = array();
        $data = $this->data;
        $saveData['lang'] = $data['lang'];
        $saveData['status'] = (int)$postData['status'];
        $saveData['display'] = (int)$postData['display'];
        
        $saveData['title'] = $postData['title'];
        $saveData['content'] = $postData['content'];
        
        $saveData['gender'] = $postData['gender'];
        //$saveData['loc'] = $postData['loc'];
        if($postData['loc']){
            $saveData['loc'] = implode(",", $postData['loc']);
        }
        if($postData['fund']){
            $saveData['fund'] = implode(",", $postData['fund']);
        }
        
        if(empty($postData['category'])) $postData['category'] = array();
        $saveData['category'] = implode(",", $postData['category']);
        $saveData['recommend'] = $postData['recommend'];
        
        $saveData['size'] = $postData['size'];
        $saveData['color'] = $postData['color'];
        $saveData['thumb'] = $postData['thumb'];
        
        $saveData['slug'] = $postData['slug'];
        
        $j=0;
        for($i=0;$i< 7 ;$i++){
            if(empty($postData['image'.$i])) continue;
            $saveData['image'.$j] = $postData['image'.$i];
            $j++;
        }
        for($i=$j;$i<7 ;$i++){
            $saveData['image'.$i] = '';
        }
        //$saveData['status'] = ($saveData['thumb'] && $saveData['image0'])?$postData['status']:0;
//        print_r($postData);
//        print_r($saveData);die();
                
        $saveData['url'] = $postData['url'];
        
        $saveData['order'] = $postData['order'];
                
        $id = $this->team_model->Save($saveData,$postData['id']);
        
        $saveData = array();
        $saveData['title'] = $postData['title'];
        $saveData['subtitle'] = $postData['subtitle'];
        $saveData['excerpt'] = $postData['excerpt'];
        $saveData['content'] = $postData['content'];
        
        $lang = $this->data['lang'];
        $this->team_model->SaveItem($saveData,$lang,$id);
        
//        if(empty($saveData['thumb']) && empty($saveData['image0']) && $postData['status']){
//            $this->session->set_flashdata( 'message', array( 'title' => '未上传缩略图和照片的无法发布', 'content' => '未上传缩略图和照片的新闻无法发布', 'type' => 'message' ));  
//            redirect('/admin/team/edit?id='.$id);
//        }else{
//            redirect('/admin/team/');
//        }
        
        if($id){
            $this->session->set_flashdata( 'message', array( 'title' => '保存成功', 'content' => '保存成功', 'type' => 'success' ));  
        }else{
            $this->session->set_flashdata( 'message', array( 'title' => '保存失败', 'content' => '保存失败', 'type' => 'message' ));  
        }
        //$this->session->set_flashdata( 'message', array( 'title' => '未上传缩略图和照片的无法发布', 'content' => '未上传缩略图和照片的新闻无法发布', 'type' => 'message' ));  
        redirect('/admin/team/edit?id='.$id);
        
  }
  function search(){
    $data = $this->data;
    $this->load->model('team_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $category = (int)$this->input->get('category');
    

    if($category) $condition[] = array('where','category',$category);
    if($q) $condition[] = array('like','title',$q);
    //$condition[] = array('where','lang',$data['lang']);
    $condition[] = array('where','delete',0);
    $condition[] = array('order','order','asc');
    
    $posts = $this->team_model->Search(0,999,$condition);
    $total = $this->team_model->GetCountSearch($condition);
    
    $lang = $this->data['lang'];
    
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->team_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
            endif;
        endfor;
        $data['posts'] = $posts;
    }
    $data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    
    
    $this->load->helper('form');
    $data['news_gender_list'] = $this->news_gender_list;
    $data['news_category_list'] = $this->news_category_list;
    $data['news_recommend_list'] = $this->news_recommend_list;
    
    $data['q'] = $q;
    $data['gender'] = $gender;
    $data['category'] = $category;
    $data['recommend'] = $recommend;
    $this->load->view('admin/team-list',$data);
  }
  function batch(){
        $postData = $this->input->post(null, true);
        $action = $postData['action'];
        $this->load->model('team_model');
        $ids = $postData['ids'];
        switch($postData['action']){
            case 'set_publ':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->team_model->UpdateBatch('status',$data);
                $this->team_model->resetStatus($changed);
                if($changed){
                    $this->session->set_flashdata( 'message', array( 'title' => '未上传照片的新闻无法发布', 'content' => '未上传照片的记录无法发布', 'type' => 'message' ));  
                }
                
                break;
            case 'set_draf':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->team_model->UpdateBatch('status',$data);
                break;
                break;
            case 'set_show':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->team_model->UpdateBatch('display',$data);
                break;
            case 'set_hide':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=0;
                }
                $this->team_model->UpdateBatch('display',$data);
                break;
            case 'set_dele':
                for($i=0;$i<count($ids);$i++){
                    $data[$ids[$i]]=1;
                }
                $this->team_model->UpdateBatch('delete',$data);
                //$this->team_model->RemoveById($ids);
                break;
            case 'set_orde':
                $this->team_model->UpdateOrder($postData['orders']);
                break;
        }
        redirect($_SERVER['HTTP_REFERER']);
  }

  function add() {
    $this->load->model('public_model');
    
    $this->load->library('form_builder',array('id'=>'post_form','submit_value' => '<input type="submit" class="btn btn-primary" value="提交" />','form_attrs'=>'method="post" action="'.site_url().'/admin/team/create_new_post"'));

    $fields['title'] = array('size' => 25, 'label' => '标题', 'required' => TRUE);
    $fields['content'] = array('type' => 'textarea', 'label' => '正文', 'required' => FALSE);
    $fields['user_id'] = array('size' => 25, 'label' => '用户id', 'required' => TRUE);

    $fields['active'] = array('type'=>'checkbox','name'=>'active','label' => '激活','value'=>ACTIVE_T, 'checked' => TRUE);
    $this->form_builder->set_fields($fields);
    
    $values['active'] = '1';
    $this->form_builder->set_field_values($values);
    
    $this->form_builder->show_required = TRUE;
    $this->form_builder->set_fields($fields);
    
    $data['post_new_form'] = $this->form_builder->render();
    
    $this->load->view('admin/team-edit',$data);
  }  
  
  
  function view() {
    $this->load->model('public_model');
    $id = $this->input->get('id');
    // Load all of the logged-in user's posts

    if( count($id) ) {
      $query = $this->db->get_where(TBL_POST, array('id' => $id), 1,0);
      if($query->num_rows() > 0){
          $post = $query->row_array();
      }
    }
    $data['post'] = $post;
    $user_id = $post['user_id'];
    if( count($user_id) ) {
      $query = $this->db->get_where(TBL_USER, array('id' => $user_id), 1,0);
      if($query->num_rows() > 0){
          $user = $query->row_array();
      }
    }
    
    $posts = $this->public_model->get_posts_for_user($user_id);
    
    $data['posts'] = $posts;
    $data['user'] = $user;
    $this->load->view('admin/team-detail',$data);
  }  
  
  

  function create_new_post() {
    $postData = $this->input->post(null,true);
    
    if( count($postData) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->create_new_post($postData);
    }

    if ( isset($saved) && $saved ) {
        redirect('/admin/team/?message=success');
    }else{
        redirect('/admin/team/?message=failure');
    }
  }

  
  function unactive() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->unactive($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  function active() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->active($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    $user_id = $this->input->get('user_id');
    if( count($id) ) {
      $this->load->model('public_model');
      $saved = $this->public_model->remove($id);
    }
    redirect('admin/post?user_id='.$user_id.'&message=success');
  }
  
    function get_post_tags($post_id,$user_id){
    $this->db->from(TBL_PTAG);
    $where = array('post_id' => $post_id,'user_id'=>$user_id);
    $this->db->where($where);
    $r = $this->db->get()->result_array();
    if ( is_array($r) && count($r) > 0) {
        return $r;
    }
    return false;
  }
  
  
  
  function import(){
      
      $this->load->model('team_model');
        include('tplx/team.php');
      
        $lang = 'zh_CN';
        $list = $team[$lang];
        //ksort($list);
        //usort($list, "cmp");
        $i=1;
        foreach($list as $k=>$_n){
            $saveData = array();
            $saveData['title'] = $_n['name'];
            $saveData['content'] = $_n['desc'];
            $saveData['subtitle'] = $_n['option'];
            $saveData['status'] = 1;
            $saveData['delete'] = 0;
            $saveData['lang'] = $lang;
            $r = md5(rand(10000,9999).$k);
            $saveData['thumb'] = $r.'.jpg';
            $saveData['image0'] = 'r-'.$r.'.jpg';
            copy(FCPATH.'assets/img/team/'.($k+1).'.jpg',FCPATH.'uploads/'.$r.'.jpg');
            copy(FCPATH.'assets/img/team/r-'.($k+1).'.jpg',FCPATH.'uploads/r_'.$r.'.jpg');
//            $saveData['image0'] = "g_$r.jpg";
//            $saveData['image1'] = "c_$r.jpg";
            $saveData['lang'] = $lang;
            
            $saveData['order'] = ++$i;
            $id = $this->team_model->Save($saveData);
            
            $saveData = array();
            $saveData['title'] = $_n['name'];
            $saveData['content'] = $_n['desc'];
            $saveData['subtitle'] = $_n['option'];
            $this->team_model->SaveItem($saveData,$lang,$id);
            
            
            $_n = $team['en_US'][$k];
            $saveData = array();
            $saveData['title'] = $_n['name'];
            $saveData['content'] = $_n['desc'];
            $saveData['subtitle'] = $_n['option'];
            $this->team_model->SaveItem($saveData,'en_US',$id);
        }
        
//        
//        $lang = 'en_US';
//        $list = $team[$lang];
//        //ksort($list);
//        //usort($list, "cmp");
//        $i=1;
//        foreach($list as $_n){
//            $saveData = array();
//            $saveData['title'] = $_n['name'];
//            $saveData['content'] = $_n['desc'];
//            $saveData['subtitle'] = $_n['option'];
//            $saveData['status'] = 1;
//            $saveData['delete'] = 0;
//            $saveData['lang'] = $lang;
//            $r = md5(rand(10000,9999));
//            $saveData['thumb'] = $r.'.jpg';
//            copy(FCPATH.'assets/img/team/'.$i.'.jpg',FCPATH.'uploads/'.$r.'.jpg');
////            $saveData['image0'] = "g_$r.jpg";
////            $saveData['image1'] = "c_$r.jpg";
//            $saveData['lang'] = $lang;
//            
//            $saveData['order'] = ++$i;
//            $id = $this->team_model->Save($saveData);
//            
//            $saveData = array();
//            $saveData['title'] = $_n['name'];
//            $saveData['content'] = $_n['desc'];
//            $saveData['subtitle'] = $_n['option'];
//            $this->team_model->SaveItem($saveData,$lang,$id);
//        }
                    
  }
  

}
