<?php

class ecode extends CI_Controller{

  public function __construct()
  {
    parent::__construct();

    if( !$this->session->userdata('isLoggedIn') ) {
        redirect('/admin/login/show_login');
    }
  }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->ecode_list();
  }

  function ecode_list(){
    $this->load->model('ecode_model');
    $page = (int)$this->input->get('page');
    $page = ($page)?$page:0;
    $ecodes = $this->ecode_model->get_ecode();
    if ($ecodes) {
      $data['ecodes'] = $ecodes;
    }
    $this->load->view('admin/ecode-list',$data);
  }
  
  
  function create_new() {
    $num =(int) $this->input->get('num',true);
    if( $num) {
      $this->load->model('ecode_model');
      $saved = $this->ecode_model->generate_ecode($num);
    }
    redirect('admin/ecode');
  }

  
  function unactive() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('ecode_model');
      $saved = $this->ecode_model->unactive($id);
    }
    redirect('admin/book?message=success');
  }
  function active() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('ecode_model');
      $saved = $this->ecode_model->active($id);
    }
    redirect('admin/book?message=success');
  }
  
  function remove() {
    $id = $this->input->get('id');
    if( count($id) ) {
      $this->load->model('ecode_model');
      $saved = $this->ecode_model->remove($id);
    }
    redirect('admin/book?message=success');
  }
}
