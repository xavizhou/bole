<?php
class consultant extends MY_User{
    private $city_list;
    private $industry_list;
    public function __construct()
    {
      parent::__construct();
        read('assets/city.json',$content);
        if($content):
            $content = json_decode($content,TRUE);
        endif;
    $this->industry_list = $this->config->item('industry_list');
    $this->data['industry_list'] = $this->industry_list;
        $this->city_list = $content;
        $this->data['city_list'] = $content;

    }

  /**
   * This is the controller method that drives the application.
   * After a user logs in, show_main() is called and the main
   * application screen is set up.
   */
  function index() {
      $this->show_list();
  }
  
  function show_list(){
      
    $data = $this->data;
    $this->load->model('consultant_model');
    $q = trim($this->input->get('q'));
    $page = (int)$this->input->get('page');
    $category = (int)$this->input->get('category');
    $city = trim($this->input->get('city'));
    $industry = trim($this->input->get('industry'));
    $star = (int)($this->input->get('star'));
    

    if($category) $condition[] = array('where','category',$category);
    
    if($city) $condition[] = array('like','loc',$city);
    if($industry) $condition[] = array('where','industry',$industry);
    if($star) $condition[] = array('where','status',2);
    
    if($q) $condition[] = array('where','((title LIKE "%'.$q.'%") OR (content LIKE "%'.$q.'%") OR (keywords LIKE "%'.$q.'%"))',NULL);
    
    $condition[] = array('where','delete',0);
    $condition[] = array('order','order','desc');
    
    $lang = $this->data['lang'];
    $posts = $this->consultant_model->Search(0,999,$condition);
    
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->consultant_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
                $posts[$i]['language_skill'] = $item['language_skill'];
            endif;
            
            $posts[$i]['thumb'] = 'consultant-'.($i%4+1).'.jpg';
            
        endfor;
        $data['posts'] = $posts;
    }
    
    //$data['total'] = $total;
    $data['page'] = $page;
    $data['per'] = PAGE_NUM;
    $data['url_para'] = array(
        'city'=>$city,
        'industry'=>$industry,
        'star'=>$star,
    );
    
    
    $this->load->helper('form');
    
    $data['q'] = $q;
    $data['category'] = $category;
    $this->load->view('consultants',$data);
  }
  
  
  function detail() {
    $data = $this->data;
    $this->load->model('consultant_model');
    $id = (int)$this->input->get('id');
   
    
    $lang = $data['lang'];
    $post = $this->consultant_model->Get($id);
    $item = $this->consultant_model->GetItem($post['id'],$lang);
    
    if(is_array($item)):
        $post['title'] = $item['title'];
        $post['subtitle'] = $item['subtitle'];
        $post['excerpt'] = $item['excerpt'];
        $post['content'] = $item['content'];
        $post['language_skill'] = $item['language_skill'];
    endif;
    
    
    if ($post) {
      $data['post'] = $post;
    }
    if ($post) {
      $data['type'] = $post['type'];
    }
    
    $this->load->view('consultant-detail',$data);
  }
  
  
  function ajax_star(){
    $data = $this->data;
    $this->load->model('consultant_model');
    $loc = trim($this->input->get('loc'));
    $condition[] = array('like','loc',$loc);
    $condition[] = array('where','delete',0);
    //$condition[] = array('where','status',2);
    $condition[] = array('order','status','desc');
    
    $lang = $this->data['lang'];
    $posts = $this->consultant_model->Search(0,4,$condition);
    if ($posts) {
        for($i=0;$i<count($posts);$i++):
            $item = $this->consultant_model->GetItem($posts[$i]['id'],$lang);
            if($item):
                $posts[$i]['title'] = $item['title'];
                $posts[$i]['subtitle'] = $item['subtitle'];
                $posts[$i]['excerpt'] = $item['excerpt'];
                $posts[$i]['content'] = $item['content'];
                $posts[$i]['language_skill'] = $item['language_skill'];
            endif;
            $posts[$i]['thumb'] = 'consultant-'.($i%4+1).'.jpg';
        endfor;
        
    }else{
        $posts = array();
    }
    echo json_encode($posts);
  }
  
  
  function import(){
      return;
    $this->load->model('consultant_model');
        $this->db->from('test');
        $r = $this->db->get()->result_array();
        $dataSet = array();
        for($i=0;$i<count($r);$i++):
            switch($r[$i]['LangId']):
            case 2:
                $lang = 'en_US';
                break;
            case 1:
                $lang = 'zh_CN';
                break;
            case 3:
                $lang = 'zh_TW';
                break;
            endswitch;
            if($dataSet[$r[$i]['Title']][$lang]):
                $dataSet[$r[$i]['Title']][$lang]['loc'][] = $r[$i]['CateId'];
            else:
                $dataSet[$r[$i]['Title']][$lang] = $r[$i];
                $dataSet[$r[$i]['Title']][$lang]['loc'] = array($r[$i]['CateId']);
            endif;
        endfor;
        
        $c = "1,2,Beijing,0,1,1
2,2,Chengdu,0,2,1
3,2,Chongqing,0,3,0
4,2,Dalian,0,4,1
5,2,Guangzhou,0,5,1
6,2,Shanghai,0,6,1
7,2,Shenzhen,0,7,1
8,2,Suzhou,0,8,1
9,2,Tianjin,0,9,1
10,2,Hong Kong,0,10,1
11,2,Taipei,0,11,1
12,2,Jakarta,0,12,1
13,2,Kuala Lumpur,0,13,1
14,2,Manila,0,14,1
15,2,Bangkok,0,15,1
16,2,Ho Chi Minh City,0,16,0";
        $_tmp = explode("\n", $c);
        $city = array();
        for($i=0;$i<count($_tmp);$i++):
            list($_id,$_l,$_city) = explode(",", $_tmp[$i]);
            $city[$_id] = $_city;
        endfor;
        
        foreach($dataSet as $k=>$v):
            $saveData = array();
            $saveData['lang'] = 'en_US';
            $vv = $v['en_US'];
            if(!$vv):
                $saveData['lang'] = 'zh_CN';
                $vv = $v['zh_CN'];
            endif;
            if(!$vv):
                $saveData['lang'] = 'zh_TW';
                $vv = $v['zh_TW'];
            endif;
            
            $saveData['status'] = 1;
            $saveData['display'] = 1;
            $saveData['title'] = $k;
            $saveData['subtitle'] = $vv['info'];
            $saveData['content'] = $vv['HtmlContent'];

            $saveData['gender'] = 0;
            $saveData['category'] = '';
            $saveData['recommend'] = '';


            $saveData['language_skill'] = '';
            $saveData['practice'] = '';

            $saveData['createdate'] = $vv['CreateTime'];
            
            $saveData['email'] = $vv['email'];
            $saveData['thumb'] = $vv['pic'];
            
            $loc2 = array();
            foreach($vv['loc'] as $kk):
                if($kk < 37):
                    $kk = ($kk - 1) % 16 + 1;
                elseif($kk < 48):
                    $kk = ($kk) % 16 + 1;
                else:
                    $kk = 5;
                endif;
                $loc2[] = $city[$kk];
            endforeach;
            $saveData['loc'] = implode(',', $loc2);

            $saveData['status'] = ($vv['IsTop'])?2:1;

            $saveData['order'] = $vv['OrderId'];

            $id = $this->consultant_model->Save($saveData,0);
        
            foreach($v as $lang=>$vv):
                $saveData = array();
                $saveData['title'] = $vv['Title'];
                $saveData['subtitle'] = $vv['info'];
                $saveData['content'] = $vv['HtmlContent'];

                $this->consultant_model->SaveItem($saveData,$lang,$id);
            endforeach;
            
        endforeach;
        
  }

}
