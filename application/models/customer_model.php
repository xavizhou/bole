<?php
class Customer_Model extends MY_Model {
    var $details;
    function __construct()
    {
        parent::__construct('customer');
    }
    
    function existing_user( $username ) {
        $this->db->from($this->tbname);
        $this->db->where('username',$username );
        $login = $this->db->get()->result();

        if ( is_array($login) && count($login) == 1 ) {
            return $login[0];
        }
        return false;
    }
    function existing_email( $email ) {
        $this->db->from($this->tbname);
        $this->db->where('email',$email );
        $login = $this->db->get()->result_array();

        if ( is_array($login) && count($login) > 0 ) {
            return $login[0];
        }
        return false;
    }
    
    function getByRange($s,$e){
        $s = strtotime($s);
        if($s){
            $this->db->where('createdate >= ', date('Y-m-d H:i:s',$s));
        }
        $e = strtotime($e);
        if($e){
            $e = mktime(0,0,0,date('m',$e),date('d',$e)+1,date('Y',$e));
            $this->db->where('createdate < ', date('Y-m-d H:i:s',$e));
        }
        $this->db->from($this->tbname);
        $r = $this->db->get()->result_array();
        
        return $r;
    }
    
    function validate_user_auto($email){
        $this->db->from($this->tbname);
        $this->db->where('email',$email );
        //$this->db->where('password', md5($password) );
        $login = $this->db->get()->result();
        if ( is_array($login) && count($login) > 0 ) {
            $this->details = $login[0];
            $this->set_session();
            return (array)$login[0];
        }
    }
    
    function validate_user( $email, $password ) {
        $this->db->from($this->tbname);
        //$this->db->where('email',$email );
        $this->db->where('username',$email );
        $this->db->where('password', md5($password) );
        $login = $this->db->get()->result();
        if ( is_array($login) && count($login) > 0 ) {
            $this->details = $login[0];
            $this->set_session();
            return (array)$login[0];
        }

        return false;
    }
    
    function get_by_condition($q, &$total, $month = 0, $start = 0, $num = 0){
        if($start && $num){
            $limit = " LIMIT $start , $num";
        }
        if($month){
            $ext = "AND
                (
                (B.to <>'2038-01-01' AND (PERIOD_DIFF(DATE_FORMAT(B.to,'%Y%m'),DATE_FORMAT(B.from,'%Y%m')) > $month ))
                OR 
                (B.to ='2038-01-01' AND (PERIOD_DIFF(DATE_FORMAT(NOW(),'%Y%m'),DATE_FORMAT(B.from,'%Y%m')) > $month ))
                )";
        }
        $sql = "SELECT COUNT(DISTINCT A.id) AS AA FROM 
                ".$this->db->dbprefix('customer')." AS A
                LEFT JOIN ".$this->db->dbprefix('customer_employment')." AS B ON A.id = B.customer_id
                LEFT JOIN ".$this->db->dbprefix('customer_education')." AS C ON A.id = C.customer_id
                LEFT JOIN ".$this->db->dbprefix('customer_awards')." AS D ON A.id = D.customer_id
                WHERE (A.name LIKE '%$q%' 
                    OR A.firstname LIKE '%$q%' 
                    OR A.middlename LIKE '%$q%' 
                    OR A.surname LIKE '%$q%' 
                    OR A.email LIKE '%$q%' 
                    OR A.jobtitle LIKE '%$q%' 
                    OR A.company LIKE '%$q%' 
                    OR A.cv_content LIKE '%$q%' 
                    OR B.employer LIKE '%$q%' 
                    OR B.jobtitle LIKE '%$q%' 
                    OR C.school LIKE '%$q%' 
                    OR C.program LIKE '%$q%' 
                    OR D.award LIKE '%$q%')
                        $ext
                ";
        
        $total = $this->db->query($sql)->result_array();
        $total = $total[0]['AA'];
        $sql = "SELECT DISTINCT A.* FROM 
                ".$this->db->dbprefix('customer')." AS A
                LEFT JOIN ".$this->db->dbprefix('customer_employment')." AS B ON A.id = B.customer_id
                LEFT JOIN ".$this->db->dbprefix('customer_education')." AS C ON A.id = C.customer_id
                LEFT JOIN ".$this->db->dbprefix('customer_awards')." AS D ON A.id = D.customer_id
                WHERE (A.name LIKE '%$q%' 
                    OR A.firstname LIKE '%$q%' 
                    OR A.middlename LIKE '%$q%' 
                    OR A.surname LIKE '%$q%' 
                    OR A.email LIKE '%$q%' 
                    OR A.jobtitle LIKE '%$q%' 
                    OR A.company LIKE '%$q%' 
                    OR A.cv_content LIKE '%$q%' 
                    OR B.employer LIKE '%$q%' 
                    OR B.jobtitle LIKE '%$q%' 
                    OR C.school LIKE '%$q%' 
                    OR C.program LIKE '%$q%' 
                    OR D.award LIKE '%$q%')
                        $ext
                ".$limit;
        
        return $this->db->query($sql)->result_array();
        
    }

    function set_session() {
        $this->session->set_userdata( array(
            'id'=>$this->details->id,
            'rooms'=>($this->details->room)?explode(",", $this->details->room):array(),
            'isAdmin'=>0,
            'isLoggedIn'=>true,
            )
        );
    }

    function set_last_login($id){
            $data = array('last_login'=>  GetCurrentTime());
         $this->db->where('id', $id);
         $r = $this->db->update($this->tbname, $data);  
    }
    
    
}
