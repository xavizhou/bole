<?php
class Job_Model extends MY_Model {
    function __construct()
    {
        parent::__construct('jobs');
    }
    
    function list_jobs(&$total,$condition=array(),$limit = 20,$offset=0){
        $this->db->from($this->tbname);
        $valid_con = array('location','type','discipline','industry');
        foreach($valid_con as $v):
            if($condition[$v]) $this->db->where($v,$condition[$v]);
        endforeach;
        
        $valid_con = array('title','keywords','content');
        foreach($valid_con as $v):
            if($condition[$v]) $this->db->like($v,$condition[$v]);
        endforeach;
        $total = $this->db->count_all_results();

        $this->db->from($this->tbname);
        $valid_con = array('location','type','discipline','industry');
        foreach($valid_con as $v):
            if($condition[$v]) $this->db->where($v,$condition[$v]);
        endforeach;
        
//        $valid_con = array('title','keywords');
//        foreach($valid_con as $v):
//            if($condition[$v]) $this->db->like($v,$condition[$v]);
//        endforeach;
        $this->db->limit($limit,$offset);
        
        $r = $this->db->get()->result_array();
        return $r;
    }
    
    function add_job(){
        $data['title'] = $jobData['title'];
        $data['createdate'] = GetCurrentTime();
        $data['reference'] = $jobData['reference'];
        $data['excerpt'] = $jobData['excerpt'];
        $data['content'] = $jobData['content'];
        $data['keywords'] = $jobData['keywords'];
        $data['company'] = $jobData['company'];
        
        $data['location'] = $jobData['location'];
        $data['industry'] = (int)$jobData['industry'];
        $data['type'] = (int) $jobData['type'];
        
        $data['status'] = (int) $jobData['status'];
        
        return $this->db->insert('jobs',$data);
    }
    
    function disable_job_by_id($id){
        $data = array('status' => 0);
        $this->db->where('id', $id);
        $this->db->update('jobs', $data); 
        return $r;
    }
    function enable_job_by_id($id){
        $data = array('status' => 1);
        $this->db->where('id', $id);
        $this->db->update('jobs', $data); 
        return $r;
    }
}
