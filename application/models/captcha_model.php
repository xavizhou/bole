<?php
class Captcha_Model extends MY_Model {
    
    function __construct()
    {
        parent::__construct('captcha');
    }
    
    function check_captcha($captcha){
        // First, delete old captchas
        $expiration = time()-7200; // Two hour limit
        $this->db->query("DELETE FROM ".$this->db->dbprefix($this->tbname)." WHERE `captcha_time` < ".$expiration);	

        // Then see if a captcha exists:
//        $sql = "SELECT COUNT(*) AS count FROM ".$this->db->dbprefix($this->tbname)." WHERE word = ? AND ip_address = ? AND captcha_time > ?";
//
//        $binds = array($captcha, $this->input->ip_address(), $expiration);
        
        
        $sql = "SELECT COUNT(*) AS count FROM ".$this->db->dbprefix($this->tbname)." WHERE word = ?  AND captcha_time > ?";
        $binds = array($captcha, $expiration);
        
        
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0)
        {
            return false;
        }
        return true;
    }
    
    function create_captcha(){
//        $this->load->helper('recaptchalib');
//        
//        return ;
        //captcha
        $this->load->helper('captcha');
        $vals = array(
        'word'	 => '',
        'word_length' => 4,
        'font_size' => 40,
        'img_path'	 => './captcha/',
        'img_url'	 => base_url().'/captcha/',
        'font_path'	 => base_url().'/assets/fonts/HelveticaNeueLTPro-Th.ttf',
        'img_width'	 => 100,
        'img_height' => 40,
        'expiration' => 7200
        );
        $cap = create_captcha($vals);
        
        $data = array(
            'captcha_time'	=> $cap['time'],
            'ip_address'	=> $this->input->ip_address(),
            'word'	 => $cap['word']
        );

        $query = $this->db->insert_string('captcha', $data);
        $this->db->query($query);
        
        
        return $cap;
        
        
    }
}
