<?php
class Portfolio_Model extends MY_Table {
    function __construct()
    {
        parent::__construct('portfolio');
    }
    
    function GetPrev($order){
        $r = $this->db->order_by('order', 'desc')->get_where($this->tbname, array('order <' => $order,'status' => 1,'display' => 1), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
    function GetNext($order){
        $r = $this->db->order_by('order', 'asc')->get_where($this->tbname, array('order >' => $order,'status' => 1,'display' => 1), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
        
}
