<?php
class Post_Model extends MY_Model {
    function __construct()
    {
        parent::__construct('post');
    }
    
    
	
    function resetStatus(&$changed)
    {
        
        $r = $this->db->get_where($this->tbname, array('((thumb = "") OR (thumb IS NULL))' => NULL,'status'=>1))->result_array();
        
        if( is_array($r) && (count($r)>0)) {
          $changed = true;
        }else{
           $changed = false;
          return true;
        }
        
        $data = array('status'=>0);
         $this->db->where('thumb', '');
         $this->db->or_where('thumb IS NULL', NULL);
         $r = $this->db->update($this->tbname, $data);    
         return $r;
    }
    
    function GetPrev($order,$category){
        $r = $this->db->order_by('order', 'desc')->get_where($this->tbname, array('order <' => $order,'category' => $category,'status' => 1,'display' => 1,'delete' => 0), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
    function GetNext($order,$category){
        $r = $this->db->order_by('order', 'asc')->get_where($this->tbname, array('order >' => $order,'category' => $category,'status' => 1,'display' => 1,'delete' => 0), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
        
}
