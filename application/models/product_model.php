<?php
class Product_Model extends MY_Model {
    function __construct()
    {
        parent::__construct('product');
    }
    function resetStatus(&$changed)
    {
        
        $r = $this->db->get_where($this->tbname, array('image0' => '','status'=>1))->result_array();
        if( is_array($r) && (count($r)>0)) {
          $changed = true;
        }else{
           $changed = false;
          return true;
        }
        
         $data = array('status'=>0);
         $this->db->where('image0', '');
         $this->db->or_where('image0 IS NULL', NULL);
         $r = $this->db->update($this->tbname, $data);    
         return $r;
    }
}
