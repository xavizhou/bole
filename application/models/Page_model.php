<?php
class Page_Model extends MY_Model {
    function __construct()
    {
        parent::__construct('page');
    }
    
    function GetBySlug($slug,$lang)
    {
        //$this->db->from($this->tbname);
        $r = $this->db->get_where($this->tbname, array('slug' => $slug,'lang'=>$lang), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
}
