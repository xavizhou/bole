<?php
class Email_Model extends CI_Model {
    function sendmail($data,&$error) {
        //return true;
        $this->load->library('email');
        //$this->email->from($data['from'], $data['from_name']);
        $this->email->from(SYS_MAIL);
        $this->email->to($data['to']); 
        if($data['cc']){
            $this->email->cc($data['cc']); 
        }
        $this->email->subject($data['subject']);
        $this->email->message($data['message']);
        
        if($this->email->send()==true){
            return true;
        }
        else{
            return false;
        }

        return true;
    }
    
}
