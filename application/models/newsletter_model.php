<?php
class Newsletter_Model extends MY_Model {
    function __construct()
    {
        parent::__construct('newsletter');
    }
    function existing_email( $email ) {
        $this->db->from($this->tbname);
        $this->db->where('email',$email );
        $login = $this->db->get()->result_array();

        if ( is_array($login) && count($login) > 0 ) {
            return $login[0];
        }
        return false;
    }
}
