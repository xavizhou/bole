<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * XAVI's TOOL
 * http://www.getfuelcms.com
 *
 * An open source Content Management System based on the 
 * Codeigniter framework (http://codeigniter.com)
 *
 */


if (!defined('_COMM_FUNC_PHP'))
{
	define('_COMM_FUNC_PHP', 1);

	/*
	* Get current time of server
	*/
	function GetCurrentTime()
	{
		return Date('Y-m-d H:i:s');
	}

	/*
	* Get current time by GMT format
	*/
	function GetServerGmtTime()
	{
		return Date('Y-m-d H:i:s');// Mon, 6 Aug 20001 19:55:08 GMT
	}

	/*
	* Get Server Type
	*/
	function GetServerType()
	{
		return $_SERVER['SERVER_SOFTWARE']; // TODO
	}
	/*
	* Get remote IP
	*/
	function GetRemoteIP()
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		if (empty($ip))
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			if ( empty ($ip) )
			{
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		}
		return $ip;
	}

	// alias name of GetRemoteIP()
	function GetClientIP()
	{
		return GetRemoteIP();
	}
	/*
	* Calculate the difference between two dates
	*/
	function DateDiff($date1, $date2)
	{
		$diff = GetDays($date1) - GetDays($date2);
		return ($diff >= 0 ? $diff : -$diff);
	}
	
	/*Get Date*/	
	function GetYMD($datetime)
	{
		$timestamp = strtotime ($datetime);
		return ($timestamp)?$today = date("Y-m-d",$timestamp):$today = date("Y-m-d");
	}

	/*
	* Get the number of days
	* return days
	*/
	function GetDays($date)
	{
		$days = (int)((strtotime($date) / 3600) / 24);
		//echo "days = $days<br/>";// for test
		return $days;
	}

	/*
	* MUST check every variable before using
	*/
	function IsVariable($var)
	{
		if ( preg_match('/^[a-zA-Z0-9\_]+$/', $var) )
		{
			return true;
		} else {
			return false;
		}
	}

	function IsExistFile($filename,$path='')
	{
		if(empty($filename))	return false;
		if(!file_exists($path.$filename)) return false;
		return $path.$filename;
	}

	//function getImage($filename,$path='',$para='',$size='tiny')
	function getImage($filename,$path='',$arg=array())
	{
		if(empty($filename)) return false;
		$default = array(
		'title'=>'',
		'alt'=>'',
		'show_default'=>false,
		'size'=>'tiny'
		);
		$arg = array_merge($default, $arg);
		
		if(($img =IsExistFile($filename,$path))==false){
			if($show_default == false) return false;
			$img =  RP.'images/missing.png';
		}
                list($width, $height, $type, $attr) = getimagesize($img);

                if($arg['width'] && !$arg['height'] ){
                        $arg['height'] = $arg['width']/$width*$height;
                }
                if(!$arg['width'] && $arg['height'] ){
                        $arg['width'] = $arg['height']/$height*$width;
                }
                if(!$arg['width'] && !$arg['height'] ){
                        $arg['width'] = $width;
                        $arg['height'] = $height;
                }
    	
		if(is_array($arg)){
			foreach($arg as $k=>$v){
				if($k=='show_default' || $k=='size') continue;
				$s .= "$k='$v' ";
			}
		}
		
		return "<img src=\"$img?".rand()."\" $s />";
	}

	function IsImage($filename,$path='',$size='tiny')
	{
		if(IsExistFile($filename,$path)==false){
			switch($size){
				case 'large':
					$img = 'images/missing_800.gif';
					break;
				case 'mid':
					$img = 'images/missing_400.gif';
					break;
				case 'small':
					$img = 'images/missing_180.gif';
					break;
				case 'tiny':
				default:
					$img = 'images/missing_90.gif';
					break;
			}
			return RP.$img;
		}
		return $path.urlencode($filename);
	}

	function IsEmail($var)
	{
		if ( preg_match('/^[\w\-\.]+@([\w\-\.]+[\.])+[\w\-\.]+$/', $var))
		{
			return true;
		} else {
			return false;
		}
	}

	/* check whether it is decimal or not */
	function IsNumerics($var)
	{
		if ( preg_match('/^[0-9\.]+$/', $var) )
		{
			return true;
		} else {
			return false;
		}
	}

	/* check whether it is alphabet or not */
	function IsAlpha($var)
	{
		if ( preg_match('/^[a-zA-Z]+$/', $var) )
		{
			return true;
		} else {
			return false;
		}
	}

	/* check whether it is alphabet or not */
	function IsUsername($var,$length=6)
	{
            if(strlen($var) < $length ){
                return false;
            }
            if ( preg_match('/^[a-zA-Z0-9]+$/', $var) )
            {
                    return true;
            } else {
                    return false;
            }
	}
	/*
	* check whether it is url or not
	*/
	function IsUrl($url)
	{
		if ( preg_match('/^http\:\/\/[0-9a-z\.\?#&]+/i', $url) )
		{
			return true;
		} else {
			return false;
		}
	}
	/*
	* check whether it is phone(xxx-xxxxxxxx or xxxxxxxx) or not
	*/
	function IsPhone($str)
	{
		if ( preg_match('/^(\d{2}(-|\s))?(\d{2,3}(-|\s))?\d{8}((-|\s|[*])\d{1,})?$/i', $str) )
		{
			return true;
		} else {
			return false;
		}
	}
	/*
	* check whether it is mobile(13xxxxxxxxx or 15xxxxxxxxx) or not
	*/
	function IsMobile($str)
	{
		if ( preg_match('/^([1][3|5])\d{9}$/i', $str) )
		{
			return true;
		} else {
			return false;
		}
	}

	function TransformVariableRemoveQuotes($var,$quote = 0)
	{
		if (get_magic_quotes_gpc()) {
			switch($quote){
				case 0:
					$var = stripslashes($var);
					break;

				case 1:
					$var = str_replace("\'","'",$var);
					break;

				case 2:
					$var = str_replace('\"','"',$var);
					break;
			}
		}
		return $var;
	}
	/*
	* variables from the web form should be transformed
	*/
	function TransformVariable($var, $bConvertHtml = true)
	{
		$var = trim($var);
		if ($bConvertHtml)
		{
			$var = TransformHtmlTag($var);
		}

		/* check the switch of magic_quotes_gps */
		if (!get_magic_quotes_gpc())
		{
			$var = addslashes($var);
		}
		return $var;
	}	
	
	function TransformSlug($var)
	{
		global $_CONFIG;
		$var = trim($var);
		
		$patterns = array_keys($_CONFIG['TRANSFORM']['SLUG']);
		$replacements = array_values($_CONFIG['TRANSFORM']['SLUG']);
		for($i=0;$i<count($patterns);$i++){
			$patterns[$i] = '/'.$patterns[$i].'/';
		}
		$var = preg_replace($patterns, $replacements, $var);				
		$var = preg_replace('/[\s|(|)]+/', '_', $var);
		$var = preg_replace('/[^\w-]+/', '', $var);
		$var = preg_replace('/[_+]+/', '_', $var);
		$var = preg_replace('/^_|_$/', '', $var);
		return $var;
	}

	function TransformForTextArea( $var )
	{
		$var = str_replace('&', '&amp;', $var);
		$var = str_replace('<', '&lt;', $var);
		$var = str_replace('>', '&gt;', $var);
		$var = preg_replace('/([\r\n]+)([ ]+)/e',
		"'\\1'. str_replace(' ', '&nbsp;', '\\2')", $var);
		return $var;
	}

	function TransformForJavascript($var)
	{
		$var = str_replace("\r", '&#13;', $var);
		$var = str_replace("\n", '&#10;', $var);
		$var = str_replace('"', '\\"', $var);
		$var = str_replace("'", "\\'", $var);
		return $var;
	}

	function TransformHtmlTag( $var )
	{
		$var = str_replace('&', '&amp;', $var);
		$var = str_replace('<', '&lt;', $var);
		$var = str_replace('>', '&gt;', $var);
		//$var = str_replace(' ', '&nbsp;', $var);
		$var = preg_replace('/([\r\n]+)([ ]+)/e',
		"'\\1'. str_replace(' ', '&nbsp;', '\\2')", $var);
		$var = preg_replace('/([\r\n]+)/', '<br/>\1', $var);
		$var = str_replace('"', '&quot;', $var);
		return $var;
	}

	function ITransformVariable($var)
	{
		$var = str_replace('&nbsp;', ' ', $var);
		$var = str_replace('&amp;', '&', $var);
		return $var;
	}

	function TransforFlash( $var )
	{
		$var = str_replace('<strong>', '<b>', $var);
		$var = str_replace('</strong>', '</b>', $var);
		$var = str_replace('<em>', '<i>', $var);
		$var = str_replace('</em>', '</i>', $var);
		$var = str_replace('<span style="text-decoration: line-through;">',  '<span class="fla_linethrough">', $var);
		$var = str_replace('<span style="text-decoration: underline;">',  '<span class="fla_underline">', $var);
		$var = preg_replace('/<span style="color: ([\W|\w]+);">([\W|\w]+?)<\/span>/i',  "<font color='$1'>$2</font>", $var);
		return $var;

	}

	/*
	* Truncate the string
	*/
	function TruncateString($str, $num)
	{
		$str = ITransformVariable(strip_tags($str));
		if (strlen($str) <= $num) return $str;

		$i = 0;
		$count = 0;
		while ($i < $num)
		{
			if ( ord($str{$i}) >= 128 )
			{
				$count ++;
			}
			$i ++;
		}

		if ($num < strlen($str)) $tailing = " ...";
		/* 3 bytes = 1 character */
		if ( $count % 3 == 1 )
		{
			return substr($str, 0, $num + 2) . $tailing;
		} else if ( $count % 3 == 2 ) {
			return substr($str, 0, $num + 1) . $tailing;
		} else {
			return substr($str, 0, $num) . $tailing;
		}
	}

	/* Truncate the string of array */
	function TruncateArray($array, $index, $num)
	{
		for ($i  = 0; $i < sizeof( $array ) ; $i++)
		{
			$array[$i][$index] = TruncateString($array[$i][$index], $num);
		}
		return $array;
	}

	function GetExtName($filename)
	{
		$pattern = '/.*(\..+?)$/';
		if ( preg_match($pattern, $filename) )
		{
			return preg_replace($pattern, '$1', $filename);
		} else {
			return '';
		}
	}

	/*
	* Error handle
	*   Redirect to the error page
	*/
	function ErrorRedirect($strError, $bid)
	{
		$strError = urlencode("Error: $strError");
		header("Location: error.php?error=$strError&BlogID=$bid");
	}

	/*
	* Success handle
	*   Redirect to the success page
	*/
	function SuccessRedirect($strSuccess, $bid)
	{
		$strSuccess = urlencode("Success: $strSuccess");
		header("Location: success.php?success=$strSuccess&BlogID=$bid");
	}


	/*
	* Restore html special characters
	*/
	function RestoreHtmlSpecialChar($str)
	{
		$str = str_replace('&apos;', "'", $str);
		$str = str_replace('&pos;', '"', $str);
		$str = str_replace('&lt;', '<', $str);
		$str = str_replace('&gt;', '>', $str);
		$str = str_replace('&quot;', '"', $str);
		$str = str_replace('&amp;', '&', $str);
		//$str = str_replace('&nbsp;', ' ', $str);
		$str = addslashes($str);
		return $str;
	}

	/*
	* Convert html special characters
	*/
	function ConvertHtmlSpecialChar($str)
	{
		$str = str_replace('&', '&amp;', $str);
		$str = str_replace('<', '&lt;', $str);
		$str = str_replace('>', '&gt;', $str);
		return $str;
	}

	function Quote($str)
	{
		return str_replace("'", "''", $str);
	}

	/*
	* Read from the post raw data
	*/
	function &ReadRawPostData()
	{
		$ph = fopen('php://input', 'rb');
		if (!$ph) return '';
		while (!feof($ph))
		{
			$post .= fread($ph, 1024);
		}
		fclose($ph);
		return $post;
	}

	function GetWeekDay($day)
	{
		$table = array('Sunday', 'Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');

		return $table[$day];
	}

	function GetCurrentUrl()
	{
		return "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	}

	function Int2Color($number){
		$number = intval($number);
		$r = $number>>16;
		$g = ($number-($r<<16))>>8;
		$b = $number-($r<<16)-($g<<8);
		return array('r'=>$r,'g'=>$g,'b'=>$b);
	}
	function Color2Int($r,$g,$b){
		$r = intval($r);
		$g = intval($g);
		$b = intval($b);
		return ($r<<16)+($g<<8)+$b;
	}
	function FilterText($str, $slash, $tag, $property)
	{
		$tag = strtolower($tag);
		if ($tag == "a")
		{
			$property = str_replace('"', "'", $property);
			if ($slash == "/")
			{
				return "</b><${slash}a$property>";
			} else {
				return "<${slash}a$property><b>";
			}
		} else if($tag == "b" || $tag == "u" || $tag == "i" || $tag == "p")
		{
			return $str;
		} else if ($tag == "strong")
		{
			return "<${slash}b>";
		}
		return "";
	}

	function ConvertForFlash($var)
	{
		$var = preg_replace('/\<([\/]?)([\w]+)([^>]*?)\>/ies',
		'FilterText("\0", "\1", "\2", "\3");', $var);
		return $var;
	}

	function CSVFile($filename, $data)
	{
		$fp = fopen($filename, 'w+');
		fputcsv2($fp, $data);
		fclose($fp);
	}

	function fputcsv2($filePointer,$dataArray,$delimiter=",",$enclosure="\"")
	{
		// Write a line to a file
		// $filePointer = the file resource to write to
		// $dataArray = the data to write out
		// $delimeter = the field separator

		// Build the string
		$string = "";

		// for each array element, which represents a line in the csv file...
		foreach($dataArray as $line)
		{
			// No leading delimiter
			$writeDelimiter = FALSE;

			foreach($line as $dataElement){
				// Replaces a double quote with two double quotes
				$dataElement=str_replace("\"", "\"\"", $dataElement);

				// Adds a delimiter before each field (except the first)
				if($writeDelimiter) $string .= $delimiter;

				// Encloses each field with $enclosure and adds it to the string
				$string .= $enclosure . $dataElement . $enclosure;

				// Delimiters are used every time except the first.
				$writeDelimiter = TRUE;
			}
			// Append new line
			$string .= "\n";

		} // end foreach($dataArray as $line)

		// Write the string to the file
		fwrite($filePointer,$string);
	}
	function _url($array,$current,$page,$html=false){
		return changeURL($array,$current,$page,$html);
	}
	function changeURL($array,$current,$page,$html=false){
		if(is_array($array)){
			foreach($array as $k=>$v){
				if($v===null){
					unset($current[$k]);
				}else{
					$current[$k] = $v;
				}
			}
		}
		($html)?$_link = '&amp;':$_link = '&';
		foreach($current as $k=>$v){
		(empty($url))? $url = "?$k=$v":$url .= $_link."$k=$v";
		}
		return $page.$url;
	}

	function getMessageContent($message){
		switch($message){
			case 'successful':
				return 'Save successful!';
				break;
			case 'error':
				return 'Save failure!';
				break;
			default:
				return '';
		}
	}
	
	function microtime_float()
	{
	    list($usec, $sec) = explode(" ", microtime());
	    return ((float)$usec + (float)$sec);
	}
	
	
	function create_form_text($id,$name,$title,$value,$type,$attr=array()){
		$default=array('class'=>'','before'=>"\t\t<br />",'between'=>'<br />','after'=>'<br /><br />');
		$arg = array_merge($default,$attr);
		extract($arg);
		$value = str_replace("'","&#039",$value);
		$tmp = "$before";
		if($title) $tmp.="<label for='$id' class='$class'>$title</label>$between";
		$tmp .= "<input name='$name' id='$id' type='$type' class='$class' value='$value' />$after";
		 return $tmp;
	}
	
	
	function create_form_textarea($id,$name,$title,$value,$attr=array()){
		$default=array('class'=>'','before'=>"\t\t<br />",'between'=>'<br />','after'=>'<br /><br />','wysiwyg'=>true,'flash_text_editor'=>false);
		$arg = array_merge($default,$attr);
		extract($arg);
		($required)?$label_class='required':$label_class='';
		if($wysiwyg){
			$class .= ' mceEditor';
			if($flash_text_editor){
				//$class .= ' mceEditor';
			}else{
				//$after = $between."<span class='edit_rich' textid='$id'><a href='javascript:void(0)'>edit in RichEditor</a></span><br />".$after;
			}
		}
		$tmp = "$before<label for='$id' class='$class'>$title</label>
				$between
				<textarea name='$name' id='$id' type='$type' class='$class'>$value</textarea>
				$after";
		 return $tmp;
	}
	
	function create_form_checkbox($id,$name,$title,$value,$attr=array()){
		$default=array('class'=>'','before'=>"\t\t<br />",'between'=>'<br />','after'=>'<br /><br />','checked'=>false);
		$arg = array_merge($default,$attr);
		extract($arg);
		($checked)?$checked = 'checked':$checked='';
		$tmp = "$before<label for='$id' class='$class'>$title</label>
				$between
				<input name='$name' id='$id' type='checkbox' class='$class' value='$value' $checked />{$attr['text']}
				$after";
		 return $tmp;
	}
	
	/*
	filetype:image/video/document
	*/
	function create_form_file($id,$name,$title,$value,$attr=array()){
		$default=array(
		'class'=>'',
		'before'=>"\t\t<br />",
		'between'=>'<br />',
		'after'=>'<br /><br />',
		'show_text'=>true,
		'show_thumb'=>true,
		'multi'=>false,
		'filepath'=>'',
		'filetype'=>'');
		
		$arg = array_merge($default,$attr);
		extract($arg);
		
		$tmp = "$before<label for='$id' class='$class'>$title</label>
				$between";
		
		switch ($filetype){
			case 'video':
			case 'document':
				break;
			default:
				$filetype = 'image';
				break;
		}
		($multi)?$func = "create_form_file_".$filetype."_multi":$func="create_form_file_$filetype";
		
		$attr = array(		
			'class'=>$class,
			'before'=>$before,
			'between'=>$between,
			'after'=>$after,
			'show_text'=>$show_text,
			'show_thumb'=>$show_thumb,
			'show_url'=>$show_url,
			'label'=>$label,
			'filepath'=>$filepath
		);
		$tmp .= call_user_func_array($func, array($id,$name,$title,$value,$attr));
		$tmp .= "$after";
		return $tmp;
	}
	
	function create_form_file_image($id,$name,$title,$value,$attr){
		extract($attr);
		$tmp = "<div id='image_container_$name'>";
		$tmp .= getImage($value,$filepath,$attr);
		
		$tmp .= "$between
		<input name='{$name}_file' type='file' class='$class' id='{$name}_file'>
		<input name='$name' type='hidden' id='$name' value='$value' />"; 
		
		if($value){
                    $tmp .= "<a href='javascript:imgDel(\"$name\")'><span class='ui-icon ui-icon-closethick'></span>"._('Delete Image')."</a></div>";
                }
		
		return $tmp;
	}	
	
	function create_form_file_image_multi($id,$name,$title,$value,$attr){
		global  $_CONFIG;
		extract($attr);
		if(is_array($value)){
			$values=$value;
		}elseif(empty($value)){
			$values = array();
		}else{
			$values = array($value);
		}
		for($i=0;$i<count($values);$i++){
			$tmp .= "<div class='image_container'>";
			$value = $values[$i];
			$text = $value['text'];
			$thumb = $value['thumb'];
			$image = $value['file'];
			if($show_thumb){
				$tmp .= getImage($thumb,$filepath,$attr);
				$tmp .= $between;
				if($_CONFIG['max_uploads_unlimit'] ) $tmp .= "<input name='{$name}_thumb_file[]' type='file' value='$thumb' class='$class' id='{$name}_thumb_file[$i]'>"._('Thumbnail').$between;
				$tmp .= "<input name='{$name}_thumb[]' type='hidden' id='{$name}_thumb[$i]' value='$thumb' />";	
			}
			$tmp .= "<a href='$filepath$image' class='lightbox'>".getImage($image,$filepath)."</a>";
			$tmp .= "<div class='icon_tools'>";
			$tmp .= "<a href='javascript:void(0)' class='ui-icon ui-icon-circle-arrow-n icon_up'></a>";	
			$tmp .= "<a href=javascript:void(0)'' class='ui-icon ui-icon-circle-arrow-s icon_down'></a>";
			$tmp .= "<a href='javascript:void(0)' class='ui-icon ui-icon-circle-close removeImage icon_remove'></a>";	
			$tmp .= "</div>";	
			$tmp .= $between;
			if($_CONFIG['max_uploads_unlimit'] ) $tmp .= "<input name='{$name}_file[]' type='file' value='$image' class='$class' id='{$name}_file[$i]'>"._('Gallery Image').$between;
					
			$tmp .= "<input name='{$name}[]' type='hidden' id='{$name}[$i]' value='$image' />";			
			
			if($show_text){
				$text = str_replace("'","&#039",$text);
				$tmp .= "<input name='{$name}_text[]' id='{$name}_text[$i]' type='text' class='$class' value='$text' />"._('Image Caption').$between;
			}
			$tmp .= "<br clear='all' /></div>";	
		}			
		$tmp .= "$between<strong>"._('Add New')." ".$title."</strong> <br />";
		$tmp .= "<div class='image_new'>";
		if($show_thumb){$tmp .= "<input name='{$name}_thumb_file[]' type='file' class='$class' id='{$name}_thumb_file[$i]'>"._('Thumbnail').$between;}
		$tmp .= "<input name='{$name}_file[]' type='file' class='$class' id='{$name}_file[$i]'>"._('Gallery Image').$between;
		if($show_text){$tmp .= "<input name='{$name}_text[]' id='{$name}_text[$i]' type='text' class='$class' value='' />"._('Image Caption')."$after";}
		$tmp .= "</div>";
		$tmp .= "<span class='addImage'><a href='javascript:void(0)' class='icon_add'><span class='ui-icon ui-icon-plusthick'></span>"._('Continue Add')."</a></span>";
		return $tmp;
	}	
	
	function create_form_file_document($id,$name,$title,$value,$attr){
		extract($attr);
		$tmp .= "<a href='$filepath$value' target='_blank'>$value</a>";
		$tmp .= "$between
		<input name='{$name}_file' type='file' class='$class' id='{$name}_file'>
		<input name='$name' type='hidden' id='$name' value='$value' />"; 
		return $tmp;
	}
	
	function create_form_file_document_multi($id,$name,$title,$value,$attr){
		global  $_CONFIG;
		extract($attr);
		if(is_array($value)){
			$values=$value;
		}elseif(empty($value)){
			$values = array();
		}else{
			$values = array($value);
		}
		//print_r($values);
		for($i=0;$i<count($values);$i++){
			$tmp .= "<div class='image_container'>";
			$value = $values[$i];
			$text = $value['text'];
			$thumb = $value['thumb'];
			$document = $value['file'];
			if($show_thumb){
				$tmp .= "<a href='$filepath$document' target='_blank'>".getImage($thumb,$filepath,$attr)."</a>";
				$tmp .= $between;
				if($_CONFIG['max_uploads_unlimit']) $tmp .= "<input name='{$name}_thumb_file[]' type='file' value='$thumb' class='$class' id='{$name}_thumb_file[$i]'>".TXT_DOC_THUMB.$between;
				$tmp .= "<input name='{$name}_thumb[]' type='hidden' id='{$name}_thumb[$i]' value='$thumb' />";			
			}
			$tmp .= "<a href='$filepath$document' target='_blank'>$document</a>";	
			$tmp .= "<div class='icon_tools'>";
			$tmp .= "<a href='' class='icon icon_up'></a>";	
			$tmp .= "<a href='' class='icon icon_down'></a>";
			$tmp .= "<a href='' class='removeImage icon icon_remove'></a>";	
			$tmp .= "</div>";	
			$tmp .= $between;
			if($_CONFIG['max_uploads_unlimit']) $tmp .= "<input name='{$name}_file[]' type='file' value='$document' class='$class' id='{$name}_file[$i]'>".TXT_DOC.$between;
			$tmp .= "<input name='{$name}[]' type='hidden' id='{$name}[$i]' value='$document' />";			
			
			if($show_text){
				$text = str_replace("'","&#039",$text);
				$tmp .= "<input name='{$name}_text[]' id='{$name}_text[$i]' type='text' class='$class' value='$text' />".TXT_DOC_TEXT.$between;
			}
			$tmp .= "</div>";	
		}		
		
		$tmp .= "$between<strong>".TXT_ADD_NEW.$title."</strong> <br />";
		$tmp .= "<div class='image_new'>";
		if($show_thumb){$tmp .= "<input name='{$name}_thumb_file[]' type='file' class='$class' id='{$name}_thumb_file[$i]'>$between";}
		$tmp .= "<input name='{$name}_file[]' type='file' class='$class' id='{$name}_file[$i]'>$between";
		if($show_text){$tmp .= "<input name='{$name}_text[]' id='{$name}_text[$i]' type='text' class='$class' value='' />$after</div>";}
		$tmp .= "<span class='addImage'><a href='javascript:void(0)' class='icon icon_add'>".TXT_CONTINUE_ADD."</a></span>";
		
		return $tmp;
	}	
	
	
	function create_form_file_video($id,$name,$title,$value,$attr){
		extract($attr);
		$text = $value['text'];
		$thumb = $value['thumb'];
		$video = $value['file'];
		
		$tmp .= "<a href='$video' target='_blank' class='media'>".getImage($thumb,$filepath,$attr)."</a>";
		$tmp .= "$between<input name='{$name}_thumb_file' type='file' value='$thumb' class='$class' id='{$name}_thumb_file'>".TXT_VIDEO_THUMB.$between;
		$tmp .= "<input name='{$name}_thumb' type='hidden' id='{$name}_thumb' value='$thumb' />";			

		$video = str_replace("'","&#039",$video);
		$tmp .= "<input name='{$name}' type='text' class='$class' id='{$name}' value='$video' />".TXT_VIDEO; 
		
		$text = str_replace("'","&#039",$text);
		$tmp .= "$between<input name='{$name}_text' id='{$name}_text' type='text' class='$class' value='$text' />".TXT_VIDEO_TEXT;
		return $tmp;
	}
	
	
	function create_form_file_video_multi($id,$name,$title,$value,$attr){
		global  $_CONFIG;
		extract($attr);
		if(is_array($value)){
			$values=$value;
		}elseif(empty($value)){
			$values = array();
		}else{
			$values = array($value);
		}
		//print_r($values);
		for($i=0;$i<count($values);$i++){
			$tmp .= "<div class='image_container'>";
			$value = $values[$i];
			$text = $value['text'];
			$thumb = $value['thumb'];
			$video = $value['file'];
			
			$tmp .= "<a href='$video' target='_blank' class='media'>".getImage($thumb,$filepath,$attr)."</a>";
			$tmp .= $between;
			if($_CONFIG['max_uploads_unlimit']) $tmp .= "<input name='{$name}_thumb_file[]' type='file' value='$thumb' class='$class' id='{$name}_thumb_file[$i]'>".TXT_VIDEO_THUMB.$between;
			$tmp .= "<input name='{$name}_thumb[]' type='hidden' id='{$name}_thumb[$i]' value='$thumb' />";			
			$tmp .= "<div class='icon_tools'>";
			$tmp .= "<a href='' class='icon icon_up'></a>";	
			$tmp .= "<a href='' class='icon icon_down'></a>";
			$tmp .= "<a href='' class='removeImage icon icon_remove'></a>";	
			$tmp .= "</div>";	
			$video = str_replace("'","&#039",$video);
			$tmp .= "<input name='{$name}[]' type='text' value='$video' class='$class' id='{$name}[$i]'>".TXT_VIDEO.$between;
			if($show_text){
				$text = str_replace("'","&#039",$text);
				$tmp .= "<input name='{$name}_text[]' id='{$name}_text[$i]' type='text' class='$class' value='$text' />".TXT_VIDEO_TEXT;
			}
			$tmp .= $between;
			$tmp .= "</div>";	
		}		
		
		$tmp .= "$between<strong>".TXT_ADD_NEW.$title."</strong> <br />";
		$tmp .= "<div class='image_new'>";
		if($show_thumb){$tmp .= "<input name='{$name}_thumb_file[]' type='file' class='$class' id='{$name}_thumb_file[$i]'>".TXT_VIDEO_THUMB.$between;}
		$tmp .= "<input name='{$name}[]' type='text' class='$class' id='{$name}_url[$i]'>".TXT_VIDEO.$between;
		if($show_text){$tmp .= "<input name='{$name}_text[]' id='{$name}_text[$i]' type='text' class='$class' value='' />".TXT_VIDEO_TEXT."$after</div>";}
		$tmp .= "<span class='addImage'><a href='javascript:void(0)'>".TXT_CONTINUE_ADD."</a></span>";
		
		return $tmp;
	}	
	
	
	
	function create_form_select($id,$name,$title,$value,$attr=array(),$list=array()){
		$default=array('class'=>'','before'=>"\t\t<br />",'between'=>'<br />','after'=>'<br /><br />','is_name'=>false);
		$arg = array_merge($default,$attr);
		extract($arg);
		$tmp = "$before<label for='$id' class='$class'>$title</label>
				$between
		 		<select name='$name' id='$id' class='$class'>";
		
		if(is_array($list)){
			foreach($list as $k=>$v){
				if(empty($v)) continue;
				($is_name)?$o=$v:$o=$k;
				($o == $target)?$select = 'selected':$select = '';
				$tmp .= "<option value='{$k}' $select>{$v}</option>";
			}
		}
		 $tmp .= "</select>
				$after";
		 
		 return $tmp;
	}
	
	/*
	$ext
	option list for select
	
	$attr
	option setting for file (multi/single,filepath,filetype)
	*/
	function create_form_element($id,$name,$title,$type,$value,$attr=array(),$ext=array()){		
		   	switch ($type){
		   		case 'text':
		   		case 'password':
		   			$result = create_form_text($id,$name,$title,$value,$type,$attr);
		   			break;
		   		case 'textarea':
		   			$result = create_form_textarea($id,$name,$title,$value,$attr);
		   			break;
		   		case 'button':
		   			$result = create_form_textarea($id,$name,$title,$value,$attr);
		   			break;
		   		case 'checkbox':
		   			$result = create_form_checkbox($id,$name,$title,$value,$attr);
		   			break;
		   		case 'select':
		   			$result = create_form_select($id,$name,$title,$value,$attr,$ext);
		   			break;
		   		case 'radio':
		   			$result = create_form_radio($id,$name,$title,$value,$attr);
		   		case 'file':
		   			$result = create_form_file($id,$name,$title,$value,$attr);
		   			break;
	   			default:
	   				break;
		   	}
		   	return $result;
	}

/*
 array(
		"id"=>"title",
		"name"=>"title",
		"title"=>the title of input,
		"type"=>"text",
		"required"=>"1",
		"WYSIWYG"=>"1",
		"value"=>$Info['Title']
	),
*/
	function createForm($form){
		global $_CONFIG;
		if(!is_array($form)) return "";
		
		
		foreach($form as $val){
			$default=array('WYSIWYG'=>false,'required'=>false);
			if(!is_array($val['attr'])) $val['attr'] = array($val['attr']);
			$arg = array_merge($default,$val['attr']);
			extract($arg);
		   	switch ($val['type']){
		   		case 'text':
		   		case 'password':
		   		case 'textarea':
		   		case 'button':
		   		case 'checkbox':
		   		case 'radio':
		   		case 'file':
		   			$result .= create_form_element(
		   			$val['id'],
		   			$val['name'],
		   			$val['title'],
		   			$val['type'],
		   			$val['value'],
		   			$val['attr'],
		   			$val['ext']
		   			);
		   			break;
	   			default:
	   				break;
		   	}
		}
		
		return $result;
		
		$result = "";
		$lang_flag = array();
		foreach($form as $val){
			$tmp = "";
			extract($val);
			//if(empty($id)) $id = $type."_".$key;
			if(!empty($lang_code)){
				$id = $id."_$lang_code";
				$name = $name."[$lang_code]";
				//$title = $title." $lang_name";
				$lang_flag[$lang_code]=1;
			}
			$tmp = "\t\t<div class='lang $lang_code'>\t\t<label for='$id'><strong>$title</strong></label><br />\n";
			if($type == "textarea"){
				if($WYSIWYG == true && $_CONFIG['FlashArticle']==true){
					$tmp .= "\t\t<textarea name='$name' id='$id' type='$type' class='$class mceEditor'>$value</textarea><br />\n";
				}else{
					$tmp .= "\t\t<textarea name='$name' id='$id' type='$type' class='$class'>$value</textarea><br />\n";
				}
				if($WYSIWYG == true && $_CONFIG['FlashArticle']==false){
					//$tmp .= "\t\t <span class='edit_rich' textid='$id'><a href='javascript:void(0)'>edit in RichEditor</a></span><br /><br />\n";
				}else{
					//$tmp .= "\t\t<br />\n";
				}
			}else{
				//$value = htmlspecialchars($value);
				$tmp .= "\t\t<input name='$name' id='$id' type='$type' class=\"$class\" value=\"$value\" /><br /><br />\n";
			}
			$tmp .='</div>';
			$result.=$tmp;
		}
		$tmp = '';
		if(is_array($lang_flag)){
			foreach ($lang_flag as $k=>$v){
				$flag = createLanguageFlag($k);
				$tmp .= "<span class='frm_lang_select' id='frm_lang_select_$k' gid='$k'>$flag</span>";
			}
		}
		$result = "<br />$tmp<br />".$result;
		return $result;
	}
        
	function createFormSubmit(){
		$str = '<input name="reset" type="reset" class="form_button" id="button_reset" value="Clear" />
              <input name="submit" type="submit" class="form_button" id="button_submit" value="Save" />';
		return $str;
	}

	
	function createMenu($list,$target,$is_name=0){
            //print_r($list);
		if(is_array($list)){
			foreach($list as $k=>$v){
				if(empty($v)) continue;
				if($is_name){
				(in_array($v,$target))?$select = 'selected':$select = '';
				}else{
				(in_array($k,$target))?$select = 'selected':$select = '';
				}
				$s .= "<option value='{$k}' $select>{$v}</option>";
			}
		}
		return $s;
	}

        function createPageNav($max,$queryData,$page,$base_url,$wrap='li'){
            $s = '';
            if($max > 1):
                for($i=0;$i<$max;$i++):
                    $queryData['page'] = $i;
                    $_url = $base_url.'?'.http_build_query($queryData);
                    ($i == $page)?$class = 'selected':$class = '';
                    $s .= "<$wrap><a href='$_url' class='$class'>".($i+1)."</a></$wrap>\n";
                endfor;
            endif;
            return $s;
        }

	function jsCheckForm($form){
		if(!is_array($form)) return;
		$result ="";
		foreach($form as $key=>$val){
			extract($val);
			if(empty($id)) $id = $type.'_'.$key;
			//	"content"=>array(
			//		"id"=>"",
			//		"title"=>"content",
			//		"type"=>"textarea",
			//		"max"=>"0",
			//		"min"=>"1",
			//		"format"=>_FORMAT_NONE,
			//		"value"=>""
			//	)
			if($val['type'] == 'text') $result.= "checkInput('text','$id','$max','$min','$format')";
		}
		return $result;
	}

	function createValidateJS($form,&$rules,&$messages){
		for($i=0;$i<count($form);$i++){

//			if(!empty($form[$i]['lang_code'])){
//				$form[$i]['id'] .= "_{$form[$i]['lang_code']}";
//				$form[$i]['name'] .= "[{$form[$i]['lang_code']}]";
//				$form[$i]['title'] .= " {$form[$i]['lang_name']}";
//				$flag = createLanguageFlag($form[$i]['lang_code']);
//			}

			$t = $form[$i];
			$tmp_rules ="";
			$tmp_messages ="";
			if(!empty($form[$i]["required"])){
				$tmp_rules = 'required:true';
				$tmp_messages = 'required:"<br />Please enter '.$form[$i]["title"].'"';
			}else{
				$tmp_rules = 'required:false';
				$tmp_messages = 'required:""';
			}
			if(!empty($form[$i]["minlength"])){
				$tmp_rules .= ',minlength:'.$form[$i]["minlength"];
				$tmp_messages .= ',minlength:"<br />Your ['.$form[$i]["title"].'] must be at least '.$form[$i]["minlength"].' characters long"';
			}
			if(!empty($form[$i]["maxlength"])){
				$tmp_rules .= ',maxlength:'.$form[$i]["maxlength"];
				$tmp_messages .= ',maxlength:"<br />Your ['.$form[$i]["title"].'] must be at most '.$form[$i]["maxlength"].' characters long"';
			}
			if(!empty($rules)){$rules .= ",\n";}
			$rules .= '"'.$form[$i]["name"]  . '"'.":{" .$tmp_rules."}";
			if(!empty($messages)){$messages .= ",\n";}
			$messages .= '"'.$form[$i]["name"] . '"'.":{" .$tmp_messages."}";
		}
	}

	function createSelect($list,$target,$is_name=0){
		if(is_array($list)){
			foreach($list as $k=>$v){
				if(empty($v)) continue;
				if($is_name){
				($v == $target)?$select = 'selected':$select = '';
				}else{
				($k == $target)?$select = 'selected':$select = '';
				}
				$s .= "<option value='{$k}' $select>{$v}</option>";
			}
		}
		return $s;
	}
        
        function read($filename,&$contents){
		$filename = urldecode($filename) ;
		if(empty($filename)) return false;
		if(!file_exists( $filename )) return false;

                $handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		fclose($handle);
                
		return true;
	}

	function write($filename,$content){
		if (file_exists($filename) && !is_writable($filename)) return false;
		if (!$handle = fopen($filename, 'w')) return false;
		if (fwrite($handle, $content) === FALSE) return false;
		fclose($handle);
		return true;
	}
        
	function upload_file($upload_file,$file_path,$org,$validateExt=array()){
		global $_CONFIG;
		if(is_array($upload_file['size'])){
			for($i=0;$i<count($upload_file['size']);$i++){
				$tmp = array();
				foreach($upload_file as $k=>$v){
					$tmp[$k] = $v[$i];
				}
				$tmp = upload($tmp,$file_path,$validateExt);
				if($tmp){
					($_CONFIG['max_uploads_unlimit'])?$org[$i] = $tmp:$org[] = $tmp;
				}
			}
                        if(empty($org)) $org = array();
			return serialize($org);
		}else{
			$tmp = upload($upload_file,$file_path,$validateExt);
			return ($tmp)?$tmp:$org;
		}
	}
	
	function upload($image_file,$imgPath,$validateExt=array()){
		if($image_file['size'] > 0){
			$path_parts = pathinfo($image_file['name']);
			if(!empty($validateExt)){
				if(!is_array($validateExt)) $validateExt = array($validateExt);
				if(!in_array (strtolower($path_parts["extension"]),$validateExt)) return false;
			}
			
				$patterns = array("/[^(\w,\s)]+/","/^(\s)/","/(\s)+/");
				$replacements = array("","","_");
				$path_parts["filename"] = preg_replace($patterns, $replacements, $path_parts["filename"]);
				if(empty($path_parts["filename"])) $path_parts["filename"] = rand(0,1000);
				//echo $path_parts["filename"];
				
/*			if($image_file['randname'] == true){
				$image_file['name'] = time().$path_parts["filename"].'.'.strtolower($path_parts["extension"]);
			}*/
			$prex = date("ymd").'_';
			$image_file['name'] = $prex.$path_parts["filename"].'.'.strtolower($path_parts["extension"]);
			$uploadfile  = $imgPath.$image_file['name'];
			for($i=1;$i<1000;$i++){
				if(file_exists($uploadfile)){
					$image_file['name'] = $prex.$path_parts["filename"].'_'.$i.'.'.strtolower($path_parts["extension"]);
					$uploadfile  = $imgPath.$image_file['name'];
				}else{
					break;
				}
			}
			if(move_uploaded_file ($image_file['tmp_name'], $uploadfile)==true){
				$image = urlencode($image_file['name']);
			}else{
				return false;
			}
		}else{
			return false;
		}
		return $image;
	}
	

	function download($filename){
		$filename = urldecode($filename) ;
		if(empty($filename)) return false;
		if(!file_exists( $filename )) return false;

		$path_parts = pathinfo($filename);
		$ext = $path_parts["extension"];
		switch( $ext ){
			case "pdf": $ctype="application/pdf"; break;
			case "doc": $ctype="application/msword"; break;
			case "docx": $ctype="application/msword"; break;
			case "gif": $ctype="image/gif"; break;
			case "jpe": $ctype="image/jpeg"; break;
			case "jpeg": $ctype="image/jpeg"; break;
			case "jpg": $ctype="image/jpeg"; break;
			case "xla": $ctype="application/vnd.ms-excel"; break;
			case "xlc": $ctype="application/vnd.ms-excel"; break;
			case "xlm": $ctype="application/vnd.ms-excel"; break;
			case "xls": $ctype="application/vnd.ms-excel"; break;
			case "xlt": $ctype="application/vnd.ms-excel"; break;
			case "xlw": $ctype="application/vnd.ms-excel"; break;

			case "323 ": $ctype="text/h323"; break;
			case "acx": $ctype="application/internet-property-stream"; break;
			case "ai": $ctype="application/postscript"; break;
			case "aif": $ctype="audio/x-aiff"; break;
			case "aifc": $ctype="audio/x-aiff"; break;
			case "aiff": $ctype="audio/x-aiff"; break;
			case "asf": $ctype="video/x-ms-asf"; break;
			case "asr": $ctype="video/x-ms-asf"; break;
			case "asx": $ctype="video/x-ms-asf"; break;
			case "au": $ctype="audio/basic"; break;
			case "avi": $ctype="video/x-msvideo"; break;
			case "axs": $ctype="application/olescript"; break;
			case "bas": $ctype="text/plain"; break;
			case "bcpio": $ctype="application/x-bcpio"; break;
			case "bin": $ctype="application/octet-stream"; break;
			case "bmp": $ctype="image/bmp"; break;
			case "c": $ctype="text/plain"; break;
			case "cat": $ctype="application/vnd.ms-pkiseccat"; break;
			case "cdf": $ctype="application/x-cdf"; break;
			case "cer": $ctype="application/x-x509-ca-cert"; break;
			case "class": $ctype="application/octet-stream"; break;
			case "clp": $ctype="application/x-msclip"; break;
			case "cmx": $ctype="image/x-cmx"; break;
			case "cod": $ctype="image/cis-cod"; break;
			case "cpio": $ctype="application/x-cpio"; break;
			case "crd": $ctype="application/x-mscardfile"; break;
			case "crl": $ctype="application/pkix-crl"; break;
			case "crt": $ctype="application/x-x509-ca-cert"; break;
			case "csh": $ctype="application/x-csh"; break;
			case "css": $ctype="text/css"; break;
			case "dcr": $ctype="application/x-director"; break;
			case "der": $ctype="application/x-x509-ca-cert"; break;
			case "dir": $ctype="application/x-director"; break;
			case "dll": $ctype="application/x-msdownload"; break;
			case "dms": $ctype="application/octet-stream"; break;
			case "doc": $ctype="application/msword"; break;
			case "dot": $ctype="application/msword"; break;
			case "dvi": $ctype="application/x-dvi"; break;
			case "dxr": $ctype="application/x-director"; break;
			case "eps": $ctype="application/postscript"; break;
			case "etx": $ctype="text/x-setext"; break;
			case "evy": $ctype="application/envoy"; break;
			case "exe": $ctype="application/octet-stream"; break;
			case "fif": $ctype="application/fractals"; break;
			case "flr": $ctype="x-world/x-vrml"; break;
			case "gtar": $ctype="application/x-gtar"; break;
			case "gz": $ctype="application/x-gzip"; break;
			case "h": $ctype="text/plain"; break;
			case "hdf": $ctype="application/x-hdf"; break;
			case "hlp": $ctype="application/winhlp"; break;
			case "hqx": $ctype="application/mac-binhex40"; break;
			case "hta": $ctype="application/hta"; break;
			case "htc": $ctype="text/x-component"; break;
			case "htm": $ctype="text/html"; break;
			case "html": $ctype="text/html"; break;
			case "htt": $ctype="text/webviewhtml"; break;
			case "ico": $ctype="image/x-icon"; break;
			case "ief": $ctype="image/ief"; break;
			case "iii": $ctype="application/x-iphone"; break;
			case "ins": $ctype="application/x-internet-signup"; break;
			case "isp": $ctype="application/x-internet-signup"; break;
			case "jfif": $ctype="image/pipeg"; break;
			case "js": $ctype="application/x-javascript"; break;
			case "latex": $ctype="application/x-latex"; break;
			case "lha": $ctype="application/octet-stream"; break;
			case "lsf": $ctype="video/x-la-asf"; break;
			case "lsx": $ctype="video/x-la-asf"; break;
			case "lzh": $ctype="application/octet-stream"; break;
			case "m13": $ctype="application/x-msmediaview"; break;
			case "m14": $ctype="application/x-msmediaview"; break;
			case "m3u": $ctype="audio/x-mpegurl"; break;
			case "man": $ctype="application/x-troff-man"; break;
			case "mdb": $ctype="application/x-msaccess"; break;
			case "me": $ctype="application/x-troff-me"; break;
			case "mht": $ctype="message/rfc822"; break;
			case "mhtml": $ctype="message/rfc822"; break;
			case "mid": $ctype="audio/mid"; break;
			case "mny": $ctype="application/x-msmoney"; break;
			case "mov": $ctype="video/quicktime"; break;
			case "movie": $ctype="video/x-sgi-movie"; break;
			case "mp2": $ctype="video/mpeg"; break;
			case "mp3": $ctype="audio/mpeg"; break;
			case "mpa": $ctype="video/mpeg"; break;
			case "mpe": $ctype="video/mpeg"; break;
			case "mpeg": $ctype="video/mpeg"; break;
			case "mpg": $ctype="video/mpeg"; break;
			case "mpp": $ctype="application/vnd.ms-project"; break;
			case "mpv2": $ctype="video/mpeg"; break;
			case "ms": $ctype="application/x-troff-ms"; break;
			case "mvb": $ctype="application/x-msmediaview"; break;
			case "nws": $ctype="message/rfc822"; break;
			case "oda": $ctype="application/oda"; break;
			case "p10": $ctype="application/pkcs10"; break;
			case "p12": $ctype="application/x-pkcs12"; break;
			case "p7b": $ctype="application/x-pkcs7-certificates"; break;
			case "p7c": $ctype="application/x-pkcs7-mime"; break;
			case "p7m": $ctype="application/x-pkcs7-mime"; break;
			case "p7r": $ctype="application/x-pkcs7-certreqresp"; break;
			case "p7s": $ctype="application/x-pkcs7-signature"; break;
			case "pbm": $ctype="image/x-portable-bitmap"; break;
			case "pdf": $ctype="application/pdf"; break;
			case "pfx": $ctype="application/x-pkcs12"; break;
			case "pgm": $ctype="image/x-portable-graymap"; break;
			case "pko": $ctype="application/ynd.ms-pkipko"; break;
			case "pma": $ctype="application/x-perfmon"; break;
			case "pmc": $ctype="application/x-perfmon"; break;
			case "pml": $ctype="application/x-perfmon"; break;
			case "pmr": $ctype="application/x-perfmon"; break;
			case "pmw": $ctype="application/x-perfmon"; break;
			case "pnm": $ctype="image/x-portable-anymap"; break;
			case "pot,": $ctype="application/vnd.ms-powerpoint"; break;
			case "ppm": $ctype="image/x-portable-pixmap"; break;
			case "pps": $ctype="application/vnd.ms-powerpoint"; break;
			case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
			case "prf": $ctype="application/pics-rules"; break;
			case "ps": $ctype="application/postscript"; break;
			case "pub": $ctype="application/x-mspublisher"; break;
			case "qt": $ctype="video/quicktime"; break;
			case "ra": $ctype="audio/x-pn-realaudio"; break;
			case "ram": $ctype="audio/x-pn-realaudio"; break;
			case "ras": $ctype="image/x-cmu-raster"; break;
			case "rgb": $ctype="image/x-rgb"; break;
			case "rmi": $ctype="audio/mid"; break;
			case "roff": $ctype="application/x-troff"; break;
			case "rtf": $ctype="application/rtf"; break;
			case "rtx": $ctype="text/richtext"; break;
			case "scd": $ctype="application/x-msschedule"; break;
			case "sct": $ctype="text/scriptlet"; break;
			case "setpay": $ctype="application/set-payment-initiation"; break;
			case "setreg": $ctype="application/set-registration-initiation"; break;
			case "sh": $ctype="application/x-sh"; break;
			case "shar": $ctype="application/x-shar"; break;
			case "sit": $ctype="application/x-stuffit"; break;
			case "snd": $ctype="audio/basic"; break;
			case "spc": $ctype="application/x-pkcs7-certificates"; break;
			case "spl": $ctype="application/futuresplash"; break;
			case "src": $ctype="application/x-wais-source"; break;
			case "sst": $ctype="application/vnd.ms-pkicertstore"; break;
			case "stl": $ctype="application/vnd.ms-pkistl"; break;
			case "stm": $ctype="text/html"; break;
			case "svg": $ctype="image/svg+xml"; break;
			case "sv4cpio": $ctype="application/x-sv4cpio"; break;
			case "sv4crc": $ctype="application/x-sv4crc"; break;
			case "swf": $ctype="application/x-shockwave-flash"; break;
			case "t": $ctype="application/x-troff"; break;
			case "tar": $ctype="application/x-tar"; break;
			case "tcl": $ctype="application/x-tcl"; break;
			case "tex": $ctype="application/x-tex"; break;
			case "texi": $ctype="application/x-texinfo"; break;
			case "texinfo": $ctype="application/x-texinfo"; break;
			case "tgz": $ctype="application/x-compressed"; break;
			case "tif": $ctype="image/tiff"; break;
			case "tiff": $ctype="image/tiff"; break;
			case "tr": $ctype="application/x-troff"; break;
			case "trm": $ctype="application/x-msterminal"; break;
			case "tsv": $ctype="text/tab-separated-values"; break;
			case "txt": $ctype="text/plain"; break;
			case "uls": $ctype="text/iuls"; break;
			case "ustar": $ctype="application/x-ustar"; break;
			case "vcf": $ctype="text/x-vcard"; break;
			case "vrml": $ctype="x-world/x-vrml"; break;
			case "wav": $ctype="audio/x-wav"; break;
			case "wcm": $ctype="application/vnd.ms-works"; break;
			case "wdb": $ctype="application/vnd.ms-works"; break;
			case "wks": $ctype="application/vnd.ms-works"; break;
			case "wmf": $ctype="application/x-msmetafile"; break;
			case "wps": $ctype="application/vnd.ms-works"; break;
			case "wri": $ctype="application/x-mswrite"; break;
			case "wrl": $ctype="x-world/x-vrml"; break;
			case "wrz": $ctype="x-world/x-vrml"; break;
			case "xaf": $ctype="x-world/x-vrml"; break;
			case "xbm": $ctype="image/x-xbitmap"; break;
			case "xof": $ctype="x-world/x-vrml"; break;
			case "xpm": $ctype="image/x-xpixmap"; break;
			case "xwd": $ctype="image/x-xwindowdump"; break;
			case "z": $ctype="application/x-compress"; break;
			case "zip": $ctype="application/zip"; break;
			default:
				$ctype="application/unknown";
		}
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: $ctype");
		$user_agent = strtolower ($_SERVER["HTTP_USER_AGENT"]);
		header( "Content-Disposition: attachment; filename=".basename($filename).";"
		);
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($filename));

		$handle = fopen($filename, "r");
		$contents = fread($handle, filesize($filename));
		fclose($handle);
		print ($contents);
		flush();
		return true;
	}
	/*start string,filename,url function*/
	//12345678901234567890.jpg,9 => 1234...0.jpg
	function func_str_shortpath($str,$limit){
		$t = '...';
		if(!(IsNumerics($limit) && $limit > 7)) $limit = 7;
		$length = strlen($str);
		if( $length <= $limit) return $str;
		$pos = strrpos($str,'.');
		if($pos === false) return substr($str, 0, ($limit-4)).$t.substr($str, -4, 4);
		$ext = $length - $pos;
		if($ext >= $limit) return substr($str, 0, ($limit-4)).$t.substr($str, -4, 4);
		$ext++;
		$pos--;
		return substr($str, 0, $limit - $ext).$t.substr($str, $pos, $ext);
	}
	
	function formatFileSize($fileSize){
		$size = sprintf("%u", $fileSize);
		if($size == 0) {
			return("0 B");
		}
		$sizename = array(" B", " K", " M", " G", " T", " P", " E", " Z", " Y");
		$s =  round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) ;
		(strpos($s,'.')>2)?$s = substr($s,0,3):$s = substr($s,0,4);
		return $s.$sizename[$i];


		$size = intval($size);
		if(($size >> 20) > 1){
			// > 1M
			$num = ($size / (1024*1024));
			$s = 'M';
		}elseif(($size >> 10) > 1){
			// > 1K
			$num = ($size / (1024));
			$s = 'K';
		}else{
			$num = $size;
			$s = '';
		}

		$format = "%2f%s";
		return sprintf($format, $num, $s);
	}
        
        function get_excerpt($content,$s,$max_length = 400 ,$pre_length=20){
            global $lang;
            $pre_length = 20;
            $max_length = 400;
            $tmp = strip_tags($content);
            $tmp = iconv ( 'utf-8', 'gbk//IGNORE', $tmp);
            $o = $s;
            $s = iconv ( 'utf-8', 'gbk//IGNORE', strtolower($s));
            $pos = stripos($tmp,$s);

            ($pos > $pre_length)?$start = $pos - $pre_length:$start = 0;
            $tmp = trim(substr($tmp,$start,$max_length));
            if(ord($tmp[0]) > 128) $tmp = substr($tmp,1);
            if(strlen($tmp)>=($max_length)-5) $tmp .= '...';

            //$tmp = str_ireplace($s,"<span class='keyword'>$o</span>",$tmp);
            $tmp = iconv ('gbk', 'utf-8//IGNORE', $tmp);
            return $tmp;
        }
        
        function flash_message()   
        {   
                // get flash message from CI instance   
                $ci =& get_instance();   
                $flashmsg = $ci->session->flashdata('message');  
                $html = '';   
                if (is_array($flashmsg))   
                {   
                    $c = '';
                    switch($flashmsg['type']){
                        case 'success':
                            $c = 'alert-success';
                            $i = 'fa-fw fa fa-check';
                            break;
                        case 'error':
                        case 'danger':
                            $c = 'alert-danger';
                            $i = 'fa-fw fa fa-times';
                            break;
                        case 'warning':
                            $c = 'alert-info';
                            $i = 'fa-fw fa fa-warning';
                            break;
                        default:
                            $c = 'alert-info';
                            $i = 'fa-fw fa fa-info';
                            break;
                    }
                        
                        $html = '<div id="flashmessage" class="alert '.$c.' fade in">   
                            <button class="close" data-dismiss="alert">×</button>
                                <i class="'.$i.'"></i>'.$flashmsg['title'].'
                                </div>';   
                }   
                return $html;   
        }
        
        if(!function_exists("_")){
            function _($s){
                return $s;
            }
        }

} // END OF _COMM_FUNC_PHP
/* End of file validator_helper.php */
/* Location: ./application/helpers/validator_helper.php */