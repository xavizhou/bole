<?php
$lang["News"] = "新闻";
$lang["Portfolio"] = "作品集";
$lang["Team"] = "团队";
$lang["Job"] = "工作";
$lang["Gallery"] = "图片集";

$lang["Add New"] = "新增";
$lang["Delete"] = "删除";
$lang["Edit"] = "编辑";
$lang["New"] = "新建";

$lang["Submit"] = "提交";
$lang["Browse"] = "浏览";
$lang["Upload"] = "上传";
$lang["Confirm"] = "确认";



$lang["ID"] = "ID";
$lang["Title"] = "标题";
$lang["Date"] = "日期";
$lang["Content"] = "内容";
$lang["Description"] = "描述";
$lang["Category"] = "分类";
$lang["Location"] = "地区";
$lang["Industry"] = "工种";
$lang["Type"] = "种类";

$lang["ph_title"] = "请输入标题";
$lang["ph_content"] = "请输入内容";
$lang["ph_thumb"] = "缩略图";
$lang["ph_description"] = "请输入描述";


$lang["valid_req_title"] = "请输入标题";
$lang["valid_req_name"] = "请输入名字";
$lang["valid_req_description"] = "请输入描述";




