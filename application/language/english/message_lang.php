<?php
//basic button
$lang["News"] = "News";
$lang["Portfolio"] = "Portfolio";
$lang["Team"] = "Team";
$lang["Job"] = "Job";
$lang["Gallery"] = "Gallery";
$lang["CV"] = "CV";

$lang["Add New"] = "Add New";
$lang["Delete"] = "Delete";
$lang["Edit"] = "Edit";
$lang["New"] = "New";

$lang["Submit"] = "Submit";
$lang["Browse"] = "Browse";
$lang["Upload"] = "Upload";
$lang["Confirm"] = "Confirm";
$lang["Reset"] = "Reset";
$lang["Send"] = "Send";
$lang["Send_Success"] = "Send Successful";



//navigation
$lang["nav_dashboard"] = "Dashboard";
$lang["nav_home"] = "Home";
$lang["nav_awards_honors"] = "Awards & Honors";
$lang["nav_star_consultants"] = "Star consultants";
$lang["nav_successful_cases"] = "Successful cases";
$lang["nav_who_are_we"] = "Who are we";
$lang["nav_origin_of_bole"] = "Origin of Bó Lè";
$lang["nav_bole_history"] = "Bó Lè History";
$lang["nav_about_bole_associate"] = "About Bó Lè Associate";
$lang["nav_bole_associates_service"] = "Bó Lè Associates Service";
$lang["nav_standard_procedure"] = "Standard Procedure";
$lang["nav_why_are_we"] = "Why are we";
$lang["nav_meet_us"] = "Meet us";
$lang["nav_management_team"] = "Management team";
$lang["nav_consultants"] = "Consultants";
$lang["nav_consultant_office"] = "Office City";
$lang["nav_successful_cases"] = "Successful cases";
$lang["nav_jobs"] = "Jobs";
$lang["nav_submit_cv"] = "Submit CV";
$lang["nav_submit_vacancy"] = "Submit Vacancy";
$lang["nav_browse_jobs"] = "Browse Jobs";
$lang["nav_bo_le_institute"] = "Bo Le Institute";
$lang["nav_report_insights"] = "Report & Insights";
$lang["nav_knowledge_center"] = "Knowledge Center";
$lang["nav_e_newsletter"] = "E-Newsletter";
$lang["nav_blog"] = "Blog";
$lang["nav_workwithus"] = "Work with us";

$lang["nav_backup"] = "Database Backup";


$lang["nav_meet_consultants"] = "Meet Consultants";
$lang["nav_meet_team"] = "Meet Management Team";
$lang["nav_successful_cases"] = "Successful Cases";


//form
$lang["ID"] = "ID";
$lang["Title"] = "Title";
$lang["Company"] = "Company";
$lang["Email"] = "Email";
$lang["Date"] = "Date";
$lang["Content"] = "Content";
$lang["Description"] = "Description";
$lang["Category"] = "Category";
$lang["Location"] = "Location";
$lang["Industry"] = "Industry";
$lang["Type"] = "Type";

$lang["Name"] = "Name";
$lang["Office"] = "Office";
$lang["Cons_Title"] = "Title";
$lang["Cons_Parctice"] = "Parctice";
$lang["Cons_Language"] = "Language Skill";
$lang["Cons_Resume"] = "Resume";

$lang["His_Title"] = "Title";
$lang["His_Content"] = "Content";
$lang["His_Date"] = "Date";

$lang["CV_Name"] = "Your CV Name";
$lang["CV_Info"] = "More Infomation";
$lang["CV_Email"] = "Your Email";
$lang["CV_Phone"] = "Your Phone";
$lang["CV_Attach"] = "Attach Your CV here";
$lang["CV_tip1"] = "Drag the file to here, to finish upload";
$lang["CV_tip2"] = "* Accepted formats: PDF or Word DOC, Max size 1MB";
$lang["CV_Choose"] = "Choose File";
$lang["CV_Choose_nofile"] = "No file selected...";


//$lang["Title"] = "Title";

$lang["ph_title"] = "Please input title";
$lang["ph_email"] = "Please input email";
$lang["ph_content"] = "Please input content";
$lang["ph_thumb"] = "Thumb";
$lang["ph_video"] = "Video";
$lang["ph_description"] = "Please input description";
$lang["ph_star"] = "Star";
$lang["ph_name"] = "Please input your name";
$lang["ph_email"] = "Please input your email";
$lang["ph_phone"] = "Please input your phone number";
$lang["ph_cur_position"] = "Current position";
$lang["ph_company"] = "Comapny";
$lang["ph_position"] = "position";


$lang["ph_cons_title"] = "Please input title";
$lang["ph_cons_practice"] = "Please input the practice years";
$lang["ph_cons_language"] = "Please input language skills";

$lang["ph_date"] = "Please input date";


$lang["valid_req_title"] = "Please enter the title";
$lang["valid_req_name"] = "Please enter the name";
$lang["valid_req_description"] = "Please enter the description";

$lang["Backup"] = "Backup";
$lang["Click to download"] = "Click to download";

$lang["homepage_news"] = 'Bó Lè News';
$lang["homepage_news_excerpt"] = 'Our news, events, and also download Bole report';

$lang["homepage_title_1"] = 'A wholly-owned subsidiary of RGF, the world’s fifth largest HR service provider';
$lang["homepage_slug_1"] = 'Connecting <br />Better <br />Life';

$lang["homepage_title_2"] = 'Our Vision';
$lang["homepage_slug_2"] = 'To be an organization that connects 
people and opportunities<br /><br />
 – leading to better lives.';
$lang["homepage_link_2"] = 'Learn more about Bó Lè';

$lang["homepage_title_3"] = 'Our Mission';
$lang["homepage_slug_3"] = 'o build a platform – for knowledge, 
opportunities, and relationships –  
that will enable all our clients, 
candidates and employees to 
become successful.';
$lang["homepage_link_3"] = 'Why Are We';

$lang["check_office"] = "Check <a href='#' class='curr-city'></a> office";
$lang["check_office2"] = "Meet our super consultants in Bole <a href='#' class='curr-city'></a> Office.";
$lang["check_other_office"] = "Check other office ";

$lang["meet_us_intro"] = "<big>330+</big>Consultants &amp; Researchers <big class=\"blue\">50+</big>Supporting Team Members";

$lang["more_cities"] = "More cities";
$lang["learn_more"] = "Learn more";
$lang["download"] = "Download";
$lang["go_back"] = "Go back";
$lang["share"] = "share";

$lang["consultant_meet"] = "Meet";
$lang["leave_a_message"] = "Leave a message";

$lang["consultant_choose_industry"] = "Choose Industry";
$lang["consultant_choose_city"] = "Choose Office";


$lang["form_drag_cv"] = "Drag your CV to here, to finish upload";
$lang["form_cv_tips"] = "* Accepted formats: PDF or Word DOC, Max size 1MB";
$lang["form_choose_file"] = "Choose File";
$lang["form_no_file"] = "No file selected...";

//why are we
$lang["why_title"] = "Why <del>NOT</del> Bole?";
$lang["why_excerpt"] = "Do you know any other executive search firm which is more excellent and that has been in the market for twenty years?";

$lang["why_excerpt2"] = 'took the reins and had the ABILITY to be in the search business.';
$lang["why_map1"] = 'Wholly owned offices - 13 cities in Asia';
$lang["why_map2"] = 'Our Parent Company RGF';
$lang["why_map3"] = 'has <big>45</big> offices, in <big>26</big> cities <br />across <big>11</big> countries and markets';
$lang["why_map4"] = 'Better than a city guide, because we have the';
$lang["why_map5"] = '<big>network of professionals<br />at our fingertips.</big>';

$lang["why_intro_q"] = 'On average, how many resumes do you think are received by one of our clients for available positions? ';
$lang["why_intro_title"] = 'Thousands...';
$lang["why_intro_text"] = 'With Bo Le, you only need to read <b>FIVE</b> that will be PERFECTLY suited to what you expect at your FINAL interview.';

$lang["why_efficient"] = 'Efficient as the CIA, Professional as the President';
$lang["why_cal"] = '<big>10</big>days to present the first candidate';
$lang["why_clock1"] = 'For 2015, every <big>6</big> minutes ONE Bó Lè candidate having the interview with client.';
$lang["why_clock2"] = 'based on <small>21036 client interviews / 2,000 hour work year</small>';
$lang["why_hourglass1"] = '<big>Every 1</big>hour Bó Lè Associates place a professional in a new job.';
$lang["why_hourglass2"] = 'based on <small>726 successful placement /2,000 hour work year</small>';

$lang["why_pool_title"] = 'Pool of ONE MILLION Candidates';
$lang["why_pool_excerpt"] = 'A pool that has been built and taken care of for twenty years. <br />We have experts in EVERY field, from financial experts to industrial factory workers';

$lang["why_pool_donut1"] = 'Specializations';
$lang["why_pool_donut2"] = 'Distribution of placements by job level';

$lang["why_ranking_title"] = 'TOP Ranking';
$lang["why_ranking_text"] = 'Bó Lè Associates has been the top ranked executive search firm across Asia in particular for China and Indonesia – large localized execution teams with high conversion and completion numbers.';

$lang["why_topnotch_title"] = 'Top-notch Client list';
$lang["why_topnotch_text"] = 'With 8000 clients over the past twenty years! 19.1% of them are Fortune 500 companies. 49.8% Repeat Client Ratio. We meet 100 clients, half of them are our business partners and friends; and half of them are now becoming our friends!';



$lang["CR_CN"] = "China";
$lang["CR_HK"] = "Hongkong";
$lang["CR_TW"] = "Taiwan";
$lang["CR_IN"] = "Indonesia";
$lang["CR_MA"] = "Malaysia";
$lang["CR_PI"] = "Philippines";
$lang["CR_TA"] = "Tailand";
//why are we










