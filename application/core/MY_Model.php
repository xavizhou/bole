<?php
class MY_Model extends CI_Model
{
    protected $tbname;
    public function __construct($tbname='')
    {
        $this->tbname = $tbname;
        parent::__construct();
    }

    public function GetAll($order_field = '' , $desc = false)
    {
        $this->db->from($this->tbname);
        if ( !empty($order_field) ){
                $sort = ($desc)?'desc':'asc';
                $this->db->order_by($order_field, $sort); 
        }
        
        $r = $this->db->get()->result_array();

        if( is_array($r)) {
          return $r;
        }else{
            return false;
        }
    }

    function Get($id)
    {
        //$this->db->from($this->tbname);
        $r = $this->db->get_where($this->tbname, array('id' => $id), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
    
    function GetBySlug($slug)
    {
        //$this->db->from($this->tbname);
        $r = $this->db->get_where($this->tbname, array('slug' => $slug), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }

    function GetPrev($order){
        $r = $this->db->order_by('order', 'desc')->get_where($this->tbname, array('order <' => $order), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
    function GetNext($order){
        $r = $this->db->order_by('order', 'asc')->get_where($this->tbname, array('order >' => $order), 1, 0)->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
            return false;
        }
    }
    
    function GetRandom()
    {
        $this->db->from($this->tbname);
        $this->db->order_by('id', 'random'); 
        $r = $this->db->get()->result_array();

        if( is_array($r)) {
          return $r;
        }else{
          return false;
        }
    }

    function Remove($id)
    {
        return $this->db->delete($this->tbname, array('id' => $id)); 
    }
    function RemoveByCondition($condition)
    {
        return $this->db->delete($this->tbname, $condition); 
    }
    
    function RemoveById($ids)
    {
        if(!is_array($ids)) $ids = array($ids);
        $this->db->where_in('id', $ids);
        return $this->db->delete($this->tbname);
    }
    
    function RemoveAll()
    {
        return $this->db->empty_table($this->tbname); 
    }
    
	
    function Clear()
    {
        return $this->db->truncate($this->tbname); 
    }
    
    function RecordCount()
    {
        return $this->db->count_all_results($this->tbname);
    }
    

    function Save($data,$id='')
    {
        
        if(!empty($id)){
			$row = $this->Get($id);
		}
		
        if (empty($row))
		{
			return $this->Add($data);
        }
	return $this->Edit($data,$id);
    }	
		
	function Add($data)
	{
            if(!is_array($data)){
                    return false;
            }
            if($this->db->insert($this->tbname,$data)){
                return $this->db->insert_id();
            }else{
                return false;
            }
	}
	
	
	function Edit($data,$id)
	{
            
            if(!is_array($data)){
                    return false;
            }

             $this->db->where('id', $id);
             $r = $this->db->update($this->tbname, $data);    
             return ($r)?$id:false;
	}
	
	
	/*
	para $keyword string
	para $pagetitle string
	para $id number
	*/
	function UpdateSEO($keyword,$pagetitle,$id){
            $data = array(
                'keywords'=>$keyword,
                'pagetitle'=>$pagetitle
            );
             $this->db->where('id', $id);  
             $r = $this->db->update($this->tbname, $data);    
             return ($r)?$id:false;
	}
	
	function Search($start=0,$num=0,$condition=array())
	{
            $this->db->from($this->tbname);
            if(is_array($condition) && $condition){
                foreach($condition as $c){
                    switch($c[0]){
                        case 'or_where':
                            $this->db->or_where($c[1], $c[2]); 
                            break;
                        case 'where_in':
                            $this->db->where_in($c[1], $c[2]); 
                            break;
                        case 'like':
                            $this->db->like($c[1], $c[2]); 
                            break;
                        case 'or_like':
                            $this->db->or_like($c[1], $c[2]); 
                            break;
                        case 'order':
                            $this->db->order_by($c[1], $c[2]); 
                            break;
                        default:
                            $this->db->where($c[1], $c[2]); 
                            break;
                    }
                }
            }
            if(!empty($num)){
                $this->db->limit($num, $start);
            }
            $r = $this->db->get()->result_array();
            if( is_array($r)) {
              return $r;
            }else{
                return false;
            }
	}
	
	function GetFirst($condition){
		$result = $this->Search(0,0,$condition);
		if(is_array($result)) return $result[0];
		return null;
	}
	function GetLastest($condition){
		$result = $this->Search(0,0,$type);
		if(is_array($result)) return $result[count($result)-1];
		return null;
	}
		
	function GetCountSearch($condition)
	{
            $this->db->from($this->tbname);
            foreach($condition as $c){
                switch($c[0]){
                    case 'or_where':
                        $this->db->or_where($c[1], $c[2]); 
                        break;
                    case 'where_in':
                        $this->db->where_in($c[1], $c[2]); 
                        break;
                    case 'like':
                        $this->db->like($c[1], $c[2]); 
                        break;
                    case 'order':
                        $this->db->order_by($c[1], $c[2]); 
                        break;
                    default:
                        $this->db->where($c[1], $c[2]); 
                        break;
                }
            }
            if(!empty($num)){
                $this->db->limit($num, $start);
            }
            return $this->db->count_all_results();
	}
	
	
//	function AddCondition($condition,$para =''){
//		if(empty($para)){
//		 $this->condition .= ' AND '.$condition;
//		}elseif($para == 1){
//		 $this->condition .= ' '.$condition;		
//		}elseif($para == 2){
//		 $this->condition .= ' OR '.$condition;	
//		}
//	}
//	
//	function ClearCondition(){
//		 unset($this->condition);
//	}
//	
//	function RemoveByCondition()
//	{
//		
//                $query = "DELETE FROM `$this->tbname` WHERE 1=1 $this->condition";	
//		$db->execute($query) ;
//	}
	
	
        function GetMaxOrder($array=array())
        {	 
                $this->db->from($this->tbname);
                $this->db->select_max('order','max_order');
                $this->db->where($array); 
                $r = $this->db->get()->result_array();
                if( is_array($r) && count($r) > 0 ) {
                  return $r[0]['max_order'];
                }else{
                    return false;
                }
        }
	
		
	function UpdateOrder($orders=array()){
//            $data= array();
//            foreach($orders as $id=>$order){
//                $data[] = array(
//                    'id'=>$id,
//                    'order'=>$order
//                );
//            }
//            $this->db->update_batch($this->tbname, $data, 'id'); 
            $this->UpdateBatch('order',$orders);
	}	
	function UpdateBatch($field,$orders=array()){
            $data= array();
            if(is_array($orders) && $orders){
                foreach($orders as $id=>$v){
                    $data[] = array(
                        'id'=>$id,
                        $field=>$v
                    );
                }
                $this->db->update_batch($this->tbname, $data, 'id'); 
            }
	}
        
//        
//        
//
//    /**
//     * Return the method name for the current return type
//     */
//    protected function _return_type($multi = FALSE)
//    {
//        $method = ($multi) ? 'result' : 'row';
//        return $this->_temporary_return_type == 'array' ? $method . '_array' : $method;
//    }
}



class MY_Table extends MY_Model
{
    protected $tbname;
    protected $tblang;
    
    public function __construct($tbname='')
    {
        parent::__construct();
        $this->tbname = $tbname;
        $this->tblang = $tbname . "_lang";
    }

    function GetBySlug($slug,$lang='')
    {
        $post = parent::GetBySlug($slug);
        if($post && $lang):
            $post['_item'] = $this->GetItem($post['id'],$lang);
        endif;
        return $post;
    }
    
    function GetItem($pid,$lang)
    {
        $this->db->from($this->tblang);
        $array = array(
            'pid'=>$pid,
            'lang'=>$lang
        );
        $this->db->where($array); 
        $r = $this->db->get()->result_array();
        if( is_array($r)) {
          return $r[0];
        }else{
          return false;
        }
    }
    
    function SaveItem($data,$lang,$pid){
        $row = $this->GetItem($pid,$lang);
		
        if (empty($row))
	{
             return $this->AddItem($data,$lang,$pid);
        }else{
            return $this->UpdateItem($data,$lang,$pid);
        }
    }
    function AddItem($arg,$lang,$pid){
        if(!is_array($arg)){
            return false;
        }
        $data = $arg;
        $data['lang'] = $lang;
        $data['pid'] = $pid;
        if($this->db->insert($this->tblang,$data)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    function UpdateItem($arg,$lang,$pid){
        if(!is_array($arg)){
                return false;
        }
         $this->db->where(array('lang'=>$lang,'pid'=>$pid));
         $r = $this->db->update($this->tblang, $arg);    
         return ($r)?$id:false;
    }

    
    function Remove($id)
    {
        parent::Remove($id);
        $this->db->delete($this->tblang, array('pid' => $id)); 
    }
}
?>
