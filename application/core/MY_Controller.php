<?php
class MY_Controller extends CI_Controller{
    protected $data;
    public function __construct()
    {
      parent::__construct();

      if( !$this->session->userdata('isAdmLoggedIn') ) {
          redirect('/admin/login/show_login');
      }
      $lang = $this->input->get('lang',true);
      if(empty($lang)):
          $lang = $this->session->userdata('lang');
      endif;
      if(empty($lang)):
          $lang = $this->config->item('lang_default');
      endif;
      
      $this->session->set_userdata('lang',$lang);
      
      $lang_list = $this->config->item('lang_list');
      $lang_name = $lang_list[$lang]['name'];
      
//      switch($lang):
//          case 'zh_CN':
//              $lang_name = 'chinese';
//              break;
//          case 'en_US':
//              $lang_name = 'english';
//              break;
//      endswitch;
      $this->lang->load("message",$lang_name);
      
      $data['lang'] = $lang;
      $data['lang_name'] = $lang_name;
      $data['lang_list'] = $lang_list;
      $data['email'] = $this->session->userdata('adm_email');
      $data['username'] = $this->session->userdata('adm_username');
      
      
      $nav = $this->input->get('nav',true);
      
      if($nav && !is_array($nav)):
          $nav = array($nav);
      endif;
      $data['nav'] = $nav;
      
      $this->data = $data;
  }
}


class MY_User extends CI_Controller{
    protected $data;
    public function __construct()
    {
      parent::__construct();

      $lang = $this->input->get('lang',true);
      if(empty($lang)):
          $lang = $this->session->userdata('lang');
      endif;
      if(empty($lang)):
          $lang = 'en_US';
      endif;
      
      
      $this->session->set_userdata('lang',$lang);
      
      $lang_list = $this->config->item('lang_list');
      $lang_name = $lang_list[$lang]['name'];
      
      $this->lang->load("message",$lang_name);
      $this->session->set_userdata('lang',$lang);
      $data['lang'] = $lang;
      $data['lang_list'] = $lang_list;
      $data['sitemap'] = $this->config->item('sitemap');
      $data['sitemap_label'] = $this->config->item('sitemap_label');
      
      
      
      //get city list
      $content = '';
      $city_list = array();
        read('assets/city.json',$content);
        if($content):
            $city_list = json_decode($content,TRUE);
        endif;
        $data['city_list'] = $city_list;
          
      
      
      //get latest news
        $this->load->model('news_model');
        $condition = array();
        $condition[] = array('where','category','e_newsletter');
        $condition[] = array('where','delete',0);
        $condition[] = array('order','order','desc');
        $post = $this->news_model->GetFirst($condition);
        $item = $this->news_model->GetItem($post['id'],$lang);
        if($item):
            $post['title'] = $item['title'];
            $post['subtitle'] = $item['subtitle'];
            $post['excerpt'] = $item['excerpt'];
            $post['content'] = $item['content'];
        endif;
        $data['news']['e_newsletter'] = $post;
        
        $condition = array();
        $condition[] = array('where','category','blog');
        $condition[] = array('where','delete',0);
        $condition[] = array('order','order','desc');
        $post = $this->news_model->GetFirst($condition);
        $item = $this->news_model->GetItem($post['id'],$lang);
        if($item):
            $post['title'] = $item['title'];
            $post['subtitle'] = $item['subtitle'];
            $post['excerpt'] = $item['excerpt'];
            $post['content'] = $item['content'];
        endif;
        $data['news']['blog'] = $post;
        
        $this->load->model('job_model');
        $condition = array();
        $condition[] = array('where','delete',0);
        $condition[] = array('order','order','desc');
        $post = $this->job_model->GetFirst($condition);
//        $item = $this->job_model->GetItem($post['id'],$lang);
//        if($item):
//            $post['title'] = $item['title'];
//            $post['subtitle'] = $item['subtitle'];
//            $post['excerpt'] = $item['excerpt'];
//            $post['content'] = $item['content'];
//        endif;
        $data['news']['job'] = $post;
        
      $this->data = $data;    
        
//      $this->session->set_userdata('lang',$lang);
//      $data['lang'] = $lang;
//      $user['id'] = $this->session->userdata('id');
//      $user['email'] = $this->session->userdata('email');
//      $user['username'] = $this->session->userdata('username');
//      $data['user'] = $user;
//      $data['sitemap'] = $this->config->item('sitemap');
//      $data['sitemap_label'] = $this->config->item('sitemap_label');
//      $this->data = $data;
  }
}
?>
