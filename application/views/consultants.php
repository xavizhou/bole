<?php include 'inc/inc.head.php';?>
    <body class="page-consultant">
<?php include 'inc/inc.header-v.php';?>


<div class="banner"></div>
<div class="consultant">
<div class="title">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_meet_consultants');?></h1>
            </div>
            <div class="col-md-5 text-subnav">
                    <a href='team'><?php echo $this->lang->line('nav_meet_team');?></a>
                    
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' />
                    </a>
            </div>
        </div>
    </div>
</div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="location col-xs-12">
                        <?php echo $this->lang->line('check_office2');?>
                    <div class="other-office">
                        <div class="">
                        <button type="button" class="btn btn-default drop-group hidden-xs" id='checkStar' data-rel="checkStarDropdown">
                            <?php echo $this->lang->line('check_other_office');?>
                            <i class="fa fa-angle-down"></i>
                          </button>
                    <div class="dropdown hidden check-office-dropdown" id="checkStarDropdown">
                    <?php include('inc/inc.city.php')?>
                    </div>
                        </div>

                    </div>
                    </div>
                </div>
                <div class="row">
                <div class="select-group">
                    
                          <div class="btn-group select visible-xs btn-group-city">
                              <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php 
                                $flg = false;
                                foreach($city_list as $k=>$v):
                                    if($k == $url_para['city']):
                                        $flg = true;
                                        echo $v[$lang];
                                        break;
                                    endif;
                                endforeach;
                                if(!$flg):
                                    echo $this->lang->line('consultant_choose_city');
                                endif;
                                ?>
                                <i class="fa fa-angle-down dn"></i>
                              </button>
                              <ul class="dropdown-menu">
                                  <?php
                                    if($flg):
                                      $_para = $url_para;
                                      $_para['city'] = '';
                                      $_url = 'consultant/?'.http_build_query($_para);
                                        echo '<li><a href="'.$_url.'">'.$this->lang->line('consultant_choose_city').'</a></li>';
                                    endif;
                                  foreach($city_list as $k=>$v):
                                      $_para = $url_para;
                                      $_para['city'] = $k;
                                      $_url = 'consultant/?'.http_build_query($_para);
                                      echo '<li><a href="'.$_url.'">'.$v[$lang].'</a></li>';
                                  endforeach;
                                  ?>
                              </ul>
                            </div>
                    
                    
                          <div class="btn-group select">
                              <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php 
                                $flg = false;
                                foreach($industry_list as $v):
                                    if($v == $url_para['industry']):
                                        $flg = true;
                                        echo $v;
                                        break;
                                    endif;
                                endforeach;
                                if(!$flg):
                                    echo $this->lang->line('consultant_choose_industry');
                                endif;
                                ?>
                                <i class="fa fa-angle-down dn"></i>
                              </button>
                              <ul class="dropdown-menu">
                                  <?php
                                    if($flg):
                                      $_para = $url_para;
                                      $_para['industry'] = '';
                                      $_url = 'consultant/?'.http_build_query($_para);
                                        echo '<li><a href="'.$_url.'">'.$this->lang->line('consultant_choose_industry').'</a></li>';
                                    endif;
                                  foreach($industry_list as $v):
                                      $_para = $url_para;
                                      $_para['industry'] = $v;
                                      $_url = 'consultant/?'.http_build_query($_para);
                                      echo '<li><a href="'.$_url.'">'.$v.'</a></li>';
                                  endforeach;
                                  ?>
                              </ul>
                            </div>
<!--                          <div class="btn-group select">
                              <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Large button <i class="fa fa-angle-down dn"></i>
                              </button>
                              <ul class="dropdown-menu">
                                                        <li><a href="#">Meet Consultants</a></li>
                                                        <li><a href="#">Meet Consultants</a></li>
                              </ul>
                            </div>-->

                          <div class="btn-group pull-right filter_star">
                              <?php
                                    $_para = $url_para;
                                    if($url_para['star']):
                                      $_para['star'] = 0;
                                      $_url = 'consultant/?'.http_build_query($_para);
                                      $img = "<img src='assets/img/icon-check-v.png' />";
                                    else:
                                      $_para['star'] = 1;
                                      $_url = 'consultant/?'.http_build_query($_para);
                                      $img = "<img src='assets/img/icon-check.png' />";
                                    endif;
                              ?>
                              <a href="<?php echo $_url;?>">
                              <?php echo $img.$this->lang->line('nav_star_consultants');?>
                              </a>
                            </div>
                </div>
<!--                 <div class="pull-right">
                    <select>check other office </select>
                </div> -->
                </div>
                
                <div class="list">
                    <?php
                    for($i=0;$i<count($posts);$i++):
                        $post = $posts[$i];
                    ?>
                    <div class="item" onclick="window.location.href='consultant/detail?id=<?php echo $post['id']?>'">
                        <img src="uploads/<?php echo $post['thumb'];?>" />
                        <div class="text">
                            <h2><?php echo $post['title'];?></h2>
                            <h5><?php echo $post['subtitle'];?></h5>
                        </div>
                    </div>
                    <?php    
                    endfor;
                    ?>
                </div>

            </div>
        </div>
    </div>


    <div class="loading hidden">
        <img src="assets/img/loading.png" />
    </div>
</div>



<?php include 'inc/inc.news.php';?>




<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    </script>
    </body>
</html>