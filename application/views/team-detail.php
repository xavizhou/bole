<?php include 'inc/inc.head.php';?>
    <body class="page-consultant-detail">

<?php include 'inc/inc.header-v.php';?>


<div class="consultant">
<div class="title">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <?php echo $this->lang->line('consultant_meet');?> 
                <h1 class=""><?php echo $post['title'];?> </h1>
            </div>
            <div class="col-md-5 text-subnav">
                <a href='javascript:history.go(-1)' class="btn has-icon btn-default">
                    <span><?php echo $this->lang->line('go_back');?> </span>
                    <img src='assets/img/icon-arrow-left.png' />
                </a>
            </div>
        </div>
    </div>
</div>

    
<?php
if($post['template'] == 1):
?>
    <div class="pic">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class='row thumb' style="background-image:url(uploads/<?php echo $post['thumb']?>)" ></div>
            </div>
            <div class="col-md-1 hidden-xs">
                <div class='row intro'>
                </div>
            </div>
            <div class="col-md-2">
                <div class='row intro'>
                    <h2><?php echo $post['subtitle']?></h2>
                    <a href="consultant/detail?id=<?php echo $post['id']?>#aForm" class="a-message">
                        <?php echo $this->lang->line('leave_a_message');?>
                    </a>
                </div>
            </div>
            <div class="col-md-2 hidden-xs">
                <div class="share row">
                    <?php include 'inc/inc.share.php';?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
else:
?>    
    
<div class="pic">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class='row thumb' style="background-image:url(uploads/<?php echo $post['thumb']?>)" ></div>
            </div>
            <div class="col-md-1 hidden-xs">
                <div class='row intro'>
                </div>
            </div>
            <div class="col-md-6">
                <div class='row intro'>
                    <h2><?php echo $post['subtitle']?></h2>
                    <a href="consultant/detail?id=<?php echo $post['id']?>#aForm" class="a-message">
                        <?php echo $this->lang->line('leave_a_message');?>
                    </a>
                </div>
            </div>
            <div class="col-md-2 hidden-xs">
                <div class="share row">
                    <?php include 'inc/inc.share.php';?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
endif;
?>    
    

<div class="detail">
    <div class=" container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <?php echo $post['content']?>
            </div>
        </div>
    </div>
</div>


</div>

    
        <a name="aForm" id="aForm"></a>
<div class='form form-cv' data-action="submit/consultant">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1><?php echo $this->lang->line('leave_a_message');?></h1>
            </div>
            <div class="col-md-4 col-md-offset-2">
                <div class="input-form">
                    <input type="text" required="required" placeholder="|  <?php echo $this->lang->line('ph_name');?>" />
                    <span class="required">*</span>
                </div>
                <div class="input-form">
                    <input type="text" required="required" placeholder="|  <?php echo $this->lang->line('ph_position');?>" />
                    <span class="required">*</span>
                </div>
                <div class="input-form">
                <input type="text"  placeholder="|  <?php echo $this->lang->line('ph_company');?>" />
                </div>
                <div class="input-form">
                <input type="text"  placeholder="|  <?php echo $this->lang->line('ph_email');?>" />
                </div>
                <div class="input-form">
                <input type="text"  placeholder="|  <?php echo $this->lang->line('ph_position');?>" />
                </div>
                
            </div>
            <div class="col-md-5">
                <div class="input-form">
                    <textarea placeholder="|  Message"></textarea>
                </div>
                <div class="input-form input-file">
                    <?php echo $this->lang->line('form_drag_cv');?>
                    <em><?php echo $this->lang->line('form_cv_tips');?></em>
                    <hr />
                    <input type="file" class="hidden" name="cv_file" />
                    <input type="hidden" class="hidden" name="id" value="<?php echo $post['id'];?>" />
                    <a href='javascript:void(0)' class='btn btn-choose-file'><?php echo $this->lang->line('form_choose_file');?></a> |  
                    <b class="filename"><?php echo $this->lang->line('form_no_file');?></b>
                </div>
            </div>


            <div class="col-md-11 text-right input-button">
                <a href='' class='btn btn-default'><?php echo $this->lang->line('Reset');?></a>
                <a href='' class='btn btn-default btn-submit'><?php echo $this->lang->line('Send');?></a>
            </div>
        </div>
    </div>
</div>


<?php include 'inc/inc.news.php';?>






<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    </script>
    </body>
</html>