<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
//switch(count($nav)):
//    case 2:
//        $page_nav[$nav[0]]["sub"][$nav[1]]["active"] = true;
//        break;
//    case 1:
//        $page_nav[$nav[0]]["active"] = true;
//        break;
//endswitch;
    $page_nav['meet_us']["sub"]['management_team']["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["team"] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa fa-pencil-square-o fa-fw "></i> 
						<?php echo $this->lang->line("Team");?>
				</h1>
			</div>
		</div>
		
		<!-- widget grid -->
		<section id="widget-grid" class="">
			<!-- row -->
			<div class="row">
		
				<!-- NEW WIDGET START -->
				<article class="col-sm-12 col-md-12 col-lg-12">
		
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-editbutton="false">
						
						<header>
                                                    
						</header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body">
                                                            <?php echo @flash_message();?>
                                                            <div class="widget-body-toolbar">
                                                               <form action="<?php echo APP_URL?>team/save_batch" method="post">
                                                                   <input name="SortOrder" type="hidden" value="-1" />
                                                                    <a class="btn btn-info btn-md " href="<?php echo APP_URL.'team/edit'?>"><?php echo $this->lang->line("Add New");?></a> 
                                                                    <button class="btn btn-warning btn-md hidden" disabled="" id="saveButton" type="submit">Save</button> 
                                                               </form>
                                                            </div>
		
                                                        
                                                            <?php
                                                            //thumb
                                                            if($posts):
                                                                echo '<ul id="teamList">';
                                                                for($i=0;$i<count($posts);$i++):
                                                            ?>
                                                                <li data-id="<?php echo $posts[$i]['id'];?>" class="col-xs-6 col-sm-3 col-lg-2" >
                                                                    <div class="box" style="background-image:url(<?php echo base_url().'uploads/'.$posts[$i]['thumb'] ;?>)">
                                                                        <div class="text">
                                                                        <?php echo $posts[$i]['title'];?>
                                                                        </div>
                                                                        <div class="tool">
                                                                            <a href="<?php echo APP_URL?>team/edit?id=<?php echo $posts[$i]['id'];?>" class="btn btn-xs btn-default btn-edit"><span class="fa fa-pencil"></span></a>
                                                                            <a href="#" class="btn btn-xs btn-default btn-remove"><span class="fa fa-times"></span></a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php

                                                                endfor;
                                                                echo '</ul>';
                                                            endif;
                                                            ?>
		
							<div class="clearfix"></div>
                                                            
                                                        </div>
							<!-- end widget content -->
		
						</div>
						<!-- end widget div -->
		
					</div>
					<!-- end widget -->
		
				</article>
				<!-- WIDGET END -->
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
		
<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script> -->


<script src="<?php echo ASSETS_URL; ?>/js/plugin/jquery-dragsort/jquery.dragsort-0.5.2.min.js"></script>


<script type="text/javascript">
    var _ID_REMOVE = [];
$(document).ready(function() {
    $("#teamList").dragsort({ dragSelector: "div", dragBetween: true, dragEnd: saveOrder, placeHolderTemplate: "<li class='placeHolder'><div></div></li>" });

    $("#teamList li .btn-remove").click(function(){
        var o = $(this).parents('li');
        var id = o.data('id');
        _ID_REMOVE.push(id);
        //o.addClass('danger');
        o.remove();
        $('#saveButton').prop('disabled',false);
        $('#saveButton').removeClass('hidden');
        var data = $("#teamList li").map(function() { return $(this).data('id'); }).get();		
        $("input[name=SortOrder]").val(data.join(","));
        return false;
    })
    
    $('#saveButton').click(function(){
        $(this).prop('disabled',true);
        $(this).parents('form').submit();
//        save();
    })

    function saveOrder() {
        var data = $("#teamList li").map(function() { return $(this).data('id'); }).get();		
        $("input[name=SortOrder]").val(data.join(","));
        $('#saveButton').prop('disabled',false);
        $('#saveButton').removeClass('hidden');
    };
    
//    function save(){
//        var order = $('input[name=SortOrder]').val();
//        if(order.length < 1) return;
//        $('#saveButton').prop('disabled',true);
//        $.ajax({
//            url: '<?php echo APP_URL?>team/ajax_save_batch',
//            type: 'POST',
//            data: 'order'+order,
//            dataType: "json",
//            contentType: "application/x-www-form-urlencoded; charset=utf-8",
//            success: function (data, status, jqXHR) {
//                if(data.status === 'success'){
//                    $('#sortStatus').removeClass('alert-danger out').addClass('alert-success in');
//                    $('#sortStatus').html('<button class="close" data-dismiss="alert">&times;</button><i class="fa-fw fa fa-check"></i>'+data.message);
//                }else{
//                    $('#sortStatus').removeClass('alert-success out').addClass('alert-danger in');
//                    $('#sortStatus').html('<button class="close" data-dismiss="alert">&times;</button><i class="fa-fw fa fa-times"></i>'+data.message);
//                }
//                $('#saveButton').prop('disabled',false);
//            }
//        });
//        
//    }

})

</script>
<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>