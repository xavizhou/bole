<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = ($post)?$this->lang->line("Edit"):$this->lang->line("New");

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");
//switch(count($nav)):
//    case 2:
//        $page_nav[$nav[0]]["sub"][$nav[1]]["active"] = true;
//        break;
//    case 1:
//        $page_nav[$nav[0]]["active"] = true;
//        break;
//    default:
//        
//        break;
//endswitch;
    $page_nav['meet_us']["sub"]['consultants']["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["consultant"] = APP_URL.'consultant';
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa fa-pencil-square-o fa-fw "></i> 
						<?php echo $this->lang->line("nav_consultants");?>
				</h1>
			</div>
		</div>
		
		<!-- widget grid -->
		<section id="widget-grid" class="">
			<!-- row -->
			<div class="row">
				<!-- NEW WIDGET START -->
				<article class="col-sm-12 col-md-12 col-lg-12">
		
                            <?php echo @flash_message()?>
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-editbutton="false">
						<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
		
						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"
		
						-->
						<header>
						</header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body">
                                                            <form action="<?php echo APP_URL?>consultant/save" method="post" id="pageForm" enctype="multipart/form-data"  class="smart-form" novalidate="novalidate">
                                                        
									<fieldset>
                                                                            <section class="">
                                                                                <label class="label"><?php echo $this->lang->line("Name");?></label>
                                                                                <label class="input">
                                                                                    <input type="text" name="title" placeholder="<?php echo $this->lang->line("ph_title");?>" value="<?php echo $post['title'];?>">
                                                                                </label>
                                                                            </section>
                                                                            <section class="">
                                                                                <label class="label"><?php echo $this->lang->line("Cons_Title");?></label>
                                                                                <label class="input">
                                                                                    <input type="text" name="subtitle" placeholder="<?php echo $this->lang->line("ph_cons_title");?>" value="<?php echo $post['subtitle'];?>">
                                                                                </label>
                                                                            </section>
                                                                            
                                                                            <section class="">
                                                                                <label class="label"><?php echo $this->lang->line("Email");?></label>
                                                                                <label class="input">
                                                                                    <input type="email" name="email" placeholder="<?php echo $this->lang->line("ph_email");?>" value="<?php echo $post['email'];?>">
                                                                                </label>
                                                                            </section>
                                                                            
                                                                            <section class="">
                                                                                <label class="label"><?php echo $this->lang->line("Cons_Parctice");?></label>
                                                                                <label class="input">
                                                                                    <input type="text" name="practice" placeholder="<?php echo $this->lang->line("ph_cons_practice");?>" value="<?php echo $post['practice'];?>">
                                                                                </label>
                                                                            </section>
                                                                            
                                                                            <section class="">
                                                                                <label class="label"><?php echo $this->lang->line("Cons_Language");?></label>
                                                                                <label class="input">
                                                                                    <textarea name="language_skill" class="form-control summernote" placeholder="<?php echo $this->lang->line("ph_cons_language");?>" ><?php echo $post['language_skill'];?></textarea>
                                                                                </label>
                                                                            </section>
                                                                            
                                                                            <section class="">
                                                                                <label class="label"><?php echo $this->lang->line("Cons_Resume");?></label>
                                                                                <label class="input">
                                                                                    <textarea name="content" class="form-control summernote" placeholder="<?php echo $this->lang->line("ph_content");?>" ><?php echo $post['content'];?></textarea>
                                                                                </label>
                                                                            </section>
                                                                            
                                                                            
                                                                            <section>
                                                                                <?php
                                                                                if($post['thumb']):
                                                                                    echo '<input type="hidden" name="thumb" value="'.$post['thumb'].'">';
                                                                                    echo '<div class="thumb"><img src="'.base_url().'uploads/'.$post['thumb'].'"></div>';
                                                                                endif;
                                                                                ?>
										<div class="input input-file">
                                                                                    <span class="button"><input type="file" name="thumb_upload" onchange="this.parentNode.nextSibling.value = this.value"><?php echo $this->lang->line("Browse");?>(300x320)</span><input type="text" placeholder="<?php echo $this->lang->line("ph_thumb");?>" readonly="">
                                                                                </div>
                                                                            </section>
									<fieldset>
                                                                            <section>
										<label class="checkbox">
                                                                                    <input type="checkbox" name="template" value="1" <?php echo ($post['template']==1)?'checked':'';?>>
                                                                                        <i></i>Big Image(780 x 320)</label>   
                                                                                </section>
                                                                        </fieldset>


									</fieldset>

									
									<fieldset>
                                                                            <?php
                                                                            if($city_list):
                                                                                $_tmp = explode(",", $post['loc']);
                                                                                ?>
                                                                            <section>
										<div class="inline-group">
                                                                            <?php
                                                                                foreach($city_list as $k=>$v):
                                                                                ?>
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox" name="loc[]" value="<?php echo $k;?>" <?php echo (in_array($k, $_tmp))?'checked':'';?>>
										<i></i><?php echo $v[$lang];?></label>    
                                                                                <?php
                                                                                endforeach;
                                                                                ?>
											</div>
										</section>
                                                                            <?php
                                                                            endif;
                                                                            ?>
                                                                        </fieldset>

									<fieldset>
                                                                            <?php
                                                                            if($industry_list):
                                                                                $_tmp = explode(",", $post['industry']);
                                                                                ?>
                                                                            <section>
										<div class="inline-group">
                                                                            <?php
                                                                                foreach($industry_list as $k=>$v):
                                                                                ?>
                                                                                <label class="checkbox">
                                                                                    <input type="checkbox" name="industry[]" value="<?php echo $k;?>" <?php echo (in_array($k, $_tmp))?'checked':'';?>>
										<i></i><?php echo $v;?></label>    
                                                                                <?php
                                                                                endforeach;
                                                                                ?>
											</div>
										</section>
                                                                            <?php
                                                                            endif;
                                                                            ?>
                                                                        </fieldset>

									<fieldset>
                                                                            <section>
										<label class="checkbox">
                                                                                    <input type="checkbox" name="star" value="1" <?php echo ($post['status']==2)?'checked':'';?>>
                                                                                        <i></i><?php echo $this->lang->line('ph_star');?></label>   
                                                                                </section>
                                                                        </fieldset>
                                                                
                                                                
									<footer>
                                                                            <input type="hidden" name="id" value="<?php echo $post['id'];?>">
										<button type="submit" class="btn btn-primary">
											<?php echo $this->lang->line("Submit");?>
										</button>
									</footer>
								</form>
                                                            
		
							</div>
							<!-- end widget content -->
		
						</div>
						<!-- end widget div -->
		
					</div>
					<!-- end widget -->
		
				</article>
				<!-- WIDGET END -->
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
		
<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script> -->

<script src="<?php echo ASSETS_URL; ?>/js/plugin/jquery-form/jquery-form.min.js"></script>

<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>/css/summernote.css">
<script src="<?php echo ASSETS_URL; ?>/js/plugin/summernote/summernote.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/summernote/summernote-zh-CN.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/jquery-dragsort/jquery.dragsort-0.5.2.min.js"></script>

<script type="text/javascript">
    var _ID_REMOVE = [];
    $("#teamList").dragsort({ dragSelector: "div", dragBetween: true, dragEnd: saveOrder, placeHolderTemplate: "<li class='placeHolder'><div></div></li>" });

    $("#teamList li .btn-remove").click(function(){
        var o = $(this).parents('li');
        var id = o.data('id');
        _ID_REMOVE.push(id);
        o.remove();
        $('#saveButton').prop('disabled',false);
        $('#saveButton').removeClass('hidden');
        var data = $("#teamList li").map(function() { return $(this).data('id'); }).get();		
        $("input[name=SortOrder]").val(data.join(","));
    })

    function saveOrder() {
        var data = $("#teamList li").map(function() { return $(this).data('id'); }).get();		
        $("input[name=SortOrder]").val(data.join(","));
        $('#saveButton').prop('disabled',false);
        $('#saveButton').removeClass('hidden');
    };
    
    
	$(document).ready(function() {
            //this.parentNode.nextSibling.value

                function sendFile(file, editor) {
                    data = new FormData();
                    data.append("userfile", file);
                    $.ajax({
                        data: data,
                        type: "POST",
                        url: "<?php echo site_url()?>upload/summernote",
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(url) {
                            editor.summernote("insertImage", '<?php echo base_url()?>'+url, 'test'); 
                        }
                    });
                }
                

                $('.summernote').summernote({
                        height : 240,
                        focus : false,
                        tabsize : 2,
                        lang: 'zh-CN',
                        toolbar: [
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['picture', ['picture']],
                        ['insert', ['link','fullscreen']]
                      ],
                    callbacks: {
                        onImageUpload: function(files, editor, welEditable) {
                            sendFile(files[0], $('.summernote'));
                        }
                    }
                });
                
		var $checkoutForm = $('#pageForm').validate({
			rules : {
				title : {
					required : true
				}
			},

			// Messages for form validation
			messages : {
				title : {
					required : '<?php echo $this->lang->line("valid_req_title");?>'
				}
			},
//                    beforeSubmit : function(form) {
//                        var aHTML = $('.summernote').code(); //save HTML If you need(aHTML: array).
//                        $('textarea[name="content"]').html(aHTML);
//                    },

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element.parent());
			}
		});

	})

</script>

<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>