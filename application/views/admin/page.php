<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = ($post)?$post['title']:'';

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
if($nav):
    switch(count($nav)):
        case 2:
            $page_nav[$nav[0]]["sub"][$nav[1]]["active"] = true;
            break;
        case 1:
            $page_nav[$nav[0]]["active"] = true;
            break;
    endswitch;
else:
    switch($slug):
        case 'origin_of_bole':
        case 'bole_history':
        case 'about_bole_associate':
        case 'bole_associates_service':
        case 'standard_procedure':
            $page_nav['who_are_we']["sub"][$slug]["active"] = true;
            break;
        case 'workwithus':
            $page_nav['workwithus']["active"] = true;
            break;
    endswitch; 
endif;


include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs[$this->lang->line("nav_".$nav[0])] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa fa-pencil-square-o fa-fw "></i> 
						
				</h1>
			</div>
		</div>
		
		
                        <?php
                        echo @flash_message();
                        ?>
		<!-- widget grid -->
		<section id="widget-grid" class="">
			<!-- row -->
			<div class="row">
		
				<!-- NEW WIDGET START -->
				<article class="col-sm-12">
		
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-0" data-widget-colorbutton="false" data-widget-editbutton="false">
						<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
		
						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"
		
						-->
						<header>
						</header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body">
                                                        <form action="<?php echo APP_URL?>page/save" method="post" id="pageForm" enctype="multipart/form-data"  class="smart-form" novalidate="novalidate">
                                                                <fieldset>
                                                                    <section class="">
                                                                        <label class="label"><?php echo $this->lang->line("Title");?></label>
                                                                        <label class="input">
                                                                            <input type="text" name="title" placeholder="Title" value="<?php echo $post['title'];?>">
                                                                        </label>
                                                                    </section>

                                                                    <section class="">
                                                                        <label class="label"><?php echo $this->lang->line("Content");?></label>
                                                                        <label class="input">
                                                                            <textarea name="content" class="form-control summernote" placeholder="<?php echo $this->lang->line("ph_content");?>" ><?php echo $post['content'];?></textarea>
                                                                        </label>
                                                                    </section>
                                                                </fieldset>


                                                                <footer>
                                                                        <button type="submit" class="btn btn-primary">
                                                                                <?php echo $this->lang->line("Submit");?>
                                                                        </button>
                                                                </footer>
                                                            
                                                            <input type="hidden" name="slug" value="<?php echo $slug;?>" />
                                                            <input type="hidden" name="lang" value="<?php echo $lang;?>" />
                                                            <input type="hidden" name="id" value="<?php echo $post['id'];?>" />
                                                            </form>
                                                            
		
							</div>
							<!-- end widget content -->
		
						</div>
						<!-- end widget div -->
		
					</div>
					<!-- end widget -->
		
				</article>
				<!-- WIDGET END -->
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
		
<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script> -->

<script src="<?php echo ASSETS_URL; ?>/js/plugin/jquery-form/jquery-form.min.js"></script>

<!--<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>/css/summernote.css">
<script src="<?php echo ASSETS_URL; ?>/js/plugin/summernote/summernote.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/summernote/summernote-zh-CN.js"></script>-->

<link href="//cdn.bootcss.com/summernote/0.8.1/summernote.css" rel="stylesheet">
<script src="//cdn.bootcss.com/summernote/0.8.1/summernote.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
            
            function sendFile(file, editor) {
                data = new FormData();
                data.append("userfile", file);
                $.ajax({
                    data: data,
                    type: "POST",
                    url: "<?php echo site_url()?>upload/summernote",
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(url) {
                        editor.summernote("insertImage", '<?php echo base_url()?>'+url, 'test'); 
                    }
                });
            }


            $('.summernote').summernote({
                    height : 240,
                    focus : false,
                    tabsize : 2,
                    lang: 'zh-CN',
                    toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['picture', ['picture']],
                    ['insert', ['media','video','table','link','fullscreen']]
                  ],
                callbacks: {
                    onImageUpload: function(files, editor, welEditable) {
                        sendFile(files[0], $('.summernote'));
                    }
                }
            });

            var $pageForm = $('#pageForm').validate({
                    rules : {
                            title : {
                                    required : true
                            }
                    },

                    // Messages for form validation
                    messages : {
                            title : {
                                    required : 'Please enter your title'
                            }
                    },
                    beforeSubmit : function(form) {
                        var aHTML = $('.summernote').code(); //save HTML If you need(aHTML: array).
                        $('textarea[name="content"]').html(aHTML);
                    },
                    // Do not change code below
                    errorPlacement : function(error, element) {
                            error.insertAfter(element.parent());
                    }
            });
	})

</script>

<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>