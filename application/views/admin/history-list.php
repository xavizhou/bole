<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//switch(count($nav)):
//    case 2:
//        $page_nav[$nav[0]]["sub"][$nav[1]]["active"] = true;
//        break;
//    case 1:
//        $page_nav[$nav[0]]["active"] = true;
//        break;
//endswitch;
$page_nav['who_are_we']["sub"]['bole_history']["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["history"] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa fa-pencil-square-o fa-fw "></i> 
					<?php echo $this->lang->line("nav_historys");?>
				</h1>
			</div>
		</div>
		
		<!-- widget grid -->
		<section id="widget-grid" class="">
    <?php echo @flash_message(); ?>
			<!-- row -->
			<div class="row">
		
				<!-- NEW WIDGET START -->
				<article class="col-sm-12 col-md-12 col-lg-12">
		
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-0" data-widget-deletebutton="false"  data-widget-colorbutton="false" data-widget-editbutton="false">
						
						<header>
						</header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body no-padding">
		
                                                            <div class="widget-body-toolbar">
                                                               <a class="btn btn-info btn-md " href="<?php echo APP_URL.'history/edit'?>"><?php echo $this->lang->line("Add New");?></a> 
                                                               <a class="btn btn-danger btn-md btn-remove disabled" href=""><?php echo $this->lang->line("Delete");?></a> 
                                                               
                                                                
                                                            </div>
						        <table id="dt_basic" class="table table-bordered table-striped table-condensed table-hover smart-form has-tickbox" width="100%">
									<thead>	
                                                                                
										<tr>
                                                                                    <th>
<!--                                                                                        <label class="checkbox">
												<input type="checkbox" name="checkbox-inline">
												<i></i> 
                                                                                        </label>-->
                                                                                    </th>
                                                                                    <th width="20"><?php echo $this->lang->line("ID");?></th>
                                                                                    <th><?php echo $this->lang->line("His_Title");?></th>
                                                                                    <th>
                                                                                        <?php echo $this->lang->line("His_Date");?>
                                                                                    </th>
<!--                                                                                    <th>Updated Date</th>-->
										</tr>
									</thead>
									<tbody>
                                                                            <?php
                                                                            if($posts):
                                                                                for($i=0;$i<count($posts);$i++):
                                                                            ?>
										<tr>
                                                                                    <td>
                                                                                        <label class="checkbox">
                                                                                            <input type="checkbox" name="id[]" class="chk_id" value="<?php echo $posts[$i]['id'];?>">
												<i></i> 
                                                                                        </label>
                                                                                    </td>
                                                                                    <td><?php echo $i+1;?></td>
                                                                                    <td>
                                                                                        <a href="<?php echo APP_URL?>history/edit?id=<?php echo $posts[$i]['id'];?>" ><?php echo $posts[$i]['title'];?></a>
                                                                                    </td>
                                                                                    <td><?php echo $posts[$i]['date'];?></td>
                                                                                    
										</tr>
                                                                            <?php
                                                                            
                                                                                endfor;
                                                                            endif;
                                                                            ?>
										
									</tbody>
								</table>
		
							</div>
                                                            
		
							</div>
							<!-- end widget content -->
		
						</div>
						<!-- end widget div -->
		
					</div>
					<!-- end widget -->
		
				</article>
				<!-- WIDGET END -->
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
		
<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script> -->


<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
    
    var _URL_DEL = '<?php echo APP_URL.'history/remove'?>';
    $('.btn-remove').click(function(e){
        if($(this).hasClass('disabled')){
            return false;
        }
        var s = '';
        $('input.chk_id:checked').each(function(){
            (s==='')?s=$(this).val():s+=','+$(this).val();
        })
        if(s === '') return false;
        window.location.href = _URL_DEL+'?id='+s;
        return false;
    })
    
    $('input.chk_id').on( 'change', function () {
        if($('input.chk_id:checked').size() > 0){
            $('.btn-remove').removeClass('disabled');
        }else{
            $('.btn-remove').addClass('disabled');
        }
    });
	
	/* // DOM Position key index //
		
	l - Length changing (dropdown)
	f - Filtering input (search)
	t - The Table! (datatable)
	i - Information (records)
	p - Pagination (paging)
	r - pRocessing 
	< and > - div elements
	<"#id" and > - div with an id
	<"class" and > - div with a class
	<"#id.class" and > - div with an id and class
	
	Also see: http://legacy.datatables.net/usage/features
	*/	

	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};



    
    
		var otable = $('#dt_basic').DataTable({
                        "aaSorting": [[ 1, "asc" ]],
                        
                        "language": {
                            "url":"<?php echo ASSETS_URL?>/js/plugin/datatables/i18n/<?php echo ucfirst($lang_name)?>.lang"
                        },
                        "aoColumnDefs": [
                            { "sWidth": "150px", "aTargets": ["th_date"] }
                        ],
                        
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});
                
                <?php
                $s = '';
                if($history_category_list):
                    $s = '<option value="">--Category--</option>';
                    foreach($history_category_list as $k=>$v):
                        $s .= '<option value="'.$v.'">'.$v.'</option>';
                    endforeach;
                endif;
                if($s):
                    $s = str_replace("'", "\'", $s);
                    echo "$('#dt_basic_filter label').append('<select class=\'form-control\'>$s</select>');";
                endif;
                ?>
                $("#dt_basic_filter select").on( 'change', function () {
                    otable
                        .column( '3:visible' )
                        .search( this.value )
                        .draw();

                } );
                                                        
                                                        
	/* END BASIC */
	

})

</script>
<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>