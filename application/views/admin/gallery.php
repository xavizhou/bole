<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Photo Gallery";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
$page_nav["gallery"]["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row hidden-mobile">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa-fw fa fa-picture-o"></i> 
					<?php echo $this->lang->line("Gallery");?>
                                </h1>
			</div>

			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 text-align-right">
				<div class="page-title">
                                    <canvas id="myCanvas" class="hidden"></canvas>
                                    <canvas id="myCanvas2" class="hidden"></canvas>
                                    <input type="file" id="fileInput" name="image"  class="hidden" accept="image/jpeg,image/png" />
					<a href="javascript:void(0);" class="btn btn-default" id="btnUpload"><?php echo $this->lang->line("Upload");?></a>
<!--					<a href="javascript:void(0);" class="btn btn-default">Load Library</a>-->
				</div>
			</div>
		</div>

		<!-- row -->
		<div class="row">

			<!-- SuperBox -->
			<div class="superbox col-sm-12">
                            <?php
                            if($posts):
                                foreach($posts as $v):
                                ?>
				<div class="superbox-list" data-id="<?php echo $v['id'];?>">
                                    <img src="<?php echo base_url().'uploads/thumb/'.$v['thumb']; ?>" data-img="<?php echo base_url().'uploads/'.$v['file']; ?>" alt="<?php echo $v['content'];?>" title="<?php echo $v['title']?>" class="superbox-img">
				</div>
                                <?php
                                endforeach;
                            endif;
                            ?><div class="superbox-float"></div>
			</div>
			<!-- /SuperBox -->
			
			<div class="superbox-show" style="height:300px; display: none"></div>

		</div>

			<!-- end row -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	// include page footer
	include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?php echo ASSETS_URL; ?>/js/plugin/superbox/superbox.min.js"></script>


<script type="text/javascript">
    var _UPLOAD_URL = '<?php echo APP_URL?>/upload/gallery';
    var canvas = document.getElementById("myCanvas");  
    var canvas2 = document.getElementById("myCanvas2"); 
    var _INDEX = 0;
        
        
    function convertImageToCanvas(image) {
        var max = 1600;
        var max2 = 200;
        var x=0,y=0,w=0;
        if((image.height > max) || (image.width > max)){
            var r = image.height/image.width;
            if(r > 1){
                canvas.height = max;
                canvas.width = max/r;
            }else{
                canvas.width = max;
                canvas.height = max*r;
            }
        }else{
            canvas.width = image.width;
            canvas.height = image.height;
        }

        var r = image.height/image.width;
        canvas2.width = max2;
        canvas2.height = max2;
        if(r > 1){
            var sourceX = 0;
            var sourceY = image.width*(r-1)/2;
            var sourceWidth = image.width;
            var sourceHeight = image.height;
            var destWidth = max2;
            var destHeight = max2*r;

        }else{
            var sourceX = image.height*(1/r-1)/2;
            var sourceY = 0;
            var sourceWidth = image.width;
            var sourceHeight = image.height;
            var destWidth = max2/r;
            var destHeight = max2;
        }

        canvas.getContext("2d").drawImage(image, 0, 0,canvas.width,canvas.height);
        canvas2.getContext("2d").drawImage(image,sourceX,sourceY, sourceWidth, sourceHeight, 0,0, destWidth, destHeight);
    }

    function sendImage(divId,title){
        var dataurl = canvas.toDataURL("image/jpeg",.8);  
        var imagedata = (dataurl);  
        dataurl = canvas2.toDataURL("image/jpeg",.8);  
        var thumbdata = (dataurl);  
        var data = {   
          imagedata: imagedata,
          thumbdata: thumbdata,
          title: title
          //exif:exif  
        };  
        
        var s = '';

        s = "<div class='superbox-list' data-rel='' id='"+divId+"'></div>";
        $('.superbox').prepend(s);
        
        
        $.ajax( {  
            xhrFields: {
                onprogress: function (e) {
                    if (e.lengthComputable) {
                        progress = parseInt(e.loaded / e.total * 100, 10);
                        if(progress > 99.9){
                            $('.thumb#'+divId+' .text').html('处理中...');
                        }else{
                            $('.thumb#'+divId+' .text').html(progress+'%');
                        }
                    }else{
                        $('.thumb#'+divId+' .text').html('处理中...');
                    }
                }
            },
            url : _UPLOAD_URL,  
            data : data,  
            type : "POST",  
            dataType: "json",  
            success : function(data, status, jqXHR) {
                $('#btnUpload').removeClass('disabled');
                //$('#btnUpload').html('Upload');
                var file = data.file;
                var thumb = data.thumb;
                var alt = data.title;
                var id = data.id;
                if(data.status === 'success'){
                    $('.superbox-list#'+divId+' .text').html('');
                    $('.superbox-list#'+divId).html('<img src="<?php echo base_url()?>'+thumb+'" data-img="<?php echo base_url()?>'+file+'" title="" alt="'+alt+'" data-id="'+id+'" class="superbox-img" />');
                    window.location.reload();
                }else{
                    var o = $('.superbox-list#'+divId).remove();
                    alert('上传失败，请重试');
                }
            } ,
            complete:function(){
                
                $('#btnUpload').removeClass('disabled');
                $('#btnUpload').html('Upload');
            }
        });  
    };  
    
    
    
    $(document).ready(function() {
        $('.superbox').SuperBox();
        
        $('body').on('click','.btn-edit',function(){
            var id = $('.superbox-list.active').data('id');
            var title = $('.superbox #imgInfoBox h1').html();
            var text = $('.superbox-img-description').html();
            $('.superbox .btn-save').removeClass('hidden');
            $('.superbox .btn-edit').addClass('hidden');
            $('.superbox #imgInfoBox h1').html('<input name="superbox-img-title" value="'+title+'" />');
            $('.superbox-img-description').html('<textarea name="superbox-img-description">'+text+'</textarea>');
        })
        
        
        $('body').on('click','.btn-delete',function(){
            if(false === window.confirm('<?php echo $this->lang->line("Confirm");?>?')){
                return false;
            }
            var id = $('.superbox-list.active').data('id');
            $.ajax({
                url: '<?php echo APP_URL?>/gallery/ajax_remove?id='+id,
                type: 'GET',
                dataType : "json",
                contentType:"application/x-www-form-urlencoded; charset=utf-8",
                success : function(data, status, jqXHR) {
                    $('.superbox-list.active').remove();
                    $('.superbox-close').click();
                },
                compelete:function(){
                    
                }
            });
        })
        
        $('body').on('click','.btn-save',function(){
            var id = $('.superbox-list.active').data('id');
            var title = $('.superbox #imgInfoBox h1 input').val();
            var text = $('.superbox-img-description textarea').val();
            $('.superbox .btn-save').addClass('hidden');
            $('.superbox .btn-edit').removeClass('hidden');
            
            $.ajax({
                url: '<?php echo APP_URL?>/gallery/ajax_save?id='+id,
                type: 'POST',
                data: 'content='+text+'&title='+title,
                dataType : "json",
                contentType:"application/x-www-form-urlencoded; charset=utf-8",
                success : function(data, status, jqXHR) {
                    
                },
                compelete:function(){
                    
                }
            });     
            $('.superbox-img-description').html(text);
            $('.superbox #imgInfoBox h1').html(title);
            $('.superbox-list.active img').attr('alt',text);
            $('.superbox-list.active img').attr('title',title);
       

        })
        

        $('#btnUpload').click(function(){
            if($(this).hasClass('disabled')) return;
            $('#fileInput').click();
        });

        var tmpImage = new Image();
        $(tmpImage).load(function(){
            convertImageToCanvas($(this)[0]);
            _INDEX++;
            var divId = 'thumb_'+_INDEX;

            sendImage(divId,tmpImage.alt );
        })

        function render(file){
            $('#btnUpload').addClass('disabled');
            $('#btnUpload').html('Uploading <i class="fa fa-refresh fa-spin"></i>');
            var reader = new FileReader();
            reader.readAsDataURL(file);
            $('tmpImage').attr('alt',file['name']);
            tmpImage.alt = file['name'];
            reader.onload =function(evt){
                tmpImage.src = evt.target.result;
            }

          }

        $('#fileInput').change(function(){
            t1 = new Date().getTime(); 
            var files = $(this)[0].files;
            if(files.length < 1) return;
            render(files[0]);
        });

    })

</script>

<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>