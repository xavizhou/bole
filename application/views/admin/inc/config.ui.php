<?php

//CONFIGURATION for SmartAdmin UI

//ribbon breadcrumbs config
//array("Display Name" => "URL");
$breadcrumbs = array(
	"Home" => APP_URL
);

/*navigation array config

ex:
"dashboard" => array(
	"title" => "Display Title",
	"url" => "http://yoururl.com",
	"url_target" => "_self",
	"icon" => "fa-home",
	"label_htm" => "<span>Add your custom label/badge html here</span>",
	"sub" => array() //contains array of sub items with the same format as the parent
)

*/

$page_nav = array(
        "dashboard" => array(
                "title" => $this->lang->line("nav_dashboard"),
                "url" => APP_URL."/main/show_main",
                "icon" => "fa-home",
        ),
    
    	"who_are_we" => array(
		"title" => $this->lang->line("nav_who_are_we"),
		"icon" => "fa-bar-chart-o",
		"sub" => array(
			"origin_of_bole" => array(
				'title' => $this->lang->line("nav_origin_of_bole"),
				"url" => APP_URL."/page/?slug=origin_of_bole",
			),
			"bole_history" => array(
				'title' => $this->lang->line("nav_bole_history"),
				"url" => APP_URL."/history",
			),
			"about_bole_associate" => array(
				'title' => $this->lang->line("nav_about_bole_associate"),
				"url" => APP_URL."/page/?slug=about_bole_associate",
			),
			"bole_associates_service" => array(
				'title' => $this->lang->line("nav_bole_associates_service"),
				"url" => APP_URL."/page/?slug=bole_associates_service",
			),
			"standard_procedure" => array(
				'title' => $this->lang->line("nav_standard_procedure"),
				"url" => APP_URL."/page/?slug=standard_procedure",
			)
		)
	),
    
	"why_are_we" => array(
		"title" => $this->lang->line("nav_why_are_we"),
		"url" => APP_URL."/page/?slug=why_are_we",
		"icon" => "fa-file"
	),
    

    	"meet_us" => array(
		"title" => $this->lang->line("nav_meet_us"),
		"icon" => "fa-bar-chart-o",
		"sub" => array(
			"management_team" => array(
				'title' => $this->lang->line("nav_management_team"),
				"url" => APP_URL."/team",
			),
			"consultants" => array(
				'title' => $this->lang->line("nav_consultants"),
				"url" => APP_URL."/consultant",
			),
//                        'consultant_office'=> array(
//				'title' => $this->lang->line("nav_consultant_office"),
//				"url" => APP_URL."/city",
//			),
//			"successful_cases" => array(
//				'title' => $this->lang->line("nav_successful_cases"),
//				"url" => APP_URL."/portfolio",
//			)
		)
	),
    
    	"jobs" => array(
		"title" => $this->lang->line("nav_jobs"),
		"icon" => "fa-bar-chart-o",
		"sub" => array(
			"submit_cv" => array(
				'title' => $this->lang->line("nav_submit_cv"),
				"url" => APP_URL."/page/?slug=submit_cv",
			),
			"submit_vacancy" => array(
				'title' => $this->lang->line("nav_submit_vacancy"),
				"url" => APP_URL."/page/?slug=submit_vacancy",
			),
			"browse_jobs" => array(
				'title' => $this->lang->line("nav_browse_jobs"),
				"url" => APP_URL."/job/",
			)
		)
	),
    
    	"bo_le_institute" => array(
		"title" => $this->lang->line("nav_bo_le_institute"),
		"icon" => "fa-bar-chart-o",
		"sub" => array(
			"report_insights" => array(
				'title' => $this->lang->line("nav_report_insights"),
				"url" => APP_URL."/news/?category=report_insights",
			),
			"knowledge_center" => array(
				'title' => $this->lang->line("nav_knowledge_center"),
				"url" => APP_URL."/news/?category=knowledge_center",
			),
			"e_newsletter" => array(
				'title' => $this->lang->line("nav_e_newsletter"),
				"url" => APP_URL."/news/?category=e_newsletter",
			),
			"blog" => array(
				'title' => $this->lang->line("nav_blog"),
				"url" => APP_URL."/news/?category=blog",
			)
		)
	),
    
	"workwithus" => array(
		'title' => $this->lang->line("nav_workwithus"),
		"url" => APP_URL."/page/?slug=workwithus",
		"icon" => "fa-file"
	),
	"submit" => array(
		"title" => 'CV',
		"url" => APP_URL."/CV",
		"icon" => "fa-file"
	),
	"backup" => array(
		"title" => $this->lang->line("nav_backup"),
		"url" => APP_URL."/backup",
		"icon" => "fa-database"
	)
);

//configuration variables
$page_title = "";
$page_css = array();
$no_main_header = false; //set true for lock.php and login.php
$page_body_prop = array(); //optional properties for <body>
$page_html_prop = array(); //optional properties for <html>
?>