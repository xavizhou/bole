<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
          <img src="<?php echo base_url()."/assets/img/logo.png"?>" style="height: 45px;width:auto;" />
      </a>
    </div>
<?php
//print_r($sitemap);
//print_r($sitemap_label);
?>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
          <?php
          $flag = false;//last is sub-sub-menu;
          foreach($sitemap as $k=>$v):
              if(strpos($k, '_') === false):
                    $c = ($nav[0] == $k)?'active':'';
                    if(empty($v) || !is_array($v)):
                        echo '<li class="'.$c.'">';
                    
                        switch($k):
                            case 'invest':
                                $url = site_url().'/admin/invest/';
                                break;
                            case 'news':
                                $url = site_url().'/admin/news/';
                                break;
                            case 'team':
                                $url = site_url().'/admin/team/';
                                break;
                            case 'data':
                                $url = site_url().'/admin/file/';
                                break;
                            case 'user':
                                $url = site_url().'/admin/customer/';
                                break;
                            case 'admin':
                                $url = site_url().'/admin/user/edit?id=1';
                                break;
                            default :
                                $url = 'javascript:void(0)';
                                break;
                        endswitch;
                        
                        echo '<a href="'.$url.'">'.$sitemap_label[$k].'</a>';
                        echo '</li>';
                    else:
                        echo '<li class="'.$c.' dropdown">';
                        echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'.$sitemap_label[$k].' <span class="caret"></span></a>';
                        echo '<ul class="dropdown-menu" role="menu">';
                        foreach($v as $kk):
                            if($flag):
                                echo '<li class="divider"></li>';
                            endif;
                            $flag = true;
                            if($sitemap[$k.'_'.$kk]):
                                foreach($sitemap[$k.'_'.$kk] as $kkk):
                                    echo '<li><a href="admin/page/edit?slug='.$k.'_'.$kk.'_'.$kkk.'">'.$sitemap_label[$k.'_'.$kk.'_'.$kkk].'</a></li>';
                                endforeach;
                            else:
                                echo '<li><a href="admin/page/edit?slug='.$k.'_'.$kk.'">'.$sitemap_label[$k.'_'.$kk].'</a></li>';
                            endif;
                        endforeach;
                        $flag = false;
                        echo '</ul>';
                        echo '</li>';
                    endif;
              endif;
          endforeach;
          ?>
      </ul>
      
      
    <ul class="nav navbar-nav navbar-right">
        <li>
            <a href="javascript:void(0)">当前:<?php echo $lang;?></a>
        </li>
        <?php
        $_list = array('zh_CN'=>'中','en_US'=>'EN');
        foreach($_list as $_lang=>$v):
            $para = $_GET;
            $para['lang'] = $_lang;
            $url = site_url().$_SERVER['PATH_INFO'].'?'.http_build_query($para);
            echo '<li>';
            echo ($_lang == $lang)?'':'<a href="'.$url.'" >'.$v.'</a>';
            echo '</li>';
        endforeach;
        ?>
      <li>
          <a href="<?php echo site_url()?>/admin/login/logout_user" >登出</a>
      </li>
    </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>