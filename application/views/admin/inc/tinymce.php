
<script src="<?php echo base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
       selector: "textarea.tinymce",
       language : 'zh_CN',
       plugins: "template textcolor autolink link jbimages visualblocks",
       //toolbar: "template bold italic underline | formats forecolor backcolor | visualblocks | alignleft aligncenter alignright alignjustify | link jbimages | bullist numlist",
       menubar : false,
       content_css : "<?php echo base_url();?>assets/css/bootstrap.min.css,<?php echo base_url();?>assets/css/bootstrap-theme.min.css,<?php echo base_url();?>assets/css/style.css,<?php echo base_url();?>assets/tinymce/content.css"  ,
       skin: 'lightgray',
       
	formats: [
		{title: 'About Title', block: 'h2'},
	],
                
        upload_action: '<?php echo site_url()?>/admin/upload',//required
        upload_file_name: 'userfile',//required
       height:<?php echo ((int)$height)?(int)$height:400;?>,
       statusbar: false,
       relative_urls :false,
       templates : [
	{
		title: "Editor Details",
		url: "<?php echo base_url();?>template/content.html",
		description: "Adds Editors Name and Staff ID"
	}
        ],
        setup: function(editor) {
           editor.on('change', function(e) {
               changed = true;
           });
       }
   }
);
</script>