<?php
$k = $nav[0];
if(is_array($sitemap[$k]) && $sitemap[$k]):
    echo '<ul class="list-group text-uppercase">';
    foreach($sitemap[$k] as $kk):
        if($sitemap[$k.'_'.$kk]):
            foreach($sitemap[$k.'_'.$kk] as $kkk):
                echo '<li class="list-group-item">';
                echo '<a href="admin/page/edit?slug='.$k.'_'.$kk.'_'.$kkk.'">';
                echo $sitemap_label[$k.'_'.$kk.'_'.$kkk];
                echo '</a>';
                echo '</li>';
            endforeach;
        else:
            echo '<li class="list-group-item">';
            echo '<a href="admin/page/edit?slug='.$k.'_'.$kk.'">';
            echo $sitemap_label[$k.'_'.$kk];
            echo '</a>';
            echo '</li>';
        endif;

    endforeach;
    echo '</ul>';
endif;
?>