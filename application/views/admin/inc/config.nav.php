<?php

$page_nav = array(
	"dashboard" => array(
		"title" => "Dashboard",
		"url" => APP_URL,
		"icon" => "fa-home"
	),
//	"navi" => array(
//		"title" => "Navigation",
//		"url" => APP_URL."/nav-list.php",
//		"icon" => "fa-plane"
//	),
	"about" => array(
		"title" => "About Us",
		"url" => APP_URL."/page/?slug=about",
		"icon" => "fa-file"
	),
//	"contact" => array(
//		"title" => "Contact Us",
//		"url" => APP_URL."/page/?slug=contact",
//		"icon" => "fa-file"
//	),
	"news" => array(
		"title" => "News",
		"url" => APP_URL."/news/show_list",
		"icon" => "fa-list-alt"
	),
	"portfolio" => array(
		"title" => "Portfolio",
		"url" => APP_URL."/portfolio/show_list",
		"icon" => "fa-file-photo-o"
	),
	"team" => array(
		"title" => "Team",
		"url" => APP_URL."/team",
		"icon" => "fa-user"
	),
//	"file" => array(
//		"title" => "Data Room",
//		"url" => APP_URL."/dataroom",
//		"icon" => "fa-folder"
//	),
    	"job" => array(
		"title" => "Job",
		"url" => APP_URL."/job",
		"icon" => "fa-briefcase"
	),
    
	"gallery" => array(
		"title" => "Gallery",
		"url" => APP_URL."/gallery",
		"icon" => "fa-picture-o"
	),

	"backup" => array(
		"title" => "Database Backup",
		"url" => APP_URL."/backup",
		"icon" => "fa-database"
	),
//	"product" => array(
//		"title" => "Product",
//		"icon" => "fa-bar-chart-o",
//		"sub" => array(
//			"collection" => array(
//				'title' => 'Collection',
//				"url" => APP_URL.'collection/'
//			),
//			"product" => array(
//				'title' => 'Product',
//				"url" => APP_URL.'product/'
//			),
//			"designer" => array(
//				"title" => "Designer",
//				"url" => APP_URL.'designer/'
//			)
//		)
//	),
//	
//	
//	"smartui" => array(
//		"title" => "Smart UI",
//		"icon" => "fa-code",
//		"sub" => array(
//			"general" => array(
//				'title' => 'General Elements',
//				'icon' => 'fa-folder-open',
//				'sub' => array(
//					'alert' => array(
//						'title' => 'Alerts',
//						'url' => APP_URL."/smartui-alert.php"
//					),
//					'progress' => array(
//						'title' => 'Progress',
//						'url' => APP_URL.'smartui-progress.php'
//					)
//				)
//			),
//			"carousel" => array(
//				"title" => "Carousel",
//				"url" => APP_URL.'smartui-carousel.php'
//			),
//			"tab" => array(
//				"title" => "Tab",
//				"url" => APP_URL.'smartui-tab.php'
//			),
//			"accordion" => array(
//				"title" => "Accordion",
//				"url" => APP_URL.'smartui-accordion.php'
//			),
//			"widget" => array(
//				'title' => "Widget",
//				'url' => APP_URL."/smartui-widget.php"
//			),
//			"datatable" => array(
//				"title" => "DataTable",
//				"url" => APP_URL."/smartui-datatable.php"
//			),
//			"button" => array(
//				"title" => "Button",
//				"url" => APP_URL."/smartui-button.php"
//			),
//			'smartform' => array(
//				'title' => 'Smart Form',
//				'url' => APP_URL.'smartui-form.php'
//			)
//		)
//	),
);
?>