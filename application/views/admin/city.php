<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
    $page_nav['meet_us']["sub"]['consultant_office']["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["backup"] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
						<?php echo $this->lang->line("Backup");?>
				</h1>
			</div>
		</div>
		
		<!-- widget grid -->
		<section id="widget-grid" class="">
    <?php echo @flash_message(); ?>
			<!-- row -->
			<div class="row">
				<div class="col-sm-12">
                                    <form action='<?php echo site_url()?>admin/city/save' method="POST">
					<table class="table table-striped table-forum">
								<thead>
									<tr>
                                                                            <th><?php echo $this->lang->line("City Code");?></th>
                                                                            <?php
                                                                            foreach($lang_list as $k=>$v):
                                                                            ?>
                                                                            <th><?php echo $v['label'];?></th>
                                                                            <?php    
                                                                            endforeach;
                                                                            ?>
                                                                            <th><?php echo $this->lang->line("Location");?></th>
									</tr>
								</thead>
								<tbody>
				<?php
				if($city_list):
					foreach($city_list as $k=>$v):
				?>
					<!-- TR -->
					<tr>
						<td>
                                                    <input type="text" name="city[code][]" value="<?php echo $k?>" />
						</td>
                                                <?php
                                                foreach($lang_list as $k2=>$v2):
                                                ?>
                                                <td>
                                                    <input type="text" name="city[<?php echo $k2?>][]" value="<?php echo $city_list[$k][$k2];?>" />
                                                </td>
                                                <?php    
                                                endforeach;
                                                ?>
                                                <td>
                                                    <input type="text" name="city[location][]" value="<?php echo $city_list[$k]['location'];?>" />
                                                </td>
					</tr>
					<!-- end TR -->
				<?php
					endforeach;
                                        endif;
				?>
					
								</tbody>
                                                                <tfoot>	
                                                                    <tr>
                                                                        <td colspan="<?php echo 2 + count($lang_list); ?>">
                                                                            New City
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="new-tr">
                                        <td>
                                            <input type="text" name="city[code][]" />
                                        </td>
                                        <?php
                                        foreach($lang_list as $k=>$v):
                                        ?>
                                        <td>
                                            <input type="text" name="city[<?php echo $k?>][]" />
                                        </td>
                                        <?php    
                                        endforeach;
                                        ?>
                                        <td>
                                            <input type="text" name="city[location][]" value="" />
                                        </td>
                                </tr>			
								
								</tfoot>	
							</table>
							



				<a class="btn btn-primary" id='addNew'><?php echo $this->lang->line("Add New");?></a>

				<button class="btn btn-primary">Backup Now</button>
				
                                </form>
                                </div>
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
		
<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script> -->


<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
	
        $('#addNew').click(function(){
            var o = $('table tfoot tr.new-tr:last').clone();
             $('table tfoot').append(o);
        })

})

</script>
<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>