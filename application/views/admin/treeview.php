<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Tree View";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
$page_nav["file"]["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["file"] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
				<h1 class="page-title txt-color-blueDark"><i class="fa fa-desktop fa-fw "></i> 
					Dataroom
				</h1>
			</div>
			
		</div>
		<!-- widget grid -->
		<section id="widget-grid" class="">
			<!-- row -->
			<div class="row">
				<!-- NEW WIDGET START -->
				<article class="col-sm-12 col-md-12 col-lg-6">
		
					<!-- Widget ID (each widget will need unique ID)-->
                                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false"  data-widget-colorbutton="false" >

						<header>
							<span class="widget-icon"> <i class="fa fa-sitemap"></i> </span>
							<h2>Dataroom View </h2>
		
						</header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body">
		
								<div class="tree smart-form">
									<ul>
										<li>
											<span><i class="fa fa-lg fa-folder-open"></i> Parent</span>
											<ul>
												<li>
													<span><i class="fa fa-lg fa-minus-circle"></i> Child</span>
													<ul>
														<li>
															<span><i class="icon-leaf"></i> Grand Child</span>
														</li>
														<li>
															<span><i class="icon-leaf"></i> Grand Child</span>
														</li>
														<li>
															<span><i class="fa fa-lg fa-plus-circle"></i> Grand Child</span>
															<ul>
																<li style="display:none">
																	<span><i class="fa fa-lg fa-plus-circle"></i> Great Grand Child</span>
																	<ul>
																		<li style="display:none">
																			<span><i class="icon-leaf"></i> Great great Grand Child</span>
																		</li>
																		<li style="display:none">
																			<span><i class="icon-leaf"></i> Great great Grand Child</span>
																		</li>
																	</ul>
																</li>
																<li style="display:none">
																	<span><i class="icon-leaf"></i> Great Grand Child</span>
																</li>
																<li style="display:none">
																	<span><i class="icon-leaf"></i> Great Grand Child</span>
																</li>
															</ul>
														</li>
													</ul>
												</li>
											</ul>
										</li>
										<li>
											<span><i class="fa fa-lg fa-folder-open"></i> Parent2</span>
											<ul>
												<li>
													<span><i class="icon-leaf"></i> Child</span>
												</li>
											</ul>
										</li>
									</ul>
								</div>
		
							</div>
							<!-- end widget content -->
		
						</div>
						<!-- end widget div -->
		
					</div>
					<!-- end widget -->
		
				</article>
				<!-- WIDGET END -->
		
			</div>
		
			<!-- end row -->
		
			<!-- row -->
		
			<div class="row">
		
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
	// include page footer
	include("inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->

<script type="text/javascript">
	
	$(document).ready(function() {
	
		$('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
		$('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
			var children = $(this).parent('li.parent_li').find(' > ul > li');
			if (children.is(':visible')) {
				children.hide('fast');
				$(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
			} else {
				children.show('fast');
				$(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
			}
			e.stopPropagation();
		});			
	
	})

</script>

<?php 
	//include footer
	include("inc/footer.php"); 
?>