<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = ($post)?$this->lang->line("Edit"):$this->lang->line("New");

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$page_css[] = "smartadmin-production.min.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
//switch(count($nav)):
//    case 2:
//        $page_nav[$nav[0]]["sub"][$nav[1]]["active"] = true;
//        break;
//    case 1:
//        $page_nav[$nav[0]]["active"] = true;
//        break;
//endswitch;
$page_nav['bo_le_institute']["sub"][$category]["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
	$breadcrumbs["news"] = APP_URL.'news';
	include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa fa-pencil-square-o fa-fw "></i> 
					<?php echo $this->lang->line("News");?>
				</h1>
			</div>
		</div>
		
		<!-- widget grid -->
		<section id="widget-grid" class="">
			<!-- row -->
			<div class="row">

				<!-- NEW WIDGET START -->
				<article class="col-sm-12 col-md-12 col-lg-12">

					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-deletebutton="false"  data-widget-colorbutton="false" data-widget-editbutton="false">
						<!-- widget options:
						usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
		
						data-widget-colorbutton="false"
						data-widget-editbutton="false"
						data-widget-togglebutton="false"
						data-widget-deletebutton="false"
						data-widget-fullscreenbutton="false"
						data-widget-custombutton="false"
						data-widget-collapsed="true"
						data-widget-sortable="false"
		
					-->
					<header>

					</header>

					<!-- widget div-->
					<div>

						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->

						</div>
						<!-- end widget edit box -->

						<!-- widget content -->
						<div class="widget-body">
							<form action="<?php echo APP_URL?>news/save" method="post" id="pageForm" enctype="multipart/form-data"  class="smart-form" novalidate="novalidate">

								<fieldset>
									<section class="">
										<label class="label"><?php echo $this->lang->line("Title");?></label>
										<label class="input">
											<input type="text" name="title" placeholder="<?php echo $this->lang->line("ph_title");?>" value="<?php echo $post['title'];?>">
										</label>
									</section>

									<section class="">
										<label class="label"><?php echo $this->lang->line("Content");?></label>
										<label class="input">
											<textarea name="content" class="form-control summernote" placeholder="<?php echo $this->lang->line("ph_content");?>" ><?php echo $post['content'];?></textarea>
										</label>
									</section>


									<section>
										<?php
										if($post['thumb']):
											echo '<input type="hidden" name="thumb" value="'.$post['thumb'].'">';
										echo '<div class="thumb"><img src="'.base_url().'uploads/'.$post['thumb'].'" /></div>';
										endif;
										?>
										<div class="input input-file">
											<span class="button">
												<input type="file" id="thumb_upload" name="thumb_upload" onchange="this.parentNode.nextSibling.value = this.value">
												<?php echo $this->lang->line("Browse");?></span>
												<input type="text" placeholder="<?php echo $this->lang->line("ph_thumb");?>" readonly="">
											</div>
										</section>

									<section>
										<?php
										if($post['document']):
											echo '<input type="hidden" name="document" value="'.$post['document'].'">';
										echo '<div class="document"><video controls="controls" src="'.base_url().'uploads/'.$post['document'].'" /></div>';
										endif;
										?>
										<div class="input input-file">
											<span class="button">
												<input type="file" id="document_upload" name="document_upload" onchange="this.parentNode.nextSibling.value = this.value">
												<?php echo $this->lang->line("Browse");?></span>
												<input type="text" placeholder="<?php echo $this->lang->line("ph_video");?>" readonly="">
											</div>
										</section>
	


									</fieldset>



<!--									<fieldset>
										<?php
										if($news_category_list):
											?>
										<section>
											<div class="inline-group">
												<?php
												foreach($news_category_list as $k=>$v):
													?>
												<label class="radio">
													<input type="radio" name="category" value="<?php echo $k;?>" <?php echo (strpos($post['category'], $k) > -1)?'checked':'';?>>
													<i></i><?php echo $v;?></label>    
													<?php
													endforeach;
													?>
												</div>
											</section>
											<?php
											endif;
											?>





										</fieldset>-->

										<footer>
											<input type="hidden" name="id" value="<?php echo $post['id'];?>">
											<input type="hidden" name="category" value="<?php echo ($post)?$post['category']:$category;?>">
											<button type="submit" class="btn btn-primary">
												<?php echo $this->lang->line("Submit");?>
											</button>
										</footer>
									</form>


								</div>
								<!-- end widget content -->

							</div>
							<!-- end widget div -->

						</div>
						<!-- end widget -->

					</article>
					<!-- WIDGET END -->
				</div>

				<!-- end row -->

			</section>
			<!-- end widget grid -->

		</div>
		<!-- END MAIN CONTENT -->

	</div>
	<!-- END MAIN PANEL -->
	<!-- ==========================CONTENT ENDS HERE ========================== -->

	<?php 
	//include required scripts
	include("inc/scripts.php"); 
	?>

	<style>
		#preview-pane .preview-container {
			width: 400px;
			height: 225px;
		}
		.jcrop-holder #preview-pane{
			right: -420px;
		}
	</style>

	<script src="<?php echo ASSETS_URL; ?>/js/plugin/jquery-form/jquery-form.min.js"></script>
	<script data-pace-options='{ "restartOnRequestAfter": true }' src="<?php echo ASSETS_URL; ?>/js/plugin/pace/pace.min.js"></script>
	<script src="<?php echo ASSETS_URL; ?>/js/plugin/jcrop/jquery.Jcrop.min.js"></script>


	<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>/css/summernote.css">
	<script src="<?php echo ASSETS_URL; ?>/js/plugin/summernote/summernote.min.js"></script>
	<script src="<?php echo ASSETS_URL; ?>/js/plugin/summernote/summernote-zh-CN.js"></script>

	<script type="text/javascript">

		$(document).ready(function() {
            //this.parentNode.nextSibling.value

            function sendFile(file, editor) {
            	data = new FormData();
            	data.append("userfile", file);
            	$.ajax({
            		data: data,
            		type: "POST",
            		url: "<?php echo site_url()?>upload/summernote",
            		cache: false,
            		contentType: false,
            		processData: false,
            		success: function(url) {
            			editor.summernote("insertImage", '<?php echo base_url()?>'+url, 'test'); 
            		}
            	});
            }


            $('.summernote').summernote({
            	height : 240,
            	focus : false,
            	tabsize : 2,
            	lang: 'zh-CN',
            	toolbar: [
            	['style', ['bold', 'italic', 'underline', 'clear']],
            	['fontsize', ['fontsize']],
            	['color', ['color']],
            	['para', ['ul', 'ol', 'paragraph']],
            	['height', ['height']],
            	['picture', ['picture']],
            	['insert', ['link','fullscreen']]
            	],
                callbacks: {
                    onImageUpload: function(files, editor, welEditable) {
                        sendFile(files[0], $('.summernote'));
                    }
                }
            });

            var $checkoutForm = $('#pageForm').validate({
            	rules : {
            		title : {
            			required : true
            		}
            	},

				messages : {
					title : {
						required : '<?php echo $this->lang->line("valid_req_title");?>'
					}
				},

				// Do not change code below
				errorPlacement : function(error, element) {
					error.insertAfter(element.parent());
				}
			});


			$('#thumb_upload').change(function(){
				oFReader = new FileReader(), rFilter = /^(?:image\/jpeg|image\/jpeg)$/i;
				oFReader.onload = function (oFREvent) {
				  	JCrop.destroy();
				  	$('.jcrop-box').html('<img src="about:blank" id="target-3" />');
				  	$("#target-3").attr('src',oFREvent.target.result);
				  	$("#target-3").after('<div id="preview-pane"><div class="preview-container"><img src="about:blank" class="jcrop-preview" id="target-3a" alt="Preview" /></div></div>');
				  	$("#target-3a").attr('src',oFREvent.target.result);
				  	JCrop = aspect_ratio();
				};
				  if ($(this)[0].files.length === 0) { return; }
				  var oFile = $(this)[0].files[0];
				  if (!rFilter.test(oFile.type)) { alert("You must select a valid image file!"); return; }
				  oFReader.readAsDataURL(oFile);

			})

        })


				// aspect ratio

				var aspect_ratio = function() {

					// Create variables (in this scope) to hold the API and image size
					var  jcrop_api , boundx, boundy,

					// Grab some information about the preview pane
					$preview = $('#preview-pane'), 
					$pcnt = $('#preview-pane .preview-container'), 
					$pimg = $('#preview-pane .preview-container img'), 
					xsize = $pcnt.width(), 
					ysize = $pcnt.height();

					console.log('init', [xsize, ysize]);
					$('#target-3').Jcrop({
						onChange : updatePreview,
						onSelect : updatePreview,
						aspectRatio : xsize / ysize
					}, function() {
						// Use the API to get the real image size
						var bounds = this.getBounds();
						boundx = bounds[0];
						boundy = bounds[1];
						// Store the API in the jcrop_api variable
						jcrop_api = this;

						// Move the preview into the jcrop container for css positioning
						$preview.appendTo(jcrop_api.ui.holder);
					});

					function updatePreview(c) {
						if (parseInt(c.w) > 0) {
							var rx = xsize / c.w;
							var ry = ysize / c.h;

							$pimg.css({
								width : Math.round(rx * boundx) + 'px',
								height : Math.round(ry * boundy) + 'px',
								marginLeft : '-' + Math.round(rx * c.x) + 'px',
								marginTop : '-' + Math.round(ry * c.y) + 'px'
							});
						}
					};

					return jcrop_api;


				}
				// end aspect_ratio

				var JCrop = aspect_ratio();



			</script>

			<?php 
	//include footer
			include("inc/google-analytics.php"); 
			?>