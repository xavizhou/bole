<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
$page_nav["jobs"]["sub"]["browse_jobs"]["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["job"] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
					<i class="fa fa-pencil-square-o fa-fw "></i> 
						<?php echo $this->lang->line("Job");?>
				</h1>
			</div>
		</div>
		<!-- widget grid -->
		<section id="widget-grid" class="">
			<!-- row -->
			<div class="row">
		
				<!-- NEW WIDGET START -->
				<article class="col-sm-12 col-md-12 col-lg-12">
		
					<!-- Widget ID (each widget will need unique ID)-->
					<div class="jarviswidget" id="wid-id-0" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-colorbutton="false" data-widget-editbutton="false">
						
						<header>
                                                    
						</header>
		
						<!-- widget div-->
						<div>
		
							<!-- widget edit box -->
							<div class="jarviswidget-editbox">
								<!-- This area used as dropdown edit box -->
		
							</div>
							<!-- end widget edit box -->
		
							<!-- widget content -->
							<div class="widget-body no-padding">
                                                            <div class="widget-body-toolbar">
                                                               <a class="btn btn-info btn-md " href="<?php echo APP_URL;?>job/edit"><?php echo $this->lang->line("Add New");?></a> 
                                                            </div>
		
						        <table id="dt_basic" class="table table-bordered table-striped table-condensed table-hover has-tickbox" width="100%">
									<thead>		                
										<tr>
                                                                                    <th></th>
                                                                                    <th class="hasinput" >
                                                                                    <input type="text" class="form-control" placeholder="<?php echo $this->lang->line("Title");?>" />
                                                                                    </th>
                                                                                    <th class="hasinput" >
                                                                                        <select class="form-control">
                                                                                            <?php
                                                                                            if($location_list):
                                                                                                echo '<option value="">--'.$this->lang->line("Location").'--</option>';
                                                                                                foreach($location_list as $k=>$v):
                                                                                                    echo '<option value="'.$v.'">'.$v.'</option>';
                                                                                                endforeach;
                                                                                            endif;
                                                                                            ?>
                                                                                        </select>
                                                                                    </th>
                                                                                    <th class="hasinput" >
                                                                                    <input type="text" class="form-control" placeholder="<?php echo $this->lang->line("Industry");?>" />
                                                                                    </th>
                                                                                    <th class="hasinput" >
                                                                                        <select class="form-control">
                                                                                            <?php
                                                                                            if($jobtype_list):
                                                                                                echo '<option value="">--'.$this->lang->line("Type").'--</option>';
                                                                                                foreach($jobtype_list as $k=>$v):
                                                                                                    echo '<option value="'.$v.'">'.$v.'</option>';
                                                                                                endforeach;
                                                                                            endif;
                                                                                            ?>
                                                                                        </select>
                                                                                    </th>
                                                                                    <th class="hasinput" >
                                                                                    <input type="text" class="form-control" placeholder="<?php echo $this->lang->line("Date");?>" />
                                                                                    </th>

<!--                                                                                    <th>Updated Date</th>-->
										</tr>	                
										<tr>
                                                                                    <th></th>
                                                                                    <th><?php echo $this->lang->line("Title");?></th>
                                                                                    <th><?php echo $this->lang->line("Location");?></th>
                                                                                    <th><?php echo $this->lang->line("Industry");?></th>
                                                                                    <th><?php echo $this->lang->line("Type");?></th>
                                                                                    <th class="th_date"><?php echo $this->lang->line("Date");?></th>
<!--                                                                                    <th>Updated Date</th>-->
										</tr>
									</thead>
                                                                        <tbody class="smart-form">
                                                                            <?php
                                                                            if($posts):
                                                                                for($i=0;$i<count($posts);$i++):
                                                                            ?>
                                                                            <tr class="<?php echo ($posts[$i]['status'])?'success':'danger'?>">
                                                                                    <td>
                                                                                        <label class="checkbox">
                                                                                            <input type="checkbox" name="checkbox-inline" value="<?php echo $posts[$i]['id'];?>">
												<i></i> 
                                                                                        </label>
                                                                                    </td>
<!--                                                                                    <td><?php echo $i+1;?></td>-->
                                                                                    <td>
                                                                                        <a href="<?php echo APP_URL?>job/edit?id=<?php echo $posts[$i]['id'];?>" ><?php echo $posts[$i]['title'];?></a>
                                                                                    </td>
    
                                                                                    <td><?php echo $location_list[$posts[$i]['location']];?></td>
                                                                                    <td>
                                                                                        <?php
                                                                                        if($posts[$i]['industry']):
                                                                                            $_tmp = explode(",",$posts[$i]['industry']);
                                                                                            $_tmp2 = array();
                                                                                            foreach($_tmp as $v):
                                                                                                if($industry_list[$v]):
                                                                                                    $_tmp2[] = $industry_list[$v];
                                                                                                endif;
                                                                                            endforeach;
                                                                                            echo implode(",", $_tmp2);
                                                                                        endif;
                                                                                        ?>
                                                                                    </td>
                                                                                    <td><?php echo $jobtype_list[$posts[$i]['type']];?></td>
                                                                                    
                                                                                    <td><?php echo $posts[$i]['createdate'];?></td>
<!--                                                                                    <td><?php echo $posts[$i]['updatedate'];?></td>-->
										</tr>
                                                                            <?php
                                                                            
                                                                                endfor;
                                                                            endif;
                                                                            ?>
										
									</tbody>
								</table>
		
							</div>
                                                            
		
							</div>
							<!-- end widget content -->
		
						</div>
						<!-- end widget div -->
		
					</div>
					<!-- end widget -->
		
				</article>
				<!-- WIDGET END -->
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
		
<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script> -->


<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
	
	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};
                

		var otable = $('#dt_basic').DataTable({
//                        "aaSorting": [[ 1, "asc" ]],
                        "language": {
                            "url":"<?php echo ASSETS_URL?>/js/plugin/datatables/i18n/<?php echo ucfirst($lang_name)?>.lang"
                        },
                        "aoColumnDefs": [
                            { "sWidth": "150px", "aTargets": ["th_date"] }
                        ],
                        
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

                // custom toolbar
                $("div.toolbar").html('<div class="text-right"><img src="img/logo.png" alt="SmartAdmin" style="width: 111px; margin-top: 3px; margin-right: 10px;"></div>');

                // Apply the filter
                $("#dt_basic thead th input[type=text]").on( 'keyup change', function () {

                    otable
                        .column( $(this).parent().index()+':visible' )
                        .search( this.value )
                        .draw();

                } );
                $("#dt_basic thead th select").on( 'change', function () {

                    otable
                        .column( $(this).parent().index()+':visible' )
                        .search( this.value )
                        .draw();

                } );
    
	/* END BASIC */
	

})

</script>
<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>