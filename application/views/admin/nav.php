<?php

//initilize the page
require_once("lib/config.php");

//require UI configuration (nav, ribbon, etc.)
require_once("inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
include("inc/header.php");

//include left panel (navigation)
//follow the tree in inc/config.ui.php
$page_nav["backup"]["active"] = true;
include("inc/nav.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
	<?php
		//configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
		//$breadcrumbs["New Crumb"] => "http://url.com"
		$breadcrumbs["backup"] = "";
		include("inc/ribbon.php");
	?>

	<!-- MAIN CONTENT -->
	<div id="content">

		<div class="row">
			<div class="col-xs-12">
				<h1 class="page-title txt-color-blueDark">
						<?php echo $this->lang->line("Backup");?>
				</h1>
			</div>
		</div>
		
		<!-- widget grid -->
		<section id="widget-grid" class="">
    <?php echo @flash_message(); ?>
			<!-- row -->
			<div class="row">
				<div class="col-sm-12">
				<?php
				if($files):?>
					<table class="table table-striped table-forum">
								<thead>
									<tr>
										<th><?php echo $this->lang->line("Click to download");?></th>
									</tr>
								</thead>
								<tbody>
				<?php
					foreach($files as $file):
				?>
					<!-- TR -->
					<tr>
						<td>
							<h4><a href="<?php echo base_url()?>uploads/database/<?php echo $file;?>" target="_blank">
								<?php echo $file;?>
							</a>
							</h4>
						</td>
					</tr>
					<!-- end TR -->
				<?php
					endforeach;
				?>
									
									
								</tbody>
							</table>
				<?php
					endif;
				?>

							




				<a class="btn btn-primary" href="<?php echo site_url().'admin/backup/create';?>">Backup Now</a>
				</div>
			</div>
		
			<!-- end row -->
		
		</section>
		<!-- end widget grid -->

	</div>
	<!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
	//include required scripts
	include("inc/scripts.php"); 
?>
		
<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script> -->


<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php echo ASSETS_URL; ?>/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
	
    var _URL_DEL = '<?php echo APP_URL.'news/remove'?>';
    $('.btn-remove').click(function(e){
        if($(this).hasClass('disabled')){
            return false;
        }
        var s = '';
        $('input.chk_id:checked').each(function(){
            (s==='')?s=$(this).val():s+=','+$(this).val();
        })
        if(s === '') return false;
        window.location.href = _URL_DEL+'?id='+s;
        return false;
    })
    
    $('input.chk_id').on( 'change', function () {
        if($('input.chk_id:checked').size() > 0){
            $('.btn-remove').removeClass('disabled');
        }else{
            $('.btn-remove').addClass('disabled');
        }
    });
	/* BASIC ;*/
		var responsiveHelper_dt_basic = undefined;
		
		var breakpointDefinition = {
			tablet : 1024,
			phone : 480
		};

		$('#dt_basic').dataTable({
                        "aaSorting": [[ 1, "asc" ]],
                        
                        "language": {
                            "url":"<?php echo ASSETS_URL?>/js/plugin/datatables/i18n/<?php echo ucfirst($lang_name)?>.lang"
                        },
                        "aoColumnDefs": [
                            { "sWidth": "150px", "aTargets": ["th_date"] }
                        ],
                        
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_dt_basic) {
					responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
				responsiveHelper_dt_basic.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_dt_basic.respond();
			}
		});

	/* END BASIC */
	

})

</script>
<?php 
	//include footer
	include("inc/google-analytics.php"); 
?>