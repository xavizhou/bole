<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>
<div class="container cms-box">
    <?php echo @flash_message(); ?>
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/file/search?type=<?php echo $type;?>" method="get">
    <p>
        <a href="<?php echo site_url()."/admin/file/edit_room"?>" class="btn btn-primary btn-sm">新增文件夹</a>
        <a href="<?php echo site_url()."/admin/file/edit"?>" class="btn btn-primary btn-sm">新增文件</a>
    </p>
    </form>
    
    <?php
    $rooms_sort = array();
    $posts_sort = array();
    if($rooms_all):
        foreach($rooms_all as $k=>$v):
            $rooms_sort[$v['fid']][] = $v;
        endforeach;
    endif;
    if($posts):
        foreach($posts as $v):
            $posts_sort[$v['room_id']][] = $v;
        endforeach;
    endif;
    ?>
    
    <br />
    <?php
    if(empty($rooms_all)):
    ?>
    没有文件
    <?php
    else:
    ?>
    <?php
    function show_list($room_id,&$rooms_sort,&$posts_sort){
        $s = '';
        $rooms = $rooms_sort[$room_id];
        $posts = $posts_sort[$room_id];
        
        if($rooms && is_array($rooms)):
            foreach($rooms as $v):
                $s.= '<li class="folder">';
                $s.= '<span><i class="glyphicon glyphicon-menu-right"></i>'.$v['name'];
                $s .= '<a href="'.site_url().'/admin/file/edit_room?id='.$v['id'].'" class="a-add-room" data-id="'.$v['id'].'">编辑</a>';
                $s .= '<a href="'.site_url().'/admin/file/edit_room?fid='.$v['id'].'" class="a-add-room" data-id="'.$v['id'].'">建立子文件夹</a>';
                $s .= '<a href="'.site_url().'/admin/file/edit?fid='.$v['id'].'" class="a-add-room" data-id="'.$v['id'].'">添加文件</a>';
                $s .= '<a href="'.site_url().'/admin/file/remove_room?id='.$v['id'].'" class="a-remove-room" data-id="'.$v['id'].'">删除</a>';
                $s.= '</span>';
                $s.= show_list($v['id'],$rooms_sort,$posts_sort);
                $s.= '</li>';
            endforeach;
        endif;
        if($posts && is_array($posts)):
            foreach($posts as $v):
                $s.= '<li>';
                $s.= '<span>'.$v['filename'];
                $s.= '<a href="'.base_url().'/uploads/'.$v['path'].'" class="" target="_blank" data-id="'.$v['id'].'">查看</a>';
                $s.= '<a href="'.site_url().'/admin/file/edit?id='.$v['id'].'" class="a-edit-file" data-id="'.$v['id'].'">编辑</a>';
                $s.= '<a href="'.site_url().'/admin/file/remove?id='.$v['id'].'" class="a-remove-file" data-id="'.$v['id'].'">删除</a>';
                $s.= '</span>';
                $s.= '</li>';
            endforeach;
        endif;
        if($s):
            $s = '<ul>'.$s.'</ul>';
        endif;
        return $s;
    }
    //print_r($posts_sort);
    echo '<div class="room_box">';
    echo show_list(0,$rooms_sort,$posts_sort);
    echo '</div>';
    
    
    ?>
    <?php
    endif;
    ?>
</div>
<?php $this->load->view('admin/footer') ?>
<script>
$('select[name=room_id]').change(function(){
    window.location.href= "<?php echo site_url().'/admin/file?room_id='?>"+$(this).val();
})

$('.room_box .folder span').click(function(){
    $(this).parent('.folder').toggleClass('ext');
});

$('.a-remove-room').click(function(){
    if(!window.confirm('Are you sure you want to remove this folder and all the files under the folder?')){
        return false;
    }
})

$('.a-remove-file').click(function(){
    if(!window.confirm('Are you sure you want to remove this file?')){
        return false;
    }
})
</script>