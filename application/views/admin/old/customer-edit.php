<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>
      <?php
      if($post):
          $h = '编辑用户';
      else:
          $h = '新增用户';
      endif;
      
      $password = ($post['password'])?'******':'';
      ?>
  <div class="container">
    <?php echo @flash_message(); ?>
      <?php
        $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
        echo form_open_multipart('admin/customer/save', $attributes);
      ?>
      <input name="id" value="<?php echo $post['id'];?>" type="hidden" />
      <div class="col-lg-8">
          <div class="row">
        <p>
            <span class="icon-home"></span>
            <?php echo $h;?> 
        </p>
        
        <div class="form-group">
          <label for="username">用户名称 * </label>
          <?php echo form_input(array('name' => 'username', 'id' => 'username', 'class' => 'form-control', 'value' => $post['username'])); ?>
        </div>
        <div class="form-group">
          <label for="title">密码 * </label>
          <?php echo form_password(array('name' => 'password', 'id' => 'password', 'class' => 'form-control', 'value' => $password)); ?>
        </div>
        <div class="form-group">
          <label for="title">确认密码 * </label>
          <?php echo form_password(array('name' => 'confirm_password', 'id' => 'confirm_password', 'class' => 'form-control', 'value' => $password)); ?>
        </div>
        
        
        <div class="form-group">
            <label for="gender">DATAROOM</label>
              <?php
                $tmp = explode(",", $post['room']);
                foreach($rooms as $k=>$v){
                  $data = array(
                  'name'        => 'room[]',
                  'value'       => $k,
                  'checked'     => (in_array($k, $tmp))?TRUE:FALSE,
                  'style'       => 'margin-left:20px',
                  );
                  echo form_checkbox($data);
                  echo $v;
                }
              ?>
          </div>
        <input class="btn btn-primary" type="submit" value="发布">
          </div>
      </div>
      
      <div class="col-lg-4">
      </div>
      <div class="clearfix"></div>
      
      
      <?php
      echo form_close();
      ?>
  </div>

<script>
    
function ajax_check($func,$k,$v,$msg){
    $.ajax({
        url: '<?php echo site_url()?>/admin/user/'+$func,
        type: 'GET',
        data: 'q='+$v,
        dataType : "json",
        success : function(data, status, jqXHR) {
            if(data.status === 'failure'){
                alert($msg);
                $('#'+$k).focus();
            }else{
            }
        }
    });
}


$('#username').blur(function(){
    var v = $(this).val();
    v = $.trim(v);
    if(v.length > 0){
        ajax_check('ajax_check_username','username',v,'<?php echo _('该用户名已存在')?>');
    }else{
        $(this).parents('.form-group').find('error').removeClass('error');
    }
})
$('#email').blur(function(){
    var v = $(this).val();
    v = $.trim(v);
    if(v.length > 0){
        ajax_check('ajax_check_email','email',v,'<?php echo _('该邮箱已存在')?>');
    }else{
        $(this).parents('.form-group').find('error').removeClass('error');
    }
})


    $('#myform').submit(function(){
        $('.tab-content').each(function(){
            var t = $(this).attr('data-rel');
            var o = $('input[name="'+t+'"]');
            var v = $(this).find('.tab-pane.active').attr('data-val');
            o.val(v);
        })
    })

    $("#myform").validate({
            rules: {
                    username: {
                            required: true,
                            minlength: 2
                    },
                    password: {
                            required: true,
                            minlength: 6
                    },
                    confirm_password: {
                            required: true,
                            minlength: 6,
                            equalTo: "#password"
                    },
                    email: {
                            required: true,
                            email: true
                    }
            },
            messages: {
                    username: {
                            required: "请输入用户名",
                            minlength: "用户名至少2位"
                    },
                    password: {
                            required: "请输入密码",
                            minlength: "密码至少6位"
                    },
                    confirm_password: {
                            required: "请再次输入密码",
                            minlength: "密码至少6位",
                            equalTo: "两次输入不匹配"
                    },
                    email: "请输入合法的邮箱"
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>
<?php $this->load->view('admin/footer') ?>