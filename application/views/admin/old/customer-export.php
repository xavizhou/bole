<?php $this->load->view('admin/header') ?>
<?php
$data = array('nav'=>'customer');
?>
<?php $this->load->view('admin/nav',$data); ?>


<link rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/datepicker3.css" type="text/css" />
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/locales/bootstrap-datepicker.zh-CN.js"></script>


<div class="container cms-box">
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/customer/export" method="post" target="_blank">
    <p>
        <span class="icon-diamonds"></span>会员天地 <span class="icon-question"></span> 
    </p>
    
    <div class="row">
        <div class="col-md-12">
            从
            <div class="form-group">
              <input type="text" id="startDate" name="startDate" class="form-datepicker form-control">
            </div>
            到
            <div class="form-group">
              <input type="text" id="endDate" name="endDate" class="form-datepicker  form-control" value="<?php echo date('Y-m-d')?>">
            </div>
            <input type="submit"  class="btn btn-primary btn-sm" value="导出"> <a href="<?php echo site_url()?>/admin/customer/export" target="_blank">导出全部</a>
        </div>
    </div>
    </form>
    
    <script>
    $('.form-datepicker').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: "linked",
        language: "zh-CN",
        autoclose: true,
        todayHighlight: true
    });
    </script>
</div>
<?php $this->load->view('admin/footer') ?>