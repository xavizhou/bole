<?php $this->load->view('admin/header') ?>
<?php
$data = array('nav'=>'job');
?>
<?php $this->load->view('admin/nav',$data); ?>
<div class="container cms-box">
    <?php echo @flash_message(); ?>
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/application/search?type=<?php echo $type;?>" method="get">
    <p>
        <span class="icon-table2"></span>工作管理 <span class="icon-question"></span>
    </p>
    <div class="row">
    <div class="col-md-10">
          <div class="form-group">
            <select name="action" class="form-control">
                <option value="">--请选择以下功能--</option>
                <option value="set_show">已处理</option>
                <option value="set_hide">未处理</option>
            </select>
          </div>
            <input type="hidden" name="mode" value="" />
          <button type="button" class="btn btn-default" id="btn-update">应用</button>
    </div>
    

    <?php
    if($page){
        $para = $_GET;
        $para['page'] = $page-1;
        $url_prev = site_url().'/admin/job/?'.http_build_query($para);
    }
    if($page < (ceil($total / $per) -1)){
        $para = $_GET;
        $para['page'] = $page+1;
        $url_next = site_url().'/admin/job/?'.http_build_query($para);
    }
    ?>
    <div class="col-md-2 text-right">
        <div class="form-inline">
            <?php
            if($url_prev):
            ?>
            <a href="<?php echo $url_prev;?>">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <?php
            endif;
            ?>
           <input type="text" class="form-control w50 text-center" value="<?php echo $page+1;?>">
            <?php
            if($url_next):
            ?>
            <a href="<?php echo $url_next;?>">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <?php
            endif;
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    </form>
    <br />
    <?php
    if(empty($posts)):
    ?>
    没有产品
    <?php
    else:
    ?>
    <form id="frm_table" class="form-inline" role="search" action="<?php echo site_url()?>/admin/application/batch" method="post">
<input type="hidden" name="action" />
<table class="table tablesorter table-bordered">
        <thead>
          <tr>
            <th>批量选择<input type="checkbox" id="select_all"/></th>
            <th>ID</th>
            <th>申请人</th>
            <th>申请工作</th>
            <th>状态</th>
            <th>最后更新日期</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $i=0;
            foreach($posts as $post):
            ?>
          <tr>
            <td><input type="checkbox" name="ids[]" value="<?php echo $post['id']?>" /></td>
            <td><?php echo ++$i;?></td>
            <td><a href="<?php echo site_url()."/admin/user/edit?id={$post['user']['id']}"?>"><?php echo $post['user']['name'].'('.$post['user']['email'].')';?></a></td>
            <td><a href="<?php echo site_url()."/admin/job/edit?id={$post['job']['id']}"?>"><?php echo $post['job']['title']?></a></td>

            <?php
            if($post['status']):
            ?>
            <td class="success">已处理</td>
            <?php
            else:
            ?>
            <td class="danger">未处理</td>
            <?php
            endif;
            ?>
            
            <td><?php echo $post['updatedate'];?></td>
            
          </tr>
            <?php
            endforeach;
            ?>
        </tbody>
      </table>
    <?php
    endif;
    ?>
    </form>
</div>
<?php $this->load->view('admin/footer') ?>