<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/nav') ?>

      <?php
      if($post):
          $h = '编辑';
      else:
          $h = '新增';
      endif;
      ?>
  <div class="container">
    <?php echo @flash_message(); ?>
      <?php
        $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
        echo form_open_multipart('admin/file/save', $attributes);
      ?>
      <input name="id" value="<?php echo $post['id'];?>" type="hidden" />
      <input name="type" value="<?php echo $type;?>" type="hidden" />
      <div class="col-lg-8">
          <div class="row">
        <p>
            <span class="icon-home"></span>
            <?php echo $h;?>
        </p>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="outURL" data-val="1">
              
            <?php
            $t = ($post['file'])?'重新添加':'添加';
            ?>
              
                <div class="form-group">
                    <?php
                    if($post):
                        echo $post['filename'].'<br />';
                        echo $post['size'].'k<br />';
                        echo $post['date'].'<br />';
                    endif;
                    ?>
                    
                  <label for="title">文件</label>
<!--                  <a class="btn btn-default add-image show-prev" data-upload="thumb_upload"><?php echo $t;?></a>-->
                  <?php echo form_input(array('name' => 'filename', 'id' => 'filename', 'class' => 'hidden', 'value' => $post['filename'])); ?>
                  <?php echo form_input(array('name' => 'path', 'id' => 'path', 'class' => 'hidden', 'value' => $post['path'])); ?>
                  
                    <?php
                    if($rooms):
                    ?>
                    <div class="form-group">
                      <?php 
                      $options = $rooms;
                      //$options[0] = '-文件夹-';
                      ksort ($options);
                      $_id = ($post['room_id'])?$post['room_id']:$fid;
                      echo form_dropdown('room_id', $options, $_id,'class="form-control"');
                      ?>
                    </div>
                    <?php
                    endif;
                    ?>
                        
                        <?php echo form_upload(array('name' => 'thumb_upload', 'id' => 'thumb_upload', 'class' => '')); ?>
                  
                  <span class="image-preview hidden"></span>
                </div>
          </div>
            <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-publish" data-form="myform">发布</a>
            
        </div>
        
          </div>
      </div>
      
      <div class="col-lg-4 hidden">
          <div class="widget-box">
          <div class="widget-title">
              发布栏
          </div>
          <table class="table">
              <tbody>
                  <tr>
                    <td>状态</td>
                    <td>
<!--                        <ul class="pagination">
                            <li class="<?php if($post['status']) echo 'active';?>"><a href="#">显示</a></li>
                            <li class="<?php if(!$post['status']) echo 'active';?>"><a href="#">隐藏</a></li>
                          </ul>-->
                           <?php
                           echo ($post['status'])?'发布':'草稿';
                           ?>
                            <input name="status" value="<?php echo $post['status'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>显示</td>
                    <td>
                        <ul class="pagination radio-switch" data-input="display">
                            <li class="<?php if($post['display']) echo 'active';?>" data-value="1"><a href="javascript:void(0)">显示</a></li>
                            <li class="<?php if(!$post['display']) echo 'active';?>" data-input="0"><a href="javascript:void(0)">隐藏</a></li>
                          </ul>
                            <input name="display" id="display" value="<?php echo $post['display'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>顺序</td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="order" value="<?php echo $post['order'];?>" id="order" class="w50 form-control">
                        </div>
                    </td>
                  </tr>
              </tbody>
          </table>
          <div class="widget-button">
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-draft" data-form="myform">保存为草稿</a>
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-publish" data-form="myform">发布</a>
          </div>
          </div>
      </div>
      <div class="clearfix"></div>
      
      
      <?php
      echo form_close();
      ?>
  </div>

<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入文件标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>
<?php $this->load->view('admin/footer') ?>