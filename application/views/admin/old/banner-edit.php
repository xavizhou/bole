<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/nav') ?>
  
<script src="<?php echo base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
       selector: "textarea.tinymce",
       language : 'zh_CN',
       plugins: "textcolor autolink link image upload",
       toolbar: "bold italic underline | fontsizeselect forecolor backcolor |  alignleft aligncenter alignright alignjustify | link upload | bullist numlist",
       menubar : false,
        upload_action: '<?php echo site_url()?>/admin/upload',//required
        upload_file_name: 'userfile',//required
       skin: 'lightgray',
       height:<?php echo ((int)$height)?(int)$height:400;?>,
       relative_urls :false,
       statusbar: false,
        setup: function(editor) {
           editor.on('change', function(e) {
               changed = true;
           });
       }
   }
);
</script>
      <?php
      if($post):
          $h = '编辑';
      else:
          $h = '新增';
      endif;
      
      if($type==2):
          $t = '小横幅';
      else:
          $t = '大横幅';
            $type = 1;
      endif;
      $t = '广告';
      $type = 1;
      ?>
  <div class="container">
    <?php echo @flash_message(); ?>
      <?php
        $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
        echo form_open_multipart('admin/banner/save', $attributes);
      ?>
      <input name="id" value="<?php echo $post['id'];?>" type="hidden" />
      <input name="type" value="<?php echo $type;?>" type="hidden" />
      <div class="col-lg-8">
          <div class="row">
        <p>
            <span class="icon-home"></span>
            <?php echo $h;?> > <?php echo $t;?> 
            <span class="icon-question"></span> 
            <a href="<?php echo site_url()."/admin/banner/?type=$type"?>" class="btn btn-primary btn-sm">
                <span class="icon-paragraph-justify2"></span>
            </a>
        </p>
        <?php
        $t = ($post['image'])?'重新添加':'添加图片';
        $is_link = (empty($post['is_link']))?1:$post['is_link'];
        ?>
        <div class="form-group">
          <label for="title">图片</label>
          <a class="btn btn-default add-image show-prev" data-upload="image_upload"><?php echo $t;?></a>
          (尺寸：
          <?php
          echo ($type==2)?'330x148':'width:230px';
          ?>
          )
          <?php echo form_input(array('name' => 'image', 'id' => 'image', 'class' => 'hidden', 'value' => $post['image'])); ?>
          <?php echo form_upload(array('name' => 'image_upload', 'id' => 'image_upload', 'class' => 'hidden')); ?>
          <br /><span class="image-render">
              <?php
              if($post['image']){
                  echo "<img src='".base_url() .'/uploads/'."{$post['image']}' width='200' />";
              }
              ?>
          </span>
          <span class="image-preview"></span>
        </div>
        <div class="form-group">
          <label for="title">标题 * </label>
          <?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => $post['title'])); ?>
        </div>
        <div class="form-group">
          <label for="content">简述</label>
          <?php echo form_textarea(array('name' => 'content', 'id' => 'content', 'class' => 'tinymce form-control', 'value' => $post['content'])); ?>
        </div>
        <ul class="nav nav-tabs">
          <li class="<?php if($is_link== 1)  echo 'active';?>"><a href="#outURL" data-toggle="tab">外部链接</a></li>
          <?php
          if($type==2):
          ?>
          <li class="<?php if($is_link== 2)  echo 'active';?>"><a href="#profile" data-toggle="tab">视频链接</a></li>
          <?php
          endif;
          ?>
        </ul>
        <input type="hidden" name="is_link" value="<?php echo $is_link;?>">

        <!-- Tab panes -->
        <div class="tab-content" data-rel="is_link">
          <div class="tab-pane <?php if($is_link== 1)  echo 'active';?>" id="outURL" data-val="1">
            <div class="form-group">
              <label for="content">请填写外部链接</label>
              <?php echo form_input(array('name' => 'url', 'id' => 'url', 'class' => 'form-control', 'value' => $post['url'])); ?>
              <?php
                $data = array(
                'name'        => 'url_target',
                'value'       => '_blank',
                'checked'     => ($post['url_target'] == '_blank')?TRUE:FALSE,
                'style'       => '',
                );
                echo form_radio($data);
                echo '在新页面打开';
                $data = array(
                'name'        => 'url_target',
                'value'       => '_self',
                'checked'     => ($post['url_target'] == '_self')?TRUE:FALSE,
                'style'       => 'margin-left:20px',
                );
                echo form_radio($data);
                echo '在同一页面打开';
              ?>
            </div>
          </div>
          <div class="tab-pane <?php if($is_link== 2)  echo 'active';?>" id="profile" data-val="2">
            <div class="form-group">
              <label for="content">视频链接</label>
              <?php echo form_input(array('name' => 'video', 'id' => 'video', 'class' => 'form-control', 'value' => $post['video'])); ?>
            </div>
              代码示例:<br />
              <img src="<?php echo base_url()?>assets/images/sample.jpg" />
          </div>
        </div>
        
          </div>
      </div>
      
      <div class="col-lg-4">
          <div class="widget-box">
          <div class="widget-title">
              发布栏
          </div>
          <table class="table">
              <tbody>
                  <tr>
                    <td>状态</td>
                    <td>
<!--                        <ul class="pagination">
                            <li class="<?php if($post['status']) echo 'active';?>"><a href="#">显示</a></li>
                            <li class="<?php if(!$post['status']) echo 'active';?>"><a href="#">隐藏</a></li>
                          </ul>-->
                           <?php
                           echo ($post['status'])?'发布':'草稿';
                           ?>
                            <input name="status" value="<?php echo $post['status'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>显示</td>
                    <td>
                        <ul class="pagination radio-switch" data-input="display">
                            <li class="<?php if($post['display']) echo 'active';?>" data-value="1"><a href="javascript:void(0)">显示</a></li>
                            <li class="<?php if(!$post['display']) echo 'active';?>" data-input="0"><a href="javascript:void(0)">隐藏</a></li>
                          </ul>
                            <input name="display" id="display" value="<?php echo $post['display'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>顺序</td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="order" value="<?php echo $post['order'];?>" id="order" class="w50 form-control">
                        </div>
                    </td>
                  </tr>
              </tbody>
          </table>
          <div class="widget-button">
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-draft" data-form="myform">保存为草稿</a>
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-publish" data-form="myform">发布</a>
          </div>
          </div>
      </div>
      <div class="clearfix"></div>
      
      
      <?php
      echo form_close();
      ?>
  </div>

<script>
    $('#myform').submit(function(){
        $('.tab-content').each(function(){
            var t = $(this).attr('data-rel');
            var o = $('input[name="'+t+'"]');
            var v = $(this).find('.tab-pane.active').attr('data-val');
            o.val(v);
        })
    })

    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入横幅标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>
<?php $this->load->view('admin/footer') ?>