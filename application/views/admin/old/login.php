<?php 
$this->load->view('admin/header') ;
?>

  <div class="container">

    <div class="row">
      <div class="col-lg-4 col-lg-offset-4 well">

        <div class="legend">欢迎登陆管理系统</div>

        <?php if (isset($error) && $error): ?>
          <div class="alert alert-error">
            <a class="close" data-dismiss="alert" href="#">×</a>用户名或者密码不正确
          </div>
        <?php endif; ?>

        <?php echo form_open('admin/login/login_user', 'method="post" class="form-inline"') ?>

            <div class="form-group">
              <label class="" for="exampleInputEmail2"><?php echo _('用户名');?></label>
              <input type="text" class="form-control" id="username" name="username" placeholder="">
            </div>
            <div class="form-group">
              <label class="" for="exampleInputPassword2"><?php echo _('密码');?></label>
              <input type="password" class="form-control" id="password" name="password" placeholder="">
            </div>
        <?php
        if($cap):
        ?>
            <div class="form-group form-group-captcha">
                <label><?php echo _('验证码');?></label>
                <input type="text" id="captcha" class="form-control"  name="captcha" placeholder="">
                <?php echo $cap['image'];?>
           </div>
        
        <?php
        endif;
        ?>
        
<!--            <div class="checkbox">
              <label>
                <input type="checkbox"> Remember me
              </label>
            </div>-->
            <br />
            <button type="submit" class="btn btn-default">Sign in</button>
          </form>
        <!--<label class="checkbox">
          <input type="checkbox" name="remember" value="1"> Remember Me
        </label>-->


        </form>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-6 col-lg-offset-4">

      </div>
    </div>
  </div>
<?php 
$this->load->view('admin/footer') ;
?>
<?php 
$this->load->view('admin/foot') ;
?>