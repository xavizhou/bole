<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/nav') ?>
<div class="container">
    <?php echo @flash_message(); ?>
    <form name="" action="admin/setting/save" method="post" enctype="multipart/form-data">
        <br />
            <h2>Homepage Feature</h2>
            <?php
              foreach($name_list as $name=>$label):
                  echo '<div class="col-lg-12">
                            <div class="row">
                                <div class="form-group">
                                    <label for="'.$name.'" class="control-label text-right">'.$label.'</label> ';
                  $list = $setting_list[$name];
                  $val = $setting[$name];
                  echo "<select name='$name' id='o_$name'>";
                  echo "<option value=''>"._('--Select--')."</option>";
                  echo createSelect($list, $val);
                  echo "</select>";
                  echo '</div>
                </div>
              </div>';
              endforeach;
            ?>
      <div class="clearfix"></div>
        <hr />
            <h2>Feature Job</h2>
            <?php
              foreach($job_list as $name=>$label):
                  echo '<div class="col-lg-12">
                            <div class="row">
                                <div class="form-group">
                                    <label for="'.$name.'" class="control-label text-right">'.$label.'</label> ';
                  $list = $setting_list[$name];
                  $val = $setting[$name];
                  echo "<select name='$name' id='o_$name'>";
                  echo "<option value=''>"._('--Select--')."</option>";
                  echo createSelect($list, $val);
                  echo "</select>";
                  echo '</div>
                </div>
              </div>';
              endforeach;
            ?>
      <div class="clearfix"></div>
            <h2>Images</h2>
            <?php
              foreach($image_list as $name=>$label):
                  echo '<div class="col-lg-12">
                            <div class="row">
                                <div class="form-group">
                                    <label for="'.$name.'" class="control-label text-right">'.$label.'</label><br /> ';
                  $val = $setting[$name];
                  if($val):
                    echo "<div class='img-box'>";
                    echo "<img src='".  base_url()."uploads/$val' style='max-width:400px;' /><br />";
                    echo "<a href='javascript:void(0)' class='remove'>remove</a>";
                    echo "</div>";
                  endif;
                  echo "<input type='file' name='file_$name' />";
                  echo "<input type='hidden' name='$name' value='{$val}'/>";
                  echo '</div>
                </div>
              </div>';
              endforeach;
            ?>
            <div class="clearfix"></div>
      
            <input class="btn btn-primary" type="submit" value="Submit" />
    </form>
      <div class="clearfix"></div>
      
  </div>
<script>
    $('a.remove').click(function(){
        $(this).parents('.form-group').find('input[type=hidden]').val('');
        $(this).parents('.form-group').find('.img-box').remove();
    })
</script>
<?php $this->load->view('admin/footer') ?>