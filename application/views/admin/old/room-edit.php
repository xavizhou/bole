<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>

      <?php
      if($post):
          $h = '编辑文件夹';
      else:
          $h = '新增文件夹';
      endif;
      ?>
  <div class="container">
    <?php echo @flash_message(); ?>
    <?php $this->load->view('admin/inc/breadcrumb') ?>
      <?php
        $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
        echo form_open_multipart('admin/file/save_room', $attributes);
      ?>
      <input name="id" value="<?php echo $post['id'];?>" type="hidden" />
      <input name="type" value="<?php echo $type;?>" type="hidden" />
      <div class="col-lg-8">
          <div class="row">
        <p>
            <span class="icon-home"></span>
            <?php echo $h;?>
        </p>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="outURL" data-val="1">
            <div class="form-group">
              <label for="title">名称 *</label>
              <?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => $post['name'])); ?>
            </div>
              
              <label for="title">所属ROOM *</label>
            <?php
            if($rooms):
            ?>
            <div class="form-group">
              <?php 
              $options = $rooms;
              $options[0] = '-NONE-';
              ksort ($options);
              $_id = ($post['fid'])?$post['fid']:$fid;
              echo form_dropdown('fid', $options, $_id,'class="form-control"');
              ?>
            </div>
            <?php
            endif;
            ?>
              
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-publish" data-form="myform">发布</a>
              
          </div>
        </div>
        
          </div>
      </div>
      
      <div class="col-lg-4 hidden">
          <div class="widget-box">
          <div class="widget-title">
              发布栏
          </div>
          <table class="table">
              <tbody>
                  <tr>
                    <td>状态</td>
                    <td>
<!--                        <ul class="pagination">
                            <li class="<?php if($post['status']) echo 'active';?>"><a href="#">显示</a></li>
                            <li class="<?php if(!$post['status']) echo 'active';?>"><a href="#">隐藏</a></li>
                          </ul>-->
                           <?php
                           echo ($post['status'])?'发布':'草稿';
                           ?>
                            <input name="status" value="<?php echo $post['status'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>显示</td>
                    <td>
                        <ul class="pagination radio-switch" data-input="display">
                            <li class="<?php if($post['display']) echo 'active';?>" data-value="1"><a href="javascript:void(0)">显示</a></li>
                            <li class="<?php if(!$post['display']) echo 'active';?>" data-input="0"><a href="javascript:void(0)">隐藏</a></li>
                          </ul>
                            <input name="display" id="display" value="<?php echo $post['display'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>顺序</td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="order" value="<?php echo $post['order'];?>" id="order" class="w50 form-control">
                        </div>
                    </td>
                  </tr>
              </tbody>
          </table>
          <div class="widget-button">
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-draft" data-form="myform">保存为草稿</a>
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-publish" data-form="myform">发布</a>
          </div>
          </div>
      </div>
      <div class="clearfix"></div>
      
      
      <?php
      echo form_close();
      ?>
  </div>

<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入文件夹标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>
<?php $this->load->view('admin/footer') ?>