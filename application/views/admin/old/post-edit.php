<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>

      <?php
      if($post):
          $h = '编辑投资项目';
      else:
          $h = '新增投资项目';
      endif;
      ?>
  <div class="container">
    <?php echo @flash_message(); ?>
      <?php
        $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
        echo form_open_multipart('admin/post/save', $attributes);
      ?>
      <input name="id" value="<?php echo $post['id'];?>" type="hidden" />
      <input name="type" value="<?php echo $type;?>" type="hidden" />
      <div class="col-lg-8">
          <div class="row">
        <p>
            <span class="icon-home"></span>
            <?php echo $h;?> > <?php echo $t;?> 
            <span class="icon-question"></span> 
            <a href="<?php echo site_url()."/admin/post/?type=$type"?>" class="btn btn-primary btn-sm">
                <span class="icon-paragraph-justify2"></span>
            </a>
        </p>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="outURL" data-val="1">
            <div class="form-group">
              <label for="title">名称 *</label>
              <?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => $post['title'])); ?>
            </div>
            <div class="form-group">
              <label for="title">网址</label>
              <?php echo form_input(array('name' => 'subtitle', 'id' => 'subtitle', 'class' => 'form-control', 'value' => $post['subtitle'])); ?>
            </div>
            <div class="form-group">
              <label for="content">简述</label>
              <?php echo form_textarea(array('name' => 'content', 'id' => 'content', 'class' => 'tinymce form-control', 'value' => $post['content'])); ?>
            </div>
                
            <div class="form-group">
              <label for="gender">投资项目分类</label>
              <?php
                foreach($invest_category as $k=>$v){
                  $data = array(
                  'name'        => 'category',
                  'value'       => $k,
                  'checked'     => ($post['category'] == $k)?TRUE:FALSE,
                  'style'       => 'margin-left:20px',
                  );
                  echo form_checkbox($data);
                  echo $v;
                }
              ?>
            </div>
              
              
            <?php
            $t = ($post['image'])?'重新添加':'添加图片';
            ?>
              
                <div class="form-group">
                  <label for="title">LOGO</label>
                  <a class="btn btn-default add-image show-prev" data-upload="thumb_upload"><?php echo $t;?></a>
                  <a class="btn btn-default remove-image <?php if(!$post['thumb']) echo 'hidden';?>">删除图片</a>
                  (尺寸：190x135)
                  <?php echo form_input(array('name' => 'thumb', 'id' => 'thumb', 'class' => 'hidden', 'value' => $post['thumb'])); ?>
                  <?php echo form_upload(array('name' => 'thumb_upload', 'id' => 'thumb_upload', 'class' => 'hidden')); ?>
                  <br />
                  <span class="image-render">
                      <?php
                      if($post['thumb']){
                          echo "<img src='".base_url() .'/uploads/'."{$post['thumb']}' width='200' />";
                      }
                      ?>
                  </span>
                  <span class="image-preview"></span>
                </div>
          </div>
        </div>
        
          </div>
      </div>
      
      <div class="col-lg-4">
          <div class="widget-box">
          <div class="widget-title">
              发布栏
          </div>
          <table class="table">
              <tbody>
                  <tr>
                    <td>状态</td>
                    <td>
<!--                        <ul class="pagination">
                            <li class="<?php if($post['status']) echo 'active';?>"><a href="#">显示</a></li>
                            <li class="<?php if(!$post['status']) echo 'active';?>"><a href="#">隐藏</a></li>
                          </ul>-->
                           <?php
                           echo ($post['status'])?'发布':'草稿';
                           ?>
                            <input name="status" value="<?php echo $post['status'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>显示</td>
                    <td>
                        <ul class="pagination radio-switch" data-input="display">
                            <li class="<?php if($post['display']) echo 'active';?>" data-value="1"><a href="javascript:void(0)">显示</a></li>
                            <li class="<?php if(!$post['display']) echo 'active';?>" data-input="0"><a href="javascript:void(0)">隐藏</a></li>
                          </ul>
                            <input name="display" id="display" value="<?php echo $post['display'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>顺序</td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="order" value="<?php echo $post['order'];?>" id="order" class="w50 form-control">
                        </div>
                    </td>
                  </tr>
              </tbody>
          </table>
          <div class="widget-button">
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-draft" data-form="myform">保存为草稿</a>
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-publish" data-form="myform">发布</a>
          </div>
          </div>
      </div>
      <div class="clearfix"></div>
      
      
      <?php
      echo form_close();
      ?>
  </div>

<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入投资项目标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>
<?php $this->load->view('admin/footer') ?>