<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>

<div class="container cms-box">
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/user/search?type=<?php echo $type;?>" method="get">
    <p>
        <span class="icon-diamonds"></span>用户管理 > <?php echo $t;?> <span class="icon-question"></span> <a href="<?php echo site_url()."/admin/user/edit"?>" class="btn btn-primary btn-sm">新增用户</a>
    </p>

    <div class="row">
    <div class="col-md-8">
        
          <div class="form-group">
            <select name="action" class="form-control">
                <option value="">--请选择以下功能--</option>
                <option value="set_publ">启用</option>
                <option value="set_draf">禁用</option>
            </select>
          </div>
            <input type="hidden" name="mode" value="" />
          <button type="button" class="btn btn-default" id="btn-update">应用</button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <div class="form-group">
            <input type="text" name="q" class="form-control" placeholder="关键字" value="<?php echo $q;?>">
          </div>
          <button type="button" class="btn btn-default" id="btn-search">搜索</button>
    </div>

    <?php
    if($page){
        $para = $_GET;
        $para['page'] = $page-1;
        $url_prev = site_url().'/admin/user/?'.http_build_query($para);
    }
    if($page < (ceil($total / $per) -1)){
        $para = $_GET;
        $para['page'] = $page+1;
        $url_next = site_url().'/admin/user/?'.http_build_query($para);
    }
    ?>
    <div class="col-md-2 text-right">
        <div class="form-inline">
            <?php
            if($url_prev):
            ?>
            <a href="<?php echo $url_prev;?>">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <?php
            endif;
            ?>
           <input type="text" class="form-control w50 text-center" value="<?php echo $page+1;?>">
            <?php
            if($url_next):
            ?>
            <a href="<?php echo $url_next;?>">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <?php
            endif;
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    </form>
    <?php
    if(empty($posts)):
    ?>
    没有用户
    <?php
    else:
    ?>    
    <br />
    <form id="frm_table" class="form-inline" role="search" action="<?php echo site_url()?>/admin/user/batch" method="post">
        <input type="hidden" name="action" />
    <table class="table tablesorter table-bordered">
        <thead>
          <tr>
            <th>批量选择<input type="checkbox" id="select_all"/></th>
            <th>ID</th>
            <th>用户名称</th>
            <th>邮箱</th>
            <th>状态</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $i=0;
            foreach($posts as $post):
            ?>
          <tr>
            <td><input type="checkbox" name="ids[]" value="<?php echo $post['id']?>" /></td>
            <td><?php echo ++$i;?></td>
            <td><a href="<?php echo site_url()."/admin/user/edit?id={$post['id']}"?>"><?php echo $post['username']?></a></td>
            <td><a href="<?php echo site_url()."/admin/user/edit?id={$post['id']}"?>"><?php echo $post['email']?></a></td>
           
            
            <?php
            if($post['status']):
            ?>
            <td class="success">启用</td>
            <?php
            else:
            ?>
            <td class="danger">禁用</td>
            <?php
            endif;
            ?>
            
            
          </tr>
            <?php
            endforeach;
            ?>
        </tbody>
      </table>
    <?php
    endif;
    ?>
    </form>
</div>
<?php $this->load->view('admin/footer') ?>