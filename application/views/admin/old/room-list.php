<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>
<div class="container cms-box">
    <?php echo @flash_message(); ?>
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/file/search?type=<?php echo $type;?>" method="get">
    <p>
        <span class="icon-calendar2"></span>ROOM管理 <span class="icon-question"></span> 
        <a href="<?php echo site_url()."/admin/file/"?>" class="">文件管理</a>
        <a href="<?php echo site_url()."/admin/file/edit_room"?>" class="btn btn-primary btn-sm">新增文件夹</a>
        <a href="<?php echo site_url()."/admin/file/edit"?>" class="btn btn-primary btn-sm">新增文件</a>
    </p>
    </form>
    <br />
    <?php
    if(empty($rooms)):
    ?>
    没有ROOM
    <?php
    else:
    ?>
    <form id="frm_table" class="form-inline" role="search" action="<?php echo site_url()?>/admin/file/room_batch?type=<?php echo $type;?>" method="post">
        <input type="hidden" name="action" />
    <table class="table tablesorter table-bordered">
        <thead>
          <tr>
            <th>ROOM</th>
            <th>FOLDER</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
            <?php
            foreach($rooms as $id=>$v):
            ?>
          <tr>
            <td>
                <a href="<?php echo site_url()."/admin/file/edit_room?id=$id";?>">    
                    <?php
                    echo $v;
                    ?>
                </a> 
            </td>
            <td>
                /
            </td>
            
            <td>
                <a href="<?php echo site_url()?>/admin/file/remove_room?id=<?php echo $id;?>">remove</a>
            </td>
          </tr>
          
          <?php
          if($folder[$id]):
            foreach($folder[$id] as $_id=>$_v):
            ?>
          <tr>
            <td>
            </td>
            <td>
                <a href="<?php echo site_url()."/admin/file/edit_room?id=$_id";?>">    
                    <?php
                    echo $_v;
                    ?>
                </a> 
            </td>
            <td>
                <a href="<?php echo site_url()?>/admin/file/remove_room?id=<?php echo $_id;?>">remove</a>
            </td>
          </tr>
          
          <?php
            endforeach;
          endif;
          ?>
            <?php
            endforeach;
            ?>
        </tbody>
      </table>
    </form>
    <?php
    endif;
    ?>
</div>
<?php $this->load->view('admin/footer') ?>
<script>
$('select[name=room_id]').change(function(){
    window.location.href= "<?php echo site_url().'/admin/file?room_id='?>"+$(this).val();
})
</script>