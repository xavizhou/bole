<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>
<div class="container">
    <?php echo @flash_message(); ?>
    <?php $this->load->view('admin/inc/breadcrumb') ?>
    <div class="row">
        <div class="col-md-8">
          <?php $this->load->view('admin/tpl/'.$slug) ?>
        </div>
        <div class="col-md-4">
          <?php $this->load->view('admin/inc/siderbar') ?>
        </div>
    </div>
</div>
<?php $this->load->view('admin/footer') ?>