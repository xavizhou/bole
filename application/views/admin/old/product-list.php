    <?php $this->load->view('admin/header') ?>
<?php
$data = array('nav'=>'product');
?>
<?php $this->load->view('admin/nav',$data); ?>
<div class="container cms-box">
    <?php echo @flash_message(); ?>
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/product/search?type=<?php echo $type;?>" method="get">
    <p>
        <span class="icon-table2"></span>产品管理 <span class="icon-question"></span> <a href="<?php echo site_url()."/admin/product/edit"?>" class="btn btn-primary btn-sm">新增产品</a>
    </p>
    <div class="row">
    <div class="col-md-10">
       
          <div class="form-group">
            <select name="action" class="form-control">
                <option value="">--请选择以下功能--</option>
                <option value="set_publ">状态设为发布</option>
                <option value="set_draf">状态设为草稿</option>
                <option value="set_show">显示</option>
                <option value="set_hide">隐藏</option>
                <option value="set_orde">排序</option>
                <option value="set_dele">删除</option>
            </select>
          </div>
            <input type="hidden" name="mode" value="" />
          <button type="button" class="btn btn-default" id="btn-update">应用</button>
          &nbsp;&nbsp;&nbsp;&nbsp;
<!--          <div class="form-group">
            <?php 
            $options = $product_gender_list;
            $options[0] = '-性别-';
            ksort ($options);
            echo form_dropdown('gender', $options, $gender,'class="form-control"');
            ?>
          </div>
          <div class="form-group">
            <?php 
            $options = $product_category_list;
            $options[0] = '-产品分类-';
            ksort ($options);
            echo form_dropdown('category', $options, $category,'class="form-control"');
            ?>
          </div>
          <div class="form-group">
            <?php 
            $options = $product_recommend_list;
            $options[0] = '-推广分类-';
            ksort ($options);
            echo form_dropdown('recommend', $options, $recommend,'class="form-control"');
            ?>
          </div>-->
          <div class="form-group">
            <?php 
            $options = $product_category_list;
            $options[0] = '-产品分类-';
            ksort ($options);
            echo form_dropdown('category', $options, $category,'class="form-control"');
            ?>
          </div>
          <div class="form-group">
            <input type="text" name="q" class="form-control" placeholder="关键字" value="<?php echo $q;?>">
          </div>
          <button type="button" class="btn btn-default" id="btn-search">搜索</button>
    </div>
    

    <?php
    if($page){
        $para = $_GET;
        $para['page'] = $page-1;
        $url_prev = site_url().'/admin/product/?'.http_build_query($para);
    }
    if($page < (ceil($total / $per) -1)){
        $para = $_GET;
        $para['page'] = $page+1;
        $url_next = site_url().'/admin/product/?'.http_build_query($para);
    }
    ?>
    <div class="col-md-2 text-right">
        <div class="form-inline">
            <?php
            if($url_prev):
            ?>
            <a href="<?php echo $url_prev;?>">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <?php
            endif;
            ?>
           <input type="text" class="form-control w50 text-center" value="<?php echo $page+1;?>">
            <?php
            if($url_next):
            ?>
            <a href="<?php echo $url_next;?>">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <?php
            endif;
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    </form>
    <br />
    <?php
    if(empty($posts)):
    ?>
    没有产品
    <?php
    else:
    ?>
    <form id="frm_table" class="form-inline" role="search" action="<?php echo site_url()?>/admin/product/batch" method="post">
<input type="hidden" name="action" />
<table class="table tablesorter table-bordered">
        <thead>
          <tr>
            <th>批量选择<input type="checkbox" id="select_all"/></th>
            <th>ID</th>
            <th>标题</th>
<!--            <th>性别</th>
            <th>产品分类</th>
            <th>推广分类</th>-->
            <th>分类</th>
            <th>状态</th>
            <th>显示</th>
            <th>最后更新日期</th>
            <th>排序</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $i=0;
            foreach($posts as $post):
            ?>
          <tr>
            <td><input type="checkbox" name="ids[]" value="<?php echo $post['id']?>" /></td>
            <td><?php echo ++$i;?></td>
            <td><a href="<?php echo site_url()."/admin/product/edit?id={$post['id']}"?>"><?php echo $post['title']?></a></td>
<!--            <td><?php echo $product_gender_list[$post['gender']]?></td>
            <td><?php echo $product_category_list[$post['category']]?></td>
            <td><?php echo $product_recommend_list[$post['recommend']]?></td>-->
            <td><?php echo $product_category_list[$post['category']]?></td>
           
            
            <?php
            if($post['status']):
            ?>
            <td class="success">发布</td>
            <?php
            else:
            ?>
            <td class="danger">草稿</td>
            <?php
            endif;
            ?>
            
            
            <?php
            if($post['display']):
            ?>
            <td class="success">显示</td>
            <?php
            else:
            ?>
            <td class="danger">隐藏</td>
            <?php
            endif;
            ?>
            
            <td><?php echo $post['updatedate'];?></td>
            <td>
                
            <div class="form-group">
              <label class="sr-only" for="exampleInputPassword2">order</label>
              <input type="text" class="form-control w50" name="orders[<?php echo $post['id']?>]" value="<?php echo $post['order']?>" />
            </div>
                
            </td>
            
          </tr>
            <?php
            endforeach;
            ?>
        </tbody>
      </table>
    <?php
    endif;
    ?>
    </form>
</div>
<?php $this->load->view('admin/footer') ?>