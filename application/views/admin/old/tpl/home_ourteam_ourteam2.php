<?php
  $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
  echo form_open_multipart('admin/page/save', $attributes);
?>
<input name="slug" value="<?php echo $slug;?>" type="hidden" />
<input name="id" value="<?php echo $post['id'];?>" type="hidden" />

<div class="form-group">
  <label for="content">标题</label>
  <?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => $post['title'])); ?>
</div>

<div class="form-group">
  <label for="content">正文</label>
  <?php echo form_textarea(array('name' => 'content', 'id' => 'content', 'class' => 'tinymce form-control', 'value' => $post['content'])); ?>
</div>
<input type="submit" value="保存" class="btn-lg btn-primary btn" />
<?php
echo form_close();
?>
<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入新闻标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>