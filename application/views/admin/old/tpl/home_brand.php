<?php
  $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
  echo form_open_multipart('admin/page/save', $attributes);
?>
<input name="slug" value="<?php echo $slug;?>" type="hidden" />
<input name="id" value="<?php echo $post['id'];?>" type="hidden" />

<div class="form-group">
  <label for="excerpt">简述</label>
  <?php echo form_textarea(array('name' => 'excerpt', 'id' => 'excerpt', 'class' => 'form-control', 'value' => $post['excerpt'])); ?>
</div>
<div class="form-group">
  <label for="content">正文</label>
  <?php echo form_textarea(array('name' => 'content', 'id' => 'content', 'class' => 'tinymce form-control', 'value' => $post['content'])); ?>
</div>
            
<?php
$t = ($post['image'])?'重新添加':'添加图片';
?>
<div class="form-group">
  <label for="title">图片</label>
  <a class="btn btn-default add-image show-prev" data-upload="thumb_upload"><?php echo $t;?></a>
  <a class="btn btn-default remove-image <?php if(!$post['thumb']) echo 'hidden';?>">删除图片</a>
  (尺寸：宽度 1280px)
  <?php echo form_input(array('name' => 'thumb', 'id' => 'thumb', 'class' => 'hidden', 'value' => $post['thumb'])); ?>
  <?php echo form_upload(array('name' => 'thumb_upload', 'id' => 'thumb_upload', 'class' => 'hidden')); ?>
  <br />
  <span class="image-render">
      <?php
      if($post['thumb']){
          echo "<img src='".base_url() .'/uploads/'."{$post['thumb']}' width='200' />";
      }
      ?>
  </span>
  <span class="image-preview"></span>
</div>
<input type="submit" value="保存" class="btn-lg btn-primary btn" />
<?php
echo form_close();
?>
<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入新闻标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>