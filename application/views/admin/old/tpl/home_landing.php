<?php
  $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
  echo form_open_multipart('admin/page/save', $attributes);
?>
<input name="slug" value="<?php echo $slug;?>" type="hidden" />
<input name="id" value="<?php echo $post['id'];?>" type="hidden" />

            
<?php
$t = ($post['thumb'])?'Change Image':'Add Image';
?>
<div class="form-group">
  <label for="title">Mobile Image</label>
  <a class="btn btn-default add-image show-prev" data-upload="thumb_upload"><?php echo $t;?></a>
  <a class="btn btn-default remove-image <?php if(!$post['thumb']) echo 'hidden';?>">Remove</a>
  (Width:640px)
  <?php echo form_input(array('name' => 'thumb', 'id' => 'thumb', 'class' => 'hidden', 'value' => $post['thumb'])); ?>
  <?php echo form_upload(array('name' => 'thumb_upload', 'id' => 'thumb_upload', 'class' => 'hidden')); ?>
  <br />
  <span class="image-render">
      <?php
      if($post['thumb']){
          echo "<img src='".base_url() .'/uploads/'."{$post['thumb']}' width='200' />";
      }
      ?>
  </span>
  <span class="image-preview"></span>
</div>

<?php
for($i=0;$i<1;$i++):
    $t = ($post['image'.$i])?'Change Image':'Add Image';
?>
    <div class="form-group">
      <label for="title">Mask Image</label>
      <a class="btn btn-default add-image show-prev" data-upload="image<?php echo $i;?>_upload"><?php echo $t;?></a>
      <a class="btn btn-default remove-image <?php if(!$post['image'.$i]) echo 'hidden';?>">Remove</a>
      (Size:1280px * 720px)
      <?php echo form_input(array('name' => 'image'.$i, 'id' => 'image'.$i, 'class' => 'hidden', 'value' => $post['image'.$i])); ?>
      <?php echo form_upload(array('name' => 'image'.$i.'_upload', 'id' => 'image'.$i.'_upload', 'class' => 'hidden')); ?>
      <br />
      <span class="image-render">
          <?php
          if($post['image'.$i]){
              echo "<img src='".base_url() .'/uploads/'."{$post['image'.$i]}' width='200' />";
          }
          ?>
      </span>
      <span class="image-preview"></span>
    </div>
<?php
  endfor;
?>
<input type="submit" value="保存" class="btn-lg btn-primary btn" />
<?php
echo form_close();
?>
<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入新闻标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>