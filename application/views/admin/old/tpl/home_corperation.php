<?php
  $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
  echo form_open_multipart('admin/page/save', $attributes);
?>
<input name="slug" value="<?php echo $slug;?>" type="hidden" />
<input name="id" value="<?php echo $post['id'];?>" type="hidden" />
<?php
for($i=0;$i<8;$i++):
    $t = ($post['image'.$i])?'更改图片':'添加图片';
?>
    <div class="form-group">
      <label for="title">图片<?php echo $i+1;?></label>
      <a class="btn btn-default add-image show-prev" data-upload="image<?php echo $i;?>_upload"><?php echo $t;?></a>
      <a class="btn btn-default remove-image <?php if(!$post['image'.$i]) echo 'hidden';?>">删除图片</a>
      <?php echo form_input(array('name' => 'image'.$i, 'id' => 'image'.$i, 'class' => 'hidden', 'value' => $post['image'.$i])); ?>
      <?php echo form_upload(array('name' => 'image'.$i.'_upload', 'id' => 'image'.$i.'_upload', 'class' => 'hidden')); ?>
      <br />
      <span class="image-render">
          <?php
          if($post['image'.$i]){
              echo "<img src='".base_url() .'/uploads/'."{$post['image'.$i]}' width='200' />";
          }
          ?>
      </span>
      <span class="image-preview"></span>
    </div>
<?php
  endfor;
?>
<input type="submit" value="保存" class="btn-lg btn-primary btn" />
<?php
echo form_close();
?>
<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入新闻标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>