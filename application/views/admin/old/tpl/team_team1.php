<?php
  $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
  echo form_open_multipart('admin/page/save', $attributes);
?>
<input name="slug" value="<?php echo $slug;?>" type="hidden" />
<input name="id" value="<?php echo $post['id'];?>" type="hidden" />

<div class="form-group">
  <label for="content">Name</label>
  <?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => $post['title'])); ?>
</div> 
<div class="form-group">
  <label for="content">Title</label>
  <?php echo form_input(array('name' => 'subtitle', 'id' => 'subtitle', 'class' => 'form-control', 'value' => $post['subtitle'])); ?>
</div> 
<div class="form-group">
  <label for="content">content</label>
  <?php echo form_textarea(array('name' => 'content', 'id' => 'content', 'class' => 'form-control', 'value' => $post['content'])); ?>
</div>
<?php
$t = ($post['thumb'])?'Change Image':'Add Image';
?>
<div class="form-group">
  <label for="title">Background Image</label>
  <a class="btn btn-default add-image show-prev" data-upload="thumb_upload"><?php echo $t;?></a>
  <a class="btn btn-default remove-image <?php if(!$post['thumb']) echo 'hidden';?>">Remove</a>
  (Size:300px * 215px)
  <?php echo form_input(array('name' => 'thumb', 'id' => 'thumb', 'class' => 'hidden', 'value' => $post['thumb'])); ?>
  <?php echo form_upload(array('name' => 'thumb_upload', 'id' => 'thumb_upload', 'class' => 'hidden')); ?>
  <br />
  <span class="image-render">
      <?php
      if($post['thumb']){
          echo "<img src='".base_url() .'/uploads/'."{$post['thumb']}' width='200' />";
      }
      ?>
  </span>
  <span class="image-preview"></span>
</div>

<input type="submit" value="保存" class="btn-lg btn-primary btn" />
<?php
echo form_close();
?>
<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入新闻标题"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
</script>