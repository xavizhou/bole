<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/inc/tinymce') ?>
<?php $this->load->view('admin/inc/nav') ?>
<div class="container cms-box">
    <?php echo @flash_message(); ?>
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/file/search?type=<?php echo $type;?>" method="get">
    <p>
        <span class="icon-calendar2"></span>文件管理 <span class="icon-question"></span> 
        <a href="<?php echo site_url()."/admin/file/room_list"?>" class="">ROOM管理</a>
        <a href="<?php echo site_url()."/admin/file/edit_room"?>" class="btn btn-primary btn-sm">新增文件夹</a>
        <a href="<?php echo site_url()."/admin/file/edit"?>" class="btn btn-primary btn-sm">新增文件</a>
    </p>
    <div class="row">
    <div class="col-md-10">
       
          <div class="form-group">
            <select name="action" class="form-control">
                <option value="">--请选择以下功能--</option>
<!--                <option value="set_publ">状态设为发布</option>
                <option value="set_draf">状态设为草稿</option>
                <option value="set_show">显示</option>
                <option value="set_hide">隐藏</option>
                <option value="set_orde">排序</option>-->
                <option value="set_dele">删除</option>
            </select>
          </div>
          <input type="hidden" name="mode" value="" />
          <button type="button" class="btn btn-default" id="btn-update">应用</button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <?php
          if($rooms):
          ?>
          <div class="form-group">
            <?php 
            $options = $rooms;
            $options[0] = '-文件分类-';
            ksort ($options);
            echo form_dropdown('room_id', $options, $room_id,'class="form-control"');
            ?>
          </div>
          <?php
          endif;
          ?>
<!--          <div class="form-group">
            <input type="text" name="q" class="form-control" placeholder="关键字" value="<?php echo $q;?>">
          </div>
          <button type="button" class="btn btn-default" id="btn-search">搜索</button>-->
    </div>
    <?php
    if($page){
        $para = $_GET;
        $para['page'] = $page-1;
        $url_prev = site_url().'/admin/file/?'.http_build_query($para);
    }
    if($page < (ceil($total / $per) -1)){
        $para = $_GET;
        $para['page'] = $page+1;
        $url_next = site_url().'/admin/file/?'.http_build_query($para);
    }
    ?>
    <div class="col-md-2 text-right">
        <div class="form-inline">
            <?php
            if($url_prev):
            ?>
            <a href="<?php echo $url_prev;?>">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <?php
            endif;
            ?>
           <input type="text" class="form-control w50 text-center" value="<?php echo $page+1;?>">
            <?php
            if($url_next):
            ?>
            <a href="<?php echo $url_next;?>">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <?php
            endif;
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    </form>
    <br />
    <?php
    if(empty($posts)):
    ?>
    没有文件
    <?php
    else:
    ?>
    <form id="frm_table" class="form-inline" role="search" action="<?php echo site_url()?>/admin/file/batch?type=<?php echo $type;?>" method="post">
        <input type="hidden" name="action" />
    <table class="table tablesorter table-bordered">
        <thead>
          <tr>
            <th>批量选择<input type="checkbox" id="select_all"/></th>
            <th>ROOM</th>
            <th>FOLDER</th>
            <th>名称</th>
            <th>最后更新日期</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $i=0;
            $offset = $page*$per;
            foreach($posts as $post):
            ?>
          <tr>
            <td><input type="checkbox" name="ids[]" value="<?php echo $post['id']?>" /></td>
            <td>
                
                <?php
                if($rooms_all[$post['room_id']]['fid']):
                    $fid = $rooms_all[$post['room_id']]['fid'];
                else:
                    $fid = $post['room_id'];
                endif;
                ?>
                <a href="<?php echo site_url()."/admin/file/edit_room?id=$fid";?>">    
                    <?php
                    echo $rooms_all[$fid]['name'];
                    ?>
                </a> 
            </td>
            <td>
                <?php
                if($rooms_all[$post['room_id']]['fid']):
                ?>
                <a href="<?php echo site_url()."/admin/file/edit_room?id={$post['room_id']}"?>">
                <?php
                echo $rooms_all[$post['room_id']]['name'];
                ?>
                    </a>
                <?php
                endif;
                ?>
            </td>
            <td>
                <a href="<?php echo site_url()."/admin/file/edit?id={$post['id']}"?>"><?php echo $post['filename']?></a>
            </td>
            <td>
                <?php echo $post['date']?>
            </td>
          </tr>
            <?php
            endforeach;
            ?>
        </tbody>
      </table>
    </form>
    <?php
    endif;
    ?>
</div>
<?php $this->load->view('admin/footer') ?>
<script>
$('select[name=room_id]').change(function(){
    window.location.href= "<?php echo site_url().'/admin/file?room_id='?>"+$(this).val();
})
</script>