<?php $this->load->view('admin/header') ?>
<?php
$data = array('nav'=>'banner');
?>
<?php $this->load->view('admin/nav',$data); ?>

<!--<div class="container grey-block cms-box">
    <div class="">
        <h2>第一步：<small>请选择横幅 </small></h2>
        <div class="text-center col-lg-4">
        <a class="btn btn-outline-inverse btn-lg" href="<?php echo site_url()?>/admin/banner/?type=1">大横幅</a>
        <a class="btn btn-outline-inverse btn-lg" href="<?php echo site_url()?>/admin/banner/?type=2">小横幅</a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>-->
<div class="container cms-box">
    <?php echo @flash_message(); ?>
    <form id="frm_batch" class="form-inline" role="search" action="<?php echo site_url()?>/admin/banner/search?type=<?php echo $type;?>" method="get">
      <?php
      if($type==2):
          $t = '小横幅';
      else:
          $t = '大横幅';
            $type = 1;
      endif;
      $t = '广告';
      $type = 1;
      ?>
    <p>
        <span class="icon-home2"></span>首页横幅 > <?php echo $t;?> <span class="icon-question"></span> <a href="<?php echo site_url()."/admin/banner/edit?type=$type"?>" class="btn btn-primary btn-sm">新增信息</a>
    </p>
    
    <div class="row">
    <div class="col-md-8">
        
          <div class="form-group">
            <select name="action" class="form-control">
                <option value="">--请选择以下功能--</option>
                <option value="set_publ">状态设为发布</option>
                <option value="set_draf">状态设为草稿</option>
                <option value="set_show">显示</option>
                <option value="set_hide">隐藏</option>
                <option value="set_orde">排序</option>
                <option value="set_dele">删除</option>
            </select>
          </div>
            <input type="hidden" name="mode" value="" />
            <input type="hidden" name="type" value="<?php echo $type;?>" />
          <button type="button" class="btn btn-default" id="btn-update">应用</button>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <div class="form-group">
            <input type="text" name="q" class="form-control" placeholder="关键字" value="<?php echo $q;?>">
          </div>
          <button type="button" class="btn btn-default" id="btn-search">搜索</button>
    </div>
    

    <?php
    if($page){
        $para = $_GET;
        $para['page'] = $page-1;
        $url_prev = site_url().'/admin/banner/?'.http_build_query($para);
    }
    if($page < (ceil($total / $per) -1)){
        $para = $_GET;
        $para['page'] = $page+1;
        $url_next = site_url().'/admin/banner/?'.http_build_query($para);
    }
    ?>
    <div class="col-md-2 text-right">
        <div class="form-inline">
            <?php
            if($url_prev):
            ?>
            <a href="<?php echo $url_prev;?>">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <?php
            endif;
            ?>
           <input type="text" class="form-control w50 text-center" value="<?php echo $page+1;?>">
            <?php
            if($url_next):
            ?>
            <a href="<?php echo $url_next;?>">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
            <?php
            endif;
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    </form>
    <br />
    <?php
    if(empty($posts)):
    ?>
    您还没有添加横幅
    <?php
    else:
    ?>
    <form id="frm_table" class="form-inline" role="search" action="<?php echo site_url()?>/admin/banner/batch?type=<?php echo $type;?>" method="post">
<input type="hidden" name="action" />
<table class="table tablesorter table-bordered">
        <thead>
          <tr>
            <th>批量选择<input type="checkbox" id="select_all"/></th>
            <th>ID</th>
            <th>标题</th>
            <th>状态</th>
            <th>显示</th>
            <th>排序</th>
          </tr>
        </thead>
        <tbody>
            <?php
            $i=0;
            foreach($posts as $post):
            ?>
          <tr>
            <td><input type="checkbox" name="ids[]" value="<?php echo $post['id']?>" /></td>
            <td><?php echo ++$i;?></td>
            <td><a href="<?php echo site_url()."/admin/banner/edit?id={$post['id']}"?>"><?php echo $post['title']?></a></td>
           
            
            <?php
            if($post['status']):
            ?>
            <td class="success">发布</td>
            <?php
            else:
            ?>
            <td class="danger">草稿</td>
            <?php
            endif;
            ?>
            
            
            <?php
            if($post['display']):
            ?>
            <td class="success">显示</td>
            <?php
            else:
            ?>
            <td class="danger">隐藏</td>
            <?php
            endif;
            ?>
            
            <td>
                
  <div class="form-group">
    <label class="sr-only" for="exampleInputPassword2">Password</label>
    <input type="text" class="form-control w50" name="orders[<?php echo $post['id']?>]" value="<?php echo $post['order']?>" />
  </div>
                
            </td>
            
          </tr>
            <?php
            endforeach;
            ?>
        </tbody>
      </table>
    <?php
    endif;
    ?>
    </form>
</div>
<?php $this->load->view('admin/footer') ?>