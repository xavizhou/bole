<?php $this->load->view('admin/header') ?>
<?php $this->load->view('admin/nav') ?>
  
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/pick-a-color-1.2.3.min.css" type="text/css" />
<script src="<?php echo base_url();?>assets/js/tinycolor-0.9.15.min.js"></script>
<script src="<?php echo base_url();?>assets/js/pick-a-color-1.2.3.min.js"></script>
<script src="<?php echo base_url()?>assets/tinymce/tinymce.min.js"></script>
<script>
tinymce.init({
       selector: "textarea.tinymce",
       language : 'zh_CN',
       plugins: "textcolor autolink link upload",
       toolbar: "bold italic underline | fontsizeselect forecolor backcolor |  alignleft aligncenter alignright alignjustify | link upload | bullist numlist",
       menubar : false,
        upload_action: '<?php echo site_url()?>/admin/upload',//required
        upload_file_name: 'userfile',//required
       skin: 'lightgray',
       height:<?php echo ((int)$height)?(int)$height:400;?>,
       statusbar: false,
       relative_urls :false,
        setup: function(editor) {
           editor.on('change', function(e) {
               changed = true;
           });
       }
   }
);
</script>
      <?php
      if($post):
          $h = '编辑产品';
      else:
          $h = '新增产品';
      endif;
      ?>
  <div class="container">
    <?php echo @flash_message(); ?>
      <?php
        $attributes = array('class' => '', 'id' => 'myform','method'=>'post');
        echo form_open_multipart('admin/product/save', $attributes);
      ?>
      <input name="id" value="<?php echo $post['id'];?>" type="hidden" />
      <input name="type" value="<?php echo $type;?>" type="hidden" />
      <div class="col-lg-8">
          <div class="row">
        <p>
            <span class="icon-home"></span>
            <?php echo $h;?> > <?php echo $t;?> 
            <span class="icon-question"></span> 
            <a href="<?php echo site_url()."/admin/product/?type=$type"?>" class="btn btn-primary btn-sm">
                <span class="icon-paragraph-justify2"></span>
            </a>
        </p>
        
        <ul class="nav nav-tabs">
          <li class="active"><a href="#outURL" data-toggle="tab">详细信息</a></li>
          <li><a href="#profile" data-toggle="tab">图片上传</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="outURL" data-val="1">
            <div class="form-group">
              <label for="title">标题 * </label>
              <?php echo form_input(array('name' => 'title', 'id' => 'title', 'class' => 'form-control', 'value' => $post['title'])); ?>
            </div>
            <div class="form-group">
              <label for="content">简述</label>
              <?php echo form_textarea(array('name' => 'content', 'id' => 'content', 'class' => 'form-control tinymce', 'value' => $post['content'])); ?>
            </div>
                
<!--            <div class="form-group">
              <label for="gender">性别</label>
              <?php
                foreach($product_gender_list as $k=>$v){
                  $data = array(
                  'name'        => 'gender',
                  'value'       => $k,
                  'checked'     => ($post['gender'] == $k)?TRUE:FALSE,
                  'style'       => 'margin-left:20px',
                  );
                  echo form_radio($data);
                  echo $v;
                }
              ?>
            </div>-->
              
            <div class="form-group">
              <label for="gender">分类</label>
              <?php
                foreach($product_category_list as $k=>$v){
                  $data = array(
                  'name'        => 'category',
                  'value'       => $k,
                  'checked'     => ($post['category'] == $k)?TRUE:FALSE,
                  'style'       => 'margin-left:20px',
                  );
                  echo form_radio($data);
                  echo $v;
                }
              ?>
            </div>
              
<!--            <div class="form-group">
              <label for="gender">推荐分类</label>
              <?php
                  $data = array(
                  'name'        => 'recommend',
                  'value'       => 0,
                  'checked'     => (empty($post['recommend']))?TRUE:FALSE,
                  'style'       => 'margin-left:20px',
                  );
                  echo form_radio($data);
                  echo 'None';
                  
                foreach($product_recommend_list as $k=>$v){
                  $data = array(
                  'name'        => 'recommend',
                  'value'       => $k,
                  'checked'     => ($post['recommend'] == $k)?TRUE:FALSE,
                  'style'       => 'margin-left:20px',
                  );
                  echo form_radio($data);
                  echo $v;
                }
              ?>
            </div>-->
              
<!--            <div class="form-group">
              <label for="color">颜色</label>
              <div class="row">
              <?php
              $colors = explode(",",$post['color']);
              for($i=0;$i<12;$i++){
                echo "<div class='col-md-3'>";
                echo form_input(array('name' => 'color[]', 'class' => 'form-control colorpicker', 'value' => $colors[$i]));  
                echo '</div>';
              }
              ?>
              </div>
            </div>-->
<!--            <div class="form-group">
              <label for="size">尺寸</label>
              <?php echo form_input(array('name' => 'size', 'id' => 'size', 'class' => 'form-control', 'value' => $post['size'])); ?>
            </div>
              
            <div class="form-group">
              <label for="size">TMall超链接</label>
              <?php echo form_input(array('name' => 'url', 'id' => 'url', 'class' => 'form-control', 'value' => $post['url'])); ?>
            </div>-->
              
              
          </div>
          <div class="tab-pane" id="profile" data-val="2">
            <?php
            $t = ($post['image'])?'重新添加':'添加图片';
            ?>
              
                <div class="form-group">
                  <label for="title">缩略图</label>
                  <a class="btn btn-default add-image show-prev" data-upload="thumb_upload"><?php echo $t;?></a>
                  <a class="btn btn-default remove-image <?php if(!$post['thumb']) echo 'hidden';?>">删除图片</a>
                  (尺寸：400 x 400)
                  <?php echo form_input(array('name' => 'thumb', 'id' => 'thumb', 'class' => 'hidden', 'value' => $post['thumb'])); ?>
                  <?php echo form_upload(array('name' => 'thumb_upload', 'id' => 'thumb_upload', 'class' => 'hidden')); ?>
                  <br />
                  <span class="image-render">
                      <?php
                      if($post['thumb']){
                          echo "<img src='".base_url() .'/uploads/'."{$post['thumb']}' width='200' />";
                      }
                      ?>
                  </span>
                  <span class="image-preview"></span>
                </div>
              
              
              <?php
              for($i=0;$i<7;$i++):
              ?>
                <div class="form-group">
                  <label for="title">图片<?php echo $i+1;?></label>
                  <a class="btn btn-default add-image show-prev" data-upload="image<?php echo $i;?>_upload"><?php echo $t;?></a>
                  <a class="btn btn-default remove-image <?php if(!$post['image'.$i]) echo 'hidden';?>">删除图片</a>
                  (尺寸：960x240)
                  <?php echo form_input(array('name' => 'image'.$i, 'id' => 'image'.$i, 'class' => 'hidden', 'value' => $post['image'.$i])); ?>
                  <?php echo form_upload(array('name' => 'image'.$i.'_upload', 'id' => 'image'.$i.'_upload', 'class' => 'hidden')); ?>
                  <br />
                  <span class="image-render">
                      <?php
                      if($post['image'.$i]){
                          echo "<img src='".base_url() .'/uploads/'."{$post['image'.$i]}' width='200' />";
                      }
                      ?>
                  </span>
                  <span class="image-preview"></span>
                </div>
              <?php
                endfor;
              ?>
          </div>
        </div>
        
          </div>
      </div>
      
      <div class="col-lg-4">
          <div class="widget-box">
          <div class="widget-title">
              发布栏
          </div>
          <table class="table">
              <tbody>
                  <tr>
                    <td>状态</td>
                    <td>
<!--                        <ul class="pagination">
                            <li class="<?php if($post['status']) echo 'active';?>"><a href="#">显示</a></li>
                            <li class="<?php if(!$post['status']) echo 'active';?>"><a href="#">隐藏</a></li>
                          </ul>-->
                           <?php
                           echo ($post['status'])?'发布':'草稿';
                           ?>
                            <input name="status" value="<?php echo $post['status'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>显示</td>
                    <td>
                        <ul class="pagination radio-switch" data-input="display">
                            <li class="<?php if($post['display']) echo 'active';?>" data-value="1"><a href="javascript:void(0)">显示</a></li>
                            <li class="<?php if(!$post['display']) echo 'active';?>" data-input="0"><a href="javascript:void(0)">隐藏</a></li>
                          </ul>
                            <input name="display" id="display" value="<?php echo $post['display'];?>" type="hidden" />
                    </td>
                  </tr>
                  <tr>
                    <td>顺序</td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="order" value="<?php echo $post['order'];?>" id="order" class="w50 form-control">
                        </div>
                    </td>
                  </tr>
              </tbody>
          </table>
          <div class="widget-button">
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-draft" data-form="myform">保存为草稿</a>
              <a href="javascript:void(0)" class="btn btn-primary" id="btn-save-publish" data-form="myform">发布</a>
          </div>
          </div>
      </div>
      <div class="clearfix"></div>
      
      
      <?php
      echo form_close();
      ?>
  </div>

<script>
    $("#myform").validate({
            rules: {
                    title: {
                            required: true
                    }
            },
            messages: {
                    title: {
                            required: "请输入产品名称"
                    }
            },
                    
            errorPlacement: function (error, element) {
                $(element).before(error.prepend('<span class="icon-warning"></span>'));
            }  ,
            errorElement: 'em'
    });
    

    $(".colorpicker").pickAColor({
      showSpectrum            : true,
            showSavedColors         : true,
            saveColorsPerElement    : true,
            fadeMenuToggle          : true,
            showAdvanced		: true,
            showBasicColors         : true,
            showHexInput            : true,
            allowBlank							: true,
            inlineDropdown					: true
    });
</script>
<?php $this->load->view('admin/footer') ?>