<?php include 'inc/inc.head.php';?>
    <body class="page-news-list">
<?php include 'inc/inc.header-v.php';?>


<div class="banner"></div>


<div class="news">
<div class="title">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_'.$category)?></h1>
            </div>
            <div class="col-md-6 text-subnav">
                    <?php
                    $list = array('report_insights',
                        'knowledge_center',
                        'e_newsletter',
                        'blog');
                    $flg = false;
                    for($i=0;$i<count($list);$i++):
                        if($list[$i] == $category):
                            continue;
                        endif;
                        echo ($flg)?' |  ':'';
                        $flg = true;
                        echo "<a href='institute/{$list[$i]}'>".$this->lang->line('nav_'.$list[$i])."</a>";
                    endfor;
                    ?>
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' /></a>
            </div>
        </div>
    </div>
</div>

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" id="newsList">
                
                

            </div>
        </div>
    </div>


    <div class="loading">
        <img src="assets/img/loading.png" />
    </div>
</div>






<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    
    var PAGE = 0;
    var iLoading = false;
    var iLoadComplete = false;
    function setNews(page){
       if(iLoading || iLoadComplete) return;
       
        $('.loading').removeClass('hidden');
       iLoading = true;
        $.ajax({
            url: '<?php echo site_url()?>institute/ajax_get?page='+page+'&category=<?php echo $category?>',
            type: 'GET',
            dataType : "json",
            contentType:"application/x-www-form-urlencoded; charset=utf-8",
            success : function(data, status, jqXHR) {
                iLoading = false;
                if(data.length < 10){
                    iLoadComplete = false;
                    $('.loading').remove();
                }
                var s = '';
                for(var i =0 ; i < data.length;i++){
                    if(data[i]['thumb'] == ''){
                        s += '<div class="row list no-img">';
                        s += '<div class="category">';
                        s += '<h4></h4>';
                        s += '</div>';
                        s += '<div class="excerpt">';
                        s += '<h2>'+data[i]['title']+'</h2>';
                        s += '<div class="date">'+data[i]['createdate'].substr(0,10)+'</div>';
                        s += '<p>'+data[i]['content']+'</p>';
                        s += '<a href="institute/detail?id='+data[i]['id']+'" class="btn btn-default"><?php echo $this->lang->line('learn_more')?>...</a>';
                        s += '</div></div>'; 
                    }else{
                        s += '<div class="row list">';
                        s += '<div class="img-box">';
                        s += '<img src="uploads/'+data[i]['thumb']+'" />';
                        s += '</div>';
                        
                        s += '<div class="excerpt">';
                        s += '<h4></h4>';
                        s += '<h2>'+data[i]['title']+'</h2>';
                        s += '<div class="date">'+data[i]['createdate'].substr(0,10)+'</div>';
                        s += '<p>'+data[i]['content']+'</p>';
                        s += '<a href="institute/detail?id='+data[i]['id']+'" class="btn btn-default"><?php echo $this->lang->line('learn_more')?>...</a>';
                        s += '</div></div>'; 
                    }
                }
                $('#newsList').append(s);
                
                $('.loading').addClass('hidden');
                
                $('#newsList .list').click(function(){
                    var href = $(this).find('a').attr('href');
                    window.location.href = href;
                })
                
            },
            compelete:function(){
                
            }
        });
    }
    setNews(PAGE);
    
    
    $(window).scroll(function(){
        if(($(document).scrollTop() + $(window).height()) > $('footer').offset().top){
            setNews(PAGE);
        }
    })
    </script>
    </body>
</html>