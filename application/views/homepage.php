<?php include 'inc/inc.head.php';?>

    <body class="homepage">
        <?php if(!$isMobile):?>
        <div id="bg" class="hidden-xs">
            <video src="assets/bl.mp4" autoplay="true" loop="loop" />
        </div>
        <?php
        else:
        ?>
        <?php
        endif;
        ?>
        <div id="mbg" class="visible-xs"></div>
        <div id="bgMask" class="hidden-xs"></div>
<?php include 'inc/inc.header.php';?>
<?php include 'inc/inc.header-v.php';?>



<div class="intro">
<!-- <div id='round'>
    <p>Para 1</p>
    <p>Para 2</p>
    <p>Para 3</p>
    <p>Para 4</p>
    <p>Para 5</p>
    <p>Para 6</p>
    <p class="pulse">L</p>
</div> -->

<section class="sec-text sec-text-1">
    <div>
        <div class="tips text-center">
            <?php echo $this->lang->line('homepage_title_1');?>
        </div>
    </div>
    <div><h1><?php echo $this->lang->line('homepage_slug_1');?></h1></div>
</section>

<section class="sec-text sec-text-2">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-2 tips">
                <?php echo $this->lang->line('homepage_title_2');?>
            </div>
            <div class="col-md-8">
                <p><?php echo $this->lang->line('homepage_slug_2');?></p>
                <a href='' class='btn btn-primary'>
                    <?php echo $this->lang->line('homepage_link_2');?>
                </a>
            </div>
        </div>
    </div>
</section>
<section class="sec-text sec-text-3">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-2 tips">
                <?php echo $this->lang->line('homepage_title_3');?>
            </div>
            <div class="col-md-8">
                <p><?php echo $this->lang->line('homepage_slug_3');?></p>
                <a href='' class='btn btn-primary'>
                    <?php echo $this->lang->line('homepage_link_3');?>
                </a>
            </div>
        </div>
    </div>
</section>
</div>

<div class="awards">
    <div class="container">
        <h1>Awards &amp; Honors</h1>
        

    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active row">
            <div class="col-md-1 col-md-offset-2">
                <img src="assets/img/awards-1.png" />
            </div>
            <div class="col-md-7">
            <p>
            Voted as one of the Top 10 Executive Search Firms by a global survey conducted by TopExecutiveSearchFirms.com
            </p>
            </div>
        </div>
        <div class="item">
            <div class="col-md-1 col-md-offset-2">
                <img src="assets/img/awards-1.png" />
            </div>
            <div class="col-md-7">
            <p>
            2 Voted as one of the Top 10 Executive Search Firms by a global survey conducted by TopExecutiveSearchFirms.com
            </p>
            </div>
        </div>
        <div class="item">
            <div class="col-md-1 col-md-offset-2">
                <img src="assets/img/awards-1.png" />
            </div>
            <div class="col-md-7">
            <p>
            3 Voted as one of the Top 10 Executive Search Firms by a global survey conducted by TopExecutiveSearchFirms.com
            </p>
            </div>
        </div>
      </div>
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
      </ol>


    </div>
    </div>
</div>


<div class="consultant">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h1><?php echo $this->lang->line('nav_star_consultants');?></h1>
                <div class="location"><i class="fa fa-map-marker fa-5" aria-hidden="true"></i><?php echo $this->lang->line('check_office2');?>
                    <div class="other-office">
                        <div class="">
                          <button type="button" class="btn btn-default drop-group" id='checkStar' data-rel="checkStarDropdown">
                            <?php echo $this->lang->line('check_other_office');?>
                            <i class="fa fa-angle-down"></i>
                          </button>
                    <div class="dropdown hidden check-office-dropdown" id="checkStarDropdown">
                    <?php include('inc/inc.city.php')?>
                    </div>

                        </div>

                    </div>
                </div>

                
                
                <div class="row" id="starConsultantBox">
                    
                </div>

            </div>
        </div>
    </div>


    <div class="avatar">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row" id="starConsultantAvatarBox">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="overview">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="pull-left">
                            <?php echo $this->lang->line('meet_us_intro');?>
                            
                        </div>
                        <div  class="pull-right">
                            <a href='#' class="btn btn-primary">
                                <?php echo $this->lang->line('nav_meet_us');?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="stuff">
    <div class="container">

    <div id="carousel-stuff" class="carousel slide" data-ride="carousel">
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active row">
            <div class="col-md-6 col-md-offset-3">
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            </div>

            <hr class="col-md-4 col-md-offset-4" />
            <div class="col-md-6 col-md-offset-3">
            <h5>CEO of Lorem Group</h5>
            <h4>Jack Goodman</h4>

            </div>
        </div>
        <div class="item row">
            <div class="col-md-6 col-md-offset-3">
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            </div>

            <hr class="col-md-4 col-md-offset-4" />
            <div class="col-md-6 col-md-offset-3">
            <h5>CEO of Lorem Group</h5>
            <h4>Jack Goodman</h4>

            </div>
        </div>
        <div class="item row">
            <div class="col-md-6 col-md-offset-3">
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
            </div>

            <hr class="col-md-4 col-md-offset-4" />
            <div class="col-md-6 col-md-offset-3">
            <h5>CEO of Lorem Group</h5>
            <h4>Jack Goodman</h4>

            </div>
        </div>
      </div>
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-stuff" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-stuff" data-slide-to="1"></li>
        <li data-target="#carousel-stuff" data-slide-to="2"></li>
      </ol>


    </div>
    </div>
</div>


<?php include 'inc/inc.news.php';?>
<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
        
    var _POS = [];
    var _LAST_I = -1;
    var _LAST_TOP = -1;
    var _DRECTION = 0;
    var _iCOUNT = 0;
    var _SEC = 6;

    function init_round(){
        for(var i = 0 ; i < 6 ; i++){
            _POS[i] = ($('.intro').height() - WIN_H)/6 * i;
        }
        $('#round p').each(function(i){
            $(this).css('transform','scale('+ (1 + .3*i) +')');
            $(this).css('z-index',(100-i));
            deal_scroll();
        })

        $(document).scroll(function(){
            var top = $(document).scrollTop();
            _DRECTION = (_LAST_TOP > top)?-1:1;
            clearTimeout(_iCOUNT);
            _iCOUNT = setTimeout(function(){append_scroll()},50);

            deal_scroll();
        })

        $('#headerInverse').removeClass('hidden').fadeOut(0);
        $('#headerDefault').removeClass('hidden').fadeIn(0);
    }
    function append_scroll(){

    }
    function deal_scroll(){
        var top = $(document).scrollTop();

        if(top > 0){
            $('#headerInverse').fadeIn(300);
            $('#headerDefault').fadeOut(300);
        }else{
            $('#headerInverse').fadeOut(300);
            $('#headerDefault').fadeIn(300);
        }


        for(var j = 0 ; j < _POS.length ; j++){
            if((_POS[j] - WIN_H) > top){
                break;
            }
        }
        var cur = j;
        $('#round').css('top', (top/ 1.1  + WIN_H ) + 'px');
        $('#bgMask').css('opacity', (1 - top / _POS[_SEC -1 ] ));

        if(_LAST_I == cur){
            return;
        }else{
            if(cur==0){
                $('#round p:eq(0)').css('background-color','rgba(255,0,0,.6)');
                $('#round p:eq(0)').addClass('fadein');
                $('#round p:gt(0)').removeClass('fadein');
                return;
            }

            for(var k = 0 ; k < _SEC; k++){
                if(k < cur){
                    var g = Math.round(255 * (k/cur)); 
                    $('#round p:eq('+k+')').css('background-color','rgba(255,'+g+','+g+',.6)');
                    $('#round p:eq('+k+')').addClass('fadein');
                }else{
                    $('#round p:eq('+k+')').removeClass('fadein');
                }
                
            }
        }
    }
    init_round();
    


    var WIN_W = $(window).width();
    var WIN_H = $(window).height();
    setSec();
    function setSec(){
        WIN_H = $(window).height();
        $('.sec-text').css('height',WIN_H+'px');
        $('#bg video').height(WIN_H);
        $('#bg video').width('auto');
    }

    $(window).resize(function(){
        setSec();
    })
    
    
    
    //set counsultants
    function setCounsultants(loc){
        $.ajax({
            url: '<?php echo site_url()?>consultant/ajax_star',
            type: 'GET',
            data: 'loc='+loc,
            dataType : "json",
            contentType:"application/x-www-form-urlencoded; charset=utf-8",
            success : function(data, status, jqXHR) {
                var s = '';
                var s2 = '';
                for(var i =0 ; i < data.length;i++){
                    s += '<div class="col-md-3 item">';
                    s += '<h2>'+data[i]['title']+'</h2>';
                    s += '<h5>'+data[i]['subtitle']+'</h5>';
                    s += '<a href="consultant/detail?id='+data[i]['id']+'" class="more"><?php echo $this->lang->line('learn_more');?></a>';
                    s += '</div>';
                    
                    s2 += '<div class="col-md-3 item">';
                    s2 += '<img src="uploads/'+data[i]['thumb']+'" />';
                    s2 += '</div>';
                }
                $('#starConsultantBox').html(s);
                $('#starConsultantAvatarBox').html(s2);
                
            },
            compelete:function(){
                
            }
        });
    }
    //setCounsultants('Shanghai');
    </script>
    </body>
</html>