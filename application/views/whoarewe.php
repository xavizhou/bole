<?php include 'inc/inc.head.php';?>
    <body class="page-whoarewe">
<?php include 'inc/inc.header-v.php';?>


<div class="banner"></div>


<div class="">
<div class="headline hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_who_are_we')?></h1>
            </div>
            <div class="col-md-5 text-subnav">
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' /></a>
            </div>
        </div>
    </div>
</div>
<div class="title visible-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_who_are_we')?></h1>
            </div>
            <div class="col-md-5 text-subnav">
                <div  class="visible-xs">
                    <?php
                    $list = array('origin_of_bole',
                        'bole_history',
                        'about_bole_associate',
                        'bole_associates_service',
                        'standard_procedure'
                    );
                    $flg = false;
                    foreach ($list as $v):
                        if($v == $post['slug']):
                            continue;
                        endif;
                        echo ($flg)?' |  ':'';
                        $flg = true;
                        echo "<a href='whoarewe/$v'>".$this->lang->line("nav_".$v)."</a>";
                    endforeach;
                    ?>
                </div>
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' /></a>
            </div>
        </div>
    </div>
</div>

    <div class="container main-box">
        <div class="row">
            <div class="col-md-4 menu hidden-xs">
                <div class="row">
                    <div class="col-md-8 col-md-offset-3">
                        <h2 ><?php echo $this->lang->line("nav_".$post['slug'])?></h2>
                    <menu>
                        <ul>
                            <?php
                            $list = array('origin_of_bole',
                                'bole_history',
                                'about_bole_associate',
                                'bole_associates_service',
                                'standard_procedure'
                            );
                            foreach ($list as $v):
                                if($v == $post['slug']):
                                    continue;
                                endif;
                                echo "<li><a href='whoarewe/$v'>".$this->lang->line("nav_".$v)."</a></li>";
                            endforeach;
                            ?>
                        </ul>
                    </menu>
                    </div>
                </div>
            

            </div>
            <div class="col-md-8 content-box">
                <div class='row'>
                <article>
                <h2 class='visible-xs'><?php echo $this->lang->line("nav_".$post['slug'])?></h2>
                <strong><?php echo $post['title'];?></strong>
                <?php echo $post['content'];?>
                </article>

                </div>


            </div>
        </div>

        <div class="row hidden-xs">
            
                <div class="share">
                    <img src="assets/img/icon-share.png" />
                    <?php echo $this->lang->line('share')?>
                </div>
        </div>
    </div>

</div>



<?php include 'inc/inc.news.php';?>


<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    </script>
    </body>
</html>