<?php include 'inc/inc.head.php';?>

	<link rel="stylesheet" href="assets/css/style-history.css"> <!-- Resource style -->
    <body class="page-whoarewe">
<?php include 'inc/inc.header-v.php';?>


<div class="banner"></div>


<div class="">
<div class="headline hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_who_are_we')?></h1>
            </div>
            <div class="col-md-5 text-subnav">
               
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' /></a>
            </div>
        </div>
    </div>
</div>
    
<div class="title visible-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_who_are_we')?></h1>
            </div>
            <div class="col-md-5 text-subnav">
                <div  class="visible-xs">
                    <?php
                    $list = array('origin_of_bole',
                        'bole_history',
                        'about_bole_associate',
                        'bole_associates_service',
                        'standard_procedure'
                    );
                    $flg = false;
                    foreach ($list as $v):
                        if($v == $post['slug']):
                            continue;
                        endif;
                        echo ($flg)?' |  ':'';
                        $flg = true;
                        echo "<a href='whoarewe/$v'>".$this->lang->line("nav_".$v)."</a>";
                    endforeach;
                    ?>
                </div>
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' /></a>
            </div>
        </div>
    </div>
</div>
    

    <div class="main-box">
        <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2><?php echo $this->lang->line("nav_bole_history")?></h2>
            </div>
            <div class="col-md-10 col-md-offset-1 content-box">
                <section class="cd-horizontal-timeline">

	<div class="events-content">
		<ol>
                    <?php
                    for($i=0;$i<count($posts);$i++):
                        $post = $posts[$i];
                        $t = (int)strtotime($post['date']);
                    ?>
                    <li class="<?php echo ($i)?'':'selected';?>" data-date="<?php echo date('d/m/Y',  $t)?>">
                            <div class="date">
                                <?php echo date('F dS',$t);?>
                                <small><?php echo date('Y',$t)?></small>
                            </div>
                            <div class="content">
				<h2><?php echo $post['title'];?></h2>
                                <article>
                                    <div class="thumb">
                                        <?php
                                        if($post['thumb']):
                                            echo '<img src="uploads/'.$post['thumb'].'" />';
                                        endif;
                                        ?>
                                    </div>
                                    <div class="text">
                                    <?php echo $post['content'];?>
                                    </div>
                                </article>
                            </div>
			</li>
                    <?php    
                    endfor;
                    ?>
		</ol>
	</div> <!-- .events-content -->
        
        
	<div class="timeline">
		<div class="events-wrapper">
                    <div class="events">
                        <ol>
                            <?php
                            for($i=0;$i<count($posts);$i++):
                                $post = $posts[$i];
                                $t = (int)strtotime($post['date']);
                            ?>
                            <li>
                                <a href="#<?php echo $i;?>" data-date="<?php echo date('d/m/Y', $t)?>" class="<?php echo ($i)?'':'selected';?>">
                                <?php echo date('M,Y', $t)?>
                                </a>
                            </li>
                            <?php    
                            endfor;
                            ?>
                        </ol>

                        <span class="filling-line" aria-hidden="true"></span>
                    </div> <!-- .events -->
		</div> <!-- .events-wrapper -->
			
		<ul class="cd-timeline-navigation">
			<li><a href="#0" class="prev inactive">Prev</a></li>
			<li><a href="#0" class="next">Next</a></li>
		</ul> <!-- .cd-timeline-navigation -->
	</div> <!-- .timeline -->
        
        
</section>
                
                
                
            </div>
        </div>

    </div>
    </div>

</div>



<?php include 'inc/inc.news.php';?>


<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>


<script src="assets/js/jquery.mobile.custom.min.js"></script>
<script src="assets/js/main.js"></script> <!-- Resource jQuery -->
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    </script>
    </body>
</html>