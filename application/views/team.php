<?php include 'inc/inc.head.php';?>
    <body class="page-consultant">
<?php include 'inc/inc.header-v.php';?>


<div class="banner"></div>


<div class="consultant">
<div class="title">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_meet_team');?></h1>
            </div>
            <div class="col-md-5 text-subnav">
                    <a href='consultant'><?php echo $this->lang->line('nav_meet_consultants');?></a>
                    
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' />
                    </a>
            </div>
        </div>
    </div>
</div>

    
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
<!--                 <div class="pull-right">
                    <select>check other office </select>
                </div> -->
                </div>
                
                <div class="list">
                    <?php
                    for($i=0;$i<count($posts);$i++):
                        $post = $posts[$i];
                    ?>
                    <div class="item" onclick="window.location.href='team/detail?id=<?php echo $post['id']?>'">
                        <img src="uploads/<?php echo $post['thumb'];?>" />
                        <div class="text">
                            <h2><?php echo $post['title'];?></h2>
                            <h5><?php echo $post['subtitle'];?></h5>
                        </div>
                    </div>
                    <?php    
                    endfor;
                    ?>
                </div>

            </div>
        </div>
    </div>
<!--

    <div class="loading">
        <img src="assets/img/loading.png" />
    </div>-->
</div>



<?php include 'inc/inc.news.php';?>





<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    </script>
    </body>
</html>