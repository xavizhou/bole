<?php include 'inc/inc.head.php';?>
    <body class="page-whyarewe">
<?php include 'inc/inc.header-v.php';?>
        <div id="bgMask"></div>
<div class="top">
    <div class="container">
        <div class="row row-1">
                <div class="col-md-6 col-md-offset-3">
                    <h1><?php echo $this->lang->line('why_title');?></h1>
                    <p class="excerpt"><?php echo $this->lang->line('why_excerpt');?></p>
                </div>
        </div>
        <div class="row row-2">
                <div class="col-md-4 col-md-offset-4">
                    <div class="row">
                        <p class="content">
                        <img src="assets/img/whyarewe-logo.png" />
                        <?php echo $this->lang->line('why_excerpt2');?></p>
                    </div>
                </div>
        </div>
    </div>
</div>


<div class="map" id="map">
    <div class="bg bg1"></div>
<!--    <div class="bg bg2"></div>-->
    <div class="bg" id="mapCity"></div>
    <div class="container">
        <div class="row">
                <div class="col-md-5 col-md-offset-6" id="mapCityText">
                    <p id="mapCityTextLine1"><?php echo $this->lang->line('why_map1');?></p>

                    <h3 id="mapCityTextLine2"><?php echo $this->lang->line('why_map2');?></h3>
                    <p id="mapCityTextLine3"><?php echo $this->lang->line('why_map3');?></p>

                    <h3 id="mapCityTextLine4"><?php echo $this->lang->line('why_map4');?></h3>
                    <p id="mapCityTextLine5"><?php echo $this->lang->line('why_map5');?></p>
                </div>
        </div>
    </div>
</div>


<div class="intro">
    <div class="container">
        <div class="row">
                <div class="col-md-12 col-md-offset-0">
                    <?php echo $this->lang->line('why_intro_q');?>
                    <h3><?php echo $this->lang->line('why_intro_title');?></h3>
                </div>
                <div class="col-md-8 col-md-offset-2">
                    <p><?php echo $this->lang->line('why_intro_text');?></p>
                </div>
        </div>
    </div>
</div>



<div class="efficient">
    <div class="bg bg1"></div>
    <div class="bg bg2">
        
    </div>
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <img src="assets/img/whyarewe-logo.png" />
                    <?php echo $this->lang->line('why_efficient');?>
                </div>
        </div>
    </div>
</div>

<div class="show show-cal" id="show-cal">
    <div class="container">
        <div class="row">
                <div class="col-md-2 col-md-offset-1 col-xs-6 col-xs-offset-3">
                    
                    <div id="animate1">
                    <img src="assets/img/whyarewe-cal.png" class="hidden-wx"/>
                    <img src="assets/img/whyarewe-cal-m.png" class="visible-wx hidden"/>
                    <i class="i1"></i>
                    <i class="i2"></i>
                    <i class="i3"></i>
                    <i class="i4"></i>
                    <i class="i5"></i>
                    <i class="i6"></i>
                    </div>
                    
                </div>
                <div class="col-md-8 text col-xs-12" id="textfade1">
                    <?php echo $this->lang->line('why_cal');?>
                    
                </div>
        </div>
    </div>
</div>
<div class="show show-clock" id="show-clock">
    <div class="container">
        <div class="row">
                <div class="col-md-2  col-xs-6 col-xs-offset-3 col-md-offset-0 pull-right">
                    <div id="animate2">
                    <img src="assets/img/whyarewe-clock.png" />
                    <img src="assets/img/whyarewe-clock-scale.png" id='animate22'/> 
                    </div>
                </div>
                <div class="col-md-7 col-md-offset-2   col-xs-12 pull-right text" id='textfade2'>
                    <div>
                    <p><?php echo $this->lang->line('why_clock1');?></p> 
                    <p><?php echo $this->lang->line('why_clock2');?></p>
                    </div>
                </div>
        </div>
    </div>
</div>
<div class="show show-hourglass" id="show-hourglass">
    <div class="container">
        <div class="row">
                <div class="col-md-2 col-md-offset-1 col-xs-6 col-xs-offset-3">
                    <img src="assets/img/whyarewe-timer.png" id='animate3'/>
                </div>
                <div class="col-md-8  col-xs-12 text" id='textfade3'>
                    <div>
                    <p><?php echo $this->lang->line('why_hourglass1');?></p> 
                    <p><?php echo $this->lang->line('why_hourglass2');?></p>
                    </div> 
                </div>
        </div>
    </div>
</div>
<div class="pool">
    <div class="container">
        <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="overview">
                        <h1><?php echo $this->lang->line('why_pool_title');?></h1>
                        <p><?php echo $this->lang->line('why_pool_excerpt');?></p>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="row">
                        <div class="title">
                            <h1><?php echo $this->lang->line('why_pool_donut1');?></h1>
                        </div>

                        <div class="donut" id="donut1">
                            <svg id="svg" class="hidden-xs"></svg> 
                            <svg id="svgm" class="visible-xs"></svg> 
                            <img src="assets/img/chart1-m.png" class="visible-xs" />
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <div class="title">
                            <h1><?php echo $this->lang->line('why_pool_donut2');?></h1>
                        </div>
                        <div class="donut" id="donut2">
                            <svg id="svg2" class="hidden-xs"></svg> 
                            <svg id="svgm2" class="visible-xs"></svg> 
                            <img src="assets/img/chart2-m.png" class="visible-xs" />
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>

<div class="topranking">
    <div class="bg" id="toprankingBg"></div>

    <div class="container">
        <div class="row">
        <div class="col-md-8 col-md-offset-2 text">
            <article>
            <h1><?php echo $this->lang->line('why_ranking_title');?></h1>
            <p><?php echo $this->lang->line('why_ranking_text');?></p>
            </article>
        </div>
        </div>
    </div>
</div>


<div class="topnotch">
    <div class="container">
        <div class="row">
        <div class="col-md-8 col-md-offset-2 text">
            <article>
            <h1><?php echo $this->lang->line('why_ranking_title');?></h1>
            <p><?php echo $this->lang->line('why_ranking_text');?></p>
            </article>
        </div>
        </div>
    </div>
</div>



<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>

    <script type="text/javascript" src="assets/js/TweenMax.min.js"></script>
    <script type="text/javascript" src="assets/js/scrollmagic/uncompressed/ScrollMagic.js"></script>
    <script type="text/javascript" src="assets/js//scrollmagic/uncompressed/plugins/animation.gsap.js"></script>

    <script type="text/javascript" src="assets/js/snap.svg-min.js"></script> 
    <script>
    var programmingSkills = [
        {
            value: 18,
            label: 'Finance Function',
            color: '#d6d7eb'
        },
        {
            value: 8,
            label: 'Financial Service',
            color: '#9a9ece'
        },
        {
            value: 13,
            label: 'FMCG / Retail',
            color: '#d6d7eb'
        },
        {
            value: 7,
            label: 'Healthcare',
            color: '#9a9ece'
        },
        {
            value: 8,
            label: 'HR Function',
            color: '#5d6bb0'
        },
        {
            value: 24,
            label: 'Industrial',
            color: '#1f499c'
        },
        {
            value: 7,
            label: 'Professional Service',
            color: '#9a9ece'
        },
        {
            value: 4,
            label: 'Real Estate',
            color: '#d6d7eb'
        },
        {
            value: 11,
            label: 'TMT',
            color: '#9a9ece'
        }
    ];


    var programmingSkills2 = [
        {
            value: 55,
            label: 'Middle Management',
            color: '#8d4b98'
        },
        {
            value: 7,
            label: 'Professionals',
            color: '#d2bcda'
        },
        {
            value: 8,
            label: 'Supporting Staff',
            color: '#be9bc6'
        },
        {
            value: 30,
            label: 'Senior Level Executives',
            color: '#a777b0'
        }
    ];

    </script> 
    <script type="text/javascript" src="assets/js/svg-donut-chart-framework.js"></script>


    <script>
    $('.carousel').carousel();

    var WIN_H = jQuery(window).height();//$(window).height();
    var WIN_W = $(window).width();
    $('del').each(function(){
        $(this).wrap('<span class="del" />').parent('span').html($(this).html());
        setTimeout(function(){
            $('span.del').addClass('in');
        },100);
    })

    $('#svg').data('init',0);
    $('#map').data('init',0);
    var controller = new ScrollMagic.Controller();
    // build scene
    var scene = new ScrollMagic.Scene({
                        triggerElement: "#show-cal"
                    })
                    .setClassToggle("#animate1", "in")
                    //.setTween("#animate1", 0.5, {scale: 2.5}) // trigger a TweenMax.to tween
                    .addTo(controller);

    new ScrollMagic.Scene({triggerElement: "#show-cal"})
                    .setClassToggle("#textfade1", "in") // add class toggle
                    .addTo(controller);
                    
    new ScrollMagic.Scene({triggerElement: "#show-clock"})
                    .setClassToggle("#animate2", "in") // add class toggle
                    .addTo(controller);
            
            
    new ScrollMagic.Scene({triggerElement: "#show-clock"})
                    .setClassToggle("#textfade2", "in") // add class toggle
                    .addTo(controller);
                    
    new ScrollMagic.Scene({triggerElement: "#show-hourglass"})
                    .setClassToggle("#animate3", "in") // add class toggle
                    .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#show-hourglass"})
                    .setClassToggle("#textfade3", "in") // add class toggle
                    .addTo(controller);


    new ScrollMagic.Scene({triggerElement: "#svg"})
                    .setClassToggle("#svg", "in") // add class toggle
                    .addTo(controller);
    new ScrollMagic.Scene({triggerElement: "#svg2"})
                    .setClassToggle("#svg2", "in") // add class toggle
                    .addTo(controller);

    new ScrollMagic.Scene({triggerElement: ".topranking", duration: 250})
                    .setPin("#toprankingBg")
                    .addTo(controller);


    new ScrollMagic.Scene({triggerElement: ".map"})
                    .setClassToggle(".map", "in") // add class toggle
                    .addTo(controller);

    new ScrollMagic.Scene({triggerElement: ".efficient"})
                    .setClassToggle(".efficient .bg.bg2", "in") // add class toggle
                    .addTo(controller);
    new ScrollMagic.Scene({triggerElement: ".efficient"})
                    .setClassToggle(".efficient .bg.bg1", "in") // add class toggle
                    .addTo(controller);
    </script>
    <script>
    $('#headerInverse').removeClass('hidden fixed');

    function init_map(){
        var mapData = {
            'Beijing':{
                x:356,//292
                y:105,//96
                d:'r',//96
                v:'b'
            },
            'Tianjin':{
                x:364,
                y:110,
                d:'r',
                v:'b'
            },
            'Dalian':{
                x:380,
                y:109,
                d:'r',
                v:'t'
            },
            'Shanghai':{
                x:386,
                y:150,
                d:'r',
                v:'b'
            },
            'Suzhou':{
                x:380,
                y:152,
                d:'r',
                v:'b'
            },
            'Taipei':{
                x:394,
                y:179,
                d:'r',
                v:'b'
            },
            'Shenzhen':{
                x:376,
                y:182,
                d:'r',
                v:'t'
            },
            'Hong Kong':{
                x:367,
                y:190,
                d:'l',
                v:'b'
            },
            'Guangzhou':{
                x:363,
                y:181,
                d:'l',
                v:'b'
            },
            'Chengdu':{
                x:330,
                y:155,
                d:'l',
                v:'b'
            },
            'Bangkok':{
                x:320,
                y:230,
                d:'r',
                v:'b'
            },
            'Kuala Lumpur':{
                x:324,
                y:270,
                d:'l',
                v:'b'
            },
            'Jakarta':{
                x:346,
                y:310,
                d:'r',
                v:'b'
            }
        }
        
        $('#mapCity').css('z-index',99);
        var _tmp ='';
        var iCount = 0;
        for(var k in mapData){
            var o = $('<div class="map-point" data-name="'+k+'"></div>');
            var img = new Image();
            //$(img).attr('src','assets/img/map/'+ k +'.png');
            //continue;
            setTimeout(function(){
                
            },iCount*500)
            $( img ).on( "load", function() {
                var k = $(this).data('k');
                var iCount = $(this).data('iCount');
                var v = mapData[k];
                
                if(v.d === 'r'){
                    $(this).css({
                    'left':v.x - 67
                    })
                }else{
                    $(this).css({
                    'left':v.x - 67 - $(this)[0].width/2
                    })
                }
                if(v.v === 't'){
                    $(this).css({
                    'top':v.y - 12 + 2 
                    })
                }else{
                    $(this).css({
                    'top':v.y - 12  + 2 - $(this)[0].height/2
                    })
                }
                $(this).css({
                    'transition-delay': (iCount*.8+.1)+'s',
                    'width': $(this)[0].width/2+'px',
                    'height': $(this)[0].height/2+'px'
                })
                $('#mapCity').append($(this));
              }).data('k',k)
              .data('iCount',iCount)
              .addClass('map-city')
              .attr('src','assets/img/map/'+ k +'X2.png');;


            var v = mapData[k];
            o.css({
                'top':v.y - 12,
                'left':v.x - 67,
                'animation-delay': (iCount*.8)+'s'
            })
            $('#mapCity').append(o);
            iCount++;
        }
        
        setTimeout(function(){
            $('.map-city').addClass('in');
        },100);
    }

    $(window).scroll(function(){
        if($('#donut1').hasClass('in')){
            if($('#svg').data('init') == 0){
                $('#svg').data('init',1);
                donut.startShowAnimation();
                dM('#svgm');
            }
        }
        if($('#donut2').hasClass('in')){
            if($('#svg2').data('init') == 0){
                $('#svg2').data('init',1);
                donut2.startShowAnimation();
                dM('#svgm2');
            }
        }
        if($('#map').hasClass('in')){
            if($('#map').data('init') == 0){
                $('#map').data('init',1);
                init_map();
            }
        }
    });
    
    
    function dM(id){
    	var s = Snap(id);
    	var k = 0;
    	var t = 0;
        var _w  = $(id).width();
        var _h  = $(id).height();
        var r = $(id).height()/2;
    	function transPoint(arc){
    		arc = arc * Math.PI / 180;
    		p = [];
    		p.x = r*(1 + Math.sin(arc));
    		p.y = r*(1 - Math.cos(arc));
    		return p;
    	}
    	var el;
    	t = setInterval(function(){
    		if(k>359){
    			s.remove();
    			el.remove();
    			clearInterval(t);
    		}
    		k += 2;
    		var p = transPoint(k);
    		console.log(p);
    		if(el){
    			el.remove();
    		}
    		el = s.path("M"+r+" 0 A "+r+" "+r+", 0, 1, 0, "+p.x+" "+p.y+" L " + _w/2 +" "+ _h/2 +" Z").attr({
	        fill: "#eeefef",
	        stroke: "none",
	        strokeWidth: 1
	    });
    	},10)
    }
    
    
    
    </script>
    </body>
</html>