<?php include 'inc/inc.head.php';?>
    <body class="page-workwithus">
<?php include 'inc/inc.header-v.php';?>
    <div class="headline">
        <div class="container">
            <div class="row">
            <div class="col-md-10 col-md-offset-1  col">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="">Who are we</h1>
                    </div>
                    <div class="col-md-6 text-right">
                            <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                                <span><?php echo $this->lang->line('go_back');?></span>                 
                                <img src='assets/img/icon-arrow-left.png' /></a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
        <div class="video-box">
            <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col">
                    <video src="assets/bl.mp4" controls=""></video>
                </div>
            
            </div>
            </div>
        </div>
        
        
        
        
        <div class="form-box">
        <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1  col">
                <div class='form form-cv' data-action="submit/workwithus">

                            <label><?php echo $this->lang->line('Name');?>:</label>
                            <div class="input-form">
                                <input type="text" name="name" required="required" placeholder="|  <?php echo $this->lang->line('ph_name');?>" />
                                <span class="required">*</span>
                            </div>
                            <label><?php echo $this->lang->line('CV_Info');?>:</label>
                            <div class="input-form">
                                <input type="text" name="email" required="required" placeholder="|  <?php echo $this->lang->line('ph_email');?>" />
                                <span class="required">*</span>
                            </div>
                            <div class="input-form">
                            <input type="text"  name="phone" placeholder="|  <?php echo $this->lang->line('ph_phone');?>" />
                            </div>
                            
                            <a href='' class='btn btn-default'><?php echo $this->lang->line('Reset');?></a>
                            <a href='' class='btn btn-default btn-submit'><?php echo $this->lang->line('Send');?></a>
                            <div class="status"></div>
                </div>
                
                <div class='content-box'>
                <article>
                    
                <h3><?php echo $post['title'];?></h3>
                <?php echo $post['content'];?>
                </article>

                </div>


            </div>
            </div>





        </div>
        </div>
        
        
        

<?php include 'inc/inc.news.php';?>


<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    </script>
    </body>
</html>