<?php include 'inc/inc.head.php';?>
    <body class="page-news">
<?php include 'inc/inc.header-v.php';?>

<div class="banner">
    <div class="container">
        <ol class="breadcrumb ">
            <li><a href="home"><?php echo $this->lang->line('nav_home');?></a></li>
            <li class="active">
                <?php echo $this->lang->line('nav_'.$category);?>            
            </li>
        </ol>
    </div>
</div>   



        
        <section class="sec-news-detail">
            <div class="container">
                <div class="row row-title">
                    <div class="col-md-10">
                        <h1><?php echo $post['title']?></h1>
                    </div>
                    <div class="col-md-2 news-date  visible-lg visible-md">
                        <?php $time = strtotime($post['createdate'])?>
                        <big><?php echo date('Y',$time)?></big> | 
                        <?php echo date('m-d',$time)?>                  
                    </div>
                    
                    <div class="col-xs-6 news-date  visible-sm visible-xs">
                        <big><?php echo date('Y',$time)?></big> | 
                        <?php echo date('m-d',$time)?>             
                    </div>
                    <div class="col-xs-6 news-back visible-sm visible-xs">
                        <a href="javascript:history.go(-1)" class="return">
                            <big><span class="glyphicon glyphicon-remove-circle"></span></big>
                            <?php echo $this->lang->line('go_back');?>                        
                        </a>
                    </div>
                    
                </div>
                <div class="row row-content">
                    <div class="col-md-10">
                        <article>
                            <?php echo $post['content'];?>
                        </article>
                    </div>
                    <div class="col-md-2 news-back visible-lg visible-md">
                        <a href="javascript:history.go(-1)" class="return">
                            <img src="assets/img/icon-close.png" />
                            <?php echo $this->lang->line('go_back');?>
                            </a>
                    </div>
                    
                </div>
                
            </div>            
        </section>









<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    </script>
    </body>
</html>