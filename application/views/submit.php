<?php include 'inc/inc.head.php';?>
    <body class="page-submit">
<?php include 'inc/inc.header-v.php';?>


<div class="banner"></div>


<div class="">
<div class="headline">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h1 class=""><?php echo $this->lang->line('nav_submit_cv');?></h1>
            </div>
            <div class="col-md-5 text-subnav">
                    <a href='javascript:history.go(-1)' class="btn btn has-icon btn-default">
                        <span><?php echo $this->lang->line('go_back');?></span>
                        <img src='assets/img/icon-arrow-left.png' />
                    </a>
            </div>
        </div>
    </div>
</div>

    <div class="container main-box">
        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <div class='form form-cv' data-action="submit/consultant">

                            <label><?php echo $this->lang->line('CV_Name');?>:</label>
                            <div class="input-form">
                                <input type="text" name="name" required="required" placeholder="|  <?php echo $this->lang->line('ph_name');?>" />
                                <span class="required">*</span>
                            </div>
                            <label><?php echo $this->lang->line('CV_Info');?>:</label>
                            <div class="input-form">
                                <input type="text" name="email" required="required" placeholder="|  <?php echo $this->lang->line('ph_email');?>" />
                                <span class="required">*</span>
                            </div>
                            <div class="input-form">
                            <input type="text"  name="phone" placeholder="|  <?php echo $this->lang->line('ph_phone');?>" />
                            </div>
                            
                             <label><?php echo $this->lang->line('CV_Attach');?>:</label>
                            <div class="input-form input-file">
                                <?php echo $this->lang->line('CV_tip1');?>
                                <em><?php echo $this->lang->line('CV_tip2');?></em>
                                <hr />
                                <input type="file" class="hidden" name="cv_file" />
                                <a href='javacript:void(0)' class='btn btn-choose-file'><?php echo $this->lang->line('CV_Choose');?></a> |  
                                <b class="filename" data-default="<?php echo $this->lang->line('CV_Choose_nofile');?>"><?php echo $this->lang->line('CV_Choose_nofile');?></b>
                            </div>
                            <a href='' class='btn btn-default'><?php echo $this->lang->line('Reset');?></a>
                            <a href='' class='btn btn-default btn-submit'><?php echo $this->lang->line('Send');?></a>
                            <div class="status"></div>
                </div>

            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class='row'>
                <article>
                    
                <h3><?php echo $post['title'];?></h3>
                <?php echo $post['content'];?>
                </article>

                </div>


            </div>
            </div>





        </div>
    </div>

</div>



    <div class="headline2">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-1 col-xs-7">
                    <h1 class="">Browse Job List</h1>
                </div>
                <div class="col-md-5 col-xs-5 text-subnav">
                        <big><?php echo count($job_list)?></big>jobs available
                </div>
            </div>
        </div>
    </div>
    <div class="joblist">
        <div class="container">
            <?php
            if($job_list):
                foreach($job_list as $job):
            ?>
            <div class="row item">
                <div class="col-xs-2 col-sm-1 col-sm-offset-1">
                    <div class='row'>
                        <span class="sq"></span>
                    </div>
                </div>
                <div class="col-md-7 col-xs-9">
                    <a href='#' class='title'><?php echo $job['title']?></a>
                    <?php echo $job['company']?>
                    <address><?php echo $job['location']?></address>
                </div>
                <div class="col-md-2 hidden-xs">
                    <div class="share row">
                        <?php include('inc/inc.share.php');?>
                    </div>
                </div>
            </div>
            <?php    
                endforeach;
            endif;
            ?>
        </div>
    </div>




    <div class="loading hidden">
        <img src="assets/img/loading.png" />
    </div>



<?php include 'inc/inc.news.php';?>
<?php include 'inc/inc.footer.php';?>
<?php include 'inc/inc.bottom.php';?>
    <script>
    $('#headerInverse').removeClass('hidden fixed');
    
    
    $('.share').mouseenter(function(){
        var title = $(this).parents('.item').find('.title').html();
        var description = $(this).parents('.item').find('.title').parent('div').text();
        addthis_share =
        {
            title:title,
            description:description
        }
    })

    </script>
    </body>
</html>