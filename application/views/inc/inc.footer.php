<footer>
    <div class="container hidden-xs">
        <div class="row">
            <div class="col-md-2 col-md-offset-1">
                <div class="row">
                <h3><?php echo $this->lang->line('nav_who_are_we');?></h3>
                <ul>
                    <li><a href='whoarewe/origin_of_bole'><?php echo $this->lang->line('nav_origin_of_bole');?></a></li>
                    <li><a href='whoarewe/bole_history'><?php echo $this->lang->line('nav_bole_history');?></a></li>
                    <li><a href='whoarewe/about_bole_associate'><?php echo $this->lang->line('nav_about_bole_associate');?></a></li>
                    <li><a href='whoarewe/bole_associates_service'><?php echo $this->lang->line('nav_bole_associates_service');?></a></li>
                    <li><a href='whoarewe/standard_procedure'><?php echo $this->lang->line('nav_standard_procedure');?></a></li>
                </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                <h3><?php echo $this->lang->line('nav_why_are_we');?></h3>
                <h3><?php echo $this->lang->line('nav_meet_us');?></h3>
                <ul>
                    <li><a href='consultant'><?php echo $this->lang->line('nav_management_team');?></a></li>
                    <li><a href='team'><?php echo $this->lang->line('nav_consultants');?></a></li>
                </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                <h3><?php echo $this->lang->line('nav_jobs');?></h3>
                <ul>
                    <li><a href='job'><?php echo $this->lang->line('nav_submit_cv');?></a></li>
                    <li><a href='job'><?php echo $this->lang->line('nav_submit_vacancy');?></a></li>
                    <li><a href='job'><?php echo $this->lang->line('nav_browse_jobs');?></a></li>
                </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row">
                <h3><?php echo $this->lang->line('nav_bo_le_institute');?></h3>
                <ul>
                    <li><a href='institute/report_insights'><?php echo $this->lang->line('nav_report_insights');?></a></li>
                    <li><a href='institute/knowledge_center'><?php echo $this->lang->line('nav_knowledge_center');?></a></li>
                    <li><a href='institute/e_newsletter'><?php echo $this->lang->line('nav_e_newsletter');?></a></li>
                    <li><a href='institute/blog'><?php echo $this->lang->line('nav_blog');?></a></li>
                </ul>
                </div>
            </div>

            <div class="col-md-2">
                <div class="row">
                <h2>Contact Us</h2>
                <address>
                <strong>Shanghai, China</strong>
                32F Lippo Plaza, 222 Huaihai Road (M) 
                </address>
                <i class="fa fa-phone"></i>+86 21 53966686 <br />
                <i class="fa fa-fax"></i>+86 21 53966687<br />
                <i class="fa fa-envelope-o"></i>sha@bo-le.com
                </div>
            </div>
        </div>

        <hr class="col-md-10 col-md-offset-1" />

        <div class="row ext">
            <div class="col-md-8 col-md-offset-1">
                <div class="row">
                    <a href='workwithus'>BD and Marketing cooperation</a>  |  
                    <a href='workwithus'>Work with us</a>   |  
                    <a href='workwithus'>Join our Community</a>   |  <a href='workwithus'>RGF website link</a> 
                </div>
            </div>
            <div class="col-md-2">
                <a href='workwithus' class='btn btn-primary'>More contact info...</a>
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    <div class="container  visible-xs">
        <div class="row">
            <div class="col col-lt">
                <h3><?php echo $this->lang->line('nav_who_are_we');?></h3>
                <ul>
                    <li><a href='whoarewe/origin_of_bole'><?php echo $this->lang->line('nav_origin_of_bole');?></a></li>
                    <li><a href='whoarewe/bole_history'><?php echo $this->lang->line('nav_bole_history');?></a></li>
                    <li><a href='whoarewe/about_bole_associate'><?php echo $this->lang->line('nav_about_bole_associate');?></a></li>
                    <li><a href='whoarewe/bole_associates_service'><?php echo $this->lang->line('nav_bole_associates_service');?></a></li>
                    <li><a href='whoarewe/standard_procedure'><?php echo $this->lang->line('nav_standard_procedure');?></a></li>
                </ul>
                
                <h3><?php echo $this->lang->line('nav_jobs');?></h3>
                <ul>
                    <li><a href='job'><?php echo $this->lang->line('nav_submit_cv');?></a></li>
                    <li><a href='job'><?php echo $this->lang->line('nav_submit_vacancy');?></a></li>
                    <li><a href='job'><?php echo $this->lang->line('nav_browse_jobs');?></a></li>
                </ul>
            </div>
            <div class="col col-rt">
                <h3><?php echo $this->lang->line('nav_why_are_we');?></h3>
                <h3><?php echo $this->lang->line('nav_meet_us');?></h3>
                <ul>
                    <li><a href='consultant'><?php echo $this->lang->line('nav_management_team');?></a></li>
                    <li><a href='team'><?php echo $this->lang->line('nav_consultants');?></a></li>
                </ul>
                
                <h3><?php echo $this->lang->line('nav_bo_le_institute');?></h3>
                <ul>
                    <li><a href='institute/report_insights'><?php echo $this->lang->line('nav_report_insights');?></a></li>
                    <li><a href='institute/knowledge_center'><?php echo $this->lang->line('nav_knowledge_center');?></a></li>
                    <li><a href='institute/e_newsletter'><?php echo $this->lang->line('nav_e_newsletter');?></a></li>
                    <li><a href='institute/blog'><?php echo $this->lang->line('nav_blog');?></a></li>
                </ul>
            </div>
            
        </div>

        <hr class="" />
        <div class="row ext">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="row">
                    <a href='workwithus'>BD and Marketing cooperation</a>  |  
                    <a href='workwithus'>Work with us</a>   |  
                    <a href='workwithus'>Join our Community</a>   |  
                    <a href='workwithus'>RGF website link</a> 
                </div>
            </div>
        </div>
        <hr class="" />

            <div class="col-xs-10 col-xs-offset-1">
                <div class="row">
                <h1>Contact Us</h1>
                <address>
                <strong>Shanghai, China</strong>
                32F Lippo Plaza, 222 Huaihai Road (M) 
                </address>
                <i class="fa fa-phone"></i>+86 21 53966686 <br />
                <i class="fa fa-fax"></i>+86 21 53966687<br />
                <i class="fa fa-envelope-o"></i>sha@bo-le.com
                </div>
            </div>
        
    </div>
    
    
</footer>