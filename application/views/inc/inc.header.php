<header class="fixed text-uppercase" id="headerDefault">
    <div class="container">
        <nav class="navbar row">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </button>             
                <div class="navbar-brand">
                    <a class="" href="home">
                        <img src="assets/img/logo.png" alt="" title="" />
                    </a>
                  </div>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <div class="office-nearby">
                <i class="fa fa-map-marker fa-5" aria-hidden="true"></i>
                    <?php echo $this->lang->line("check_office");?>  &nbsp; |
                    <span class="drop-group drop-check-office hidden-xs" id="checkOffice" data-rel="checkOfficeDropdown">
                        <?php echo $this->lang->line("more_cities");?>
                    <i class="fa fa-angle-down dn" aria-hidden="true"></i>
                    </span>
                </div>


                <div class="dropdown hidden check-office-dropdown" id="checkOfficeDropdown">
                    <?php include('inc.city.php')?>
                </div>

                    <ul  class="navbar-lang">
                        
                        <li>
                            <a class="" href="javascript:void(0)">
                                <?php echo $lang_list[$lang]['label'];?>
                            </a>
                            <i class="fa fa-angle-down dn" aria-hidden="true"></i>
                        </li>
                        <?php
                        foreach($lang_list as $k=>$v):
                            if($k == $lang)                                
                                continue;
                        ?>
                            <li class="other">
                                <a class="" href="?lang=<?php echo $k;?>">
                                    <?php echo $v['label'];?>
                                </a>
                            </li>
                        <?php    
                        endforeach;
                        ?>
                    </ul>
  <!--               <div>
                    <a class="other" href="?lang=zh_CN">简体中文</a>
                    <a class="other" href="?lang=zh_CN">繁体中文</a>
                </div> -->

                <ul class="nav navbar-nav">
                    <li><a  href="home"><?php echo $this->lang->line("nav_home");?></a></li>
                    <li><a  href="whoarewe"><?php echo $this->lang->line("nav_who_are_we");?></a></li>

                    <li><a  href="whyarewe"><?php echo $this->lang->line("nav_why_are_we");?></a></li>
                    <li class="li-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php echo $this->lang->line("nav_meet_us");?>
                        </a>
                      <ul class="dropdown-menu">
                        <li><a href="consultant"><?php echo $this->lang->line("nav_meet_consultants");?></a></li>
                        <li><a href="team"><?php echo $this->lang->line("nav_meet_team");?></a></li>
                      </ul>
                    </li>

                    <li><a  href="job"><?php echo $this->lang->line("nav_jobs");?></a></li>
                    <li><a  href="institute"><?php echo $this->lang->line("nav_bo_le_institute");?></a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
          </nav>
    </div>
</header>


