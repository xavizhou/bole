<div class="news">
    <div class="title container">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1">
                <div class="row">
                    <h1><?php echo $this->lang->line('homepage_news');?></h1>
                    <p class="hidden-xs"><?php echo $this->lang->line('homepage_news_excerpt');?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="content container">
        <div class="row">
            <div class="col-md-1 col-xs-2" id="newsIndex">
                <div class="row index text-center">
                    <ul>
                        <li><a href='#'>1</a></li>
                        <li><a href='#'>2</a></li>
                        <li><a href='#'>3</a></li>
                        <li><a href='#'>4</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10">
                <div class="row news-box">
                    <div class="item hidden">
                        <div class="thumb">
                            <a href='institute'>
                                <?php
                                $time = strtotime($news['e_newsletter']['createdate']);
                                if($news['e_newsletter']['thumb']):
                                    echo '<img src="uploads/'.$news['e_newsletter']['thumb'].'" />';
                                endif;
                                ?>
                            </a>
                        </div>
                        <div class="date">
                            <div class="type"></div>
                            <div class="day"><?php echo date('d',$time);?></div>
                            <div class="month"><?php echo date('M',$time);?></div>
                        </div>
                        <div class="excerpt">
                            <h2>
                                <a href="institute/detail?id=<?php echo $news['e_newsletter']['id'];?>">
                                    <?php echo $news['e_newsletter']['title'];?>
                                </a>
                            </h2>
                            <p><?php echo mb_substr(strip_tags($news['e_newsletter']['content']), 0,200,'UTF-8');?></p>
                            <a href='institute/detail?id=<?php echo $news['e_newsletter']['id'];?>' class='btn btn-primary'><?php echo $this->lang->line('learn_more');?></a>
                        </div>
                    </div>
                    
                    
                    
                    <div class="item hidden">
                        <div class="thumb">
                            <a href="institute/detail?id=<?php echo $news['blog']['id'];?>">
                                <?php
                                $time = strtotime($news['blog']['createdate']);
                                if($news['blog']['thumb']):
                                    echo '<img src="uploads/'.$news['blog']['thumb'].'" />';
                                endif;
                                ?>
                            </a>
                        </div>
                        <div class="date">
                            <div class="type"></div>
                            <div class="day"><?php echo date('d',$time);?></div>
                            <div class="month"><?php echo date('M',$time);?></div>
                        </div>
                        <div class="excerpt">
                            <h2>
                                <a href="institute/detail?id=<?php echo $news['blog']['id'];?>">
                                    <?php echo $news['blog']['title'];?>
                                </a>
                            </h2>
                            <p><?php echo mb_substr(strip_tags($news['blog']['content']), 0,200,'UTF-8');?></p>
                            <a href='institute/detail?id=<?php echo $news['blog']['id'];?>' class='btn btn-primary'><?php echo $this->lang->line('learn_more');?></a>
                        </div>
                    </div>
                    
                    
                    <div class="item download-item hidden">
                        <div class="date">
                            <img src="assets/img/icon-book.png" />
                        </div>
                        <div class="excerpt">
                            <h2>
                                <a href="#">
                                    Download File
                                </a>
                            </h2>
                            <p></p>
                            <a href='#' class='btn btn-primary'><?php echo $this->lang->line('download');?></a>
                        </div>
                    </div>
                    
                    
                    <div class="item job-item hidden">
                        <div class="nav hidden-xs"><?php echo $this->lang->line('nav_jobs');?></div>
                        <div class="job">
                            <div class="date hidden-xs">
                                <span class="sq"></span>
                            </div>
                            <div class="excerpt">
                                <a href="#" class="title">
                                    <?php echo $news['job']['title'];?>
                                </a>
                                <?php echo $news['job']['company'];?>                    
                                <address><?php echo $news['job']['location'];?></address>
                                <a href='#' class='btn btn-primary'><?php echo $this->lang->line('download');?></a>
                            </div>
                            <div class="share hidden-xs">
                                <?php include('inc.share.php')?>
                            </div>
                        </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
                <div class=""></div>
            </div>
            
            
        </div>
    </div>