<div class="bottom hidden-xs">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-1">
                <div class="row info">
                <img src="assets/img/logo-bottom.png" /> 
                    ©2015 Bó Lè Associates Ltd. All rights reserved.&nbsp;
                    <a href='#'>Privacy Policy &amp; Data Protection</a> &nbsp; |   &nbsp;
                    <a href='#'>Terms of Use</a>
                </div>
            </div>
            <div class="col-md-2">
                <div class="row social text-right">
                    <a href='#'>
                        <img src="assets/img/social-fb.png" />
                    </a>
                    <a href='#'>
                        <img src="assets/img/social-tw.png" />
                    </a>
                    <a href='#'>
                        <img src="assets/img/social-ln.png" />
                    </a>
                    <a href='#'>
                        <img src="assets/img/social-lp.png" />
                    </a>
                    <a href='#'>
                        <img src="assets/img/social-wx.png" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="bottom visible-xs">
    <div class="container">
        <div class="row">
            <div class="col-xs-7">
                <div class="social text-center">
                    <h1>Follow Us</h1>
                    <a href='#'>
                        <img src="assets/img/social-fb.png" />
                    </a>
                    <a href='#'>
                        <img src="assets/img/social-tw.png" />
                    </a>
                    <a href='#'>
                        <img src="assets/img/social-ln.png" />
                    </a>
                    <a href='#'>
                        <img src="assets/img/social-lp.png" />
                    </a>
                </div>
            </div>
            <div class="col-xs-5">
                <img src="assets/img/qr.png" />
                <small>Follow up on <img src="assets/img/social-wx.png" /></small>
            </div>
        </div>
        <div class="row bottom-logo">
            <img src="assets/img/logo-bottom.png" />
            
        </div>
        <div class="row">
            <div class="col-xs-11 col-xs-offset-1">
                <div class="row info">
                    <a href='#'>Privacy Policy &amp; Data Protection</a>   |  
                    <a href='#'>Terms of Use</a><br />
                    ©2015 Bó Lè Associates Ltd. All rights reserved.&nbsp;
                </div>
            </div>
            
        </div>
    </div>
</div>


<script src="assets/js/prefixfree.min.js"></script>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.cookie.js"></script>

<script>
    $('.carousel').carousel();


    var WIN_H = jQuery(window).height();//$(window).height();
    var WIN_W = $(window).width();




    //dropdown
    var _iDrop;
    $('.drop-group').each(function(){
        var rel_id = $(this).data('rel');
        var id = $(this).attr('id');
        $(this).mouseenter(function(){
            clearTimeout(_iDrop);
            $('#'+rel_id).removeClass('hidden');
            $('#'+id).addClass('hover');
            $('.li-dropdown').removeClass('open');
        })
        $(this).mouseleave(function(){
            _iDrop = setTimeout(function(){
                $('#'+rel_id).addClass('hidden');
                $('#'+id).removeClass('hover');
            },300)
        })
        $('#'+rel_id).mouseenter(function(){
            clearTimeout(_iDrop);
            $('.li-dropdown').removeClass('open');
        })
        $('#'+rel_id).mouseleave(function(){
            _iDrop = setTimeout(function(){
                $('#'+rel_id).addClass('hidden');
                $('#'+id).removeClass('hover');
            },300);
        })
    })


    $('.li-dropdown').mouseenter(function(){
        $(this).addClass('open');
    }).mouseleave(function(){
        $(this).removeClass('open');
    })
    
    
    $(document).ready(function() {
        var formCV = new FormData();
	$('.form-cv .input-file').on('dragenter', function (e){
	e.preventDefault();
	
	});

	$('.form-cv .input-file').on('dragover', function (e){
	e.preventDefault();
	});

	$('.form-cv .input-file').on('drop', function (e){
            e.preventDefault();
            var image = e.originalEvent.dataTransfer.files;
            $('.form-cv .filename').html(e.originalEvent.dataTransfer.files[0].name);
            createFormData(image);
	});
        
        $('.form-cv .btn-choose-file').click(function(){
            $('.form-cv input[name=cv_file]').click();
        })
        $('.form-cv input[name=cv_file]').change(function(e){
            if(e.target.files.length > 0){
                $('.form-cv .filename').html(e.target.files[0].name);
                formCV.append('cvFile', e.target.files[0]);
            }else{
                $('.form-cv .filename').html($('.form-cv .filename').data('default'));
            }
        })
        
        function createFormData(image) {
            formCV.append('cvFile', image[0]);
        }
        
        $('.form-cv .btn-submit').click(function(e){
            e.preventDefault();
            $('.form-cv input[required=required]').each(function(){
                $(this).removeClass('err');
                if($(this).val() === ''){
                    $(this).addClass('err');
                    $(this).focus();
                    return false;
                }
            })
            
            $('.form-cv input,.form-cv textarea,.form-cv select').each(function(){
                formCV.append($(this).attr('name'), $(this).val());
            })
            
            uploadFormData(formCV);
            return false;
        })
        

        function uploadFormData(formData) {
            $.ajax({
                url: $('.form-cv').data('action'),
                type: "POST",
                data: formData,
                contentType:false,
                cache: false,
                processData: false,
                success: function(data){
                    $('.form .status').html('<?php echo $this->lang->line('Send_Success')?>');
                }});
        }

    });

    </script>
    <script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=YRG8VQakD7MRn4xF6mG5Mm1V"></script>

    <script>
        $(document).ready(function(){
            var city_code = $.cookie('city_code');
            //var city_code = '';

            if(!!!city_code){
                var geolocation = new BMap.Geolocation();
                var geoc = new BMap.Geocoder();  
                geolocation.getCurrentPosition(function(r){
                    if(this.getStatus() === BMAP_STATUS_SUCCESS){
                        var pt = r.point;
                        getLatestCity(pt.lat,pt.lng,'');
                    }
                    else {
                    }        
                },{enableHighAccuracy: true});
                
            }else{
                getLatestCity(0,0,city_code);
            }

            function getLatestCity(lat,lng,city_code){
                if(city_code !== ''){
                    if(typeof setCounsultants === 'function'){
                        setCounsultants(city_code);
                    }
                }
                $.ajax({
                    url: 'city/get_city_nearby',
                    type: "GET",
                    data: "lat="+lat+'&lng='+lng+'&city_code='+city_code,
                    dataType : "json",
                    contentType:"application/x-www-form-urlencoded; charset=utf-8",
                    //cache: false,
                    processData: false,
                    success: function(data){
                        $('.curr-city').html(data.city_name);
                        $.cookie('city_code',data.city_code,{expires:7});
                        
                        if(typeof setCounsultants === 'function'){
                            setCounsultants(data.city_code);
                        }
                    }});
            }
        })
        
        
        
        $('.news-box .item').each(function(){
            if(($(this).find('.thumb').size() > 0) && ($(this).find('.thumb img').size() < 1)){
                $(this).addClass('no-img');
                $(this).find('.thumb').remove();
            }
        })
        $('.news .index li').mouseenter(function(){
            var i = $('.news .index li').index($(this));
            $('.news .index li').removeClass('active');
            $(this).addClass('active');
            $('.news-box .item').addClass('hidden');
            $('.news-box .item').eq(i).removeClass('hidden');
        })
        $('.news .index li').click(function(event){
            event.preventDefault();
            $(this).mouseenter();
        });
        $('.news .index li').eq(0).mouseenter();
    </script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<?php if(!$isMobile):?>
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-57ab44d0534d6762"></script>
<script>
    
var addthis_share =
{
  
}
$('.addthis_inline_share_toolbox').hide();
$('.share').mouseenter(function(){
    $(this).find('.addthis_inline_share_toolbox').show();
    $(this).find('.share-button').hide();
    
}).mouseleave(function(){
    $(this).find('.addthis_inline_share_toolbox').hide();
    $(this).find('.share-button').show();
});-->
<?php endif;?>
</script>
<!--    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e82c39a6aa039d3"></script>-->
