/***********************************************************
 * Create framework
 ***********************************************************/
(function ($, doc, win, Snap) {
    "use strict";

    function Donut(el, opts) {
        this.$el = $(el);
        this.opts = opts;
        this.snap = Snap(el);

        this.startShowAnimation = Donut.prototype.startShowAnimation;
  
        this.init();
    }

    /***********************************************************
     * Init donut
     ***********************************************************/
    Donut.prototype.init = function () {
        this.drawLegend();
        this.drawBaseCircle();
    };

    /***********************************************************
     * Draw base circle
     ***********************************************************/
    Donut.prototype.drawBaseCircle = function () {
        var s = this.snap,
            o = this.opts;

        // create diagonal pattern
        var pattern = s.path("M10-5-10,15M15,0,0,15M0-5-20,15").attr({
            fill: "none",
            stroke: "none",
            strokeWidth: 0
        });

        // create masking circle
        var mask = s.circle(o.center.y, o.center.y, o.radius).attr({
            fill: pattern
        });

        // create circle
        Donut.prototype.circle = s.circle(o.center.y, o.center.y, o.radius).attr({
            fill: "r(0.5, 0.5, 0.5)#FFC575-#fff",
            mask: mask,
            stroke: '#eeefef',
            strokeWidth: 0,
            opacity: 0
        });

        // create border circle
        s.circle(o.center.y, o.center.y, o.radius).attr({
            stroke: '#eeefef',
            fill: '#eeefef',
            strokeWidth: 1
        });
    };

    /***********************************************************
     * Draw legend
     ***********************************************************/
    Donut.prototype.drawLegend = function () {
        var s = this.snap,
            o = this.opts,
            data = o.data;

        var x = o.center.x + o.radius + 50,
            step = 30,
            radius = 6,
            y = o.center.y - (data.length / 2 * step) + radius / 2 + (step - radius) / 2;



        var x2 = o.center.x,
        y2 = o.center.y,
        r = 0,
        rx=0,ry=0,
        radius2 = 200* o.r + 30;
        var rx2,ry2;
        var tx,ty;
        var p1=[],p2=[],p3=[];

        var offset_arc = (o.tri.w / 2) / o.radius;//arcsin a 接近于 a 
        var offset_dis = o.radius - Math.sqrt(o.radius*o.radius - (o.tri.w/2)*(o.tri.w/2));//arcsin a 接近于 a 

        for (var i = 0; i < data.length; i++) {
            var d = data[i];
            r += data[i].value;
            var arc  = 2 * Math.PI * ((r - data[i].value/2) / 100 + .75);

            ry = Math.sin(arc) * radius2/2;
            rx = Math.cos(arc) * radius2/2;

            // var tp = this.translatedPoint(o.center, o.radius + 25, r - data[i].value/2);

            // console.log(x2 + rx );
            // console.log(tp);
            if(rx < 0){
              tx = x2 + rx - d.label.length * 5 - 10;
            }else{
              tx = x2 + rx + 10;
            }

            if(ry < 0){
              ty = y2 + ry - 10;
            }else{
              ty = y2 + ry + 10;
            }

            d.legend = {
                baseCircle: s.circle(x, y, radius),
                baseCircleRadius: radius,
                //text: s.text(x + step, y + radius / 2, d.label),
                //text: s.text(tx, ty, d.label),
                //textp: s.text(tx, ty + 12, d.value+'%'),
                center: {
                    x: x,
                    y: y,
                }
            }
            // d.legend.text.attr({
            //     "font-size": "10px"
            // });
            // d.legend.textp.attr({
            //     "font-size": "10px"
            // });
      

            d.legend.baseCircle.attr({
                stroke: '0',
                fill: 'none',
                strokeWidth: 0,
              color:''
            });


            //画小三角
            p1 = {
              x: x2 + Math.cos(arc) * (o.radius + 20 + 12 - offset_dis),//offset_arc
              y: y2 + Math.sin(arc) * (o.radius + 20 + 12 - offset_dis)
            }
            p2 = {
              x: x2 + Math.cos(arc+offset_arc) * (o.radius+ 20),//offset_arc
              y: y2 + Math.sin(arc+offset_arc) * (o.radius+ 20)
            }
            p3 = {
              x: x2 + Math.cos(arc-offset_arc) * (o.radius+ 20),//offset_arc
              y: y2 + Math.sin(arc-offset_arc) * (o.radius+ 20)
            }

            d.legend.tri = {
              p1:p1,
              p2:p2,
              p3:p3,
              color:d.color,
              text:{
                x:tx, 
                y:ty, 
                t:d.label
              },
              textp:{
                x:tx, 
                y:ty + 12, 
                t:d.value+'%'
              }
            }

            // s.path("M"+p1.x+","+p1.y+" L"+p2.x+","+p2.y+" L"+p3.x+","+p3.y+" L"+p1.x+","+p1.y+"").attr({
            //   fill: d.color     
            // });

            y += step;
        }
    };

    /***********************************************************
     * Build an arc svg path
     ***********************************************************/
    Donut.prototype.buildArcPath = function (center, radius, degreesStart, degreesEnd) {
        var start = this.translatedPoint(center, radius, degreesStart),
            end = this.translatedPoint(center, radius, degreesEnd),
            largeArc = (degreesEnd - degreesStart) > 180 ? 1 : 0;

        return "M" + start.x + "," + start.y + " A" + radius + "," + radius + " 0 " + largeArc + ",1 " + end.x + "," + end.y;
    };
      
    /***********************************************************
     * Animate an arc throw his degrees 
     ***********************************************************/
    Donut.prototype.animateArcByDegrees = function (arc, duration, callback) {
        var s = this.snap,
            t = this;

        Snap.animate(arc.degreesStart, arc.degreesEnd,
            function (degrees) {
                if (arc.draw) {
                    arc.draw.remove();
                }

                arc.path = t.buildArcPath(arc.center, arc.radius, arc.degreesStart, degrees);
                arc.draw = s.path(arc.path);

                arc.draw.attr({
                    stroke: arc.color,
                    fill: 'none',
                    strokeWidth: arc.strokeWidth,
                    cursor: 'pointer'
                });
            },
            duration,
            mina.linear,
            function () {
                if (callback) {
                    callback();
                }
            });
    };

    /***********************************************************
     * Animate an arc throw his stroke width 
     ***********************************************************/
    Donut.prototype.animateArcByStroke = function (arc, strokeWidthStart, strokeWidthEnd, duration, callback) {
        var s = this.snap,
            t = this;

        Snap.animate(strokeWidthStart, strokeWidthEnd,
            function (strokeWidth) {
                if (arc.draw) {
                    arc.draw.remove();
                }

                var radius = arc.radius + (strokeWidth - arc.strokeWidth) / 6;
                arc.path = t.buildArcPath(arc.center, radius, arc.degreesStart, arc.degreesEnd);
                arc.draw = s.path(arc.path);

                arc.draw.attr({
                    stroke: arc.color,
                    fill: 'none',
                    strokeWidth: strokeWidth,
                    cursor: 'pointer'
                });
            },
            duration,
            mina.bounce,
            function () {
                if (callback) {
                    callback();
                }
            });
    };

    /***********************************************************
     * Create the legend circle
     ***********************************************************/
    Donut.prototype.createAngle = function (tri) {
        var t = this.snap.path("M"+tri.p1.x+","+tri.p1.y+" L"+tri.p2.x+","+tri.p2.y+" L"+tri.p3.x+","+tri.p3.y+" L"+tri.p1.x+","+tri.p1.y+"").attr({
              fill: tri.color     
            });

        t.attr({
                "opacity":"0"
            });
        var t1 = this.snap.text(tri.text.x, tri.text.y, tri.text.t);
        var t2 = this.snap.text(tri.textp.x, tri.textp.y, tri.textp.t);
        t1.attr({
                "font-size": "10px",
                "opacity":"0"
            });
         t2.attr({
                "font-size": "10px",
                "opacity":"0"
            });

          t.animate({ opacity: 1 }, 300, mina.bounce);
         t1.animate({ opacity: 1 }, 300, mina.bounce);
         t2.animate({ opacity: 1 }, 600, mina.bounce);
    };

    /***********************************************************
     * Create the legend circle
     ***********************************************************/
    Donut.prototype.createLegendCircle = function (c) {
        c.draw = this.snap.circle(c.x, c.y, c.radius);
        c.draw.attr({
          fill: c.color
        });
    };

    /***********************************************************
     * Start show animation of legend circle
     ***********************************************************/
    Donut.prototype.animateShowLegendCircle = function (c, duration) {
        c.draw.transform('s0,'+c.x+','+c.y)
        c.draw.animate({ transform: 's1,'+c.x+','+c.y }, duration, mina.bounce);
    };

    /***********************************************************
     * Start show animation of donut and legend for one data entry
     ***********************************************************/
    Donut.prototype.startShowAnimationOneEntry = function (d, percentStart, percentEnd, center, radius, callback) {
        d.arc = {
          percentStart : percentStart,
          percentEnd : percentEnd,
          degreesStart : parseFloat(percentStart) / 100 * 360,
          degreesEnd : parseFloat(percentEnd) / 100 * 360,
          color : d.color,
          strokeWidth : 40,
          center: center, 
          radius: radius,
        }
      

        this.animateArcByDegrees(d.arc, 50 + (percentEnd - percentStart) * 10, callback);
      
        d.legend.circle = {
          color : d.color,
          x : d.legend.center.x,
          y : d.legend.center.y,
          radius: d.legend.baseCircleRadius + 2
        }
        
        //this.createLegendCircle(d.legend.circle);
        //this.animateShowLegendCircle(d.legend.circle, 400);
    };

    /***********************************************************
     * Translate a point by some degrees via a center and a radius
     ***********************************************************/
    Donut.prototype.translatedPoint = function (center, radius, degrees) {
        //var radians = Math.PI * (degrees - 270) / 180;
        var radians = Math.PI * (degrees - 90) / 180;
        return {
            x: center.x + radius * Math.cos(radians),
            y: center.y + radius * Math.sin(radians),
        }
    };

    /***********************************************************
     * Highlight entry element legend circle and arc
     ***********************************************************/
    Donut.prototype.hightlightElement = function (d) {
        var s = this.snap,
            o = this.opts,
            data = o.data;
      
        if(this.previousHighlightElement == d){
          return;
        }
      
        if(this.previousHighlightElement){
          //this.normalElement(this.previousHighlightElement);
        }
        this.previousHighlightElement = d;
      
        // var c = d.legend.circle;
        // c.draw.animate({ transform: 's1.3,'+c.x+','+c.y }, 200, mina.bounce);
      
         //this.animateArcByStroke(d.arc, d.arc.strokeWidth, d.arc.strokeWidth + 20, 500);
    };

    /***********************************************************
     * Normal entry element legend circle and arc
     ***********************************************************/
    Donut.prototype.normalElement = function (d) {
        var s = this.snap,
            o = this.opts,
            t = this,
            data = o.data;
      
        // var c = d.legend.circle;
        // c.draw.animate({ transform: 's1,'+c.x+','+c.y }, 500, mina.bounce);
      
        this.animateArcByStroke(d.arc, d.arc.strokeWidth + 6, d.arc.strokeWidth, 500, function(){
          d.arc.draw.mouseover(function () {
            t.hightlightElement(d);
          });
          d.arc.draw.click(function () {
            t.hightlightElement(d);
          });
        });
    };

    /***********************************************************
     * Init animation interaction
     ***********************************************************/
    Donut.prototype.initInteraction = function (d, step) {
        var s = this.snap,
            t = this,
            l = d.legend,
            a = d.arc;
          
        l.button = s.rect(l.center.x - 2 * l.baseCircleRadius, l.center.y - step / 2, 200, step);

        l.button.attr({
          opacity: 0,
          cursor: 'pointer',
          color:'red'
        });

        l.button.mouseover(function () {
          t.hightlightElement(d);
          
          
        });
        l.button.click(function () {
          t.hightlightElement(d);
          
        });
      
        a.draw.mouseover(function () {
          t.hightlightElement(d);
            
          
        });
        a.draw.click(function () {
          t.hightlightElement(d);
        });
    };

    /***********************************************************
     * Init animation interactions after showing animation
     ***********************************************************/
    Donut.prototype.initInteractions = function () {
        var s = this.snap,
            o = this.opts,
            t = this,
            data = o.data;
      
        for (var i = 0; i < data.length; i++) {
            this.initInteraction(data[i], 30);
        }
    };

    /***********************************************************
     * Start show animation of donut and legend for all data entry
     ***********************************************************/
    Donut.prototype.startShowAnimation = function (index, totalPct, callback) {
        if(index && !totalPct){
          callback = index;
          index = totalPct;
        }
      
        var s = this.snap,
            o = this.opts,
            t = this,
            data = o.data;
      
        var i = 0,
            total = 0;
      
        if(index){
          i = index;
          total = totalPct;
        }else{
          for(var j = 0 ; j < data.length ; j++){
            var d = data[j];
            if(d.arc && d.arc.draw){
              d.arc.draw.remove();
            }
            if(d.legend && d.legend.circle && d.legend.circle.draw){
              d.legend.circle.draw.remove();
            }
          }
          
          Snap.animate(0, 0.5, function (value) {
              Donut.prototype.circle.attr({
                opacity: value
              });
          }, 800);
        }
      
        if(data.length > i){
          var d = data[i];
          this.startShowAnimationOneEntry(d, total, total + d.value, o.center, o.radius, function(){
            t.startShowAnimation(i+1, total + d.value, callback);
            t.createAngle(d.legend.tri);
          });
        }else{
          this.initInteractions();
          if(callback){
            callback();
          }
        }
    };

    /***********************************************************
     * Requesting framework
     ***********************************************************/
    $.fn.donut = function (opts) {
        var donuts = [];
      
        this.each(function () {
            donuts.push(new Donut(this, opts));
        });
      
        if(donuts.length == 1){
          return donuts[0];
        }
      
        return donuts;
    };
})(jQuery, document, window, Snap);

/***********************************************************
 * Start framework
 ***********************************************************/
var donutSize = 300;
var r = 1;
if($(window).width() < 480){
 r = $(window).width()/480;
 donutSize *= r;   
}
        
var donut = $('#svg').donut({
    donutSize: donutSize,
    center: {
        x: donutSize / 2 + 90*r,
        y: donutSize / 2
    },
    radius: donutSize * 0.3 / 1,
    tri:{//小三角
      w:10,
      h:10
    },
    r:r,
    data: programmingSkills
});





var donut2 = $('#svg2').donut({
    donutSize: donutSize,
    center: {
        x: donutSize / 2 + 90*r,
        y: donutSize / 2
    },
    radius: donutSize * 0.3 / 1,
    tri:{//小三角
      w:10,
      h:10
    },
    r:r,
    data: programmingSkills2
});


